<?php

use Illuminate\Support\Facades\Route;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', function () {
    return view('welcome');
});
Route::get('/auth/thanks', [
    'as'   => 'auth.thanks',
    'uses' => 'Controller@thanks'
]);
Auth::routes();

Route::group([ 'middleware' => ['auth', 'roles'], 'roles' =>  ['super_admin', 'supplier', 'company_user']], function () {
    Route::get('/home', 'HomeController@index')->name('home');
});

//Route::get('/thanks', 'HomeController@thanks')->name('thanks');

// Check role in route middleware
Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'roles'], 'roles' => 'super_admin'], function () {
    Route::get('/', 'Admin\AdminController@index');

    Route::resource('roles', 'Admin\RolesController');
    Route::resource('permissions', 'Admin\PermissionsController');

    Route::resource('pages', 'Admin\PagesController');
    Route::resource('activitylogs', 'Admin\ActivityLogsController')->only([
        'index', 'show', 'destroy'
    ]);
    Route::get('users/new_users', 'Admin\UsersController@give_access_to_new_users');

    Route::get('generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@getGenerator']);
    Route::post('generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@postGenerator']);

    Route::get('suppliersById/{product_id}', 'Admin\SuppliersController@suppliersById')->name('suppliersById');
    Route::get('orderlinesbyorderid/{order_id}', 'Admin\OrdersController@orderLinesbyOrderId')->name('orderlinesbyorderid');
    Route::get('usercompany/{company_id}', 'Admin\CompanyController@showusercompany')->name('usercompany');
    Route::get('showproduct/{product_id}', 'Admin\ProductsController@showproduct')->name('showproduct');
    Route::get('showsupplierproduct/{product_id}', 'Admin\ProductsController@showSupplierProduct')->name('showsupplierproduct');

    Route::get('showsupplier/{supplier_id}', 'Admin\SuppliersController@showsupplier')->name('showSupplier');

    Route::get('showsuppliercontact/{supplier_id}', 'Admin\ContactsController@showSupplierContact')->name('showsuppliercontact');



    Route::get('viewcontactproducts/{contact_id}', 'Admin\ContactsController@viewContactProducts')->name('viewcontactproducts');

    Route::get('viewproductsupplier/{product_id}', 'Admin\ProductsController@viewProductSupplier')->name('viewproductsupplier');

    Route::get('contacts/{contact_id}/editscannedcontacts', 'Admin\ContactsController@editscannedcontacts')->name('editscannedcontacts');

    //editable route
    Route::post('ordertabledit/action', 'Admin\OrdersController@action')->name('ordertabledit.action');

    //Super_admin Custom CURDS


    Route::resource('display-ready', 'Admin\\DisplayReadyController');
    Route::resource('estimate-order', 'Admin\\EstimateOrderController');
    Route::resource('containers', 'Admin\\ContainersController');
    Route::resource('profit-margin', 'Admin\\ProfitMarginController');
    Route::resource('order-lines', 'Admin\\OrderLinesController');
    Route::resource('contact-products', 'Admin\\ContactProductsController');
    Route::resource('supplier-products', 'Admin\\SupplierProductsController');
    Route::resource('supplier-contacts', 'Admin\\SupplierContactsController');
    Route::resource('shipping-plans', 'Admin\\ShippingPlansController');
    Route::resource('order-lines', 'Admin\\OrderLinesController');
    Route::resource('order-documents', 'Admin\\OrderDocumentsController');
    Route::resource('comments', 'Admin\\CommentsController');

    //ends

    Route::any('addsupplier', 'Admin\\SuppliersController@addSupplierThroughAjax')->name('addsupplier');
    Route::any('scannedcontacts', 'Admin\\ContactsController@showScannedContactList')->name('admin/scannedcontacts');

    Route::any('addcontactsupplier', 'Admin\\ContactsController@addContactSupplierThroughAjax')->name('addcontactsupplier');


    Route::post('getProductRowByProductId', 'Admin\\SuppliersController@getProductRowByProductId');
    Route::post('getContactRowByContactId', 'Admin\\SuppliersController@getContactRowByContactId');

    Route::any('delete_template_products', 'Admin\\OrdersController@delete_template_products');

    //box_dimension calculation
    Route::post('load_dimension_details', 'Admin\\OrdersController@load_dimension_details');

    //ends
    //order_lines
    

    Route::post('convert_current_to_base_currency', 'Admin\\OrdersController@convert_current_to_base_currency');


    
});


//*************************check route for admin and supplier only
Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'roles'], 'roles' => ['super_admin', 'supplier', 'company_user']], function () {
    Route::resource('suppliers', 'Admin\\SuppliersController');
    Route::resource('products', 'Admin\\ProductsController');
    Route::resource('contacts', 'Admin\\ContactsController');
    Route::resource('quotes', 'Admin\\QuotesController');
    Route::resource('company', 'Admin\\CompanyController');
    Route::get('/profile', 'Admin\AdminController@profile')->name('profile');
    Route::get('viewsuppliercontacts/{supplier_id}', 'Admin\SuppliersController@viewSupplierContacts')->name('viewsuppliercontacts');
    Route::post('add_supplier_modal_data', 'Admin\SuppliersController@add_supplier_data_through_modal');
    Route::post('load_shipping_totals', 'Admin\\OrdersController@load_shipping_totals');
    Route::post('load_order_overview_totals', 'Admin\\OrdersController@load_order_overview_totals');
    Route::get('viewsupplierproducts/{supplier_id}', 'Admin\SuppliersController@viewSupplierProducts')->name('viewsupplierproducts');
    Route::any('getSupplierProducts', 'Admin\\SuppliersController@getSupplierProducts');
    
    Route::any('addcontact', 'Admin\\ContactsController@addContactThroughAjax')->name('addcontact');

    Route::any('getSupplierProductsForOrders', 'Admin\\OrdersController@getSupplierProductsForOrders');
    //quote routes
    Route::post('load_quote_lines', 'Admin\\QuotesController@load_quote_lines');
    Route::post('load_comments', 'Admin\\QuotesController@load_comments');
    Route::post('insert_quote_lines', 'Admin\\QuotesController@insert_quote_lines');
    //packaging options
    Route::post('insert_packaging_options', 'Admin\\ProductsController@insert_packaging_options');
    Route::post('insert_nested_product', 'Admin\\ProductsController@insert_nested_product');
    Route::post('update_quantity_per_package', 'Admin\\ProductsController@update_quantity_per_package');
    Route::post('add_remove_default_nested_products', 'Admin\\ProductsController@add_remove_default_nested_products');
    Route::post('load_nested_products', 'Admin\\ProductsController@load_nested_products');
    Route::post('load_nested_products_for_shipping_view_loading', 'Admin\\ProductsController@load_nested_products_for_shipping_view_loading');
    Route::post('submit_quote_ajax', 'Admin\\QuotesController@submit_quote_ajax');
    Route::post('submit_comment_ajax', 'Admin\\QuotesController@submit_comment_ajax');
    Route::post('/import_excel/import', 'Admin\\QuotesController@import_csv');
    Route::get('quote_lines_export/{quote_id}', 'Admin\\QuotesController@export_csv')->name('export');
  
    Route::post('load_quote_attachments_lines', 'Admin\\QuotesController@load_quote_attachments_lines');
    Route::post('load_product_attachments_lines', 'Admin\\ProductsController@load_product_attachments_lines');
    Route::post('load_certificate_attachments_lines', 'Admin\\SuppliersController@load_certificate_attachments_lines');
    Route::post('getAllCurrencyExhange', 'Admin\\OrdersController@getAllCurrencyExhange');
    Route::post('check_shipping_product_exists', 'Admin\\OrdersController@check_shipping_product_exists');
    Route::any('getproductdetails', 'Admin\\ProductsController@getProductDetailsThroughAjax')->name('getproductdetails');

    Route::post('getproductdetailsonorders', 'Admin\\ProductsController@getProductDetailsThroughAjaxForOrders')->name('getproductdetailsonorders');
    Route::any('deleteOrderDetail', 'Admin\\OrdersController@deleteOrderDetail');
    Route::post('dropzoneStore', 'Admin\OrdersController@dropzone_store');
    //order_attachments
    Route::post('dropzoneQuoteAttachment', 'Admin\QuotesController@dropzone_quote_attachment');
    Route::post('dropzoneProductAttachment', 'Admin\ProductsController@dropzone_product_attachment');
    Route::post('dropzoneCertificateAttachment', 'Admin\SuppliersController@dropzone_certificate_attachment');
    Route::post('submit_quote_attachments', 'Admin\\QuotesController@submit_quote_attachments_ajax');
    Route::post('submit_product_attachments', 'Admin\\ProductsController@submit_product_attachments_ajax');
    Route::post('submit_supplier_certifications', 'Admin\\SuppliersController@submit_supplier_certifications_ajax');
    Route::any('addsuppliercontact', 'Admin\\SuppliersController@addSupplierContactThroughAjax')->name('addsuppliercontact');
    Route::any('getSupplierConatcts', 'Admin\\SuppliersController@getSupplierConatcts');
    Route::any('addsupplierproduct', 'Admin\\SuppliersController@addSupplierProductThroughAjax')->name('addsupplierproduct');
    Route::any('addproduct', 'Admin\\ProductsController@addProductThroughAjax')->name('addproduct');
    Route::any('product_quantity_per_container', 'Admin\\ProductsController@update');
    Route::patch('submit_contact_form', 'Admin\\ContactsController@update');
    Route::post('remove_image_from_quote_lines', 'Admin\\QuotesController@remove_image_from_quote_lines_ajax');
    Route::post('remove_image_from_table', 'Admin\\OrdersController@remove_image_from_table');
    //currency exchange rate
    Route::post('get_current_exchange_rate', 'Admin\\OrdersController@get_current_exchange_rate');
    
    //get product units in container
    Route::post('get_products_units_in_the_container', 'Admin\\OrdersController@products_units_in_the_container');
    Route::post('get_refresh_shipping_rate', 'Admin\\OrdersController@refresh_shipping_rate');
    Route::post('get_order_line_details_for_cal', 'Admin\\OrdersController@get_order_line_details_for_cal');
    
    Route::post('submit_certification_fields', 'Admin\\SuppliersController@submit_certification_fields_ajax');
    Route::post('load_packaging_options', 'Admin\\ProductsController@load_packaging_options');
    
    //order > tracking-order
    Route::post('load_tracking_order', 'Admin\\OrdersController@load_tracking_order');
    
    Route::post('deleteData', 'Controller@deleteData');
    
//    Route::get('orders/order/overview', 'Admin\OrdersController@load_order_overview_view');
    Route::get('orders/order/overview', ['uses'=>'Admin\OrdersController@load_order_overview', 'as'=>'orders.overview']);
});


//*************************check route for super_admin and company_user only***********
Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'roles'], 'roles' => ['super_admin', 'company_user','supplier']], function () {
    Route::resource('settings', 'Admin\SettingsController');
    Route::get('/setting', 'Admin\AdminController@setting')->name('setting');
    Route::get('/admin', 'Admin\AdminController@admin')->name('admin');
    Route::post('insert_order_lines', 'Admin\\OrdersController@insert_order_lines');
    //shipping_plans
    Route::post('load_order_lines', 'Admin\\OrdersController@load_order_lines');
    Route::post('load_shipping_plans', 'Admin\\OrdersController@load_shipping_plans');
    Route::post('insert_shipping_plans', 'Admin\\OrdersController@insert_shipping_plans');
    //shipping_container_products
    Route::post('load_shipping_container_products', 'Admin\\OrdersController@load_shipping_container_products');
    Route::post('load_shipping_container_products_for_tracking', 'Admin\\OrdersController@load_shipping_container_products_for_tracking');
    Route::post('load_order_lines_for_order_overview', 'Admin\\OrdersController@load_order_lines_for_order_overview');
    Route::post('load_shipping_container_costs', 'Admin\\OrdersController@load_shipping_container_costs');
    Route::post('insert_shipping_container_products', 'Admin\\OrdersController@insert_shipping_container_products');
    Route::post('get_shipping_packaging_option_view', 'Admin\\OrdersController@get_shipping_packaging_option_view');
    Route::post('insert_shipping_container_products_with_using', 'Admin\\OrdersController@insert_shipping_container_products_with_using');

    //orders
    Route::any('submit_order_pricing', 'Admin\\OrdersController@submit_order_pricing_ajax');
    
    //order status 
    Route::get('update_order_status', 'Admin\\OrdersController@update_order_status_ajax');

    Route::post('submit_order_documents', 'Admin\\OrdersController@submit_order_documents_ajax');

    Route::post('submit_shipping_plan', 'Admin\\OrdersController@submit_shipping_plan_ajax');
//    Route::post('submit_tracking_order_fields', 'Admin\\OrdersController@submit_tracking_order_fields_ajax');
    //copy_container
    Route::get('copy_container', 'Admin\\OrdersController@copy_container');
    
    Route::post('submit_shipping_container_products', 'Admin\\OrdersController@submit_shipping_container_products_ajax');
    Route::post('shipping_container_products', 'Admin\\OrdersController@submit_shipping_container_products_ajax');
    Route::post('load_order_documents_lines', 'Admin\\OrdersController@load_order_documents_lines');

    //order pricing costs fob/fis
    Route::get('order_pricing_costs_fob', 'Admin\\OrdersController@load_order_pricing_costs_fob');
    Route::get('order_pricing_costs_fis/{order_id}', 'Admin\\OrdersController@load_order_pricing_costs_fis');
    Route::get('order_pricing_costs_fiw/{order_id}', 'Admin\\OrdersController@load_order_pricing_costs_fiw');
    Route::any('submit_order_pricing_costs_fis', 'Admin\\OrdersController@submit_order_pricing_costs_fis_ajax');
    Route::any('update_order_type', 'Admin\\OrdersController@update_order_type');
    Route::any('submit_order_pricing_costs_fob', 'Admin\\OrdersController@submit_order_pricing_costs_fob_ajax');

    //fis template product relation
    Route::post('insert_fis_template_product_relation', 'Admin\\OrdersController@insert_fis_template_product_relation');
    Route::post('load_template_products', 'Admin\\OrdersController@load_template_products');
    Route::post('set_default_template', 'Admin\\OrdersController@set_default_template');
    //ends
    //CompanyUser and Super_admin Custom CRUD
    Route::resource('orders', 'Admin\\OrdersController');
    Route::resource('margins', 'Admin\\MarginsController');
    Route::resource('costs', 'Admin\\CostsController');
    Route::resource('materials', 'Admin\\MaterialsController');
    Route::resource('category', 'Admin\\CategoryController');
    Route::resource('quote-categories', 'Admin\\QuoteCategoriesController');
    Route::resource('product-type', 'Admin\\ProductTypeController');
    Route::resource('fis-template', 'Admin\\FisTemplateController');
    Route::resource('shipping-rate', 'Admin\\ShippingRateController');
    Route::resource('shipping-port', 'Admin\\ShippingPortController');
    Route::resource('currency-exchange-rate', 'Admin\\CurrencyExchangeRateController');
    Route::resource('users', 'Admin\UsersController');
    //ends
});

//******************************************ends***************************************


Route::get('admin/country', 'HomeController@get_country_list');
Route::get('admin/shipping_container_products', 'Admin\\OrdersController@shipping_container_products');
Route::get('admin/shipping_container_costs', 'Admin\\OrdersController@shipping_container_costs');
Route::get('disp', 'Controller@disp');


Route::resource('admin/shipping-container-products', 'Admin\\ShippingContainerProductsController');

Route::resource('admin/quotes-attachments', 'Admin\\QuotesAttachmentsController');

Route::resource('admin/supplier-category', 'Admin\SupplierCategoryController');
Route::resource('admin/product-packaging-options', 'Admin\ProductPackagingOptionsController');
Route::resource('admin/order-status', 'Admin\OrderStatusController');
Route::resource('admin/quote-status', 'Admin\QuoteStatusController');
Route::resource('admin/package-types', 'Admin\PackageTypesController');
Route::resource('admin/ethical-audits', 'Admin\EthicalAuditsController');
Route::resource('admin/shipping-statuses', 'Admin\ShippingStatusesController');