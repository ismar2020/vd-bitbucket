<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class FisTemplate extends Model
{
    use LogsActivity;
    use SoftDeletes;
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'fis_templates';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['template_name', 'is_default', 'transport', 'transport_currency', 'port_costs', 'port_costs_currency', 'insurance_cost', 'insurance_cost_currency', 'import_custom_duties_percentage', 'import_taxes_percentage', 'handing_costs_in_destination_terminal', 'handing_costs_currency', 'costs_of_shipping_after_destination_port', 'shipping_after_port_currency', 'user_id', 'domestic_transport', 'domestic_transport_currency', 'storage_overhead', 'storage_overhead_currency', 'merchandising', 'merchandising_currency', 'other_overheads', 'other_overheads_currency'];

    

    /**
     * Change activity log event description
     *
     * @param string $eventName
     *
     * @return string
     */
    public function getDescriptionForEvent($eventName)
    {
        return __CLASS__ . " model has been {$eventName}";
    }
}
