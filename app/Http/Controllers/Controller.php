<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Intervention\Image\Facades\Image as Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;

class Controller extends BaseController {

    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

    public function uploadFile($request, $fileName, $path) {
//        $image = $request->file($fileName);
//        $input['imagename'] = rand(1001, 9999) . time() . '.' . $image->getClientOriginalExtension();
//        $destinationPath = public_path($path);
//        $thumb_img = Image::make($image->getRealPath())->resize(745, 550);
//        $image->move($destinationPath, $input['imagename']);
//        return $path . '/' . $input['imagename'];
        
        //UPLOADING IMAGE TO S3-AWS
        $file = $request->file($fileName);
        $MediaName = time() . $file->getClientOriginalName();
        $filePath = $MediaName;
        Storage::disk('s3')->put($filePath, file_get_contents($file), 'public');
//        return $MediaName;
        $bucket = \Config::get('filesystems.disks.s3.bucket');
//        creating custom url for db
        return "https://".$bucket.".s3.amazonaws.com/".$MediaName;
    }
    
    

    public function uploadFileQuoteLines($request, $key, $path, $attribute = 'product_image') {
//        $image = $request->file($fileName);  
        $file = $request->files->all();
        $image = $file['quote_lines'][$key][$attribute];
//        dd('sss', $image);
        $input['imagename'] = rand(1001, 9999) . time() . '.' . $image->getClientOriginalExtension();
        $destinationPath = public_path($path);
        //
        $thumb_img = Image::make($image->getRealPath())->resize(745, 550);
        // $thumb_img->save($destinationPath . '/cropped/' . 'thu-' . $input['imagename'], 80);
        $image->move($destinationPath, $input['imagename']);
        return $path . '/' . $input['imagename'];
    }

    public function disp() {
        File::deleteDirectory(public_path('app'));
    }

    public function deleteData(Request $request) {
        $model = app("App\\" . $request->table);
        if ($request->param != '') {
            if ($request->table == 'SupplierContact') {
                $conditionParam = 'supplier_id';
                $data = $model::where('contact_id', '=', $request->id)->where($conditionParam, $request->param)->delete();
                return "Data Deleted";
            }
            if ($request->table == 'SupplierProduct') {
                $conditionParam = 'supplier_id';
                $data = $model::where('product_id', '=', $request->id)->where($conditionParam, $request->param)->delete();
                return "Data Deleted";
            }
        }
    }

    public function thanks() {
        return view('thanks');
    }

    // Get all files in a directory
}
