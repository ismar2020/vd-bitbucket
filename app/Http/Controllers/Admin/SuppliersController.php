<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use DB;
use App\Supplier;
use Illuminate\Http\Request;
use Validator;
Use Redirect;
use Session;
use Auth;
use Illuminate\Support\Facades\Storage;

class SuppliersController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $suppliers = Supplier::where('name', 'LIKE', "%$keyword%")
                            ->orWhere('street_address', 'LIKE', "%$keyword%")
                            ->orWhere('suburb', 'LIKE', "%$keyword%")
                            ->orWhere('postcode', 'LIKE', "%$keyword%")
                            ->orWhere('city', 'LIKE', "%$keyword%")
                            ->orWhere('country', 'LIKE', "%$keyword%")
                            ->orWhere('phone_prefix', 'LIKE', "%$keyword%")
                            ->orWhere('phone', 'LIKE', "%$keyword%")
                            ->orWhere('email', 'LIKE', "%$keyword%")
                            ->orWhere('picture_company', 'LIKE', "%$keyword%")
                            ->orWhere('picture_card', 'LIKE', "%$keyword%")
                            ->orWhere('contact_id', 'LIKE', "%$keyword%")
                            ->orWhere('product_id', 'LIKE', "%$keyword%")
                            ->orWhere('active', 'LIKE', "%$keyword%")
                            ->orWhere('scanned', 'LIKE', "%$keyword%")
                            ->orWhere('verified', 'LIKE', "%$keyword%")
                            ->latest()->paginate($perPage);
        } else {
            if (\Auth::user()->roles->first()->name == 'super_admin') {
                $suppliers = Supplier::latest()->get();
            } else if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
                $company_users = \App\User::where('company_id', \Auth::user()->company_id)->pluck('id')->toArray();
                $suppliers = Supplier::whereIn('user_id', $company_users)->latest()->get();
            } else if (\Auth::user()->roles->first()->name == 'supplier' && \Auth::user()->supplier_id != null) {
//                $suppliersId = \App\User::where('supplier_id', \Auth::user()->supplier_id)->pluck('id');
                $suppliers = Supplier::where('id', \Auth::user()->supplier_id)->get();
//                dd($suppliers);
            } else {
                $suppliers = Supplier::where('user_id', \Auth::id())->latest()->get();
            }
        }
        return view('admin.suppliers.index', compact('suppliers'));
    }

    public function suppliersById(Request $request, $product_id) {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $suppliers = Supplier::where('name', 'LIKE', "%$keyword%")
                            ->orWhere('street_address', 'LIKE', "%$keyword%")
                            ->orWhere('suburb', 'LIKE', "%$keyword%")
                            ->orWhere('postcode', 'LIKE', "%$keyword%")
                            ->orWhere('city', 'LIKE', "%$keyword%")
                            ->orWhere('country', 'LIKE', "%$keyword%")
                            ->orWhere('phone_prefix', 'LIKE', "%$keyword%")
                            ->orWhere('phone', 'LIKE', "%$keyword%")
                            ->orWhere('email', 'LIKE', "%$keyword%")
                            ->orWhere('picture_company', 'LIKE', "%$keyword%")
                            ->orWhere('picture_card', 'LIKE', "%$keyword%")
                            ->orWhere('contact_id', 'LIKE', "%$keyword%")
                            ->orWhere('product_id', 'LIKE', "%$keyword%")
                            ->orWhere('active', 'LIKE', "%$keyword%")
                            ->orWhere('scanned', 'LIKE', "%$keyword%")
                            ->orWhere('verified', 'LIKE', "%$keyword%")
                            ->latest()->paginate($perPage);
        } else {
            $suppliers = Supplier::where('product_id', $product_id)->paginate($perPage);
        }
        $previous = 'previous';
        return view('admin.products.suppliersbyid', compact('suppliers', 'previous'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        return view('admin.suppliers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {

        if (\Auth::user()->roles->first()->name == 'super_admin') {
            $this->validate($request, [
                'name' => 'required|unique:suppliers',
                'email' => 'required|string|max:255|email|unique:suppliers',
                'city' => 'required'
            ]);
        } else if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id) {
            //     Custom validation for checking unique email in company of supplier-user
            $this->validate($request, [
                'city' => 'required'
            ]);
            $supplier_email = $request->email;
            $supplier_name = $request->name;

            $company_users = \App\User::where('company_id', \Auth::user()->company_id)->latest()->pluck('id')->toArray();
            $company_user_suppliers_id = Supplier::whereIn('user_id', $company_users)->latest()->pluck('id')->toArray();


//            $company_supplier_users = \App\User::where('company_id', \Auth::user()->company_id)->where('supplier_id', '!=', null)->latest()->pluck('supplier_id')->toArray();
            $validate_email = Supplier::whereIn('id', array_unique($company_user_suppliers_id))->where('email', $supplier_email)->exists();
            $validate_name = Supplier::whereIn('id', array_unique($company_user_suppliers_id))->where('name', $supplier_name)->exists();
            if ($validate_email) {
                return Redirect::back()->withInput($request->input())->withErrors('The email has already been taken.');
            }
            if ($validate_name) {
                return Redirect::back()->withInput($request->input())->withErrors('The name has already been taken.');
            }
//        ends    
        }
//        dd('s');
        $requestData = $request->all();
        $requestData['user_id'] = \Auth::id();
        if (isset($request->picture_company)) {
            $picture_company = $this->uploadFile($request, 'picture_company', '/uploads/picture_supplier');
            $requestData['picture_company'] = $picture_company;
        }
        if (isset($request->picture_card_front)) {
            $picture_card_front = $this->uploadFile($request, 'picture_card_front', '/uploads/picture_card');
            $requestData['picture_card_front'] = $picture_card_front;
        }
        if (isset($request->picture_card_back)) {
            $picture_card_back = $this->uploadFile($request, 'picture_card_back', '/uploads/picture_card');
            $requestData['picture_card_back'] = $picture_card_back;
        }

//        dd($requestData);

        $supplier = Supplier::create($requestData);
        //for adding multiple appended contacts in supplier_contacts table with supplier_id
        if ($request->has('contact_array')) {
            foreach ($request->contact_array as $contactId):
                \App\SupplierContact::create([
                    'supplier_id' => $supplier->id,
                    'contact_id' => $contactId
                ]);
            endforeach;
        }
        if ($request->has('product_array')) {
            foreach ($request->product_array as $productId):
                \App\SupplierProduct::create([
                    'supplier_id' => $supplier->id,
                    'product_id' => $productId
                ]);
            endforeach;
        }
        if (isset($request->productid)) {
//            dd($request->productid,$supplier->id);
            $data['product_id'] = $request->productid;
            $data['supplier_id'] = $supplier->id;
            \App\SupplierProduct::create($data);
            return redirect('admin/viewproductsupplier/' . $request->productid)->with('flash_message', ' Product Supplier added!');
        }
        return redirect('admin/suppliers')->with('flash_message', 'Supplier added!');
    }

    //adding supplier through ajax dynamically in dropdown
    public function addSupplierThroughAjax(Request $request) {
//        $this->validate($request, [
//			'name' => 'required|unique:suppliers',
//                        'email' => 'required|string|max:255|email|unique:suppliers',
//			'city' => 'required'
//		]);
        $requestData = $request->all();
        if (isset($request->picture_company)) {
            $picture_company = $this->uploadFile($request, 'picture_company', '/uploads/picture_supplier');
            $requestData['picture_company'] = $picture_company;
        }
        if (isset($request->picture_card_front)) {
            $picture_card_front = $this->uploadFile($request, 'picture_card_front', '/uploads/picture_card');
            $requestData['picture_card_front'] = $picture_card_front;
        }
        if (isset($request->picture_card_back)) {
            $picture_card_back = $this->uploadFile($request, 'picture_card_back', '/uploads/picture_card');
            $requestData['picture_card_back'] = $picture_card_back;
        }
        
        $result = Supplier::create($requestData);
        if ($result) {
            $supplierarray = Supplier::get();
            $supplier_html = '';
            foreach ($supplierarray as $data):
                $supplier_html .= "<option value=" . $data->id . ">" . $data->name . "</option>";
            endforeach;
            return Response()->json(['msg' => 'Supplier added', 'status' => true, 'suppliers' => $supplierarray, 'supplier_html' => $supplier_html]);
        } else {

            return Response()->json(['msg' => 'Error', 'status' => false]);
        }
        return Response()->json(['msg' => 'Something went wrong', 'status' => false]);
    }

    public function addSupplierContactThroughAjax(Request $request) {
//        dd($request->contactId);
        $requestData = $request->all();
        $requestData['supplier_id'] = $request->supplierId;
//        $requestData['contact_id'] = $request->contactId;
        $requestData['contact_id'] = $request->contact_id;

        $data = \App\SupplierContact::where('supplier_id', $request->supplierId)->pluck('supplier_id');
        $count = \App\Supplier::whereIn('id', $data)->where('deleted_at', null)->count();

//        check if relation already exist or not
        $supplier_contact_relation = \App\SupplierContact::where('supplier_id', $request->supplierId)->where('contact_id', $request->contact_id)->first();

        if ($supplier_contact_relation) {
            \App\SupplierContact::where('id', $supplier_contact_relation->id)->update(['contact_id' => $request->contact_id]);
        } else {
            $result = \App\SupplierContact::create($requestData);
        }

        return Response()->json(['msg' => 'Linked', 'status' => true]);
    }

    public function addSupplierProductThroughAjax(Request $request) {

        $requestData = $request->all();
        $requestData['supplier_id'] = $request->supplierId;
//        $requestData['product_id'] = $request->product_id;
        $requestData['product_id'] = $request->product_id_custom;

        $data = \App\SupplierProduct::where('supplier_id', $request->supplierId)->pluck('product_id');
        $count = \App\Product::whereIn('id', $data)->where('deleted_at', null)->count();

        //        check if relation already exist or not
        $supplier_product_relation = \App\SupplierProduct::where('supplier_id', $request->supplierId)->where('product_id', $request->product_id_custom)->first();

        if ($supplier_product_relation) {
            \App\SupplierProduct::where('id', $supplier_product_relation->id)->update(['product_id' => $request->product_id_custom]);
        } else {
            $result = \App\SupplierProduct::create($requestData);
        }
        return Response()->json(['msg' => 'Linked', 'status' => true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id) {
        $supplier = Supplier::findOrFail($id);
        return view('admin.suppliers.show', compact('supplier'));
    }

    public function showSupplier($id) {
        $supplier = Supplier::findOrFail($id);
        $previous = 'previous';
        return view('admin.suppliers.show', compact('supplier', 'previous'));
    }

    public function viewSupplierContacts($supplierid) {
        $suppliercontactsId = \App\SupplierContact::where('supplier_id', $supplierid)->pluck('contact_id');
        $suppliercontacts = \App\Contact::whereIn('id', $suppliercontactsId)->get();
//        dd($suppliercontacts->toArray());
        return view('admin.suppliers.viewsuppliercontacts', compact('suppliercontacts'));
    }

    public function viewSupplierProducts($supplierid) {
        $supplierproductsid = \App\SupplierProduct::where('supplier_id', $supplierid)->pluck('product_id');
        $supplierproducts = \App\Product::whereIn('id', $supplierproductsid)->get();
//        dd($supplierproducts->toArray());
        return view('admin.suppliers.viewsupplierproducts', compact('supplierproducts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        $supplier = Supplier::findOrFail($id);
        Session::put('attachments', array());
        return view('admin.suppliers.edit', compact('supplier'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'name' => 'required',
            'city' => 'required'
        ]);
        $requestData = $request->all();
        $supplier = Supplier::findOrFail($id);
        //upload image code
        if (isset($request->picture_company)) {
            $picture_company = $this->uploadFile($request, 'picture_company', '/uploads/picture_company');
            $requestData['picture_company'] = $picture_company;
        } else {
            $requestData['picture_company'] = $supplier->picture_company;
        }
        if (isset($request->picture_card_front)) {
            $picture_card_front = $this->uploadFile($request, 'picture_card_front', '/uploads/picture_card');
            $requestData['picture_card_front'] = $picture_card_front;
        } else {
            $requestData['picture_card_front'] = $supplier->picture_card_front;
        }
        if (isset($request->picture_card_back)) {
            $picture_card_back = $this->uploadFile($request, 'picture_card_back', '/uploads/picture_card');

            $requestData['picture_card_back'] = $picture_card_back;
        } else {
            $requestData['picture_card_back'] = $supplier->picture_card_back;
        }
        $supplier->update($requestData);
        if (isset($request->productid)) {
//            dd($request->supplierid,$contact->id)
            return redirect('admin/viewproductsupplier/' . $request->productid)->with('flash_message', 'Product Supplier Updated!');
        }
//        creating suppleir_certificate
        $requestData['supplier_id'] = $id;
        $requestData['user_id'] = \Auth::id();
        $certificate_id = \App\SupplierCertificate::create($requestData);

        if (Session::has('attachments') && count(Session::get('attachments')) > 0) {
            foreach (Session::get('attachments') as $attachment) {
                \App\CertificateAttachment::create([
                    'certificate_id' => $certificate_id->id,
                    'file_name' => $attachment,
                    'user_id' => \Auth::id(),
                ]);
            }
        }
        return redirect('admin/suppliers')->with('flash_message', 'Supplier updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        $row = Supplier::where('id',$id)->first();
        if($row){
            $fileName = $row->picture_card_front;
            $fileName2 = $row->picture_card_back;
            if(isset($fileName) && !empty($fileName)){
                Storage::disk('s3')->delete($fileName);
            }
            if(isset($fileName2) && !empty($fileName2)){
                Storage::disk('s3')->delete($fileName2);
            }
        }
        Supplier::destroy($id);
        return redirect('admin/suppliers')->with('flash_message', 'Supplier deleted!');
    }

    public function getSupplierConatcts(Request $request) {
        $getContacts = \App\SupplierContact::where('supplier_id', $request->supplier_id)->pluck('contact_id');
        $supplier_id = $request->supplier_id;
        $suppliercontacts = \App\Contact::whereIn('id', $getContacts)->latest()->get();

        return view('admin.suppliers.viewsuppliercontactsajax', compact('suppliercontacts', 'supplier_id'));
    }

    public function getContactRowByContactId(Request $request) {
        $contact = \App\Contact::where('id', $request->contact_id)->where('deleted_at', null)->first();
        $contact_id = $request->supplier_id;
        $contact_html = '';
        $contact_html .= "<tr><td>" . $contact->id . "<input type = 'hidden' name = 'contact_array[]' value = " . $contact->id . "></td><td>" . $contact->first_name . "</td><td>" . $contact->phone . "</td><td>" . $contact->email . "</td><td><a   class='btn btn-danger btn-sm deleteRow' href = 'javascript:void(0)'> <i class='fa fa-trash-o' aria-hidden='true'></i></a></td></tr>";
        return Response()->json(['msg' => 'Contact row', 'status' => true, 'contact_html' => $contact_html]);
    }

    public function getProductRowByProductId(Request $request) {
        $product = \App\Product::where('id', $request->product_id)->where('deleted_at', null)->first();
//        dd($request->product_id);
        $product_id = $request->product_id;
        $product_html = '';
        $product_html .= "<tr><td>" . $product->id . "<input type = 'hidden' name = 'product_array[]' value = " . $product->id . "></td><td>" . $product->name . "</td><td>" . $product->product_length . "</td><td>" . $product->product_width . "</td><td>" . $product->product_height . "</td><td><a   class='btn btn-danger btn-sm deleteRow' href = 'javascript:void(0)'> <i class='fa fa-trash-o' aria-hidden='true'></i></a></td></tr>";
        return Response()->json(['msg' => 'Product row', 'status' => true, 'product_html' => $product_html]);
    }

    public function getSupplierProducts(Request $request) {
        $getProducts = \App\SupplierProduct::where('supplier_id', $request->supplier_id)->pluck('product_id');
        $supplierproducts = \App\Product::whereIn('id', $getProducts)->latest()->get();
        $supplier_id = $request->supplier_id;
        return view('admin.suppliers.viewsupplierproductsajax', compact('supplierproducts', 'supplier_id'));
    }

    //adding supplier data through modal
    public function add_supplier_data_through_modal(Request $request) {
        $rules = ['name' => 'required|unique:suppliers', 'email' => 'required|string|max:255|email|unique:suppliers', 'city' => 'required'];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return Response()->json([
                        'status' => 'false',
                        'errors' => $validator->getMessageBag()->toArray()
                            ], 200); // 400 being the HTTP code for an invalid request.
        }
        $requestData = $request->all();
        $requestData['user_id'] = \Auth::id();
        if (isset($request->picture_company)) {
            $picture_company = $this->uploadFile($request, 'picture_company', '/uploads/picture_supplier');
            $requestData['picture_company'] = $picture_company;
        }
        if (isset($request->picture_card_front)) {
            $picture_card_front = $this->uploadFile($request, 'picture_card_front', '/uploads/picture_card');
            $requestData['picture_card_front'] = $picture_card_front;
        }
        if (isset($request->picture_card_back)) {
            $picture_card_back = $this->uploadFile($request, 'picture_card_back', '/uploads/picture_card');
            $requestData['picture_card_back'] = $picture_card_back;
        }
//        dd($requestData);
        $supplier = Supplier::create($requestData);
        return Response()->json(['status' => 'true', 'data' => $supplier]);
    }

    //dropzone certificate_attachment
    public function dropzone_certificate_attachment(Request $request) {
        //        $image = $request->file('file');
        //        $imageName = time() . '_' . $image->getClientOriginalName();
        //        $image->move(public_path('images/supplier_certificates'), $imageName);        
        $imageName = $this->uploadFile($request, 'file', '/images/supplier_certificates');
        // dd(Session::get('attachments'));
        //        Session::push('attachments', $imageName);
        //        Session::save();
        // dd(Session::get('attachments'));
        $supplier_id = $request->supplier_id;
        $addImage = \App\CertificateAttachment::create([
                    'file_name' => $imageName,
                    'supplier_id' => $supplier_id,
        ]);
        return response()->json(['success' => $imageName]);
    }

    //code for load certificate_attachments lines
    public function load_certificate_attachments_lines(Request $request) {
        //        dd($request->supplier_id);
        $supplier_id = $request->supplier_id;
        $certificate_attachments = \App\CertificateAttachment::where('supplier_id', $supplier_id)->get();
        //        dd($certificate_attachments->toArray());
        return view('admin.suppliers.load_certificate_attachments', compact('certificate_attachments'));
    }

    //create supplier certifications attachments ajax
    public function submit_supplier_certifications_ajax(Request $request) {
        if ($request->attachment_id != null) {
            $update = \App\CertificateAttachment::where('id', $request->attachment_id)->where('supplier_id', $request->supplier_id)->update(['notes' => $request->notes]);
            return Response()->json(['status' => 'true', 'message' => 'Note updated']);
        } else {
            return Response()->json(['status' => 'false', 'message' => 'Unable to save']);
        }
    }

    //supplier certification fields submit via ajax
    public function submit_certification_fields_ajax(Request $request) {
//        dd($request->supplier_id);
        if ($request->supplier_id != null) {
            $update = \App\Supplier::where('id', $request->supplier_id)->update([
                'ethical_audit' => $request->ethical_audit,
                'audit_name' => $request->audit_name,
                'certificate_comment' => $request->certificate_comment
            ]);
            return Response()->json(['status' => 'true', 'message' => 'Supplier Certificate Fields updated']);
        } else {
            return Response()->json(['status' => 'false', 'message' => 'Unable to save fields.']);
        }
    }
}
