<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\DisplayReady;
use Illuminate\Http\Request;

class DisplayReadyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $displayready = DisplayReady::where('display_length', 'LIKE', "%$keyword%")
                ->orWhere('display_width', 'LIKE', "%$keyword%")
                ->orWhere('display_height', 'LIKE', "%$keyword%")
                ->orWhere('display_max_weight', 'LIKE', "%$keyword%")
                ->orWhere('number_of_displays', 'LIKE', "%$keyword%")
                ->orWhere('product_price_per_unit', 'LIKE', "%$keyword%")
                ->orWhere('product_currency', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $displayready = DisplayReady::latest()->paginate($perPage);
        }
        return view('admin.display-ready.index', compact('displayready'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.display-ready.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'display_length' => 'required',
			'display_width' => 'required',
			'display_height' => 'required',
			'product_price_per_unit' => 'required',
			'product_currency' => 'required'
		]);
        $requestData = $request->all();     
        DisplayReady::create($requestData);
        return redirect('admin/display-ready')->with('flash_message', 'DisplayReady added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $displayready = DisplayReady::findOrFail($id);
        return view('admin.display-ready.show', compact('displayready'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $displayready = DisplayReady::findOrFail($id);
        return view('admin.display-ready.edit', compact('displayready'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'display_length' => 'required',
			'display_width' => 'required',
			'display_height' => 'required',
			'product_price_per_unit' => 'required',
			'product_currency' => 'required'
		]);
        $requestData = $request->all();
        $displayready = DisplayReady::findOrFail($id);
        $displayready->update($requestData);
        return redirect('admin/display-ready')->with('flash_message', 'DisplayReady updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        DisplayReady::destroy($id);
        return redirect('admin/display-ready')->with('flash_message', 'DisplayReady deleted!');
    }
}
