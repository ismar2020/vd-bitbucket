<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\OrderLine;
use Illuminate\Http\Request;

class OrderLinesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $orderlines = OrderLine::where('purchase_order', 'LIKE', "%$keyword%")
                ->orWhere('shipping_port', 'LIKE', "%$keyword%")
                ->orWhere('product_unit_quantity', 'LIKE', "%$keyword%")
                ->orWhere('container', 'LIKE', "%$keyword%")
                ->orWhere('purchase_cost', 'LIKE', "%$keyword%")
                ->orWhere('wholesale_cost', 'LIKE', "%$keyword%")
                ->orWhere('retail_cost', 'LIKE', "%$keyword%")
                ->orWhere('total_number_of_containers', 'LIKE', "%$keyword%")
                ->orWhere('total_purchase_cost', 'LIKE', "%$keyword%")
                ->orWhere('total_wholesale_cost', 'LIKE', "%$keyword%")
                ->orWhere('total_retail_cost', 'LIKE', "%$keyword%")
                ->orWhere('confirmed', 'LIKE', "%$keyword%")
                ->orWhere('active', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $orderlines = OrderLine::latest()->paginate($perPage);
        }
        return view('admin.order-lines.index', compact('orderlines'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.order-lines.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {        
        $requestData = $request->all();        
        OrderLine::create($requestData);
        return redirect('admin/order-lines')->with('flash_message', 'OrderLine added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $orderline = OrderLine::findOrFail($id);
        return view('admin.order-lines.show', compact('orderline'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $orderline = OrderLine::findOrFail($id);
        return view('admin.order-lines.edit', compact('orderline'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {        
        $requestData = $request->all();       
        $orderline = OrderLine::findOrFail($id);
        $orderline->update($requestData);
        return redirect('admin/order-lines')->with('flash_message', 'OrderLine updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        OrderLine::destroy($id);
        return redirect('admin/order-lines')->with('flash_message', 'OrderLine deleted!');
    }
}
