<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\OrderDocument;
use Illuminate\Http\Request;

class OrderDocumentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $orderdocuments = OrderDocument::where('order_id', 'LIKE', "%$keyword%")
                ->orWhere('original_file_name', 'LIKE', "%$keyword%")
                ->orWhere('file_name', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $orderdocuments = OrderDocument::latest()->paginate($perPage);
        }
        return view('admin.order-documents.index', compact('orderdocuments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.order-documents.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {        
        $requestData = $request->all();  
        OrderDocument::create($requestData);
        return redirect('admin/order-documents')->with('flash_message', 'OrderDocument added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $orderdocument = OrderDocument::findOrFail($id);
        return view('admin.order-documents.show', compact('orderdocument'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $orderdocument = OrderDocument::findOrFail($id);
        return view('admin.order-documents.edit', compact('orderdocument'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {        
        $requestData = $request->all();  
        $orderdocument = OrderDocument::findOrFail($id);
        $orderdocument->update($requestData);
        return redirect('admin/order-documents')->with('flash_message', 'OrderDocument updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        OrderDocument::destroy($id);
        return redirect('admin/order-documents')->with('flash_message', 'OrderDocument deleted!');
    }
}