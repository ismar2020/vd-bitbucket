<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\FisTemplate;
use Illuminate\Http\Request;

class FisTemplateController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $fistemplate = FisTemplate::where('template_name', 'LIKE', "%$keyword%")
                            ->orWhere('is_default', 'LIKE', "%$keyword%")
                            ->orWhere('transport', 'LIKE', "%$keyword%")
                            ->orWhere('transport_currency', 'LIKE', "%$keyword%")
                            ->orWhere('port_costs', 'LIKE', "%$keyword%")
                            ->orWhere('port_costs_currency', 'LIKE', "%$keyword%")
                            ->orWhere('insurance_cost', 'LIKE', "%$keyword%")
                            ->orWhere('insurance_cost_currency', 'LIKE', "%$keyword%")
                            ->orWhere('import_custom_duties_percentage', 'LIKE', "%$keyword%")
                            ->orWhere('import_taxes_percentage', 'LIKE', "%$keyword%")
                            ->orWhere('handing_costs_in_destination_terminal', 'LIKE', "%$keyword%")
                            ->orWhere('handing_costs_currency', 'LIKE', "%$keyword%")
                            ->orWhere('costs_of_shipping_after_destination_port', 'LIKE', "%$keyword%")
                            ->orWhere('shipping_after_port_currency', 'LIKE', "%$keyword%")
                            ->latest()->paginate($perPage);
        } else {
            if (\Auth::user()->roles->first()->name == 'super_admin') {
                $fistemplate = FisTemplate::latest()->get();
            } else if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
                $company_users = \App\User::where('company_id', \Auth::user()->company_id)->pluck('id')->toArray();
                $fistemplate = FisTemplate::whereIn('user_id', $company_users)->latest()->get();
            } else {
                $fistemplate = FisTemplate::where('user_id', \Auth::id())->latest()->get();
            }
        }
        return view('admin.fis-template.index', compact('fistemplate'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        return view('admin.fis-template.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {
        $this->validate($request, [
            'template_name' => 'required'
        ]);
        $requestData = $request->all();
        $requestData['user_id'] = \Auth::id();
        FisTemplate::create($requestData);
        return redirect('admin/fis-template')->with('flash_message', 'Landed Cost Template added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id) {
        $fistemplate = FisTemplate::findOrFail($id);
        return view('admin.fis-template.show', compact('fistemplate'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        $fistemplate = FisTemplate::findOrFail($id);
        return view('admin.fis-template.edit', compact('fistemplate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'template_name' => 'required'
        ]);
        $requestData = $request->all();
        $fistemplate = FisTemplate::findOrFail($id);
        $fistemplate->update($requestData);
        return redirect('admin/fis-template')->with('flash_message', 'Landed Cost Template updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        FisTemplate::destroy($id);
        return redirect('admin/fis-template')->with('flash_message', 'FisTemplate deleted!');
    }

}
