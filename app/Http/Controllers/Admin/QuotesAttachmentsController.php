<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\QuotesAttachment;
use Illuminate\Http\Request;

class QuotesAttachmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $quotesattachments = QuotesAttachment::where('quote_id', 'LIKE', "%$keyword%")
                ->orWhere('original_file_name', 'LIKE', "%$keyword%")
                ->orWhere('file_name', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $quotesattachments = QuotesAttachment::latest()->paginate($perPage);
        }
        return view('admin.quotes-attachments.index', compact('quotesattachments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.quotes-attachments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {        
        $requestData = $request->all();        
        QuotesAttachment::create($requestData);
        return redirect('admin/quotes-attachments')->with('flash_message', 'QuotesAttachment added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $quotesattachment = QuotesAttachment::findOrFail($id);
        return view('admin.quotes-attachments.show', compact('quotesattachment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $quotesattachment = QuotesAttachment::findOrFail($id);
        return view('admin.quotes-attachments.edit', compact('quotesattachment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {        
        $requestData = $request->all();       
        $quotesattachment = QuotesAttachment::findOrFail($id);
        $quotesattachment->update($requestData);
        return redirect('admin/quotes-attachments')->with('flash_message', 'QuotesAttachment updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        QuotesAttachment::destroy($id);
        return redirect('admin/quotes-attachments')->with('flash_message', 'QuotesAttachment deleted!');
    }
}
