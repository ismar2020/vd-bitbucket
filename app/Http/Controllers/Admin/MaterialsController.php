<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Material;
use Illuminate\Http\Request;

class MaterialsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $materials = Material::where('name', 'LIKE', "%$keyword%")
                            ->orWhere('description', 'LIKE', "%$keyword%")
                            ->latest()->paginate($perPage);
        } else {
            if (\Auth::user()->roles->first()->name == 'super_admin') {
                $materials = Material::latest()->get();
            } else if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
                $company_users = \App\User::where('company_id', \Auth::user()->company_id)->pluck('id')->toArray();
                $materials = Material::whereIn('user_id', $company_users)->latest()->get();
            } else {
                $materials = Material::where('user_id', \Auth::id())->latest()->get();
            }
        }
        return view('admin.materials.index', compact('materials'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        return view('admin.materials.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required'
        ]);
        $requestData = $request->all();
        $requestData['user_id'] = \Auth::id();
        Material::create($requestData);
        return redirect('admin/materials')->with('flash_message', 'Material added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id) {
        $material = Material::findOrFail($id);
        return view('admin.materials.show', compact('material'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        $material = Material::findOrFail($id);
        return view('admin.materials.edit', compact('material'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'name' => 'required'
        ]);
        $requestData = $request->all();
        $material = Material::findOrFail($id);
        $material->update($requestData);
        return redirect('admin/materials')->with('flash_message', 'Material updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        Material::destroy($id);
        return redirect('admin/materials')->with('flash_message', 'Material deleted!');
    }

}
