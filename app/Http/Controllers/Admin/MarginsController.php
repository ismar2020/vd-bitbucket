<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Margin;
use Illuminate\Http\Request;

class MarginsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
            $margins = Margin::where('company_id', \Auth::user()->company_id)->latest()->get();
        } else {
            $margins = Margin::latest()->get();
        }
        return view('admin.margins.index', compact('margins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        return view('admin.margins.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required'
        ]);
        $requestData = $request->all();
        $requestData['user_id'] = \Auth::id();
        if (\Auth::user()->roles->first()->name == 'super_admin') {
            Margin::create($requestData);
            return redirect('admin/margins')->with('flash_message', 'Margin added!');
        } else {
            return redirect('admin/margins')->with('flash_message', 'You Are Not Authorised!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id) {
        $margin = Margin::findOrFail($id);
        return view('admin.margins.show', compact('margin'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        $margin = Margin::findOrFail($id);
        return view('admin.margins.edit', compact('margin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'value' => 'required'
        ]);
        $requestData = $request->all();
        $margin = Margin::findOrFail($id);
        $margin->update($requestData);
        return redirect('admin/margins')->with('flash_message', 'Margin updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        Margin::destroy($id);
        return redirect('admin/margins')->with('flash_message', 'Margin deleted!');
    }

}
