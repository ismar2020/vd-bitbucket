<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\CurrencyExchangeRate;
use Illuminate\Http\Request;

class CurrencyExchangeRateController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $currencyexchangerate = CurrencyExchangeRate::where('currency_id_from', 'LIKE', "%$keyword%")
                            ->orWhere('currency_id_to', 'LIKE', "%$keyword%")
                            ->orWhere('exchange_rate', 'LIKE', "%$keyword%")
                            ->latest()->paginate($perPage);
        } else {

            if (\Auth::user()->roles->first()->name == 'super_admin') {
                $currencyexchangerate = CurrencyExchangeRate::latest()->get();
            } else if (\Auth::user()->roles->first()->name == 'company_user') {
                $company_users = \App\User::where('company_id', \Auth::user()->company_id)->pluck('id')->toArray();
                $currencyexchangerate = CurrencyExchangeRate::whereIn('user_id', $company_users)->latest()->get();
            } else {
                $currencyexchangerate = CurrencyExchangeRate::where('user_id', \Auth::id())->latest()->get();
            }
        }
        return view('admin.currency-exchange-rate.index', compact('currencyexchangerate'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        return view('admin.currency-exchange-rate.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {
        $this->validate($request, [
            'currency_id_from' => 'required',
            'currency_id_to' => 'required',
            'exchange_rate' => 'required'
        ]);
        $requestData = $request->all();
        $requestData['user_id'] = \Auth::id();
        CurrencyExchangeRate::create($requestData);
        return redirect('admin/currency-exchange-rate')->with('flash_message', 'CurrencyExchangeRate added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id) {
        $currencyexchangerate = CurrencyExchangeRate::findOrFail($id);
        return view('admin.currency-exchange-rate.show', compact('currencyexchangerate'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        $currencyexchangerate = CurrencyExchangeRate::findOrFail($id);
        return view('admin.currency-exchange-rate.edit', compact('currencyexchangerate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'currency_id_from' => 'required',
            'currency_id_to' => 'required',
            'exchange_rate' => 'required'
        ]);
        $requestData = $request->all();
        $currencyexchangerate = CurrencyExchangeRate::findOrFail($id);
        $currencyexchangerate->update($requestData);
        return redirect('admin/currency-exchange-rate')->with('flash_message', 'CurrencyExchangeRate updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        CurrencyExchangeRate::destroy($id);
        return redirect('admin/currency-exchange-rate')->with('flash_message', 'CurrencyExchangeRate deleted!');
    }

}
