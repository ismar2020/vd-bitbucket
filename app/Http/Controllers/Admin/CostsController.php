<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use DB;
use App\Cost;
use Illuminate\Http\Request;

class CostsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $costs = Cost::where('name', 'LIKE', "%$keyword%")
                            ->orWhere('value', 'LIKE', "%$keyword%")
                            ->orWhere('currency', 'LIKE', "%$keyword%")
                            ->orWhere('cost_percentage', 'LIKE', "%$keyword%")
                            ->orWhere('use', 'LIKE', "%$keyword%")
                            ->orWhere('When_to_apply', 'LIKE', "%$keyword%")
                            ->orWhere('apply_condition', 'LIKE', "%$keyword%")
                            ->orWhere('order_id', 'LIKE', "%$keyword%")
                            ->latest()->paginate($perPage);
        } else {
            if (\Auth::user()->roles->first()->name == 'super_admin') {
                $costs = Cost::latest()->get();
            } else if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
                $company_users = \App\User::where('company_id', \Auth::user()->company_id)->pluck('id')->toArray();
                $costs = Cost::whereIn('user_id', $company_users)->latest()->get();
            } else {
                $costs = Cost::where('user_id', \Auth::id())->latest()->get();
            }
        }
        return view('admin.costs.index', compact('costs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        $orders = DB::table('orders')->pluck('name', 'id');
        return view('admin.costs.create', compact('orders'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {

        $requestData = $request->all();
        $requestData['user_id'] = \Auth::id();
        Cost::create($requestData);
        return redirect('admin/costs')->with('flash_message', 'Cost added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id) {
        $cost = Cost::findOrFail($id);
        $orders = DB::table('orders')->pluck('name', 'id');
        return view('admin.costs.show', compact('cost', 'orders'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        $cost = Cost::findOrFail($id);
        $orders = DB::table('orders')->pluck('name', 'id');
        return view('admin.costs.edit', compact('cost', 'orders'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id) {

        $requestData = $request->all();
        $cost = Cost::findOrFail($id);
        $cost->update($requestData);
        return redirect('admin/costs')->with('flash_message', 'Cost updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        Cost::destroy($id);
        return redirect('admin/costs')->with('flash_message', 'Cost deleted!');
    }

}
