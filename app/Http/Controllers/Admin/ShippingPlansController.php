<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\ShippingPlan;
use Illuminate\Http\Request;

class ShippingPlansController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $shippingplans = ShippingPlan::where('order_id', 'LIKE', "%$keyword%")
                ->orWhere('container_id', 'LIKE', "%$keyword%")
                ->orWhere('purchase_order_number', 'LIKE', "%$keyword%")
                ->orWhere('shipping_to_port', 'LIKE', "%$keyword%")
                ->orWhere('shipping_date', 'LIKE', "%$keyword%")
                ->orWhere('container_unit_quantity', 'LIKE', "%$keyword%")
                ->orWhere('container_purchase_cost', 'LIKE', "%$keyword%")
                ->orWhere('container_wholesale_price', 'LIKE', "%$keyword%")
                ->orWhere('container_costs', 'LIKE', "%$keyword%")
                ->orWhere('conatiner_costs_value', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $shippingplans = ShippingPlan::latest()->paginate($perPage);
        }
        return view('admin.shipping-plans.index', compact('shippingplans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.shipping-plans.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {        
        $requestData = $request->all();        
        ShippingPlan::create($requestData);
        return redirect('admin/shipping-plans')->with('flash_message', 'ShippingPlan added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $shippingplan = ShippingPlan::findOrFail($id);
        return view('admin.shipping-plans.show', compact('shippingplan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $shippingplan = ShippingPlan::findOrFail($id);
        return view('admin.shipping-plans.edit', compact('shippingplan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {        
        $requestData = $request->all();        
        $shippingplan = ShippingPlan::findOrFail($id);
        $shippingplan->update($requestData);
        return redirect('admin/shipping-plans')->with('flash_message', 'ShippingPlan updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        ShippingPlan::destroy($id);
        return redirect('admin/shipping-plans')->with('flash_message', 'ShippingPlan deleted!');
    }
}
