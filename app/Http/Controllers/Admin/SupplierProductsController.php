<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\SupplierProduct;
use Illuminate\Http\Request;

class SupplierProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $supplierproducts = SupplierProduct::where('supplier_id', 'LIKE', "%$keyword%")
                ->orWhere('product_id', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $supplierproducts = SupplierProduct::latest()->paginate($perPage);
        }
        return view('admin.supplier-products.index', compact('supplierproducts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.supplier-products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {        
        $requestData = $request->all();        
        SupplierProduct::create($requestData);
        return redirect('admin/supplier-products')->with('flash_message', 'SupplierProduct added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $supplierproduct = SupplierProduct::findOrFail($id);
        return view('admin.supplier-products.show', compact('supplierproduct'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $supplierproduct = SupplierProduct::findOrFail($id);
        return view('admin.supplier-products.edit', compact('supplierproduct'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {        
        $requestData = $request->all();        
        $supplierproduct = SupplierProduct::findOrFail($id);
        $supplierproduct->update($requestData);
        return redirect('admin/supplier-products')->with('flash_message', 'SupplierProduct updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        SupplierProduct::destroy($id);
        return redirect('admin/supplier-products')->with('flash_message', 'SupplierProduct deleted!');
    }
}
