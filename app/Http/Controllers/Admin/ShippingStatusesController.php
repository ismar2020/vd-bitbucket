<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\ShippingStatus;
use Illuminate\Http\Request;

class ShippingStatusesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $shippingstatuses = ShippingStatus::where('name', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->orWhere('user_id', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $shippingstatuses = ShippingStatus::latest()->paginate($perPage);
        }
        return view('admin.shipping-statuses.index', compact('shippingstatuses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.shipping-statuses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {        
        $requestData = $request->all();   
        ShippingStatus::create($requestData);
        return redirect('admin/shipping-statuses')->with('flash_message', 'ShippingStatus added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $shippingstatus = ShippingStatus::findOrFail($id);
        return view('admin.shipping-statuses.show', compact('shippingstatus'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $shippingstatus = ShippingStatus::findOrFail($id);
        return view('admin.shipping-statuses.edit', compact('shippingstatus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {        
        $requestData = $request->all();        
        $shippingstatus = ShippingStatus::findOrFail($id);
        $shippingstatus->update($requestData);
        return redirect('admin/shipping-statuses')->with('flash_message', 'ShippingStatus updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        ShippingStatus::destroy($id);
        return redirect('admin/shipping-statuses')->with('flash_message', 'ShippingStatus deleted!');
    }
}
