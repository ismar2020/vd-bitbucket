<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\EthicalAudit;
use Illuminate\Http\Request;

class EthicalAuditsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $ethicalaudits = EthicalAudit::where('name', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->orWhere('user_id', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $ethicalaudits = EthicalAudit::latest()->paginate($perPage);
        }
        return view('admin.ethical-audits.index', compact('ethicalaudits'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.ethical-audits.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {        
        $requestData = $request->all();        
        EthicalAudit::create($requestData);
        return redirect('admin/ethical-audits')->with('flash_message', 'EthicalAudit added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $ethicalaudit = EthicalAudit::findOrFail($id);
        return view('admin.ethical-audits.show', compact('ethicalaudit'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $ethicalaudit = EthicalAudit::findOrFail($id);
        return view('admin.ethical-audits.edit', compact('ethicalaudit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {        
        $requestData = $request->all();        
        $ethicalaudit = EthicalAudit::findOrFail($id);
        $ethicalaudit->update($requestData);
        return redirect('admin/ethical-audits')->with('flash_message', 'EthicalAudit updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        EthicalAudit::destroy($id);
        return redirect('admin/ethical-audits')->with('flash_message', 'EthicalAudit deleted!');
    }
}
