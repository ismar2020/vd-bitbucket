<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\ShippingContainrProduct;
use Illuminate\Http\Request;

class ShippingContainrProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $shippingcontainrproducts = ShippingContainrProduct::where('order_id', 'LIKE', "%$keyword%")
                ->orWhere('shipping_plan_id', 'LIKE', "%$keyword%")
                ->orWhere('product_name', 'LIKE', "%$keyword%")
                ->orWhere('product_image', 'LIKE', "%$keyword%")
                ->orWhere('number_of_units_that_fit', 'LIKE', "%$keyword%")
                ->orWhere('container_order_quantity', 'LIKE', "%$keyword%")
                ->orWhere('container_order_quantity_cbm', 'LIKE', "%$keyword%")
                ->orWhere('container_total_cost', 'LIKE', "%$keyword%")
                ->orWhere('container_total_wholesale_price', 'LIKE', "%$keyword%")
                ->orWhere('total_product_units_added_to_other_containers', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $shippingcontainrproducts = ShippingContainrProduct::latest()->paginate($perPage);
        }
        return view('admin.shipping-containr-products.index', compact('shippingcontainrproducts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.shipping-containr-products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {        
        $requestData = $request->all();        
        ShippingContainrProduct::create($requestData);
        return redirect('admin/shipping-containr-products')->with('flash_message', 'ShippingContainrProduct added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $shippingcontainrproduct = ShippingContainrProduct::findOrFail($id);
        return view('admin.shipping-containr-products.show', compact('shippingcontainrproduct'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $shippingcontainrproduct = ShippingContainrProduct::findOrFail($id);
        return view('admin.shipping-containr-products.edit', compact('shippingcontainrproduct'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {        
        $requestData = $request->all();        
        $shippingcontainrproduct = ShippingContainrProduct::findOrFail($id);
        $shippingcontainrproduct->update($requestData);
        return redirect('admin/shipping-containr-products')->with('flash_message', 'ShippingContainrProduct updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        ShippingContainrProduct::destroy($id);
        return redirect('admin/shipping-containr-products')->with('flash_message', 'ShippingContainrProduct deleted!');
    }
}
