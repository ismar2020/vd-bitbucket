<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\ProductPackagingOption;
use Illuminate\Http\Request;

class ProductPackagingOptionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $productpackagingoptions = ProductPackagingOption::where('product_id', 'LIKE', "%$keyword%")
                ->orWhere('packaging_type', 'LIKE', "%$keyword%")
                ->orWhere('quantity_per_page', 'LIKE', "%$keyword%")
                ->orWhere('packaging_length', 'LIKE', "%$keyword%")
                ->orWhere('packaging_width', 'LIKE', "%$keyword%")
                ->orWhere('packaging_height', 'LIKE', "%$keyword%")
                ->orWhere('gross_weight_including_packaging', 'LIKE', "%$keyword%")
                ->orWhere('packaging_cbm', 'LIKE', "%$keyword%")
                ->orWhere('floor_ready', 'LIKE', "%$keyword%")
                ->orWhere('stackable', 'LIKE', "%$keyword%")
                ->orWhere('nested_product', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $productpackagingoptions = ProductPackagingOption::latest()->paginate($perPage);
        }
        return view('admin.product-packaging-options.index', compact('productpackagingoptions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.product-packaging-options.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {        
        $requestData = $request->all();        
        ProductPackagingOption::create($requestData);
        return redirect('admin/product-packaging-options')->with('flash_message', 'ProductPackagingOption added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $productpackagingoption = ProductPackagingOption::findOrFail($id);
        return view('admin.product-packaging-options.show', compact('productpackagingoption'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $productpackagingoption = ProductPackagingOption::findOrFail($id);
        return view('admin.product-packaging-options.edit', compact('productpackagingoption'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $requestData = $request->all();   
        $productpackagingoption = ProductPackagingOption::findOrFail($id);
        $productpackagingoption->update($requestData);
        return redirect('admin/product-packaging-options')->with('flash_message', 'ProductPackagingOption updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        ProductPackagingOption::destroy($id);
        return redirect('admin/product-packaging-options')->with('flash_message', 'ProductPackagingOption deleted!');
    }
}
