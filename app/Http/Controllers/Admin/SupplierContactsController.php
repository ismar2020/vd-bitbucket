<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\SupplierContact;
use Illuminate\Http\Request;

class SupplierContactsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $suppliercontacts = SupplierContact::where('supplier_id', 'LIKE', "%$keyword%")
                ->orWhere('contact_id', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $suppliercontacts = SupplierContact::latest()->paginate($perPage);
        }
        return view('admin.supplier-contacts.index', compact('suppliercontacts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.supplier-contacts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {        
        $requestData = $request->all();        
        SupplierContact::create($requestData);
        return redirect('admin/supplier-contacts')->with('flash_message', 'SupplierContact added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $suppliercontact = SupplierContact::findOrFail($id);
        return view('admin.supplier-contacts.show', compact('suppliercontact'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $suppliercontact = SupplierContact::findOrFail($id);
        return view('admin.supplier-contacts.edit', compact('suppliercontact'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {        
        $requestData = $request->all();        
        $suppliercontact = SupplierContact::findOrFail($id);
        $suppliercontact->update($requestData);
        return redirect('admin/supplier-contacts')->with('flash_message', 'SupplierContact updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        SupplierContact::destroy($id);
        return redirect('admin/supplier-contacts')->with('flash_message', 'SupplierContact deleted!');
    }
}
