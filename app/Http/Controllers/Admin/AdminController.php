<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        return view('admin.dashboard');
    }
    public function setting()
    {
        return view('admin.settings');
    }
    public function profile()
    {
        return view('admin.profile');
    }
    public function admin()
    {
        return view('admin.admin');
    }
}
