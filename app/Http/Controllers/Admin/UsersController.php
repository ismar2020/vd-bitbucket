<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Auth;

class UsersController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request) {
        $keyword = $request->get('search');
        $perPage = 15;

        if (!empty($keyword)) {
            $users = User::where('first_name', 'LIKE', "%$keyword%")->orWhere('email', 'LIKE', "%$keyword%")
                            ->latest()->paginate($perPage);
        } else {

            if (\Auth::user()->roles->first()->name == 'super_admin') {
                $users = User::latest()->get();
            } else if (\Auth::user()->roles->first()->name == 'company_user') {
                if (\Auth::user()->company_id != null) {
                    $company_users = \App\User::where('company_id', \Auth::user()->company_id)->pluck('id')->toArray();
//                    $supplier_users = \App\User::where('company_id', \Auth::user()->company_id)->where('supplier_id','!=',null)->pluck('id')->toArray();
//                    dd($company_users);
                    $users = User::whereIn('id', $company_users)->latest()->get();
                } else {
                    $users = [];
                }
            } else {
                $users = User::latest()->get();
            }
        }
        return view('admin.users.index', compact('users'));
    }
    
    public function give_access_to_new_users() {
        $new_users = User::whereDoesntHave('roles')->latest()->get();
//        dd($new_users->count());
        return view('admin.users.new_users', compact('new_users'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create() {
        $roles = Role::select('id', 'name', 'label');
        if(Auth::check() && Auth::user()->hasRole('company_user')){
            $roles = $roles->where('name','!=','super_admin')->get();
        } else if(Auth::check() && Auth::user()->hasRole('supplier')) {
            $roles = $roles->where('name','supplier')->get();
        }else {
            $roles = $roles->get();
        }
        $roles = $roles->pluck('label', 'name');
        return view('admin.users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request) {
        $this->validate(
                $request,
                [
                    'username' => 'required|unique:users',
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'email' => 'required|string|max:255|email|unique:users',
                    'password' => 'required',
                    'roles' => 'required'
                ]
        );
        $data = $request->except('password');
        $data['password'] = bcrypt($request->password);
        if(Auth::check() && Auth::user()->hasRole('company_user')){
        $data['company_id'] = Auth::user()->company_id;
        }
        $user = User::create($data);
        foreach ($request->roles as $role) {
            $user->assignRole($role);
        }
        return redirect('admin/users')->with('flash_message', 'User added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function show($id) {
        $user = User::findOrFail($id);
        return view('admin.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function edit($id) {
//       dd('s');
        $roles = Role::select('id', 'name', 'label');
        if (Auth::check() && Auth::user()->hasRole('company_user')) {
            $roles = $roles->where('name','!=','super_admin')->get();
        } else if (Auth::check() && Auth::user()->hasRole('supplier')) {
            $roles = $roles->where('name', 'supplier')->get();
        } else {
            $roles = $roles->get();
        }
        $roles = $roles->pluck('label', 'name');

        $user = User::with('roles')->select('id', 'username', 'first_name', 'last_name', 'email', 'password', 'mobile_prefix', 'mobile', 'supplier_id', 'company_id')->findOrFail($id);
        $user_roles = [];
        foreach ($user->roles as $role) {
            $user_roles[] = $role->name;
        }
        return view('admin.users.edit', compact('user', 'roles', 'user_roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int      $id
     *
     * @return void
     */
    public function update(Request $request, $id) {
//        dd('s');
        $this->validate(
                $request,
                [
                    'username' => 'required',
                    'first_name' => 'required',
                    'email' => 'required|string|max:255|email|unique:users,email,' . $id,
                    'roles' => 'required'
                ]
        );

        $data = $request->except('password');
        if ($request->has('password')) {
            $data['password'] = bcrypt($request->password);
        }
        $user = User::findOrFail($id);
        $user->update($data);
        $user->roles()->detach();
//        dd($request->roles);
        foreach ($request->roles as $role) {   
            $user->assignRole($role);
        }
        return redirect('admin/users')->with('flash_message', 'User updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy($id) {
        User::destroy($id);
        return redirect('admin/users')->with('flash_message', 'User deleted!');
    }

}
