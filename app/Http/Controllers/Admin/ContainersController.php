<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Container;
use Illuminate\Http\Request;

class ContainersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $containers = Container::where('name', 'LIKE', "%$keyword%")
                ->orWhere('length', 'LIKE', "%$keyword%")
                ->orWhere('width', 'LIKE', "%$keyword%")
                ->orWhere('height', 'LIKE', "%$keyword%")
                ->orWhere('max_weight', 'LIKE', "%$keyword%")
                ->orWhere('container_cbm', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $containers = Container::latest()->paginate($perPage);
        }
        return view('admin.containers.index', compact('containers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.containers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'name' => 'required',
			'length' => 'required',
			'width' => 'required',
			'height' => 'required',
			'container_cbm' => 'required'
		]);
        $requestData = $request->all();        
        Container::create($requestData);
        return redirect('admin/containers')->with('flash_message', 'Container added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $container = Container::findOrFail($id);
        return view('admin.containers.show', compact('container'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $container = Container::findOrFail($id);
        return view('admin.containers.edit', compact('container'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'name' => 'required',
			'length' => 'required',
			'width' => 'required',
			'height' => 'required',
			'container_cbm' => 'required'
		]);
        $requestData = $request->all();        
        $container = Container::findOrFail($id);
        $container->update($requestData);
        return redirect('admin/containers')->with('flash_message', 'Container updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Container::destroy($id);
        return redirect('admin/containers')->with('flash_message', 'Container deleted!');
    }
}
