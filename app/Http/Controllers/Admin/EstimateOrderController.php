<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use DB;
use App\EstimateOrder;
use Illuminate\Http\Request;

class EstimateOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $estimateorder = EstimateOrder::where('quick_container_type', 'LIKE', "%$keyword%")
                ->orWhere('number_of_containers', 'LIKE', "%$keyword%")
                ->orWhere('cost_id', 'LIKE', "%$keyword%")
                ->orWhere('product_price_per_unit', 'LIKE', "%$keyword%")
                ->orWhere('product_currency', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $estimateorder = EstimateOrder::latest()->paginate($perPage);
        }
        return view('admin.estimate-order.index', compact('estimateorder'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $costs= DB::table('costs')->pluck('name', 'id');
        return view('admin.estimate-order.create', compact('costs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'product_price_per_unit' => 'required',
			'product_currency' => 'required'
		]);
        $requestData = $request->all();        
        EstimateOrder::create($requestData);
        return redirect('admin/estimate-order')->with('flash_message', 'EstimateOrder added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $estimateorder = EstimateOrder::findOrFail($id);
        $costs= DB::table('costs')->pluck('name', 'id');
        return view('admin.estimate-order.show', compact('estimateorder','costs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $estimateorder = EstimateOrder::findOrFail($id);
        $costs= DB::table('costs')->pluck('name', 'id');
        return view('admin.estimate-order.edit', compact('estimateorder','costs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'product_price_per_unit' => 'required',
			'product_currency' => 'required'
		]);
        $requestData = $request->all();
        
        $estimateorder = EstimateOrder::findOrFail($id);
        $estimateorder->update($requestData);
        return redirect('admin/estimate-order')->with('flash_message', 'EstimateOrder updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        EstimateOrder::destroy($id);
        return redirect('admin/estimate-order')->with('flash_message', 'EstimateOrder deleted!');
    }
}
