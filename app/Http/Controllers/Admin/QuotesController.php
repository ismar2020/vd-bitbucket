<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Quote;
use Illuminate\Http\Request;
use Rap2hpoutre\FastExcel\FastExcel;
use Auth;

class QuotesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $quotes = Quote::where('name', 'LIKE', "%$keyword%")
                            ->orWhere('quote_status', 'LIKE', "%$keyword%")
                            ->orWhere('quote_category', 'LIKE', "%$keyword%")
                            ->orWhere('active', 'LIKE', "%$keyword%")
                            ->orWhere('user_id', 'LIKE', "%$keyword%")
                            ->latest()->paginate($perPage);
        } else {
            if (\Auth::user()->roles->first()->name == 'super_admin') {
                $quotes = Quote::latest()->get();
            } else if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
                $company_users = \App\User::where('company_id', \Auth::user()->company_id)->pluck('id')->toArray();
                $quotes = Quote::whereIn('user_id', $company_users)->latest()->get();
            } else if (\Auth::user()->roles->first()->name == 'supplier' && \Auth::user()->supplier_id != null) {
                $suppliersId = \App\User::where('supplier_id', \Auth::user()->supplier_id)->pluck('id');

                $getAssociatedProducts = \App\Product::where('supplier_id', \Auth::user()->supplier_id)->pluck('id');
                $getQuoteId = \App\QuoteLines::whereIn('product_id', $getAssociatedProducts)->pluck('quote_id');
                $getAssociatedQuotes = Quote::whereIn('id', $getQuoteId)->get();
                //  dd($getAssociatedQuotes);
                $quotes = Quote::whereIn('user_id', $suppliersId)->latest()->get();
                //                dd($suppliersId);
                $quotes = $getAssociatedQuotes->merge($quotes);
                //$quotes = array_merge($quotes, $getAssociatedQuotes);
            } else {
                $quotes = Quote::where('user_id', \Auth::id())->latest()->get();
            }
        }

        return view('admin.quotes.index', compact('quotes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        return view('admin.quotes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {

        $requestData = $request->all();
        $requestData['user_id'] = \Auth::id();
        $quote_id = Quote::create($requestData);

//      redirecting to Quotes edit page

        $quote = Quote::findOrFail($quote_id->id);
//        return view('admin.quotes.edit', compact('quote'));
        return redirect('admin/quotes/' . $quote_id->id . '/edit')->with(['quote' => $quote])->with('flash_message', 'Quote added!');
//        return redirect('admin/quotes')->with('flash_message', 'Quote added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id) {
        $quote = Quote::findOrFail($id);

        return view('admin.quotes.show', compact('quote'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        $quote = Quote::findOrFail($id);

        return view('admin.quotes.edit', compact('quote'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id) {

        $requestData = $request->all();

        $quote = Quote::findOrFail($id);
        $quote->update($requestData);

        return redirect('admin/quotes')->with('flash_message', 'Quote updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        Quote::destroy($id);

        return redirect('admin/quotes')->with('flash_message', 'Quote deleted!');
    }

    //code for load Quote lines
    public function load_quote_lines(Request $request) {

        $quote_id = $request->quote_id;
        $quote = Quote::where('id', $quote_id)->first();
        if (\Auth::user()->roles->first()->name == 'supplier') {
//            $product_id = \App\Product::where('user_id', \Auth::id())->pluck('id');
            $product_id = \App\Product::where('supplier_id', \Auth::user()->supplier_id)->pluck('id');
            $quote_lines = \App\QuoteLines::select(['quote_lines.*', 'countries.currency_code as product_unit_currency', 'materials.name as product_material'])->where('quote_id', $quote_id)->whereIn('product_id', $product_id)->leftjoin('countries', 'countries.id', '=', 'quote_lines.product_unit_currency')->leftjoin('materials', 'materials.id', '=', 'quote_lines.product_material')->get();
        } else {
            $quote_lines = \App\QuoteLines::select(['quote_lines.*', 'countries.currency_code as product_unit_currency', 'materials.name as product_material'])->where('quote_id', $quote_id)->leftjoin('countries', 'countries.id', '=', 'quote_lines.product_unit_currency')->leftjoin('materials', 'materials.id', '=', 'quote_lines.product_material')->get();
        }
        return view('admin.quotes.load_quote_lines', compact('quote_lines', 'quote'));
    }

    public function insert_quote_lines(Request $request) {
        $quote_id = $request->quote_id;
        $product_id = $request->quote_product_line_id;
        $product = \App\Product::where('id', $product_id)->first();
        $supplier_product_details = \App\SupplierProduct::where('product_id', $product->id)->first();
        if ($supplier_product_details) {
            $product_supplier_id = $supplier_product_details->supplier_id;
        } else {
            $product_supplier_id = null;
        }
//        dd($product_supplier_id);
        $container_id1 = \App\Container::where('name', '20FT')->first();
        $container_id2 = \App\Container::where('name', '20FT HQ')->first();
        $container_id3 = \App\Container::where('name', '40FT')->first();
        $container_id4 = \App\Container::where('name', '40FT HQ')->first();


        if ($container_id1) {
            $checkIfContainerExist = \App\ProductQuantityPerContainers::where('container_id', $container_id1->id)->where('product_id', $product->id)->first();
            if ($checkIfContainerExist) {
                $product_quantity_per_20ft = $checkIfContainerExist->product_quantity;
            } else {
                $product_quantity_per_20ft = null;
            }
        }
        if ($container_id2) {
            $checkIfContainerExist = \App\ProductQuantityPerContainers::where('container_id', $container_id2->id)->where('product_id', $product->id)->first();
            if ($checkIfContainerExist) {
                $product_quantity_per_20fthq = $checkIfContainerExist->product_quantity;
            } else {
                $product_quantity_per_20fthq = null;
            }
        }
        if ($container_id3) {
            $checkIfContainerExist = \App\ProductQuantityPerContainers::where('container_id', $container_id3->id)->where('product_id', $product->id)->first();
            if ($checkIfContainerExist) {
                $product_quantity_per_40ft = $checkIfContainerExist->product_quantity;
            } else {
                $product_quantity_per_40ft = null;
            }
        }
        if ($container_id4) {
            $checkIfContainerExist = \App\ProductQuantityPerContainers::where('container_id', $container_id4->id)->where('product_id', $product->id)->first();
            if ($checkIfContainerExist) {
                $product_quantity_per_40fthq = $checkIfContainerExist->product_quantity;
            } else {
                $product_quantity_per_40fthq = null;
            }
        }
//       dd($product->product_material);
        \App\QuoteLines::create([
            'quote_id' => $quote_id,
            'product_id' => $product_id,
            'product_name' => $product->name,
            'product_length' => $product->product_length,
            'product_width' => $product->product_width,
            'product_height' => $product->product_height,
            'product_weight' => $product->product_weight,
            'product_material' => $product->product_material,
            'product_unit_price' => $product->product_price_per_unit,
            'product_unit_currency' => $product->product_currency,
            'shipping_port' => $product->shipping_port,
            'supplier' => $product_supplier_id,
            'product_quantity_per_20ft' => $product_quantity_per_20ft,
            'product_quantity_per_20fthq' => $product_quantity_per_20fthq,
            'product_quantity_per_40ft' => $product_quantity_per_40ft,
            'product_quantity_per_40fthq' => $product_quantity_per_40fthq,
            'product_quantity_per_box' => $product->product_quantity_per_box,
            'product_image' => $product->pic_id,
            'product_cbm' => $product->product_cbm,
            'product_box_length' => $product->product_box_length,
            'product_box_width' => $product->product_box_width,
            'product_box_height' => $product->product_box_height,
            'product_box_weight' => $product->product_box_weight,
            'product_box_cbm' => $product->product_box_cbm,
            'boxes_per_stack' => $product->boxes_per_stack
        ]);
        Response()->json(['status' => 'true']);
    }

    //create quote ajax
    public function submit_quote_ajax(Request $request) {
        $requestData = $request->all();
//        dd($requestData);
        if ($request->quote_id != null) {
            $quote = Quote::findOrFail($request->quote_id);
            $quote->update($requestData);
        } else {
            return Response()->json(['status' => 'false']);
        }
        $return = [];
        $image = '';
        if (isset($request->quote_lines)) {
            foreach ($request->quote_lines as $key => $value):
                if (isset($value['row_id'])):
//      for updating product image if isset in quote line and product table
                    $attribute = 'product_image';
                    if (isset($request->quote_lines[$key][$attribute])):
                        $image = $this->uploadFileQuoteLines($request, $key, '/uploads/product_picture', $attribute);
                        $value[$attribute] = $image;
                        $product_id = \App\QuoteLines::where('id', $value['row_id'])->first()->product_id;
                        \App\Product::where('id', $product_id)->update(['pic_id' => $image]);
                    endif;
//              ends
                    $quote_lines = \App\QuoteLines::findOrFail($value['row_id']);
                    $return[] = $value;
                    $quote_lines->update($value);

//******************Now Updating in product table************************************
                    $update_quote_line = \App\QuoteLines::where('id', $value['row_id'])->first();
                    if ($update_quote_line) {
                        $upate_product = \App\Product::findOrFail($update_quote_line->product_id);
//                        code for finding product qty per container
                        $container_id1 = \App\Container::where('name', '20FT')->first();
                        $container_id2 = \App\Container::where('name', '20FT HQ')->first();
                        $container_id3 = \App\Container::where('name', '40FT')->first();
                        $container_id4 = \App\Container::where('name', '40FT HQ')->first();


                        if ($container_id1) {
                            $checkIfContainerExist = \App\ProductQuantityPerContainers::where('container_id', $container_id1->id)->where('product_id', $upate_product->id)->first();
                            if ($checkIfContainerExist) {
                                \App\ProductQuantityPerContainers::where('id', $checkIfContainerExist->id)->update(['product_quantity' => $value['product_quantity_per_20ft']]);
                            } else {
                                \App\ProductQuantityPerContainers::create([
                                    'container_id' => $container_id1->id,
                                    'product_id' => $upate_product->id,
                                    'product_quantity' => $value['product_quantity_per_20ft']
                                ]);
                            }
                        }
                        if ($container_id2) {
                            $checkIfContainerExist = \App\ProductQuantityPerContainers::where('container_id', $container_id2->id)->where('product_id', $upate_product->id)->first();
                            if ($checkIfContainerExist) {
                                \App\ProductQuantityPerContainers::where('id', $checkIfContainerExist->id)->update(['product_quantity' => $value['product_quantity_per_20fthq']]);
                            } else {
                                \App\ProductQuantityPerContainers::create([
                                    'container_id' => $container_id2->id,
                                    'product_id' => $upate_product->id,
                                    'product_quantity' => $value['product_quantity_per_20fthq']
                                ]);
                            }
                        }
                        if ($container_id3) {
                            $checkIfContainerExist = \App\ProductQuantityPerContainers::where('container_id', $container_id3->id)->where('product_id', $upate_product->id)->first();
                            if ($checkIfContainerExist) {
                                \App\ProductQuantityPerContainers::where('id', $checkIfContainerExist->id)->update(['product_quantity' => $value['product_quantity_per_40ft']]);
                            } else {
                                \App\ProductQuantityPerContainers::create([
                                    'container_id' => $container_id3->id,
                                    'product_id' => $upate_product->id,
                                    'product_quantity' => $value['product_quantity_per_40ft']
                                ]);
                            }
                        }
                        if ($container_id4) {
                            $checkIfContainerExist = \App\ProductQuantityPerContainers::where('container_id', $container_id4->id)->where('product_id', $upate_product->id)->first();
                            if ($checkIfContainerExist) {
                                \App\ProductQuantityPerContainers::where('id', $checkIfContainerExist->id)->update(['product_quantity' => $value['product_quantity_per_40fthq']]);
                            } else {
                                \App\ProductQuantityPerContainers::create([
                                    'container_id' => $container_id4->id,
                                    'product_id' => $upate_product->id,
                                    'product_quantity' => $value['product_quantity_per_40fthq']
                                ]);
                            }
                        }
//                        ends

                        $upate_product->name = $value['product_name'];
                        $upate_product->product_price_per_unit = $value['product_unit_price'];
                        $upate_product->update($value);
                    }
//********************************ends update in product******************************
                endif;
            endforeach;
        }
        return Response()->json(['status' => 'true', 'image' => $image]);
    }

//    import CSV
    public function import_csv(Request $request) {

        $supplier_name = \App\User::select('first_name')->where('supplier_id',$request->supplier_id)->first();
        dd($supplier_name);
        if ($request->hasFile('csv_file')) {
            $file = $request->file('csv_file');
            $name = time() . '-' . $file->getClientOriginalName();
            $allowed = array('csv', 'xlsx');
            $ext = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
            if (!in_array($ext, $allowed)) {
                return Response()->json(['status' => 'false']);
            }
//            dd($name);
//check out the edit content on bottom of my answer for details on $storage
            $storage = '/productCsv/';
            $path = $storage;

// Moves file to folder on server
            $file->move(public_path() . '/productCsv/', $name);


            if ($request->input('quote_id') != '0') {
                $redirect = 'admin/quotes/' . $request->input('quote_id') . '/edit';
            } else {
                $redirect = 'admin/quotes';
            }
            $quote_id = $request->input('quote_id');
            $collection = (new FastExcel)->import(public_path($path . $name));

            //code for checking how many updated and how many inserted new products
            $newProductCount = 0;
            $newProductNames = [];
            $updateProductCount = 0;
            $updateProductNames = [];
            //ends
//            dd($request->toArray());
            if (count($collection) > 0) {
                //set strict mode off on sql server
                config()->set('database.connections.mysql.strict', false);
                //ends
                foreach ($collection as $row) {
                    $product_name = $row['product_name'];
//                    dd($product_name);
                    $product = \App\Product::where('name', $product_name)->first();
//                    dd($product);
                    if ($product != '' && $product != null) {
                        $product_relation = \App\SupplierProduct::where('supplier_id', $request->supplier_id)->where('product_id', $product->id)->first();
                        //product id
                        $old_or_new_product_id = $product->id;
                    }
                    $container_id1 = \App\Container::where('name', '20FT')->first();
                    $container_id2 = \App\Container::where('name', '20FT HQ')->first();
                    $container_id3 = \App\Container::where('name', '40FT')->first();
                    $container_id4 = \App\Container::where('name', '40FT HQ')->first();
//                    dd($product->supplier_id, $request->supplier_id);
//              checking if product exist or not if yes then update else create it
                    if ($product != '' && $product != null && $product->supplier_id == $request->supplier_id) {
//                         && $product->supplier_id == $request->supplier_id
                        //creating count and storing names of Updated ones for sweetalert
                        $updateProductCount++;
                        array_push($updateProductNames, $row['product_name']);
                        //ends
                        $product_update_arr = ['name' => $row['product_name'], 'supplier_id' => $request->supplier_id, 'product_length' => $row['product_length_cm'], 'product_width' => $row['product_width_cm'], 'product_height' => $row['product_height_cm'], 'product_weight' => $row['product_weight_kg'], 'product_price_per_unit' => $row['fob_price_per_unit'], 'product_cbm' => $row['product_cbm']];


                        //add currency to array if founded
                        $updateFobCurrency = \App\Country::where('id', $request->product_currency)->first();
                        if ($updateFobCurrency != '') {
                            $currToUpdate = $updateFobCurrency->id;
                            $product_update_arr['product_currency'] = $currToUpdate;
                        }
                        //add port to array if founded
                        $updatePort = \App\ShippingPort::where('id', $request->shipping_port)->first();
                        if ($updatePort != '') {
                            $portToId = $updatePort->id;
                            $product_update_arr['shipping_port'] = $portToId;
                        }
                        //add product material to array if founded
                        $updateProdMaterial = \App\Material::where('id', $request->product_material)->first();
                        if ($updateProdMaterial != '') {
                            $prodMaterialToId = $updateProdMaterial->id;
                            $product_update_arr['product_material'] = $prodMaterialToId;
                        }

                        //---------------filtering out empty, false and null values from array
                        $product_update_arr = array_filter($product_update_arr, function($value) {
                            return ($value !== null && $value !== false && $value !== '');
                        });
                        //--------------------------ends
//                        dd($product_update_arr);
                        \App\Product::where('id', $product->id)->update($product_update_arr);
                        $product_data = \App\Product::findOrFail($product->id);
                        $updated_product = \App\Product::findOrFail($product->id);

//                      checking if quote line exist, if not then creating One
                        $quote_line = \App\QuoteLines::where('product_id', $product->id)->where('quote_id', $request->quote_id)->first();
                        if ($quote_line) {

                            $updated_quote = \App\QuoteLines::findOrFail($quote_line->id);

                            $quote_update_arr = ['product_name' => $row['product_name'], 'product_length' => $row['product_length_cm'], 'product_width' => $row['product_width_cm'], 'product_height' => $row['product_height_cm'], 'product_weight' => $row['product_weight_kg'], 'product_unit_price' => $row['fob_price_per_unit'], 'product_cbm' => $row['product_cbm'], 'supplier' => $request->supplier_id, 'product_quantity_per_20ft' => $row['product_quantity_per_20ft'], 'product_quantity_per_20fthq' => $row['product_quantity_per_20fthq'], 'product_quantity_per_40ft' => $row['product_quantity_per_40ft'], 'product_quantity_per_40fthq' => $row['product_quantity_per_40fthq']];


                            //add currency to arr if founded
                            $updateFobCurrency = \App\Country::where('id', $request->product_currency)->first();
                            if ($updateFobCurrency != '') {
                                $currToUpdate = $updateFobCurrency->id;
                                $quote_update_arr['product_unit_currency'] = $currToUpdate;
                            }
                            //add port to arr if founded
                            $updatePort = \App\ShippingPort::where('id', $request->shipping_port)->first();
                            if ($updatePort != '') {
                                $portToId = $updatePort->id;
                                $quote_update_arr['shipping_port'] = $portToId;
                            }
                            //add product material to arr if founded
                            $updateProdMaterial = \App\Material::where('id', $request->product_material)->first();
                            if ($updateProdMaterial != '') {
                                $prodMaterialToId = $updateProdMaterial->id;
                                $quote_update_arr['product_material'] = $prodMaterialToId;
                            }

                            //---------------filtering out empty, false and null values from array
                            $quote_update_arr = array_filter($quote_update_arr, function($value) {
                                return ($value !== null && $value !== false && $value !== '');
                            });
                            //--------------------------ends
//                            dd($quote_update_arr);
                            $updated_quote->update($quote_update_arr);
                        } else {

                            $quote_line = \App\QuoteLines::create([
                                        'quote_id' => $request->quote_id,
                                        'product_id' => $product->id,
                                        'product_name' => $updated_product->name,
                                        'product_length' => $updated_product->product_length,
                                        'product_width' => $updated_product->product_width,
                                        'product_height' => $updated_product->product_height,
                                        'product_weight' => $updated_product->product_weight,
                                        'product_material' => $updated_product->product_material,
                                        'product_cbm' => $updated_product->product_cbm,
                                        'product_unit_price' => $updated_product->product_price_per_unit,
                                        'product_unit_currency' => $updated_product->product_currency,
                                        'shipping_port' => $updated_product->shipping_port,
                                        'supplier' => $request->supplier_id,
                                        'product_quantity_per_box' => $updated_product->product_quantity_per_box,
                                        'product_box_length' => $updated_product->product_box_length,
                                        'product_box_width' => $updated_product->product_box_width,
                                        'product_box_height' => $updated_product->product_box_height,
                                        'product_box_weight' => $updated_product->product_box_weight,
                                        'product_box_cbm' => $updated_product->product_box_cbm,
                                        'boxes_per_stack' => $updated_product->boxes_per_stack
                            ]);
                        }
//                    checking if supplier product relation exist, if not then creating One
                        $supplier_product = \App\SupplierProduct::where('supplier_id', $request->supplier_id)->first();
                        if (!$supplier_product) {
                            \App\SupplierProduct::create([
                                'supplier_id' => $request->supplier_id,
                                'product_id' => $product->id,
                            ]);
                        }
                    } else {

                        //creating count and storing names of Inserted ones for sweetalert
                        $newProductCount++;
                        array_push($newProductNames, $row['product_name']);
                        //ends
                        //creating product from excel row if not present in DB
                        $product_insert_arr = ['name' => $row['product_name'], 'product_length' => $row['product_length_cm'], 'product_width' => $row['product_width_cm'], 'product_height' => $row['product_height_cm'], 'product_weight' => $row['product_weight_kg'], 'product_price_per_unit' => $row['fob_price_per_unit'], 'product_cbm' => $row['product_cbm'], 'supplier_id' => $request->supplier_id];


                        //add currency to arr if founded
                        $updateFobCurrency = \App\Country::where('id', $request->product_currency)->first();
                        if ($updateFobCurrency != '') {
                            $currToUpdate = $updateFobCurrency->id;
                            $product_insert_arr['product_currency'] = $currToUpdate;
                        }
                        //add port to arr if founded
                        $updatePort = \App\ShippingPort::where('id', $request->shipping_port)->first();
                        if ($updatePort != '') {
                            $portToId = $updatePort->id;
                            $product_insert_arr['shipping_port'] = $portToId;
                        }
                        //add product material to arr if founded
                        $updateProdMaterial = \App\Material::where('id', $request->product_material)->first();
                        if ($updateProdMaterial != '') {
                            $prodMaterialToId = $updateProdMaterial->id;
                            $product_insert_arr['product_material'] = $prodMaterialToId;
                        }

                        //---------------filtering out empty, false and null values from array
                        $product_insert_arr = array_filter($product_insert_arr, function($value) {
                            return ($value !== null && $value !== false && $value !== '');
                        });
                        //--------------------------ends

                        $product_data = \App\Product::create($product_insert_arr);
//                  creating relation in supplier product
                        \App\SupplierProduct::create([
                            'supplier_id' => $request->supplier_id,
                            'product_id' => $product_data->id
                        ]);
                        //product id
                        $old_or_new_product_id = $product_data->id;
//                        dd($product_data->name);
//                  creating new quote line
                        $quote_line = \App\QuoteLines::create([
                                    'quote_id' => $request->quote_id,
                                    'product_id' => $product_data->id,
                                    'product_name' => $product_data->name,
                                    'product_length' => $product_data->product_length,
                                    'product_width' => $product_data->product_width,
                                    'product_height' => $product_data->product_height,
                                    'product_weight' => $product_data->product_weight,
                                    'product_material' => $product_data->product_material,
                                    'product_cbm' => $product_data->product_cbm,
                                    'product_unit_price' => $product_data->product_price_per_unit,
                                    'product_unit_currency' => $product_data->product_currency,
                                    'shipping_port' => $product_data->shipping_port,
                                    'supplier' => $request->supplier_id,
                                    'product_quantity_per_box' => $product_data->product_quantity_per_box,
                                    'product_box_length' => $product_data->product_box_length,
                                    'product_box_width' => $product_data->product_box_width,
                                    'product_box_height' => $product_data->product_box_height,
                                    'product_box_weight' => $product_data->product_box_weight,
                                    'product_box_cbm' => $product_data->product_box_cbm,
                                    'boxes_per_stack' => $product_data->boxes_per_stack
                        ]);
                    }

                    //logic for product add/update packaging option
                    $product_packaging_rows = \App\Models\ProductPackagingOption::where('product_id', $old_or_new_product_id)->take(5)->get();
//                    dd($product_packaging_rows);
//                    $product_packaging_rows = $product_packaging_rows->toArray();
                    $countProductPackagingRows = count($product_packaging_rows->toArray());
//                    dd($countProductPackagingRows);

                    for ($i = 1; $i <= 5; $i++) {
                        $packingType = '';
                        $floorReady = '';
                        $Stackable = '';
                        $nestedProduct = '';

                        //converting packingType from string to int
                        //********--with static packaging type--*******
//                        if ($row['packaging_type_' . $i] == 'Carton Box') {
//                            $packingType = '1';
//                        } else if ($row['packaging_type_' . $i] == 'Paper Bag') {
//                            $packingType = '2';
//                        }
                        //********--with dynamic packaging type--*******
                        if (get_package_type_id_by_name($row['packaging_type_' . $i]) != 'NA') {
                            $packingType = get_package_type_id_by_name($row['packaging_type_' . $i]);
                        }
                        //ends
                        //converting floor_ready from string to int
                        if ($row['floor_ready_' . $i] == 'Yes') {
                            $floorReady = '1';
                        } else if ($row['floor_ready_' . $i] == 'No') {
                            $floorReady = '2';
                        }
                        //converting stackable from string to int
                        if ($row['stackable_' . $i] == 'Yes') {
                            $Stackable = '1';
                        } else if ($row['stackable_' . $i] == 'No') {
                            $Stackable = '2';
                        }
                        //converting is_it_nested_product from string to int
                        if ($row['is_it_nested_product_' . $i] == 'Yes') {
                            $nestedProduct = '1';
                        } else if ($row['is_it_nested_product_' . $i] == 'No') {
                            $nestedProduct = '2';
                        }

                        //checking if count less then 5 then create if not then only update    
                        if ($i <= $countProductPackagingRows) {
//                            dd('s');
                            \App\Models\ProductPackagingOption::where('id', $product_packaging_rows[$i - 1]['id'])->update([
                                'packaging_type' => $packingType,
                                'quantity_per_page' => $row['qty_per_package_' . $i],
                                'packaging_length' => $row['package_length_cm_' . $i],
                                'packaging_width' => $row['package_width_cm_' . $i],
                                'packaging_height' => $row['package_height_cm_' . $i],
                                'gross_weight_including_packaging' => $row['package_weight_kg_' . $i],
                                'packaging_cbm' => $row['packaging_cbm_' . $i],
                                'floor_ready' => $floorReady,
                                'stackable' => $Stackable,
                                'nested_product' => $nestedProduct,
                            ]);
                        } else {
                            //creating empty product packaging option
                            if ($packingType != '') {
                                \App\Models\ProductPackagingOption::create([
                                    'product_id' => $old_or_new_product_id,
                                    'packaging_type' => $packingType,
                                    'quantity_per_page' => $row['qty_per_package_' . $i],
                                    'packaging_length' => $row['package_length_cm_' . $i],
                                    'packaging_width' => $row['package_width_cm_' . $i],
                                    'packaging_height' => $row['package_height_cm_' . $i],
                                    'gross_weight_including_packaging' => $row['package_weight_kg_' . $i],
                                    'packaging_cbm' => $row['packaging_cbm_' . $i],
                                    'floor_ready' => $floorReady,
                                    'stackable' => $Stackable,
                                    'nested_product' => $nestedProduct,
                                ]);
                            }
                        }
                    }

                    //ends
//                    dd($product_data);
                    //logic for product qty per containers
                    if ($container_id1) {
                        $checkIfContainerExist = \App\ProductQuantityPerContainers::where('container_id', $container_id1->id)->where('product_id', $product_data->id)->first();
                        if ($checkIfContainerExist) {
                            \App\ProductQuantityPerContainers::where('id', $checkIfContainerExist->id)->update(['product_quantity' => $row['product_quantity_per_20ft']]);
                        } else {
                            \App\ProductQuantityPerContainers::create([
                                'container_id' => $container_id1->id,
                                'product_id' => $product_data->id,
                                'product_quantity' => $row['product_quantity_per_20ft']
                            ]);
                        }
                        \App\QuoteLines::where('id', $quote_line->id)->update([
                            'product_quantity_per_20ft' => $row['product_quantity_per_20ft']
                        ]);
                    }
                    if ($container_id2) {
                        $checkIfContainerExist = \App\ProductQuantityPerContainers::where('container_id', $container_id2->id)->where('product_id', $product_data->id)->first();
                        if ($checkIfContainerExist) {
                            \App\ProductQuantityPerContainers::where('id', $checkIfContainerExist->id)->update(['product_quantity' => $row['product_quantity_per_20fthq']]);
                        } else {
                            \App\ProductQuantityPerContainers::create([
                                'container_id' => $container_id2->id,
                                'product_id' => $product_data->id,
                                'product_quantity' => $row['product_quantity_per_20fthq']
                            ]);
                        }
                        \App\QuoteLines::where('id', $quote_line->id)->update([
                            'product_quantity_per_20fthq' => $row['product_quantity_per_20fthq']
                        ]);
                    }
                    if ($container_id3) {
                        $checkIfContainerExist = \App\ProductQuantityPerContainers::where('container_id', $container_id3->id)->where('product_id', $product_data->id)->first();
                        if ($checkIfContainerExist) {
                            \App\ProductQuantityPerContainers::where('id', $checkIfContainerExist->id)->update(['product_quantity' => $row['product_quantity_per_40ft']]);
                        } else {
                            \App\ProductQuantityPerContainers::create([
                                'container_id' => $container_id3->id,
                                'product_id' => $product_data->id,
                                'product_quantity' => $row['product_quantity_per_40ft']
                            ]);
                        }
                        \App\QuoteLines::where('id', $quote_line->id)->update([
                            'product_quantity_per_40ft' => $row['product_quantity_per_40ft']
                        ]);
                    }
                    if ($container_id4) {
                        $checkIfContainerExist = \App\ProductQuantityPerContainers::where('container_id', $container_id4->id)->where('product_id', $product_data->id)->first();
                        if ($checkIfContainerExist) {
                            \App\ProductQuantityPerContainers::where('id', $checkIfContainerExist->id)->update(['product_quantity' => $row['product_quantity_per_40fthq']]);
                        } else {
                            \App\ProductQuantityPerContainers::create([
                                'container_id' => $container_id4->id,
                                'product_id' => $product_data->id,
                                'product_quantity' => $row['product_quantity_per_40fthq']
                            ]);
                        }
                        \App\QuoteLines::where('id', $quote_line->id)->update([
                            'product_quantity_per_40fthq' => $row['product_quantity_per_40fthq']
                        ]);
                    }
                }
                
                return Response()->json(['status' => 'true', 'insert_count' => $newProductCount, 'update_count' => $updateProductCount, 'added_products' => $newProductNames, 'updated_products' => $updateProductNames, 'supplier_name'=>$supplier_name['first_name']]);
            } else {
                return Response()->json(['status' => 'false']);
            }
        }
    }

    public function export_csv(Request $request) {
//        dd($request->quote_id);
        if ($request->quote_id > 0) {
            $quote_name = Quote::where('id', $request->quote_id)->first()->name;
            $myDate = date('dmyy');
            $quote_lines = \App\QuoteLines::select('quote_lines.product_id', 'product_name', 'product_length AS product_length_cm', 'product_width AS product_width_cm', 'product_height AS product_height_cm', 'product_weight AS product_weight_kg', 'product_cbm', 'materials.name as product_material', 'product_unit_price AS fob_price_per_unit', 'product_quantity_per_20ft', 'product_quantity_per_20fthq', 'product_quantity_per_40ft', 'product_quantity_per_40fthq', 'countries.currency_code as fob_currency_per_unit', 'shipping_ports.port as port_of_origin', 'suppliers.name as supplier_name')->where('quote_id', $request->quote_id)->leftjoin('countries', 'countries.id', '=', 'quote_lines.product_unit_currency')->leftjoin('shipping_ports', 'shipping_ports.id', '=', 'quote_lines.shipping_port')->leftjoin('materials', 'materials.id', '=', 'quote_lines.product_material')->leftjoin('suppliers', 'suppliers.id', '=', 'quote_lines.supplier');
            $temp = [];
            $result = [];
            $quote_lines = $quote_lines->with('packaging_options')->get();
            foreach ($quote_lines as $quote_line):

                $temp = $quote_line;
                $i = 1;

                $totalPackagingToBeShown = 5;
                $countOfProductPackaging = count($quote_line['packaging_options']);
                $leftOverCount = $totalPackagingToBeShown - $countOfProductPackaging;

                //updating rows which are already there in packaging table
                foreach ($quote_line['packaging_options'] as $pack) {
                    //assigning packaging type to var
                    $packaging_type = '';
                    $floor_ready = '';
                    $stackable = '';
                    $nested_product = '';

                    //converting packingType from int to var
                    //********--with static packaging type--*******
//                    if ($pack->packaging_type == '1') {
//                        $packaging_type = 'Carton Box';
//                    } else if ($pack->packaging_type == '2') {
//                        $packaging_type = 'Paper Bag';
//                    }
                    //                    else{ $packaging_type = 'NA'; }
                    //********--with dynamic packaging type--*******
                    if (get_package_type_name_by_id($pack->packaging_type) != 'NA') {
                        $packaging_type = get_package_type_name_by_id($pack->packaging_type);
                    }
                    //ends
                    //assigning floor ready to var
                    if ($pack->floor_ready == '1') {
                        $floor_ready = 'Yes';
                    } else if ($pack->floor_ready == '2') {
                        $floor_ready = 'No';
                    }
                    //                    else{ $floor_ready = 'NA'; }
                    //assigning stackable to var
                    if ($pack->stackable == '1') {
                        $stackable = 'Yes';
                    } else if ($pack->stackable == '2') {
                        $stackable = 'No';
                    }
                    //                    else{ $stackable = 'NA'; }
                    //assigning nested_product to var
                    if ($pack->nested_product == '1') {
                        $nested_product = 'Yes';
                    } else if ($pack->nested_product == '2') {
                        $nested_product = 'No';
                    }
                    //                    else{ $nested_product = 'NA'; }
                    //assigning and check for null and NA
                    $temp['packaging_type_' . $i] = $packaging_type;
                    if ($pack->quantity_per_page != 'null') {
                        $temp['qty_per_package_' . $i] = $pack->quantity_per_page;
                    } else {
                        $temp['qty_per_package_' . $i] = '';
                    }
                    if ($pack->packaging_length != null) {
                        $temp['package_length_cm_' . $i] = $pack->packaging_length;
                    } else {
                        $temp['package_length_cm_' . $i] = '';
                    }
                    if ($pack->packaging_width != 'null') {
                        $temp['package_width_cm_' . $i] = $pack->packaging_width;
                    } else {
                        $temp['package_width_cm_' . $i] = '';
                    }
                    if ($pack->packaging_height != 'null') {
                        $temp['package_height_cm_' . $i] = $pack->packaging_height;
                    } else {
                        $temp['package_height_cm_' . $i] = '';
                    }
                    if ($pack->gross_weight_including_packaging != 'null') {
                        $temp['package_weight_kg_' . $i] = $pack->gross_weight_including_packaging;
                    } else {
                        $temp['package_weight_kg_' . $i] = '';
                    }
                    if ($pack->packaging_cbm != 'null') {
                        $temp['packaging_cbm_' . $i] = $pack->packaging_cbm;
                    } else {
                        $temp['packaging_cbm_' . $i] = '';
                    }
                    $temp['floor_ready_' . $i] = $floor_ready;
                    $temp['stackable_' . $i] = $stackable;
                    $temp['is_it_nested_product_' . $i] = $nested_product;

                    $i++;
                }

                //creating empty product packaging fields for export 
                for ($j = $countOfProductPackaging + 1; $j <= 5; $j++) {
                    $temp['packaging_type_' . $j] = '';
                    $temp['qty_per_package_' . $j] = '';
                    $temp['package_length_cm_' . $j] = '';
                    $temp['package_width_cm_' . $j] = '';
                    $temp['package_height_cm_' . $j] = '';
                    $temp['package_weight_kg_' . $j] = '';
                    $temp['packaging_cbm_' . $j] = '';
                    $temp['floor_ready_' . $j] = '';
                    $temp['stackable_' . $j] = '';
                    $temp['is_it_nested_product_' . $j] = '';
                }

                $result[] = $temp;
            endforeach;
//            echo json_encode($result);
//            die();
            return (new FastExcel($result))->download($quote_name . '_' . $myDate . '.csv');
        }
    }

    //create comment ajax
    public function submit_comment_ajax(Request $request) {
        $requestData = $request->all();
        $quote_id = $request->quote_id;
//        dd($requestData);
        if ($request->quote_id != null) {
            $quote = Quote::findOrFail($request->quote_id);
            $quote->update($requestData);
        } else {
            return Response()->json(['status' => 'false']);
        }
        $requestData['user_id'] = Auth::id();
        \App\Comment::create($requestData);
        $comments = \App\Comment::where('quote_id', $quote_id)->orderBy('id', 'DESC')->take(5)->get();
        return view('admin.quotes.load_comments', compact('quote_id', 'comments'));
    }

    //code for comment
    public function load_comments(Request $request) {

        $quote_id = $request->quote_id;
        $comments = \App\Comment::where('quote_id', $quote_id)->orderBy('id', 'DESC')->take(5)->get();
        return view('admin.quotes.load_comments', compact('quote_id', 'comments'));
    }

    //dropzone quote_attachment
    public function dropzone_quote_attachment(Request $request) {

//        $image = $request->file('file');
//        $imageName = time() . '_' . $image->getClientOriginalName();
//        $image->move(public_path('images/documents'), $imageName);
        $quote_id = $request->quote_id;
        $imageName = $this->uploadFile($request, 'file', '/images/documents');

        $addImage = \App\QuotesAttachment::create([
                    'quote_id' => $quote_id,
                    'original_file_name' => $imageName,
                    'file_name' => $imageName,
        ]);
        return response()->json(['success' => $imageName]);
    }

    //code for load quote attachments lines
    public function load_quote_attachments_lines(Request $request) {
        $quote_id = $request->quote_id;
        $quote = Quote::where('id', $quote_id)->first();
        $quote_attachments = \App\QuotesAttachment::where('quote_id', $quote_id)->get();
        return view('admin.quotes.load_quote_attachments', compact('quote_attachments'));
    }

    //create quote attachments ajax
    public function submit_quote_attachments_ajax(Request $request) {
        $requestData = $request->all();
//        dd($request->order_pricing);
        $quote = Quote::findOrFail($request->quote_id);
        if (isset($request->quote_attachments)) {
            foreach ($request->quote_attachments as $value):
                if (isset($value['row_id'])):
                    $quote_attachments = \App\QuotesAttachment::findOrFail($value['row_id']);
                    $quote_attachments->update($value);
                endif;
            endforeach;
        }
        return Response()->json(['status' => 'true', 'message' => 'Quote attachments updated']);
    }

//    Remove image from quote lines ajax
    public function remove_image_from_quote_lines_ajax(Request $request) {
        $quote_row_id = $request->quote_row_id;
//        dd($quote_row_id);
        $quote_line = \App\QuoteLines::where('id', $quote_row_id)->first();
        if ($quote_line) {
            $update = \App\QuoteLines::where('id', $quote_row_id)->update(['product_image' => null]);
            if ($update) {
                \App\Product::where('id', $quote_line->product_id)->update(['pic_id' => null]);
                return Response()->json(['status' => 'true', 'message' => 'Quote Line Image removed']);
            }
        }
        return Response()->json(['status' => 'false', 'message' => 'Quote Line not found']);
    }

}
