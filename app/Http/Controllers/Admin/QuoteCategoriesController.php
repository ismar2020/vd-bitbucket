<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\QuoteCategory;
use Illuminate\Http\Request;

class QuoteCategoriesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $quotecategories = QuoteCategory::where('name', 'LIKE', "%$keyword%")
                            ->latest()->paginate($perPage);
        } else {
            if (\Auth::user()->roles->first()->name == 'super_admin') {
                $quotecategories = QuoteCategory::latest()->get();
            } else if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
                $company_users = \App\User::where('company_id', \Auth::user()->company_id)->pluck('id')->toArray();
                $quotecategories = QuoteCategory::whereIn('user_id', $company_users)->latest()->get();
            } else {
                $quotecategories = QuoteCategory::where('user_id', \Auth::id())->latest()->get();
            }
        }
        return view('admin.quote-categories.index', compact('quotecategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        return view('admin.quote-categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required'
        ]);
        $requestData = $request->all();
        $requestData['user_id'] = \Auth::id();
        QuoteCategory::create($requestData);
        return redirect('admin/quote-categories')->with('flash_message', 'QuoteCategory added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id) {
        $quotecategory = QuoteCategory::findOrFail($id);
        return view('admin.quote-categories.show', compact('quotecategory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        $quotecategory = QuoteCategory::findOrFail($id);
        return view('admin.quote-categories.edit', compact('quotecategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'name' => 'required'
        ]);
        $requestData = $request->all();
        $quotecategory = QuoteCategory::findOrFail($id);
        $quotecategory->update($requestData);
        return redirect('admin/quote-categories')->with('flash_message', 'QuoteCategory updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        QuoteCategory::destroy($id);
        return redirect('admin/quote-categories')->with('flash_message', 'QuoteCategory deleted!');
    }

}
