<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\ShippingRate;
use Illuminate\Http\Request;

class ShippingRateController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $shippingrate = ShippingRate::where('shipping_port_id', 'LIKE', "%$keyword%")
                            ->orWhere('shipping_to_port_id', 'LIKE', "%$keyword%")
                            ->orWhere('shipping_rate', 'LIKE', "%$keyword%")
                            ->orWhere('shipping_rate_currency', 'LIKE', "%$keyword%")
                            ->latest()->paginate($perPage);
        } else {
            if (\Auth::user()->roles->first()->name == 'super_admin') {
                $shippingrate = ShippingRate::latest()->get();
            } else if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
                $company_users = \App\User::where('company_id', \Auth::user()->company_id)->pluck('id')->toArray();
                $shippingrate = ShippingRate::whereIn('user_id', $company_users)->latest()->get();
            } else {
                $shippingrate = ShippingRate::where('user_id', \Auth::id())->latest()->get();
            }
        }
        return view('admin.shipping-rate.index', compact('shippingrate'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        return view('admin.shipping-rate.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {
//        $this->validate($request, [
//            'shipping_rate' => 'required'
//        ]);
        $requestData = $request->all();
        $requestData['user_id'] = \Auth::id();
        ShippingRate::create($requestData);
        return redirect('admin/shipping-rate')->with('flash_message', 'ShippingRate added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id) {
        $shippingrate = ShippingRate::findOrFail($id);
        return view('admin.shipping-rate.show', compact('shippingrate'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        $shippingrate = ShippingRate::findOrFail($id);
        return view('admin.shipping-rate.edit', compact('shippingrate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id) {
//        $this->validate($request, [
//            'shipping_rate' => 'required'
//        ]);
        $requestData = $request->all();
        $shippingrate = ShippingRate::findOrFail($id);
        $shippingrate->update($requestData);
        return redirect('admin/shipping-rate')->with('flash_message', 'ShippingRate updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        ShippingRate::destroy($id);
        return redirect('admin/shipping-rate')->with('flash_message', 'ShippingRate deleted!');
    }

}
