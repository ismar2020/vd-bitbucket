<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\ProfitMargin;
use Illuminate\Http\Request;

class ProfitMarginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $profitmargin = ProfitMargin::where('type', 'LIKE', "%$keyword%")
                ->orWhere('margin', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $profitmargin = ProfitMargin::latest()->paginate($perPage);
        }
        return view('admin.profit-margin.index', compact('profitmargin'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.profit-margin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {        
        $requestData = $request->all();       
        ProfitMargin::create($requestData);
        return redirect('admin/profit-margin')->with('flash_message', 'ProfitMargin added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $profitmargin = ProfitMargin::findOrFail($id);
        return view('admin.profit-margin.show', compact('profitmargin'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $profitmargin = ProfitMargin::findOrFail($id);
        return view('admin.profit-margin.edit', compact('profitmargin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {        
        $requestData = $request->all();        
        $profitmargin = ProfitMargin::findOrFail($id);
        $profitmargin->update($requestData);
        return redirect('admin/profit-margin')->with('flash_message', 'ProfitMargin updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        ProfitMargin::destroy($id);
        return redirect('admin/profit-margin')->with('flash_message', 'ProfitMargin deleted!');
    }
}
