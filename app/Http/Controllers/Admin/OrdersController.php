<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use DB;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use DataTables;

class OrdersController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $orders = Order::where('name', 'LIKE', "%$keyword%")
                            ->orWhere('purchase_order', 'LIKE', "%$keyword%")
                            ->orWhere('shiiping_port', 'LIKE', "%$keyword%")
                            ->orWhere('product_unit_quantity', 'LIKE', "%$keyword%")
                            ->orWhere('container', 'LIKE', "%$keyword%")
                            ->orWhere('purchase_cost', 'LIKE', "%$keyword%")
                            ->orWhere('wholesale_cost', 'LIKE', "%$keyword%")
                            ->orWhere('retail_cost', 'LIKE', "%$keyword%")
                            ->orWhere('total_number_of_containers', 'LIKE', "%$keyword%")
                            ->orWhere('total_purchase_cost', 'LIKE', "%$keyword%")
                            ->orWhere('total_wholesale_cost', 'LIKE', "%$keyword%")
                            ->orWhere('total_retail_cost', 'LIKE', "%$keyword%")
                            ->orWhere('confirmed', 'LIKE', "%$keyword%")
                            ->orWhere('active', 'LIKE', "%$keyword%")
                            ->latest()->paginate($perPage);
        } else {

            if (\Auth::user()->roles->first()->name == 'super_admin') {
                $orders = Order::latest()->get();
            } else if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
                $company_users = \App\User::where('company_id', \Auth::user()->company_id)->pluck('id')->toArray();
                $orders = Order::whereIn('user_id', $company_users)->latest()->get();
            }
            else if (\Auth::user()->roles->first()->name == 'supplier' && \Auth::user()->supplier_id != null) {
//                $supplier_users = \App\User::where('supplier_id', \Auth::user()->supplier_id)->pluck('id')->toArray();
                $suppliersId = \App\User::where('supplier_id', \Auth::user()->supplier_id)->pluck('id');

                $getAssociatedProducts = \App\Product::where('supplier_id', \Auth::user()->supplier_id)->pluck('id');
                $getOrderId = \App\OrderLine::whereIn('product_id', $getAssociatedProducts)->pluck('order_id');
                $getAssociatedOrder = \App\Order::whereIn('id', $getOrderId)->get();
                $order = \App\Order::whereIn('user_id', $suppliersId)->latest()->get();
                $orders = $getAssociatedOrder->merge($order);
//                dd($orders);
                
//                dd($supplier_users);
//                $orders = Order::whereIn('user_id', $supplier_users)->latest()->get();
                
            }
        }

        return view('admin.orders.index', compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        return view('admin.orders.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {
        date_default_timezone_set('Asia/Kolkata');
        $lastInsertedOrderId = DB::table('orders')->insertGetId(array(
            'created_at' => date("Y-m-d h:i:s", time()),
            'updated_at' => date("Y-m-d h:i:s", time()),
            'name' => $request->name,
            'confirmed' => $request->confirmed,
            'active' => $request->active,
            'order_type' => $request->order_type,
            'pricing_currency' => $request->pricing_currency,
            'user_id' => \Auth::id(),
            'po_type' => $request->po_type,
            'po_number' =>$request->po_number,
        ));
//        dd($lastInsertedOrderId,$request->order_detail);
//        dd($request->all());
        if ($lastInsertedOrderId > 0 && $request->order_detail != null) {
            foreach ($request->order_detail as $value):
                $value['order_id'] = $lastInsertedOrderId;
                \App\OrderLine::create($value);
            endforeach;
        }
//        redirecting to order edit page
        $order = Order::findOrFail($lastInsertedOrderId);
        $orderDetails = \App\OrderLine::where('order_id', $lastInsertedOrderId)->get();
        $shippingPlan = \App\ShippingPlan::where('order_id', $lastInsertedOrderId)->get();
//        return view('admin.orders.edit', compact('order', 'orderDetails', 'shippingPlan'));
        return redirect('admin/orders/' . $lastInsertedOrderId . '/edit')->with(['order' => $order, 'orderDetails' => $orderDetails, 'shippingPlan' => $shippingPlan])->with('flash_message', 'Order added!');
//        return redirect('admin/orders')->with('flash_message', 'Order added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id) {
        $order = Order::findOrFail($id);

        return view('admin.orders.show', compact('order'));
    }

    public function orderLinesbyOrderId($orderid) {
        $orderlines = \App\OrderLine::where('order_id', $orderid)->get();
        $previous = 'orders';
        return view('admin.orders.orderlines', compact('orderlines', 'previous', 'orderid'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {

            $order = Order::findOrFail($id);
            $orderDetails = \App\OrderLine::where('order_id', $id)->get();
            $shippingPlan = \App\ShippingPlan::where('order_id', $id)->get();


                
       
        return view('admin.orders.edit', compact('order', 'orderDetails', 'shippingPlan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id) {
        $requestData = $request->all();
//        dd($requestData);
        $order = Order::findOrFail($id);
        $order->update($requestData);

//        if(isset($request->order_detail)){
//            foreach($request->order_detail as $value):
//                if(isset($value['product_id'])):
//                    if(isset($value['row_id'])):
//                        $orderlines = \App\OrderLine::findOrFail($value['row_id']);
//                        $orderlines->update($value);
//                    else:
//                        $value['order_id'] = $id;
//                        \App\OrderLine::create($value);
//                    endif;
//                endif;
//            endforeach;
//        }

        return redirect('admin/orders')->with('flash_message', 'Order updated!');
    }

    public function shipping_container_products(Request $request) {
        $order_id = $request->order_id;
        $shipping_plan_id = $request->shipping_plan_id;
        $shipping_container_products = \App\ShippingContainerProduct::where('order_id', $order_id)->where('shipping_plan_id', $shipping_plan_id)->get();
        //dd($shipping_container_product);
        return view('admin.orders.shipping_container_products', compact('order_id', 'shipping_plan_id', 'shipping_container_products'));
    }

    public function shipping_container_costs(Request $request) {
        $order_id = $request->order_id;
        $shipping_plan_id = $request->shipping_plan_id;
        $costs = \App\Cost::where('order_id', $order_id)->get();
        //dd($shipping_container_product);
        return view('admin.orders.shipping_container_costs', compact('order_id', 'shipping_plan_id', 'costs'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        Order::destroy($id);

        return redirect('admin/orders')->with('flash_message', 'Order deleted!');
    }

    //create order pricing ajax
    public function submit_order_pricing_ajax(Request $request) {
        $requestData = $request->all();
        
        $order = Order::findOrFail($request->order_id);
        $shippingPlanRaw = \App\ShippingPlan::where('order_id', $request->order_id)->update([
              'purchase_order_number' => $request->po_number
          ]);
//        dd($requestData);
        $order->update($requestData);
        if (isset($request->order_pricing)) {
            foreach ($request->order_pricing as $value):
                if (isset($value['product_id'])):
                    if (isset($value['row_id'])):
                        //assigning pricing currency to every orderline of this order
                        if ($request->pricing_currency != null && $request->pricing_currency != '') {
                            $value['pricing_currency'] = $request->pricing_currency;
                        }
                        //ends
                        $orderlines = \App\OrderLine::findOrFail($value['row_id']);
                        $orderlines->update($value);
                    else:
//                        $value['order_id'] = $request->order_id;
//                        \App\OrderLine::create($value);
                    endif;
                endif;
            endforeach;
        };
        return Response()->json(['status' => 'true']);
    }

    //update order status ajax
    public function update_order_status_ajax(Request $request) {
        $orderlines = \App\Order::where('id',$request->id)->update(['confirmed'=>$request->val]);
        return Response()->json(['status' => 'true']);
    }
    
    
    //create order documents ajax
    public function submit_order_documents_ajax(Request $request) {
        $requestData = $request->all();
//        dd($request->order_pricing);
        $order = Order::findOrFail($request->order_id);
        if (isset($request->order_documents)) {
            foreach ($request->order_documents as $value):
                if (isset($value['row_id'])):
                    $order_documents = \App\OrderDocument::findOrFail($value['row_id']);
                    $order_documents->update($value);
                endif;
            endforeach;
        };
        return Response()->json(['status' => 'true', 'message' => 'order documents updated']);
    }

    //create order pricing fis ajax
    public function submit_order_pricing_costs_fis_ajax(Request $request) {
        $requestData = $request->all();
        $finalData = [];

        if (isset($request->order_pricing)) {
            foreach ($request->order_pricing as $value):

                if (isset($value['row_id'])):
                    foreach ($value as $k => $v) {
                        if ($v == '') {
                            $v = '0';
                        }
                        $finalData[$k] = $v;
                    }
                    $orderlines = \App\OrderLine::findOrFail($value['row_id']);
                    $orderlines->update($finalData);
                endif;
            endforeach;
        };
        return Response()->json(['status' => 'true']);
    }

    //create relation fis relation with product
    public function insert_fis_template_product_relation(Request $request) {
        $fistemplate_id = $request->fistemplate_id;
        $product_id = $request->product_id;
//        dd($fistemplate_id,$product_id);
        \App\FisTemplateProducts::create([
            'template_id' => $fistemplate_id,
            'product_id' => $product_id,
            'user_id' => \Auth::id()
        ]);
        Response()->json(['status' => 'true']);
    }

    //code for template products
    public function load_template_products(Request $request) {

        $template_id = $request->template_id;

        $template_products_ids = \App\FisTemplateProducts::where('template_id', $template_id)->pluck('product_id');
        $template_products_details = \App\Product::whereIn('id', $template_products_ids)->get();
        return view('admin.orders.load_template_products', compact('template_products_details', 'template_id'));
    }

    //code for default products
    public function set_default_template(Request $request) {

        $template_id = $request->template_id;
        $is_default = $request->is_default;
//  if login by company user update previous this company default templates to non-default and if login by super_admin then make non default to those admin ones only
        if ($is_default == '1') {
            if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
                $company_users = \App\User::where('company_id', \Auth::user()->company_id)->pluck('id')->toArray();
                \App\FisTemplate::whereIn('user_id', $company_users)->update(array('is_default' => '0'));
            } else {
                $template = DB::table('fis_templates')->where('user_id', '1')->update(['is_default' => '0']);
            }
        }
        //update is default to 1 through template Id
        $template = DB::table('fis_templates')->where('id', $template_id)->update(['is_default' => $is_default]);
        return Response()->json(['status' => 'true']);
    }

    //create order pricing FOB ajax
    public function submit_order_pricing_costs_fob_ajax(Request $request) {
        $requestData = $request->all();
        $finalData = [];

        if (isset($request->order_pricing)) {
            foreach ($request->order_pricing as $value):
                if (isset($value['row_id'])):
                    foreach ($value as $k => $v) {
                        if ($v == '') {
                            $v = '0';
                        }

                        $finalData[$k] = $v;
                    }
                    $orderlines = \App\OrderLine::findOrFail($value['row_id']);
                    $orderlines->update($finalData);
                endif;
            endforeach;
        };
        return Response()->json(['status' => 'true']);
    }

    //create shipping plan ajax
    public function submit_shipping_plan_ajax(Request $request) {
        $requestData = $request->all();
//        dd($requestData);
        if (isset($request->shipping_plan)) {
            foreach ($request->shipping_plan as $value):
                if (isset($value['row_id'])):
                    $shipping_plan = \App\ShippingPlan::findOrFail($value['row_id']);
                    $shipping_plan->update($value);
                else:
//                    $value['order_id'] = $request->order_id;
//                    \App\ShippingPlan::create($value);
                endif;
            endforeach;
        }
        return Response()->json(['status' => 'true']);
    }

    public function copy_container(Request $request){
        $shipping_plan = \App\ShippingPlan::where('id',$request->id)->first()->toarray();
//        dd($shipping_plan);
        $shipping_container = \App\ShippingContainerProduct::where('shipping_plan_id',$request->id)->get();

//        dd($shipping_plan,$shipping_container);
        $conteiner_id = \App\ShippingPlan::create($shipping_plan)->id;
            if($shipping_container){
                foreach($shipping_container as $shipping){
                    $shipping_container_product = $shipping->toarray();
                $shipping_container_product['shipping_plan_id'] = $conteiner_id;
                \App\ShippingContainerProduct::create($shipping_container_product); 
           }
            return Response()->json(['status' => 'true']);
            // dd($shipping_plan);
        }
    }

    //create shipping container products ajax
    public function submit_shipping_container_products_ajax(Request $request) {
        $requestData = $request->all();
//        dd($requestData);
        if (isset($request->shipping_container_products)) {
            foreach ($request->shipping_container_products as $value):
                if (isset($value['row_id'])):
                    $shipping_plan = \App\ShippingContainerProduct::findOrFail($value['row_id']);
                    $shipping_plan->update($value);
                else:
//                    \App\ShippingContainerProduct::create($value);
                endif;
            endforeach;
        }

        return Response()->json(['status' => 'true']);
    }

    //delete order detail by ajax
    public function deleteOrderDetail(Request $request) {
        //dd($request->table_name,($request->row_id));
        if (isset($request->table_name) && isset($request->row_id)):
            if ($request->table_name == 'shipping_plans') {
                \App\ShippingContainerProduct::where('shipping_plan_id', $request->row_id)->delete($request->row_id);
            } else if ($request->table_name == 'quotes_attachments') {
                //deleting files from s3-bucket
                $row = \App\QuotesAttachment::where('id', $request->row_id)->first();
                if ($row) {
                    $fileName = $row->original_file_name;
                    if (isset($fileName) && !empty($fileName)) {
                        Storage::disk('s3')->delete($fileName);
                    }
                }
            } else if ($request->table_name == 'order_documents') {
                //deleting files from s3-bucket
                $row = \App\OrderDocument::where('id', $request->row_id)->first();
                if ($row) {
                    $fileName = $row->original_file_name;
                    if (isset($fileName) && !empty($fileName)) {
                        Storage::disk('s3')->delete($fileName);
                    }
                }
            } else if ($request->table_name == 'certificate_attachments') {
                //deleting files from s3-bucket
                $row = \App\CertificateAttachment::where('id', $request->row_id)->first();
                if ($row) {
                    $fileName = $row->file_name;
                    if (isset($fileName) && !empty($fileName)) {
                        Storage::disk('s3')->delete($fileName);
                    }
                }
            } else if ($request->table_name == 'order_lines') {
                //deleting order-line id form ShippingContainerProduct table (manually)
                \App\ShippingContainerProduct::where('order_line_id', $request->row_id)->delete();
            }
            DB::table($request->table_name)->delete($request->row_id);
            return Response()->json(['status' => 'Row deleted']);
        endif;
    }

    //delete template products by ajax
    public function delete_template_products(Request $request) {
        if (isset($request->table_name) && isset($request->template_id) && isset($request->product_id)):
            \App\FisTemplateProducts::where('template_id', $request->template_id)->where('product_id', $request->product_id)->delete($request->row_id);
            return Response()->json(['status' => 'Template Product deleted']);
        endif;
    }

    //get suppliers products ajax
    public function getSupplierProductsForOrders(Request $request) {
        if ($request->supplier_id != null) {
            $getProducts = \App\SupplierProduct::where('supplier_id', $request->supplier_id)->get()->pluck('product_id');
            $supplierproducts = \App\Product::whereIn('id', $getProducts)->where('deleted_at', null)->get();

            if ($supplierproducts->isEmpty() != true) {
                $supplier_html = '';
                $supplier_html = '<option value="">Select Product</option>';
                foreach ($supplierproducts as $data):
                    $supplier_html .= "<option value=" . $data->id . ">" . $data->name . "</option>";
                endforeach;
            } else {
                $supplier_html = '<option value="">No Products</option>';
            }
        } else {
//            getting company users products
            $company_users = \App\User::where('company_id', \Auth::user()->company_id)->latest()->pluck('id')->toArray();
            $company_user_products = \App\Product::whereIn('user_id', $company_users)->latest()->pluck('id')->toArray();
//            get all products which are in relation with any supplier
            $all_supplier_products = \App\SupplierProduct::pluck('product_id')->toArray();

//            getting those products which are not in relation table
            $this_company_supplier_products = array_diff($company_user_products, $all_supplier_products);

//            dd($company_user_products,$all_supplier_products,$this_company_supplier_products);

            $no_supplier_assigned_products = \App\Product::whereIn('id', $this_company_supplier_products)->latest()->get();
            $supplier_html = '';
            $supplier_html = '<option value="">Select Product</option>';
            foreach ($no_supplier_assigned_products as $data):
                $supplier_html .= "<option value=" . $data->id . ">" . $data->name . "</option>";
            endforeach;
        }

        $supplier_id = $request->supplier_id;
        return Response()->json(['status' => 'true', 'supplier_id' => $supplier_id, 'supplier_html' => $supplier_html]);
    }

    //editable ajax
//    function action(Request $request)
//    {
//    	if($request->ajax())
//    	{
//    		if($request->action == 'edit')
//    		{
//    			$data = array(
//    				'name'	=>	$request->name,
//    				'confirmed'		=>	$request->confirmed,
//    				'total_purchase_cost'		=>	$request->total_purchase_cost,
//    				'total_wholesale_cost'		=>	$request->total_wholesale_cost,
//    				'total_retail_cost'		=>	$request->total_retail_cost
//    			);
//    			DB::table('orders')
//    				->where('id', $request->id)
//    				->update($data);
//    		}
//    		if($request->action == 'delete')
//    		{
//    			DB::table('orders')
//    				->where('id', $request->id)
//    				->delete();
//    		}
//    		return response()->json($request);
//    	}
//    }
    //code for getting currrent exchange rate
    public function get_current_exchange_rate(Request $request) {
        $product_unit_currency = $request->product_unit_currency;
        $pricing_currency = $request->pricing_currency;
        $data = \App\CurrencyExchangeRate::where('currency_id_from', $product_unit_currency)->where('currency_id_to', $pricing_currency)->first();
//        dd($product_unit_currency,$pricing_currency);
        if ($data != '') {
            return Response()->json(['status' => 'true', 'data' => $data]);
        } else {
            return Response()->json(['status' => 'false']);
        }
    }

    //code for products_units_in_the_container
    public function products_units_in_the_container(Request $request) {
        $container_id = $request->container_size;
        $product_id = $request->product_id;
//        dd($container_id,$product_id);
        $data = \App\ProductQuantityPerContainers::where('container_id', $container_id)->where('product_id', $product_id)->first();
        if ($data != '') {
            return Response()->json(['status' => 'true', 'data' => $data]);
        } else {
            return Response()->json(['status' => 'false']);
        }
    }

    //code for refresh_shipping_rate
    public function refresh_shipping_rate(Request $request) {
        $container_id = $request->container_size;
        $product_id = $request->product_id;
        $order_line_id = $request->order_line_id;
        $port_of_dispatch = $request->port_of_dispatch;

        //getting port_of_origin from product table
        $product_detail = \App\Product::where('id', $product_id)->first();
        $port_of_origin = null;
        if ($product_detail) {
            $port_of_origin = $product_detail->shipping_port;
        }

        //getting container_name from container ID
        $container = \App\Container::where('id', $container_id)->first();
        // strip out all whitespace
        $shipping_rate_field = null;
        if ($container != '') {
            $name = $container->name;
            $container_name = preg_replace('/\s*/', '', $name);
            // convert the string to all lowercase
            $container_name = strtolower($container_name);
            $shipping_rate_field = 'shipping_rate_' . $container_name;
        }

        //getting shipping rate from shipping rate table
        if (\Auth::user()->roles->first()->name == 'super_admin') {
            $shipping_rate = \App\ShippingRate::where('shipping_port_id', $port_of_origin)->where('shipping_to_port_id', $port_of_dispatch);
        } else if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
            $company_users = \App\User::where('company_id', \Auth::user()->company_id)->pluck('id')->toArray();
            $shipping_rate = \App\ShippingRate::whereIn('user_id', $company_users)->where('shipping_port_id', $port_of_origin)->where('shipping_to_port_id', $port_of_dispatch);
        } else if (\Auth::user()->roles->first()->name == 'supplier') {
            $supplier_users = \App\User::where('company_id', \Auth::user()->company_id)->where('supplier_id', '!=', null)->latest()->pluck('supplier_id')->toArray();
            $shipping_rate = \App\ShippingRate::whereIn('user_id', $supplier_users)->where('shipping_port_id', $port_of_origin)->where('shipping_to_port_id', $port_of_dispatch);
        }
        $shipping_rate = ($shipping_rate->get()->isEmpty() == true) ? null : $shipping_rate->first()->$shipping_rate_field;
//        dd($product_id,$port_of_origin,$port_of_dispatch,$shipping_rate);
        //updating shipping rate in orderline table through orderLineId
        $res = \App\OrderLine::where('id', $order_line_id)->update(['shipping_rate' => $shipping_rate]);
        if ($res == 1) {
            return Response()->json(['status' => 'true']);
        } else {
            return Response()->json(['status' => 'false']);
        }
    }

    public function get_order_line_details_for_cal(Request $request) {
        $order_line_id = $request->ship_order_line_id;
//        dd($order_line_id);
        $data = \App\OrderLine::where('id', $order_line_id)->first();
        if ($data != '') {
            return Response()->json(['status' => 'true', 'data' => $data]);
        } else {
            return Response()->json(['status' => 'false']);
        }
    }

    //code for load order pricing costs fob
    public function load_order_pricing_costs_fob(Request $request) {
        $order_id = $request->order_id;

        $orderDetails = \App\OrderLine::where('order_id', $order_id)->get();
        return view('admin.orders.order_pricing_costs_fob', compact('orderDetails'));
    }

    //code for load order pricing costs fis
    public function load_order_pricing_costs_fis($order_id, Request $request) {
        //$order_id = $request->order_id;
        $orderDetails = \App\OrderLine::where('order_id', $order_id)->with(['template'])->get();
//                dd($orderDetails->toArray());
        return view('admin.orders.order_pricing_costs_fis', compact('orderDetails', 'order_id'));
    }

    //code for load order pricing costs fiw
    public function load_order_pricing_costs_fiw($order_id, Request $request) {
        // $order_id = $request->order_id;
        $orderDetails = \App\OrderLine::where('order_id', $order_id)->with(['template'])->get();
        //        dd($orderDetails->toArray());
        return view('admin.orders.order_pricing_costs_fiw', compact('orderDetails', 'order_id'));
    }

    //code for order lines
    public function load_order_lines(Request $request) {
        $order_id = $request->order_id;
        $order = Order::where('id', $order_id)->first();
        $orderDetails = \App\OrderLine::where('order_id', $order_id)->get();

        //getting pricing currency from order table
        $res = Order::where('id', $order_id)->first()->pricing_currency;

        //Null check
        if ($res != null && $res != '') {
            $pricing_currency = $res;
        } else {
            $pricing_currency = null;
        }

//for updating Gross Profit Value and price To Customer as per change in FOB/FIS/FIW dropdown
        foreach ($orderDetails as $data):

            if ($order->order_type == '1'): //for FOB
                $total_costs_fob = $data->order_pricing_costs_total_fob;
                $packing_quantity = $data->packing_quantity;
                $fob_price = $data->product_unit_price;
                $gp_percentage = $data->wholesale_margin_percentage;
                $unit_of_measure = $data->order_quantity;
                $gp_value = $data->gross_profit_value;

//************getting Gross profit value onchange order type in FOB case************
                if ($pricing_currency != null && $fob_price != null && $unit_of_measure != null && $unit_of_measure != 0 && $fob_price != 0):
                    //fob price convert to pricing currency
                    $exchange_rate = \App\CurrencyExchangeRate::where('currency_id_from', $data->product_unit_currency)->where('currency_id_to', $pricing_currency)->first()->exchange_rate;
                    $converted_fob_price = $exchange_rate * $fob_price;
                    //ends
                    //calculate gp value for FOB order type
                    $gross_profit_value = (($converted_fob_price * $gp_percentage) / 100) * $unit_of_measure;
                //ends
//                    $res = DB::table('order_lines')->where('id', $data->id)->update(['gross_profit_value' => $gross_profit_value]);
                else:
//                    $res = DB::table('order_lines')->where('id', $data->id)->update(['gross_profit_value' => null]);
                endif;

//****getting Price to Customer(wholesale_cost) onchange order type in FOB case******
                if ($pricing_currency != null && $fob_price != null && $gp_value != null && $unit_of_measure != null && $unit_of_measure != 0 && $fob_price != 0):
                    //fob price convert to pricing currency
                    $exchange_rate = \App\CurrencyExchangeRate::where('currency_id_from', $data->product_unit_currency)->where('currency_id_to', $pricing_currency);

                    $converted_fob_price = 0;
                    if ($exchange_rate->get()->isEmpty() != 'true') {
                        $converted_fob_price = $exchange_rate->first()->exchange_rate * $fob_price;
//                    dd($pricing_currency,$data->product_unit_currency,$exchange_rate->first());
                    }
//                dd($pricing_currency,$data->product_unit_currency);
                    //ends
                    //calculate Price to Customer(wholesale_cost) for FOB order type
                    $price_to_customer = ($gp_value + $converted_fob_price) * $unit_of_measure;
                //ends
//                    $res = DB::table('order_lines')->where('id', $data->id)->update(['wholesale_cost' => $price_to_customer]);
                else:
//                    $res = DB::table('order_lines')->where('id', $data->id)->update(['wholesale_cost' => null]);
                endif;
//************************************ends *****************************************
//            elseif ($order->order_type == '2'): //for FIS

            elseif ($order->order_type == '2' || $order->order_type == '3'): //for FIS & FIW

                $total_costs_fis = $data->total_landed_costs;
                $landed_product_price_db = $data->landed_product_price;
                $packing_quantity = $data->packing_quantity;
                $gp_percentage = $data->wholesale_margin_percentage;
                $gp_value = $data->gross_profit_value;
                $unit_of_measure = $data->order_quantity;

//********************getting landed product price for FIS and FIW*******
                $fob = $data->product_unit_price;
                $packing_quantity = $data->packing_quantity;
                //fob price convert to pricing currency
//                dd($data->product_unit_currency,$pricing_currency);
                $exchange_rate = \App\CurrencyExchangeRate::where('currency_id_from', $data->product_unit_currency)->where('currency_id_to', $pricing_currency);
//                dd($exchange_rate->first()->toArray());
                if ($exchange_rate->get()->isEmpty() != 'true') {
                    $converted_fob_price = $exchange_rate->first()->exchange_rate * $fob;
//                    dd($pricing_currency,$data->product_unit_currency,$exchange_rate->first());
                }
//                dd($pricing_currency,$data->product_unit_currency);
                //calculating

                $landed_product_price = null;
                if ($total_costs_fis != null && $packing_quantity != null && $converted_fob_price != null && $total_costs_fis != 0 && $packing_quantity != 0):
                    $landed_product_price = $converted_fob_price + ($total_costs_fis / $packing_quantity);
//                    $template = DB::table('order_lines')->where('id', $data->id)->update(['landed_product_price' => $landed_product_price]);
                else:
//                    $template = DB::table('order_lines')->where('id', $data->id)->update(['landed_product_price' => $landed_product_price]);
                endif;
//**********getting Gross profit value onchange order type in FIS case********
                if ($landed_product_price != null && $unit_of_measure != null && $unit_of_measure != 0):
                    //calculate gp value for FIW order type
                    $gross_profit_value = (($landed_product_price * $gp_percentage) / 100) * $unit_of_measure;
                //ends
//                    $res = DB::table('order_lines')->where('id', $data->id)->update(['gross_profit_value' => $gross_profit_value]);
                else:
//                    $res = DB::table('order_lines')->where('id', $data->id)->update(['gross_profit_value' => null]);
                endif;

//*****getting Price to Customer(wholesale_cost) onchange order type in FIS case***
                if ($landed_product_price != null && $unit_of_measure != null && $gp_value != null && $unit_of_measure != 0):
                    //calculate Price to Customer(wholesale_cost) for FIW order type
                    $price_to_customer = ($gp_value + $landed_product_price) * $unit_of_measure;
                //ends
//                    $res = DB::table('order_lines')->where('id', $data->id)->update(['wholesale_cost' => $price_to_customer]);
                else:
//                    $res = DB::table('order_lines')->where('id', $data->id)->update(['wholesale_cost' => null]);
                endif;
//************************************ends *********************************
            endif;
        endforeach;
//        ends

        if(\Auth::user()->roles->first()->name == 'supplier' && \Auth::user()->supplier_id != null){
            $suppliersId = \App\User::where('supplier_id', \Auth::user()->supplier_id)->pluck('id');  
            $orderDetail = \App\Product::where('supplier_id', \Auth::user()->supplier_id)->pluck('id');
           
                $orderDetails = \App\OrderLine::where('order_id',$order_id)->whereIn('product_id', $orderDetail)->get();
                
        }else{
            $orderDetails = \App\OrderLine::where('order_id', $order_id)->get();
        }
        return view('admin.orders.order_lines', compact('order_id', 'orderDetails'));
    }

    //code for load order documents lines
    public function load_order_documents_lines(Request $request) {
        $order_id = $request->order_id;
        $order = Order::where('id', $order_id)->first();
        $order_documents = \App\OrderDocument::where('order_id', $order_id)->get();
        return view('admin.orders.load_order_documents', compact('order_documents'));
    }

    //code for load order documents lines
    public function load_shipping_totals(Request $request) {
        $order_id = $request->orderId;
//        dd($order_id);
        $order = Order::where('id', $order_id)->first();
        return view('admin.orders.load_shipping_totals', compact('order', 'order_id'));
    }

    //code for load order overview totals
    public function load_order_overview_totals(Request $request) {
//        $order_id = $request->orderId;
//        dd($order_id);
        $order = Order::get();
        return view('admin.orders.load_order_overview_totals');
    }

    //dropzone image upload
    public function dropzone_store(Request $request) {

//        $image = $request->file('file');
//        $imageName = time() . '_' . $image->getClientOriginalName();
//        $image->move(public_path('images/documents'), $imageName);
        $imageName = $this->uploadFile($request, 'file', '/images/documents');
        $order_id = $request->order_id;
        $addImage = \App\OrderDocument::create([
                    'order_id' => $order_id,
                    'original_file_name' => $imageName,
                    'file_name' => $imageName,
        ]);
        return response()->json(['success' => $imageName]);
    }

    //code for order lines
    public function update_order_type(Request $request) {
        $order_id = $request->order_id;
        $order_type = $request->order_type;
        $update = DB::table('orders')->where('id', $order_id)->update(['order_type' => $order_type]);

        Response()->json(['status' => 'true']);
    }

    public function insert_order_lines(Request $request) {
        $order_id = $request->order_id;
        $product_id = $request->order_product_line_id;
        $product = \App\Product::where('id', $product_id)->first();
        $product_currency = $product->product_currency;

        //getting pricing currency from order table
        $res = Order::where('id', $order_id)->first()->pricing_currency;

        //assigning pricing currency to newly creating orderline of this order
        if ($res != null && $res != '') {
            $pricing_currency = $res;
            //calculating exchange rate if pricing currency is not null
            $data = \App\CurrencyExchangeRate::where('currency_id_from', $product_currency)->where('currency_id_to', $pricing_currency)->first();
            if ($data != null) {
                $exchange_rate = $data->exchange_rate;
            } else {
                $exchange_rate = null;
            }
        } else {
            $pricing_currency = null;
            $exchange_rate = null;
        }



//        getting FIS
//        if (Order::where('id', $order_id)->first()->order_type == 'FIS/FIW') {
//            $fis_template = \App\FisTemplateProducts::where('product_id', $product_id)->first();
//            if ($fis_template) {
//                $purchase_price = $product->product_price_per_unit * $product->product_units_in_container ;
//            } else {
//                if (\App\FisTemplate::where('is_default', '1')->first()) {
//
//                }
//            }
//        }
//ends
//        inserting order line
        $order_line = \App\OrderLine::create([
                    'order_id' => $order_id,
                    'product_id' => $product_id,
                    'currency_exchange_rate' => $exchange_rate,
                    'pricing_currency' => $pricing_currency
        ]);
        Response()->json(['status' => 'true']);
    }

    //code for shipping plans
    public function load_shipping_plans(Request $request) {
        $order_id = $request->order_id;
        $shippingPlanRaw = \App\ShippingPlan::where('order_id', $order_id)->get();
        //calculating total cost, buy price and profit of container products
        foreach ($shippingPlanRaw as $data) {
            $total_cost_container_products = DB::table('shipping_container_products')
                    ->where('shipping_plan_id', $data->id)
                    ->sum('total_cost');
            $customer_buy_price = DB::table('shipping_container_products')
                    ->where('shipping_plan_id', $data->id)
                    ->sum('customer_buy_price');
            $profit = DB::table('shipping_container_products')
                    ->where('shipping_plan_id', $data->id)
                    ->sum('profit');
            //updating in shipping plan
            \App\ShippingPlan::where('id', $data->id)->update([
                'total_cost' => $total_cost_container_products,
                'customer_buy_price' => $customer_buy_price,
                'profit' => $profit
            ]);
        }
        $shippingPlan = \App\ShippingPlan::where('order_id', $order_id)->orderBy('id', 'desc')->get();
        

        return view('admin.orders.shippingplans', compact('shippingPlan', 'order_id'));
    }

    //code for tracking order
    public function load_tracking_order(Request $request) {
        $order_id = $request->order_id;

        $shippingPlanDetails = \App\ShippingPlan::where('order_id', $order_id)->with('shipping_container_products')->get();
//        dd($shippingPlanDetails->toArray());
        //calculating total cost, buy price and profit of container products
        foreach ($shippingPlanDetails as $data) {
            $total_cost_container_products = DB::table('shipping_container_products')
                    ->where('shipping_plan_id', $data->id)
                    ->sum('total_cost');
            $customer_buy_price = DB::table('shipping_container_products')
                    ->where('shipping_plan_id', $data->id)
                    ->sum('customer_buy_price');
            $profit = DB::table('shipping_container_products')
                    ->where('shipping_plan_id', $data->id)
                    ->sum('profit');
            //updating in shipping plan
            \App\ShippingPlan::where('id', $data->id)->update([
                'total_cost' => $total_cost_container_products,
                'customer_buy_price' => $customer_buy_price,
                'profit' => $profit
            ]);
        }
        $shippingPlan = \App\ShippingPlan::where('order_id', $order_id)->get();

        return view('admin.orders.load_tracking_order', compact('shippingPlanDetails', 'order_id'));
    }

    public function insert_shipping_plans(Request $request) {
        $order_id = $request->order_id;
        \App\ShippingPlan::create([
            'order_id' => $order_id,
            'user_id'  => \Auth::id()
        ]);
        Response()->json(['status' => 'true']);
    }

    //code for shipping_container_products
    public function load_shipping_container_products(Request $request) {
        $order_id = $request->order_id;
        $shipping_plan_id = $request->shipping_plan_id;
//        dd($order_id,$shipping_plan_id);
        $shipping_container_products = \App\ShippingContainerProduct::where('order_id', $order_id)->where('shipping_plan_id', $shipping_plan_id)->get();
//        dd($shipping_container_products);
        return view('admin.orders.load_shipping_container_products', compact('shipping_container_products', 'order_id', 'shipping_plan_id'));
    }

    //code for shipping_container_products for tracking
    public function load_shipping_container_products_for_tracking(Request $request) {

        $shipping_plan_id = $request->shipping_plan_id;
//        dd($shipping_plan_id);
        $shipping_container_products = \App\ShippingContainerProduct::where('shipping_plan_id', $shipping_plan_id)->get();
//        dd($shipping_container_products);
        return view('admin.orders.load_shipping_container_products_for_tracking', compact('shipping_container_products', 'shipping_plan_id'));
    }

    public function insert_shipping_container_products(Request $request) {
        $order_line_id = $request->order_line_id;
        $order_id = $request->order_id;
        $shipping_plan_id = $request->shipping_plan_id;
        $product_id = \App\OrderLine::where('id', $order_line_id)->first()->product_id;
        $product = \App\Product::where('id', $product_id)->first();
//        dd($order_line_id);
        //getting container ID for product qty per container
        $containerId = null;
        if (\App\ShippingPlan::where('id', $shipping_plan_id)->first()) {
            $containerId = \App\ShippingPlan::where('id', $shipping_plan_id)->first()->container_id;
        }
        //getting product qty per containers as now we have containerID and productID
        $product_qty_as_per_containerId = null;
        if (\App\ProductQuantityPerContainers::where('container_id', $containerId)->where('product_id', $product_id)->first()) {
            $product_qty_as_per_containerId = \App\ProductQuantityPerContainers::where('container_id', $containerId)->where('product_id', $product_id)->first()->product_quantity;
        }

        //checking if product has packaging type
        $packaging_options = \App\Models\ProductPackagingOption::where('product_id', $product->id)->whereNotNull('packaging_type')->where('packaging_type', '!=', '')->get();
        if ($packaging_options->isEmpty()) {
            \App\ShippingContainerProduct::create([
                'order_id' => $order_id,
                'order_line_id' => $order_line_id,
                'shipping_plan_id' => $shipping_plan_id,
                'product_name' => $product->name,
                'product_image' => $product->pic_id,
                'product_id' => $product->id,
                'supplier_id' => $product->supplier_id,
                'quantity' => $product_qty_as_per_containerId,
                'confirmed_quantity' => $product_qty_as_per_containerId
            ]);
            return Response()->json(['status' => 'true', 'packaging' => 'no']);
        } else {
            $product_qty_per_containers = \App\ProductQuantityPerContainers::where('product_id', $product->id)->get();

            return view('admin.orders.load_shipping_product_packaging_option', compact('packaging_options', 'product_qty_per_containers', 'order_line_id'));
        }
    }

    public function insert_shipping_container_products_with_using(Request $request) {
        $packaging_id = $request->packaging_id;
        $using = $request->using;
//        $quantity = $request->quantity;
        $product_id = $request->product_id;
        $order_id = $request->order_id;
        $order_line_id = $request->order_line_id;
        $shipping_plan_id = $request->shipping_plan_id;

        $product = \App\Product::where('id', $product_id)->first();

        //getting container ID for product qty per container
        $containerId = \App\ShippingPlan::where('id', $shipping_plan_id)->first()->container_id;
        //getting product qty per containers as now we have containerID and productID
        $product_qty_as_per_containerId = \App\ProductQuantityPerContainers::where('container_id', $containerId)->where('product_id', $product_id)->first()->product_quantity;

        \App\ShippingContainerProduct::create([
            'order_id' => $order_id,
            'order_line_id' => $order_line_id,
            'shipping_plan_id' => $shipping_plan_id,
            'product_name' => $product->name,
            'product_image' => $product->pic_id,
            'product_id' => $product_id,
            'supplier_id' => $product->supplier_id,
            'using' => $using,
            'quantity' => $product_qty_as_per_containerId,
            'confirmed_quantity' => $product_qty_as_per_containerId,
            'packaging_id' => $packaging_id
        ]);
        return Response()->json(['status' => 'true']);
    }

    //code for shipping_container_costs
    public function load_shipping_container_costs(Request $request) {
        $order_id = $request->order_id;
        $shipping_plan_id = $request->shipping_plan_id;
//        dd($order_id,$shipping_plan_id);
        $shipping_container_products = \App\cost::where('order_id', $order_id)->where('shipping_plan_id', $shipping_plan_id)->get();
//        dd($shipping_container_products);
        return view('admin.orders.load_shipping_container_products', compact('shipping_container_products', 'order_id', 'shipping_plan_id'));
    }

    public function get_shipping_packaging_option_view(Request $request) {
        $packaging_id = $request->packaging_id;
        $packaging_option = \App\Models\ProductPackagingOption::where('id', $packaging_id)->get();
//        dd($shipping_container_products);
        return view('admin.orders.load_particular_shipping_product_packaging_option', compact('packaging_option'));
    }

    //code for box_dimension_details
    public function load_dimension_details(Request $request) {
        $product_id = $request->product_id;
        $container_id = $request->container_id;
        $dimension_name = $request->dimension_name;
        $product = \App\Product::where('id', $product_id)->first();

        if ($dimension_name == 'box') {
            //checking if box dimesnions empty or not
            if ($product->product_quantity_per_box > 0 && $product->product_box_length > 0 && $product->product_box_width > 0 && $product->product_box_height > 0 && $product->product_box_weight > 0) {
                $container = \App\Container::where('id', $container_id)->first();
                return Response()->json(['status' => 'true', 'product' => $product, 'container' => $container]);
            } else {
                return Response()->json(['status' => 'false', 'message' => 'No Data Available']);
            }
        } else if ($dimension_name == 'stack') {
            //checking if stack dimesnions empty or not
            if ($product->units_per_stack > 0 && $product->stack_length > 0 && $product->stack_width > 0 && $product->stack_height > 0 && $product->stack_weight > 0) {
                $container = \App\Container::where('id', $container_id)->first();
                return Response()->json(['status' => 'true', 'product' => $product, 'container' => $container]);
            } else {
                return Response()->json(['status' => 'false', 'message' => 'No Data Available']);
            }
        } else if ($dimension_name == 'product') {
            //checking if product dimesnions empty or not
            if ($product->product_length > 0 && $product->product_width > 0 && $product->product_height > 0 && $product->product_weight > 0) {
                $container = \App\Container::where('id', $container_id)->first();
                return Response()->json(['status' => 'true', 'product' => $product, 'container' => $container]);
            } else {
                return Response()->json(['status' => 'false', 'message' => 'No Data Available']);
            }
        } else if ($dimension_name == 'stack_opt') {
            //checking if stack dimesnions empty or not
            if ($product->units_per_stack > 0 && $product->stack_length > 0 && $product->stack_width > 0 && $product->stack_height > 0 && $product->stack_weight > 0) {
                $container = \App\Container::where('id', $container_id)->first();
                return Response()->json(['status' => 'true', 'product' => $product, 'container' => $container]);
            } else {
                return Response()->json(['status' => 'false', 'message' => 'No Data Available']);
            }
        } else if ($dimension_name == 'product_opt') {
            //checking if product dimesnions empty or not
            if ($product->product_length > 0 && $product->product_width > 0 && $product->product_height > 0 && $product->product_weight > 0) {
                $container = \App\Container::where('id', $container_id)->first();
                return Response()->json(['status' => 'true', 'product' => $product, 'container' => $container]);
            } else {
                return Response()->json(['status' => 'false', 'message' => 'No Data Available']);
            }
        } else if ($dimension_name == 'box_opt') {
            //checking if box dimesnions empty or not
            if ($product->product_quantity_per_box > 0 && $product->product_box_length > 0 && $product->product_box_width > 0 && $product->product_box_height > 0 && $product->product_box_weight > 0) {
                $container = \App\Container::where('id', $container_id)->first();
                return Response()->json(['status' => 'true', 'product' => $product, 'container' => $container]);
            } else {
                return Response()->json(['status' => 'false', 'message' => 'No Data Available']);
            }
        } else if ($dimension_name == 'box_quantity') {

            $product_box_quantity = \App\ProductQuantityPerContainers::where('product_id', $product_id)->where('container_id', $container_id)->first();
            //checking if product dimesnions empty or not
            if (($product_box_quantity != null && $product_box_quantity->box_quantity > 0) && $product->product_quantity_per_box > 0) {
                $box_quantity = $product_box_quantity->box_quantity;
                $container = \App\Container::where('id', $container_id)->first();
                return Response()->json(['status' => 'true', 'product' => $product, 'container' => $container, 'box_quantity' => $box_quantity]);
            } else {

                return Response()->json(['status' => 'false', 'message' => 'No Data Available']);
            }
        } else if ($dimension_name == 'stack_quantity') {
            $product_stack_quantity = \App\ProductQuantityPerContainers::where('product_id', $product_id)->where('container_id', $container_id)->first();
            //checking if product dimesnions empty or not
            if (($product_stack_quantity != null && $product_stack_quantity->stack_quantity > 0) && $product->units_per_stack > 0) {
                $stack_quantity = $product_stack_quantity->stack_quantity;
                $container = \App\Container::where('id', $container_id)->first();
                return Response()->json(['status' => 'true', 'product' => $product, 'container' => $container, 'stack_quantity' => $stack_quantity]);
            } else {
                return Response()->json(['status' => 'false', 'message' => 'No Data Available']);
            }
        } else if ($dimension_name == 'product_quantity') {
            $product_product_quantity = \App\ProductQuantityPerContainers::where('product_id', $product_id)->where('container_id', $container_id)->first();
            //checking if product dimesnions empty or not
            if ($product_product_quantity != null && $product_product_quantity->product_quantity > 0) {
                $product_quantity = $product_product_quantity->product_quantity;
                $container = \App\Container::where('id', $container_id)->first();
                return Response()->json(['status' => 'true', 'product' => $product, 'container' => $container, 'product_quantity' => $product_quantity]);
            } else {
                return Response()->json(['status' => 'false', 'message' => 'No Data Available']);
            }
        }
    }

    //code for convert current to base currency
    public function convert_current_to_base_currency(Request $request) {
        $base_currency_id = $request->base;
        $current_currency_id = $request->current;
        $current_value = $request->value;
//        dd($base_currency_id,$current_currency_id,$current_value);
        $data = \App\CurrencyExchangeRate::where('currency_id_from', $current_currency_id)->where('currency_id_to', $base_currency_id)->first();

        if ($data != null) {
            $value = $data->exchange_rate * $current_value;
//            dd($value);
            return Response()->json(['status' => 'true', 'converted_value' => $value]);
        } else {
            return Response()->json(['status' => 'false']);
        }
    }

    public function getAllCurrencyExhange() {
        $getAllCurrencyExhange = \App\CurrencyExchangeRate::get();
        return Response()->json(['getAllCurrencyExhange' => $getAllCurrencyExhange]);
    }

    //    Remove image from given table or make it nullable
    public function remove_image_from_table(Request $request) {
        $rowId = $request->rowId;
        $columnName = $request->columnName;
        $tableName = $request->tableName;
//        dd($rowId, $columnName, $tableName);

        if ($rowId != '' && $columnName != '' && $tableName != '') {
            $res = DB::table($tableName)->where('id', $rowId)->update([$columnName => null]);
            return Response()->json(['status' => 'true', 'message' => 'Image removed']);
//            if ($update) {
//                \App\Product::where('id', $quote_line->product_id)->update(['pic_id' => null]);
//                return Response()->json(['status' => 'true', 'message' => 'Image removed']);
//            }
        }
        return Response()->json(['status' => 'false', 'message' => 'Data is not fully provided']);
    }

    protected $__rulesfororderoverview = ['id' => 'required', 'order_name' => 'required', 'order_status' => 'required', 'shipping_products' => 'required', 'container_name' => 'required', 'purchase_order_number' => 'required', 'bl_number' => 'required', 'description' => 'required', 'port_of_discharge' => 'required', 'cargo_ready_date' => 'required', 'dispatched_date' => 'required', 'eta_date' => 'required','fumigated' => 'required', 'total_cost' => 'required', 'customer_buy_price' => 'required', 'profit' => 'required', 'status' => 'required', 'invoiced_to_customer' => 'required','invoice_name' => 'required', 'invoice_amount' => 'required', 'deposit_paid' => 'required', 'payment_received' => 'required', 'payment_date' => 'required', 'payment_id' => 'required','comments' => 'required'];

    public function load_order_overview(Request $request) {
        $shippingPlanDetails = \App\ShippingPlan::with('shipping_container_products')->join('orders', 'shipping_plans.order_id', '=', 'orders.id')->join('shipping_container_products', 'shipping_plans.id', '=', 'shipping_container_products.shipping_plan_id')->leftjoin('order_statuses', 'orders.confirmed', '=', 'order_statuses.id')->leftjoin('containers', 'shipping_plans.container_id', '=', 'containers.id')->leftjoin('shipping_ports', 'shipping_plans.port_of_discharge', '=', 'shipping_ports.id')->leftjoin('shipping_statuses', 'shipping_plans.status', '=', 'shipping_statuses.id')->select('shipping_plans.*', 'orders.name as order_name', 'order_statuses.name as order_status', 'containers.name as container_name', 'shipping_ports.port as port_of_discharge', 'shipping_statuses.name as status', 'shipping_container_products.product_name as product_name_shiv');
//           echo json_encode($shippingPlanDetails->get()->toArray()); die();
        //calculating total cost, buy price and profit of container products
        foreach ($shippingPlanDetails as $data) {
            $total_cost_container_products = DB::table('shipping_container_products')
                    ->where('shipping_plan_id', $data->id)
                    ->sum('total_cost');
            $customer_buy_price = DB::table('shipping_container_products')
                    ->where('shipping_plan_id', $data->id)
                    ->sum('customer_buy_price');
            $profit = DB::table('shipping_container_products')
                    ->where('shipping_plan_id', $data->id)
                    ->sum('profit');
            //updating in shipping plan
            \App\ShippingPlan::where('id', $data->id)->update([
                'total_cost' => $total_cost_container_products,
                'customer_buy_price' => $customer_buy_price,
                'profit' => $profit
            ]);
        }
        $shippingPlan = \App\ShippingPlan::get();
        
        //>>>>>>Implementing Datatable
        if ($request->ajax()) {
            return Datatables::of($shippingPlanDetails)
                            ->addIndexColumn()
                            ->addColumn('+', function ($item) {
                                $return = '';
                                if (count($item->shipping_container_products) > 0) {
                                    $return .= " <a href='javascript:void(0)' title='View Products' class='btn btn-info btn-sm details-control'  style='border-radius: 50%;'><i class='fa fa-plus' aria-hidden='true'></i></a>";
                                }
                                return $return;
                            })
                            ->addColumn('order_name', function ($item) {

                                return '<p style="color: #000; white-space: break-spaces;width: 300px;">' . $item->order_name . '</p>';
                            })
                            ->addColumn('order_status', function ($item) {
                                return $item->order_status;
                            })
                            ->addColumn('container_name', function ($item) {
                                return $item->container_name;
                            })
                            ->addColumn('port_of_discharge', function ($item) {
                                return $item->port_of_discharge;
                            })
                            ->addColumn('status', function ($item) {
                                return $item->status;
                            })
                            ->editColumn('cargo_ready_date', function ($item) {
                                if (false != $item->cargo_ready_date) {
                                    $res = date_format(new \DateTime($item->cargo_ready_date), "d-M-Y");
                                    return $res;
                                }
                            })
                            ->editColumn('dispatched_date', function ($item) {
                                if (false != $item->dispatched_date) {
                                    $res = date_format(new \DateTime($item->dispatched_date), "d-M-Y");
                                    return $res;
                                }
                            })
                            ->editColumn('eta_date', function ($item) {
                                if (false != $item->eta_date) {
                                    $res = date_format(new \DateTime($item->eta_date), "d-M-Y");
                                    return $res;
                                }
                            })
                            ->editColumn('invoiced_to_customer', function ($item) {
                                if (false != $item->invoiced_to_customer) {
                                    $res = date_format(new \DateTime($item->invoiced_to_customer), "d-M-Y");
                                    return $res;
                                }
                            })
                            ->editColumn('payment_date', function ($item) {
                                if (false != $item->payment_date) {
                                    $res = date_format(new \DateTime($item->payment_date), "d-M-Y");
                                    return $res;
                                }
                            })
                            ->editColumn('description', function ($item) {
                                return ($item->description != '') ? '<p style="color: #000; white-space: break-spaces;width: 250px;">' . $item->description : 'N/A';
                            })
                            ->addColumn('shipping_products', function ($item) {
                                //this is coming from shipping plan model append
                                return $item->shipping_products;
//                    return get_container_name_by_id($item->container_id);
                            })
                            ->addColumn('shipping_products_suppliers_name', function ($item) {
                                //this is coming from shipping plan model append
                                return $item->shipping_products_suppliers_name;
                            })
                            ->rawColumns(['+', 'order_name', 'shipping_products', 'shipping_products_suppliers_name', 'description'])
                            ->make(true);
        }
        //end
        return view('admin.orders.load_order_overview', ['rules' => array_keys($this->__rulesfororderoverview)]);
    }

    //code for load order_lines for order overview
    public function load_order_lines_for_order_overview(Request $request) {
        $order_id = $request->order_id;
        $order_lines = \App\OrderLine::where('order_id', $order_id)->get();
        return view('admin.orders.load_order_lines_for_order_overview', compact('order_lines', 'order_id'));
    }

    //code for load order_lines for order overview
    public function check_shipping_product_exists(Request $request) {
        $shippingRowID = $request->shipping_rowId;
        $shipping_container_products = \App\ShippingContainerProduct::where('shipping_plan_id', $shippingRowID)->get();
        if (count($shipping_container_products) > 0) {
            return Response()->json(['status' => 'true', 'message' => 'products available']);
        } else {
            return Response()->json(['status' => 'false', 'message' => 'products not available']);
        }
    }

}
