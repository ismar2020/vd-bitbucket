<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\ShippingPort;
use Illuminate\Http\Request;

class ShippingPortController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $shippingport = ShippingPort::where('port', 'LIKE', "%$keyword%")
                            ->orWhere('country', 'LIKE', "%$keyword%")
                            ->latest()->paginate($perPage);
        } else {
            
            if (\Auth::user()->roles->first()->name == 'super_admin') {
                $shippingport = ShippingPort::latest()->get();
            } else if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
                $company_users = \App\User::where('company_id', \Auth::user()->company_id)->pluck('id')->toArray();
                $shippingport = ShippingPort::whereIn('user_id', $company_users)->latest()->get();
            } else {
                $shippingport = ShippingPort::where('user_id', \Auth::id())->latest()->get();
            }
        }
        return view('admin.shipping-port.index', compact('shippingport'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        return view('admin.shipping-port.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {

        $requestData = $request->all();
        $requestData['user_id'] = \Auth::id();
        ShippingPort::create($requestData);
        return redirect('admin/shipping-port')->with('flash_message', 'ShippingPort added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id) {
        $shippingport = ShippingPort::findOrFail($id);
        return view('admin.shipping-port.show', compact('shippingport'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        $shippingport = ShippingPort::findOrFail($id);
        return view('admin.shipping-port.edit', compact('shippingport'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id) {
        $requestData = $request->all();
        $shippingport = ShippingPort::findOrFail($id);
        $shippingport->update($requestData);
        return redirect('admin/shipping-port')->with('flash_message', 'ShippingPort updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        ShippingPort::destroy($id);
        return redirect('admin/shipping-port')->with('flash_message', 'ShippingPort deleted!');
    }

}
