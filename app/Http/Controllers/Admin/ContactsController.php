<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use DB;
use App\Contact;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Auth;
use Redirect;
use Illuminate\Support\Facades\Storage;

class ContactsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        if (\Auth::user()->roles->first()->name == 'super_admin') {
            $contacts = Contact::latest()->get();
        } else if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
            $company_users = \App\User::where('company_id', \Auth::user()->company_id)->pluck('id')->toArray();
            $contacts = Contact::whereIn('user_id', $company_users)->latest()->get();
        } else if (\Auth::user()->roles->first()->name == 'supplier' && \Auth::user()->supplier_id != null) {
            $suppliersId = \App\User::where('supplier_id', \Auth::user()->supplier_id)->pluck('id');
            $contacts = Contact::whereIn('user_id', $suppliersId)->latest()->get();
//                dd($suppliersId);
        } else {
            $contacts = Contact::where('user_id', \Auth::id())->latest()->get();
        }
        return view('admin.contacts.index', compact('contacts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        return view('admin.contacts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {
        if (\Auth::user()->roles->first()->name == 'super_admin') {
            $this->validate($request, [
                'first_name' => 'required|unique:contacts',
                'surname' => 'required',
                'email' => 'required|string|max:255|email|unique:contacts',
                'mobile' => 'required',
                'mobile_prefix' => 'required'
            ]);
        } else if (\Auth::user()->roles->first()->name == 'company_user') {
            //     Custom validation for checking unique email in company of supplier-user
            $this->validate($request, [
                'surname' => 'required',
                'mobile' => 'required',
                'mobile_prefix' => 'required'
            ]);
            $supplier_email = $request->email;
            $supplier_name = $request->first_name;
            $company_users = \App\User::where('company_id', \Auth::user()->company_id)->latest()->pluck('id')->toArray();
            $company_user_contacts_id = Contact::whereIn('user_id', $company_users)->latest()->pluck('id')->toArray();
//            for validating email
            $validate_email = Contact::whereIn('id', array_unique($company_user_contacts_id))->where('email', $supplier_email)->exists();
//            for validating name
            $validate_name = Contact::whereIn('id', array_unique($company_user_contacts_id))->where('first_name', $supplier_name)->exists();
            if ($validate_email) {
                return Redirect::back()->withInput($request->input())->withErrors('The email has already been taken.');
            }
            if ($validate_name) {
                return Redirect::back()->withInput($request->input())->withErrors('The name has already been taken.');
            }
//        ends    
        }
//        dd($request->Product_id);
        $requestData = $request->all();
        $requestData['user_id'] = \Auth::id();
        if (isset($request->picture_supplier)) {
            $picture_supplier = $this->uploadFile($request, 'picture_supplier', '/uploads/picture_supplier');
            $requestData['picture_supplier'] = $picture_supplier;
        }

        if (isset($request->picture_card)) {
            $picture_card = $this->uploadFile($request, 'picture_card', '/uploads/picture_card');
            $requestData['picture_card'] = $picture_card;
        }
        if (Auth::check() && Auth::user()->hasRole('supplier') && \Auth::user()->supplier_id != null) {
            $requestData['supplier_id'] = Auth::user()->supplier_id;
        }
        $contact = Contact::create($requestData);
//this code was using previously when we are creating supplier by adding suppleir at same time 
        if (isset($request->supplierid)) {
            $data['supplier_id'] = $request->supplierid;
            $data['contact_id'] = $contact->id;
            \App\SupplierContact::create($data);
            return redirect('admin/viewsuppliercontacts/' . $request->supplierid)->with('flash_message', 'Supplier Contact added!');
        }
//        this is using for adding supplier relation from supplier tab in contacts form
        if (isset($request->supplier_id) || Auth::user()->hasRole('supplier')) {
            if (Auth::check() && Auth::user()->hasRole('supplier') && \Auth::user()->supplier_id != null) {
                $data['supplier_id'] = Auth::user()->supplier_id;
            } else {
                $data['supplier_id'] = $request->supplier_id;
            }
            $data['contact_id'] = $contact->id;
            \App\SupplierContact::create($data);
        }
        return redirect('admin/contacts')->with('flash_message', 'Contact added!');
    }

    public function addContactSupplierThroughAjax(Request $request) {
//        dd($request->contactId);
//        $this->validate($request, [
//            'name' => 'required',
//            'product_price_per_unit' => 'required',
//            'product_currency' => 'required'
//        ]);
        $requestData = $request->all();
        $requestData['supplier_id'] = $request->supplierId;
        $requestData['contact_id'] = $request->contactId;

        $data = \App\SupplierContact::where('contact_id', $request->contactId)->pluck('supplier_id');
        $count = \App\Supplier::whereIn('id', $data)->where('deleted_at', null)->count();
//        dd($count);
        if ($count > '0') {
            return Response()->json(['msg' => 'Contact can not have more then one supplier', 'status' => false]);
        }
        $result = \App\SupplierContact::create($requestData);

        if ($result) {
            $contactsupplierarrayIds = \App\SupplierContact::where('contact_id', $request->contactId)->pluck('supplier_id');
            $suppliers = \App\Supplier::wherein('id', $contactsupplierarrayIds)->get();
            return view('admin.suppliers.supplierbyid', compact('suppliers'));
        }
        return Response()->json(['msg' => 'Something went wrong', 'status' => false]);
    }

    public function addContactThroughAjax(Request $request) {
        $this->validate($request, [
            'first_name' => 'required',
            'surname' => 'required',
            'mobile' => 'required',
            'mobile_prefix' => 'required',
            'email' => 'required'
        ]);
        $requestData = $request->all();
        if (isset($request->picture_supplier)) {
            $picture_supplier = $this->uploadFile($request, 'picture_supplier', '/uploads/picture_supplier');
            $requestData['picture_supplier'] = $picture_supplier;
        }
        if (isset($request->picture_card)) {
            $picture_card = $this->uploadFile($request, 'picture_card', '/uploads/picture_card');
            $requestData['picture_card'] = $picture_card;
        }

        $result = Contact::create($requestData);
        $contactId = $result->id;
        if (isset($request->supplierId)) {

            \App\SupplierContact::create([
                'contact_id' => $contactId,
                'supplier_id' => $request->supplierId
            ]);
        }
        if ($result) {
            $contactarray = Contact::where('deleted_at', null)->get();
            $contact_html = '';
            foreach ($contactarray as $data):
                $contact_html .= "<option value=" . $data->id . ">" . $data->first_name . "</option>";
            endforeach;
            return Response()->json(['msg' => 'Contact added', 'status' => true, 'contacts' => $contactarray, 'contact_html' => $contact_html, 'contact' => $result]);
        } else {
            return Response()->json(['msg' => 'Error', 'status' => false]);
        }
        return Response()->json(['msg' => 'Something went wrong', 'status' => false]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id) {
        $contact = Contact::findOrFail($id);
        return view('admin.contacts.show', compact('contact'));
    }

    public function showScannedContactList($id = '') {
        if ($id != '') {
            $this->validate($request, [
                'first_name' => 'required',
                'surname' => 'required',
                'mobile' => 'required',
                'mobile_prefix' => 'required',
                'email' => 'required'
            ]);
            $requestData = $request->all();

            $contact = Contact::findOrFail($id);

            if (isset($request->picture_supplier)) {
                $picture_supplier = $this->uploadFile($request, 'picture_supplier', '/uploads/picture_supplier');


                $requestData['picture_supplier'] = $picture_supplier;
            } else {
                $requestData['picture_supplier'] = $contact->picture_supplier;
            }
            if (isset($request->picture_card)) {
                $picture_card = $this->uploadFile($request, 'picture_card', '/uploads/picture_card');
                $requestData['picture_card'] = $picture_card;
            } else {
                $requestData['picture_card'] = $contact->picture_card;
            }


            $contact->update($requestData);
            return redirect('admin/contacts')->with('flash_message', 'Contact updated!');
        } else {
            $scannedcontacts = Contact::where('scanned', 'Scanned')->get();
            return view('admin.contacts.showscanned', compact('scannedcontacts'));
        }
    }

    public function viewContactProducts($contactid) {
        $contactproductsid = \App\ContactProduct::where('contact_id', $contactid)->pluck('product_id');
        $contactproducts = \App\Product::whereIn('id', $contactproductsid)->get();
//        dd($supplierproducts->toArray());
        return view('admin.contacts.viewcontactproducts', compact('contactproducts'));
    }

    public function showSupplierContact($id) {
        $contact = Contact::findOrFail($id);
        $previous = 'supplier';
        return view('admin.contacts.show', compact('contact', 'previous'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        $contact = Contact::findOrFail($id);
        return view('admin.contacts.edit', compact('contact'));
    }

    public function editscannedcontacts($id) {

        $contact = Contact::findOrFail($id);
        $contact['verified'] = '2';
        return view('admin.contacts.edit', compact('contact'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request) {
        $id = $request->contact_id;
//        die($id);
        $this->validate($request, [
            'first_name' => 'required',
            'surname' => 'required',
            'mobile' => 'required',
            'mobile_prefix' => 'required',
            'email' => 'required'
        ]);
        $requestData = $request->all();
        $contact = Contact::findOrFail($id);

        if (isset($request->picture_supplier)) {
            $picture_supplier = $this->uploadFile($request, 'picture_supplier', '/uploads/picture_supplier');
            $requestData['picture_supplier'] = $picture_supplier;
        } else {
            $requestData['picture_supplier'] = $contact->picture_supplier;
        }
        if (isset($request->picture_card)) {
            $picture_card = $this->uploadFile($request, 'picture_card', '/uploads/picture_card');
            $requestData['picture_card'] = $picture_card;
        } else {
            $requestData['picture_card'] = $contact->picture_card;
        }

 
        $contact->update($requestData);
        if (isset($request->supplier_id)) {          
            $supplier_contact = \App\SupplierContact::where('contact_id', $id);
            if ($supplier_contact->get()->isEmpty() == true) {
                \App\SupplierContact::create([
                    'supplier_id' => $request->supplier_id,
                    'contact_id' => $id,
                ]);
            } else {
                $supplier_contact->update(array(
                    'supplier_id' => $request->supplier_id,
                ));
            }
        }else {
            $supplier_contact = \App\SupplierContact::where('contact_id', $id);
            if ($supplier_contact->get()->isEmpty() != true) {
                $supplier_contact->delete();
            }
        }
        if (isset($request->supplierid)) {
            return redirect('admin/viewsuppliercontacts/' . $request->supplierid)->with('flash_message', 'Supplier Contact Updated!');
        }
        return redirect('admin/contacts')->with('flash_message', 'Contact updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request, $id) {
        if (isset($request->product_contact_id)) {
            Contact::destroy($id);
            return redirect('admin/viewsuppliercontacts/' . $request->product_contact_id)->with('flash_message', 'Supplier Contact Deleted!');
        }
        $row = Contact::where('id',$id)->first();
        if($row){
            $fileName = $row->picture_supplier;
            $fileName2 = $row->picture_card;
            if(isset($fileName) && !empty($fileName)){
                Storage::disk('s3')->delete($fileName);
            }
            if(isset($fileName2) && !empty($fileName2)){
                Storage::disk('s3')->delete($fileName2);
            }
        }
        Contact::destroy($id);
        return redirect('admin/contacts')->with('flash_message', 'Contact deleted!');
    }

}
