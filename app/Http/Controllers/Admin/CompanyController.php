<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CompanyController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $company = Company::where('name', 'LIKE', "%$keyword%")
                            ->orWhere('street_address', 'LIKE', "%$keyword%")
                            ->orWhere('suburb', 'LIKE', "%$keyword%")
                            ->orWhere('postcode', 'LIKE', "%$keyword%")
                            ->orWhere('city', 'LIKE', "%$keyword%")
                            ->orWhere('country', 'LIKE', "%$keyword%")
                            ->orWhere('phone_prefix', 'LIKE', "%$keyword%")
                            ->orWhere('phone', 'LIKE', "%$keyword%")
                            ->orWhere('email', 'LIKE', "%$keyword%")
                            ->orWhere('user_id', 'LIKE', "%$keyword%")
                            ->orWhere('subscription_type', 'LIKE', "%$keyword%")
                            ->orWhere('active', 'LIKE', "%$keyword%")
                            ->latest()->paginate($perPage);
        } else {
            $company = Company::latest()->paginate($perPage);
        }
        return view('admin.company.index', compact('company'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        return view('admin.company.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {

        $this->validate(
                $request,
                [
                    'name' => 'required|unique:companies',
                    'email' => 'required|string|max:255|email|unique:companies',
                ]
        );
        $requestData = $request->all();
        if (isset($request->company_logo)) {
            $company_logo = $this->uploadFile($request, 'company_logo', '/uploads/company_logo');
            $requestData['company_logo'] = $company_logo;           
            //TEST FOR UPLOADING IMAGE TO S3 BUCKET
//            $file = $request->file('company_logo');
//            $name = time() . $file->getClientOriginalName();
//            $filePath = $name;
//            $res = Storage::disk('s3')->put($filePath, file_get_contents($file));
//            $requestData['company_logo'] = $name;
//            dd($res);
        }
        
        $company = Company::create($requestData);
//creating dummy for newly created company so that every company has its own margins which they can edit later
        if (\App\Margin::where('company_id', null)->count() == '2') {
            foreach (\App\Margin::where('company_id', null)->pluck('name','value') as $value => $name):
            \App\Margin::create([
                'name' => $name,
                'value' => $value,
                'company_id' => $company->id
            ]);
            endforeach;
        }
        return redirect('admin/company')->with('flash_message', 'Company added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id) {
        $company = Company::findOrFail($id);
        return view('admin.company.show', compact('company'));
    }

    public function showusercompany($id) {
        $company = Company::findOrFail($id);
        $previous = 'previous';
        return view('admin.company.show', compact('company', 'previous'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        $company = Company::findOrFail($id);
        return view('admin.company.edit', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id) {

        $requestData = $request->all();
        $company = Company::findOrFail($id);

        if (isset($request->company_logo)) {
            $company_logo = $this->uploadFile($request, 'company_logo', '/uploads/company_logo');
            $requestData['company_logo'] = $company_logo;
        } else {
            $requestData['company_logo'] = $company->company_logo;
        }

        $company->update($requestData);
        return redirect('admin/company')->with('flash_message', 'Company updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        $row = Company::where('id',$id)->first();
        if($row){
            $fileName = $row->company_logo;
            if(isset($fileName) && !empty($fileName)){
                Storage::disk('s3')->delete($fileName);
            }
        }
        Company::destroy($id);
        return redirect('admin/company')->with('flash_message', 'Company deleted!');
    }

}
