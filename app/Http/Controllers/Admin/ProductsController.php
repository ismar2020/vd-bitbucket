<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Product;
use Illuminate\Http\Request;
use Auth;
use Redirect;
use DB;
use Illuminate\Support\Facades\Storage;

class ProductsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $products = Product::where('name', 'LIKE', "%$keyword%")
                            ->orWhere('category_id', 'LIKE', "%$keyword%")
                            ->orWhere('type_id', 'LIKE', "%$keyword%")
                            ->orWhere('pic_id', 'LIKE', "%$keyword%")
                            ->orWhere('product_length', 'LIKE', "%$keyword%")
                            ->orWhere('product_width', 'LIKE', "%$keyword%")
                            ->orWhere('product_height', 'LIKE', "%$keyword%")
                            ->orWhere('product_weight', 'LIKE', "%$keyword%")
                            ->orWhere('product_material', 'LIKE', "%$keyword%")
                            ->orWhere('product_cbm', 'LIKE', "%$keyword%")
                            ->orWhere('product_price_per_unit', 'LIKE', "%$keyword%")
                            ->orWhere('product_currency', 'LIKE', "%$keyword%")
                            ->orWhere('shipping_port', 'LIKE', "%$keyword%")
                            ->orWhere('notes', 'LIKE', "%$keyword%")
                            ->orWhere('product_quantity_per_box', 'LIKE', "%$keyword%")
                            ->orWhere('product_box_length', 'LIKE', "%$keyword%")
                            ->orWhere('product_box_width', 'LIKE', "%$keyword%")
                            ->orWhere('product_box_height', 'LIKE', "%$keyword%")
                            ->orWhere('product_box_weight', 'LIKE', "%$keyword%")
                            ->orWhere('product_box_cbm', 'LIKE', "%$keyword%")
                            ->orWhere('product_box_material', 'LIKE', "%$keyword%")
                            ->orWhere('boxes_per_stack', 'LIKE', "%$keyword%")
                            ->orWhere('units_per_stack', 'LIKE', "%$keyword%")
                            ->orWhere('stack_length', 'LIKE', "%$keyword%")
                            ->orWhere('stack_width', 'LIKE', "%$keyword%")
                            ->orWhere('stack_height', 'LIKE', "%$keyword%")
                            ->orWhere('stack_weight', 'LIKE', "%$keyword%")
                            ->orWhere('stack_cbm', 'LIKE', "%$keyword%")
                            ->orWhere('product_box_material', 'LIKE', "%$keyword%")
                            ->orWhere('product_quantity_per_20ft', 'LIKE', "%$keyword%")
                            ->orWhere('product_quantity_per_40ft', 'LIKE', "%$keyword%")
                            ->orWhere('product_quantity_per_40fthq', 'LIKE', "%$keyword%")
                            ->orWhere('product_box_quantity_per_20ft', 'LIKE', "%$keyword%")
                            ->orWhere('product_box_quantity_per_40ft', 'LIKE', "%$keyword%")
                            ->orWhere('product_box_quantity_per_40fthq', 'LIKE', "%$keyword%")
                            ->orWhere('active', 'LIKE', "%$keyword%")
                            ->latest()->paginate($perPage);
        }else {
            if (\Auth::user()->roles->first()->name == 'super_admin') {
                $products = Product::latest()->get();
            } else if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
                $company_users = \App\User::where('company_id', \Auth::user()->company_id)->pluck('id')->toArray();
                $products = Product::whereIn('user_id', $company_users)->latest()->get();
            } else if (\Auth::user()->roles->first()->name == 'supplier' && \Auth::user()->supplier_id != null) {
                $suppliersId = \App\User::where('supplier_id', \Auth::user()->supplier_id)->pluck('id');
                $products = Product::whereIn('user_id', $suppliersId)->latest()->get();
//                dd($suppliersId);
            } else {
                $products = Product::where('user_id', \Auth::id())->latest()->get();
            }
        }
        return view('admin.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        return view('admin.products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {
        if (\Auth::user()->roles->first()->name == 'super_admin') {
            $this->validate($request, [
                'name' => 'required|unique:products'
            ]);
        } else if (\Auth::user()->roles->first()->name == 'company_user') {
//     Custom validation for checking unique name in company of supplier-user
            $this->validate($request, [
                'product_currency' => 'required'
            ]);
            $name = $request->name;
            $company_users = \App\User::where('company_id', \Auth::user()->company_id)->latest()->pluck('id')->toArray();
            $company_user_products_id = Product::whereIn('user_id', $company_users)->latest()->pluck('id')->toArray();
//            for validating name
            $validate_name = Product::whereIn('id', array_unique($company_user_products_id))->where('name', $name)->exists();
            if ($validate_name) {
                return Redirect::back()->withInput($request->input())->withErrors('The name has already been taken.');
            }
//        ends    
        }
        $requestData = $request->all();
        $requestData['user_id'] = \Auth::id();
        if (isset($request->pic_id)) {
            $product_picture = $this->uploadFile($request, 'pic_id', '/uploads/product_picture');
            $requestData['pic_id'] = $product_picture;
        }
        if (Auth::check() && Auth::user()->hasRole('supplier') && \Auth::user()->supplier_id != null) {
            $requestData['supplier_id'] = Auth::user()->supplier_id;
        }
        $product = Product::create($requestData);
//this code was using previously when we are creating supplier by adding suppleir at same time 
        if (isset($request->supplierid)) {
            $data['supplier_id'] = $request->supplierid;
            $data['product_id'] = $product->id;
            \App\SupplierProduct::create($data);
            return redirect('admin/viewsupplierproducts/' . $request->supplierid)->with('flash_message', 'Supplier Product added!');
        }
//  this is using for adding supplier relation from supplier tab in contacts form
        if (isset($request->supplier_id) || Auth::user()->hasRole('supplier')) {
            if (Auth::check() && Auth::user()->hasRole('supplier') && \Auth::user()->supplier_id != null) {
                $data['supplier_id'] = Auth::user()->supplier_id;
            } else {
                $data['supplier_id'] = $request->supplier_id;
            }
            $data['product_id'] = $product->id;
            \App\SupplierProduct::create($data);
        }
        return redirect('admin/products/' . $product->id . '/edit')->with('flash_message', 'Product added!');
//        return redirect('admin/products')->with('flash_message', 'Product added!');
    }

    public function addProductThroughAjax(Request $request) {

        $this->validate($request, [
            'name' => 'required',
            'product_price_per_unit' => 'required',
            'product_currency' => 'required'
        ]);
        $requestData = $request->all();
        if (isset($request->pic_id)) {
            $product_picture = $this->uploadFile($request, 'pic_id', '/uploads/product_picture');
            $requestData['pic_id'] = $product_picture;
        }
        $result = Product::create($requestData);
        $productId = $result->id;

        if (isset($request->supplierId)) {
            \App\SupplierProduct::create([
                'supplier_id' => $request->supplierId,
                'product_id' => $productId
            ]);
        }
        if (isset($request->supplier_id)) {
            \App\SupplierProduct::create([
                'supplier_id' => $request->supplier_id,
                'product_id' => $productId
            ]);
        }

        if ($result) {
            $productarray = Product::get();
            $product_html = '';
            foreach ($productarray as $data):
                $product_html .= "<option value=" . $data->id . ">" . $data->name . "</option>";
            endforeach;
            return Response()->json(['msg' => 'Product added', 'status' => true, 'products' => $productarray, 'product_html' => $product_html, 'product' => $result]);
        }
        return Response()->json(['msg' => 'Something went wrong', 'status' => false]);
    }

    public function getProductDetailsThroughAjax(Request $request) {
//        dd('s');
        $result = Product::where('id', $request->product_id)->where('deleted_at', null)->first();
        if ($result) {
            return Response()->json(['msg' => 'Product found', 'status' => true, 'product' => $result]);
        }
        return Response()->json(['msg' => 'Something went wrong', 'status' => false]);
    }

    public function getProductDetailsThroughAjaxForOrders(Request $request) {

        $result = Product::where('id', $request->product_id)->where('deleted_at', null)->first();
        if ($result->pic_id == '' || $result->pic_id == null) {
            $result->pic_id = 'images/no_image.png';
            $product_img = "<img src='" . url($result->pic_id) . "' width='100'/>";
        } else {
            $product_img = "<img src='" . getFile($result->pic_id) . "' width='100'/>";
        }

        if ($result) {
            return Response()->json(['msg' => 'Product found', 'status' => true, 'product' => $result, 'product_img' => $product_img]);
        }
        return Response()->json(['msg' => 'Something went wrong', 'status' => false]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id) {
        $product = Product::findOrFail($id);
        return view('admin.products.show', compact('product'));
    }

    public function showproduct($id) {
        $product = Product::findOrFail($id);
        $previous = 'contact';
        return view('admin.products.show', compact('product', 'previous'));
    }

    public function showSupplierProduct($id) {
        $product = Product::findOrFail($id);
        $previous = 'supplier';
        return view('admin.products.show', compact('product', 'previous'));
    }

    public function viewproductsupplier($productid) {
        $productsuppliersid = \App\SupplierProduct::where('product_id', $productid)->pluck('supplier_id');
        $productsuppliers = \App\Supplier::whereIn('id', $productsuppliersid)->get();
//        dd($supplierproducts->toArray());
        return view('admin.products.viewproductsuppliers', compact('productsuppliers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        $product = Product::findOrFail($id);
        return view('admin.products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request) {
        $id = $request->product_id;
//        dd($request->quote_line_id);
        $this->validate($request, [
            'name' => 'required'
        ]);
        $requestData = $request->all();
//        dd($requestData);
        $product = Product::findOrFail($id);
        if (isset($request->quantity)) {
            foreach ($request->quantity as $value):
                if (\App\ProductQuantityPerContainers::where('container_id', $value['container_id'])->where('product_id', $value['product_id'])->get()->isEmpty() === true) {
                    \App\ProductQuantityPerContainers::create($value);
                } else {
                    $product_quanity_per_containers = \App\ProductQuantityPerContainers::where('container_id', $value['container_id'])->where('product_id', $value['product_id']);
                    $product_quanity_per_containers->update($value);
                }
            endforeach;
        }
        //update entries in product attachments table
        if (isset($request->product_attachments)) {
            $collection = collect($request->product_attachments)->except(['row_id']);
//            dd($collection);
            $foo = $collection->toArray();
            foreach ($foo as $value):
                $product_packaging_options = \App\ProductsAttachments::where('id', $value['row_id']);
                $product_packaging_options->update([
                    'file_name' => $value['file_name']
                ]);
            endforeach;
        }

//        update entries in product packaging option table
        if (isset($request->packaging_options)) {
//            dd($request->packaging_options);
            $collection = collect($request->packaging_options)->except(['row_id']);
//            dd($collection);
            $foo = $collection->toArray();
            foreach ($foo as $value):
                $product_packaging_options = \App\Models\ProductPackagingOption::where('id', $value['row_id']);
            if($value['nested_product'] == 1)
            {
                $quantity_per_page = null;
            }elseif($value['nested_product'] == 2){
                $quantity_per_page = $value['quantity_per_page'];
            }
          
                $product_packaging_options->update([
                    'packaging_type' => $value['packaging_type'],
                    'quantity_per_page' => $quantity_per_page,
                    'packaging_length' => $value['packaging_length'],
                    'packaging_width' => $value['packaging_width'],
                    'packaging_height' => $value['packaging_height'],
                    'gross_weight_including_packaging' => $value['gross_weight_including_packaging'],
                    'packaging_cbm' => $value['packaging_cbm'],
                    'floor_ready' => $value['floor_ready'],
                    'stackable' => $value['stackable'],
                    'nested_product' => $value['nested_product']
                ]);
            endforeach;
        }
//        ends
        $product_picture = null;
        if (isset($request->pic_id)) {
            $product_picture = $this->uploadFile($request, 'pic_id', '/uploads/product_picture');
            $requestData['pic_id'] = $product_picture;
        } else {
            $requestData['pic_id'] = $product->pic_id;
        }
        $product->update($requestData);
        if (isset($request->supplier_id)) {
            $supplier_product = \App\SupplierProduct::where('product_id', $request->product_id);
            if ($supplier_product->get()->isEmpty() == true) {
                \App\SupplierProduct::create([
                    'supplier_id' => $request->supplier_id,
                    'product_id' => $request->product_id,
                ]);
            } else {
                $supplier_product->update(array(
                    'supplier_id' => $request->supplier_id,
                ));
            }
        } else {
            $supplier_product = \App\SupplierProduct::where('product_id', $request->product_id)->delete();
        }
        if (isset($request->productid)) {
//            dd($request->supplierid,$contact->id)
            return redirect('admin/viewsupplierproducts/' . $request->productid)->with('flash_message', 'Supplier Product Updated!');
        }
        if (isset($request->quote_line_id)) {
//            dd($product_picture,$request->quote_line_id,$request->name,$requestData);
            $update_quote_line = \App\QuoteLines::where('id', $request->quote_line_id)->update([
                'product_name' => $request->name,
                'product_image' => $product_picture,
                'product_length' => $request->product_length,
                'product_width' => $request->product_width,
                'product_height' => $request->product_height,
                'product_weight' => $request->product_weight,
                'product_cbm' => $request->product_cbm,
                'product_unit_price' => $request->product_price_per_unit,
                'product_unit_currency' => $request->product_currency,
                'shipping_port' => $request->shipping_port,
                'product_unit_currency' => $request->product_currency,
            ]);
//            dd($update_quote_line);
//            if ($update_quote_line) {
//                $update_quote_line->product_name = $request->name;
//                $update_quote_line->product_length = $request->product_length;
//                $update_quote_line->product_width = $request->product_width;
//                $update_quote_line->product_height = $request->product_height;
//                $update_quote_line->product_weight = $request->product_weight;
//                $update_quote_line->product_cbm = $request->product_cbm;
//                $update_quote_line->product_unit_price = $request->product_price_per_unit;
//                $update_quote_line->product_unit_currency = $request->product_currency;
//                $update_quote_line->shipping_port = $request->shipping_port;
//                dd($update_quote_line->product_name);
//                $update_quote_line->update($requestData);
//            }
            return redirect('admin/products')->with('flash_message', 'Product and Quote Line updated!');
        }
        return redirect('admin/products')->with('flash_message', 'Product updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request, $id) {
//        die($request->product_supplier_id);
        if (isset($request->product_supplier_id)) {
            Product::destroy($id);
            return redirect('admin/viewsupplierproducts/' . $request->product_supplier_id)->with('flash_message', 'Supplier Product Deleted!');
        }
        //deleting product media from s3-bucket on aws
        $row = Product::where('id', $id)->first();
        if ($row) {
            $fileName = $row->pic_id;
            if (isset($fileName) && !empty($fileName)) {
                Storage::disk('s3')->delete($fileName);
            }
        }
        //deleting product id form related tables (manually)
        //from ShippingContainerProduct pluck orderLineId
        $orderLineHavingThisProduct = \App\OrderLine::where('product_id', $id)->pluck('id');
        //now getting
        \App\ShippingContainerProduct::whereIn('order_line_id', $orderLineHavingThisProduct)->delete();
        //from order_lines
        \App\OrderLine::where('product_id', $id)->delete();
        //from quote_lines
        \App\QuoteLines::where('product_id', $id)->delete();
        //from ProductQuantityPerContainers
        \App\ProductQuantityPerContainers::where('product_id', $id)->delete();
        //now deleting product id
        Product::destroy($id);
        return redirect('admin/products')->with('flash_message', 'Product deleted!');
    }

    //Product_quantity_per_container ajax
//    public function product_quantity_per_container_ajax(Request $request) {
//        $requestData = $request->all();
//        if (isset($request->quantity)) {
//            foreach ($request->quantity as $value):
//                if(\App\ProductQuantityPerContainers::where('container_id', $value['container_id'])->where('product_id', $value['product_id'])->get()->isEmpty() === true){
//                    \App\ProductQuantityPerContainers::create($value);
//                }else{
//                    $product_quanity_per_containers = \App\ProductQuantityPerContainers::where('container_id', $value['container_id'])->where('product_id', $value['product_id']);
//                    $product_quanity_per_containers->update($value); 
//                }
//                
//            endforeach;
//        }
//        return Response()->json(['status' => 'true']);
//    }
    //code for load packaging options
    public function load_packaging_options(Request $request) {
        $product_id = $request->product_id;
        $packaging_options = \App\Models\ProductPackagingOption::where('product_id', $product_id)->where('nested_id', '0')->get();
        $packaging_options_nested = \App\Models\ProductPackagingOption::where('product_id', $product_id)->where('nested_id', '!=', '0')->pluck('nested_id');
        $packaging_options_nested = \App\Models\ProductPackagingOption::whereIn('id', $packaging_options_nested)->get();
        $packaging_options = $packaging_options->merge($packaging_options_nested);
//        dd($packaging_options);
        return view('admin.products.load_packaging_options', compact('packaging_options'));
    }

    //insert_packaging_options
    public function insert_packaging_options(Request $request) {
        $product_id = $request->product_id;
        $nested_id = \App\Models\ProductPackagingOption::create([
                    'product_id' => $product_id
        ]);
        //        ends
        Response()->json(['status' => 'true']);
    }

    //insert_nested_product
    public function insert_nested_product(Request $request) {
        $product_id = $request->product_id;
        $nested_id = $request->nested_id;
        \App\Models\ProductPackagingOption::create([
            'product_id' => $product_id,
            'nested_id' => $nested_id
        ]);
        Response()->json(['status' => 'true']);
    }

//    add_remove_default_nested_product
    public function add_remove_default_nested_products(Request $request) {
        $product_id = $request->product_id;
        $nested_row_id = $request->row_id;
        $if_nested = $request->nested_product;

        if ($if_nested == '1') {
//      create default nested product in packaging table
            \App\Models\ProductPackagingOption::create([
                'product_id' => $product_id,
                'nested_id' => $nested_row_id
            ]);
        } else if ($if_nested == '2') {

            //      Update main row id quantity_per_page to 0 
            //  dd($nested_row_id);
            $nested_main_product = DB::table('product_packaging_options')
                    ->where('id', $nested_row_id)
                    ->update(['quantity_per_page' => 0]);
//            $nested_main_product = $nested_main_product->update([
//                'quantity_per_page' => null
//            ]);
            // dd($nested_main_product);
//      delete nested_id previous entries if selected NO

            $res = \App\Models\ProductPackagingOption::where('nested_id', $nested_row_id)->delete();
//            dd($res);
        }
        Response()->json(['status' => 'true']);
    }

    //code for load_nested_products
    public function load_nested_products(Request $request) {
        $nested_id = $request->nested_id;
        $nested_products = \App\Models\ProductPackagingOption::where('nested_id', $nested_id)->get();
//        getting sets per package
        $sets_per_package = \App\Models\ProductPackagingOption::where('id', $nested_id)->first()->sets_per_package;
//        dd($sets_per_package);
        return view('admin.products.load_nested_products', compact('nested_products', 'nested_id', 'sets_per_package'));
    }

    //code for load_nested_products
    public function load_nested_products_for_shipping_view_loading(Request $request) {
        $nested_id = $request->nested_id;
        $nested_products = \App\Models\ProductPackagingOption::where('nested_id', $nested_id)->get();
//        getting sets per package
        $sets_per_package = \App\Models\ProductPackagingOption::where('id', $nested_id)->first()->sets_per_package;
//        dd($sets_per_package);
        return view('admin.products.load_nested_products_for_shipping_view_loading', compact('nested_products', 'nested_id', 'sets_per_package'));
    }

    //code for update_quantity_per_package

    public function update_quantity_per_package(Request $request) {
        $row_id = $request->row_id;
        $product_packaging_options = \App\Models\ProductPackagingOption::where('id', $row_id);
        if ($request->type == 'quantity') {
            $product_packaging_options->update([
                'quantity_per_page' => $request->values
            ]);
        } else if ($request->type == 'sets') {
            $product_packaging_options->update([
                'sets_per_package' => $request->values
            ]);
        }
        //getting nested_id
        $product_packaging_options = \App\Models\ProductPackagingOption::where('id', $row_id)->first();
        //getting whole sum of qty where nested_id matched from above nested_id
//        $nested_quantity = \App\Models\ProductPackagingOption::select('*', DB::raw('SUM(quantity_per_page) as quantity'))->where('nested_id', $product_packaging_options->nested_id)->get();
        $nested_quantity = DB::table('product_packaging_options')
                ->select(DB::raw('SUM(quantity_per_page) as quantity'))
                ->where('nested_id', $product_packaging_options->nested_id)
                ->where('deleted_at', null)
                ->get();
        //ends
        return Response()->json(['status' => 'true', 'main_row_id' => $product_packaging_options->nested_id, 'nested_quantity' => $nested_quantity[0]->quantity]);
    }

    //dropzone product
    public function dropzone_product_attachment(Request $request) {
//        $image = $request->file('file');
//        $imageName = time() . '_' . $image->getClientOriginalName();
//        $image->move(public_path('images/documents'), $imageName);
        $product_id = $request->product_id;
        $imageName = $this->uploadFile($request, 'file', '/images/documents');

        $addImage = \App\ProductsAttachments::create([
                    'product_id' => $product_id,
                    'original_file_name' => $imageName,
                    'file_name' => $imageName,
        ]);
        return response()->json(['success' => $imageName]);
    }

    //code for load product attachments lines
    public function load_product_attachments_lines(Request $request) {
        $product_id = $request->product_id;
        $product = Product::where('id', $product_id)->first();
        $product_attachments = \App\ProductsAttachments::where('product_id', $product_id)->get();
        return view('admin.products.load_product_attachments', compact('product_attachments'));
    }

    //create product attachments ajax
    public function submit_product_attachments_ajax(Request $request) {
        $requestData = $request->all();
//        dd($request->order_pricing);
        $product = Product::findOrFail($request->product_id);
        if (isset($request->product_attachments)) {
            foreach ($request->product_attachments as $value):
                if (isset($value['row_id'])):
                    $product_attachments = \App\ProductsAttachments::findOrFail($value['row_id']);
                    $product_attachments->update($value);
                endif;
            endforeach;
        }
        return Response()->json(['status' => 'true', 'message' => 'Product attachments updated']);
    }

}
