<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Redirect;
use App\Rules\Captcha;


class RegisterController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Register Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles the registration of new users as well as their
      | validation and creation. By default this controller uses a trait to
      | provide this functionality without requiring any additional code.
      |
     */

use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

//    public function redirectTo() {
//        if (\Auth::user()->roles->count() > 0) {
//            dd('Register : i have role');
//            return '/home';
//        } else {
//            dd('Register : no roles');
//            return '/thanks';
//        }
//    }
    //my code
//    protected function redirectTo() {
//        Auth::logout();
//        return view('thanks');
//    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data) {
        return Validator::make($data, [
                    'name' => ['required', 'string', 'max:255'],
                    'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                    'password' => ['required', 'string', 'min:8', 'confirmed'],
                    'mobile_prefix' => ['required', 'numeric', 'min:1'],
                    'mobile' => ['required', 'numeric', 'min:9'],
                    'g-recaptcha-response' => new Captcha(),
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data) {

        return User::create([
                    'username' => $data['name'],
                    'email' => $data['email'],
                    'password' => Hash::make($data['password']),
                    'mobile_prefix' => $data['mobile_prefix'],
                    'mobile' => $data['mobile'],
        ]);
    }

    public function register(Request $request) {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        //$this->guard()->login($user);

        return Redirect::to(url('auth/thanks'));
       
        
        
        //return $this->registered($request, $user) ?: redirect($this->redirectPath());
    }

}
