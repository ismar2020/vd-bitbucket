<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class ShippingContainerProduct extends Model
{
    use LogsActivity;
    use SoftDeletes;
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'shipping_container_products';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['order_id', 'order_line_id', 'shipping_plan_id', 'product_name', 'product_image', 'number_of_units_that_fit', 'container_order_quantity', 'container_order_quantity_cbm', 'container_total_cost', 'container_total_wholesale_price', 'total_product_units_added_to_other_containers', 'user_id', 'product_id', 'supplier_id', 'using', 'quantity', 'confirmed_quantity', 'total_cost', 'customer_buy_price', 'profit', 'packaging_id'];

    

    /**
     * Change activity log event description
     *
     * @param string $eventName
     *
     * @return string
     */
    public function getDescriptionForEvent($eventName)
    {
        return __CLASS__ . " model has been {$eventName}";
    }
    
    //for ConfirmedQuantity
    public function setConfirmedQuantityAttribute($value){
        $this->attributes['confirmed_quantity'] = str_replace( ',','', $value);
    }
    //for TotalCost
    public function setTotalCostAttribute($value){
        $this->attributes['total_cost'] = str_replace( ',','', $value);
    }
    //for CustomerBuyPrice
    public function setCustomerBuyPriceAttribute($value){
        $this->attributes['customer_buy_price'] = str_replace( ',','', $value);
    }
    //for Profit
    public function setProfitAttribute($value){
        $this->attributes['profit'] = str_replace( ',','', $value);
    }
    
    public function shipping_container_products_suppliers() {
        return $this->hasMany(Product::class, 'name', 'product_name');
    }
}
