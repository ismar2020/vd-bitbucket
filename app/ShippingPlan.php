<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class ShippingPlan extends Model {

    use LogsActivity;
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'shipping_plans';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    protected $appends = ['shipping_products', 'shipping_products_suppliers_name'];

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['order_id', 'container_id', 'purchase_order_number', 'shipping_to_port', 'shipping_date', 'container_unit_quantity', 'container_purchase_cost', 'container_wholesale_price', 'container_costs', 'conatiner_costs_value', 'user_id', 'bl_number', 'description', 'port_of_discharge', 'cargo_ready_date', 'dispatched_date', 'eta_date', 'total_cost', 'customer_buy_price', 'profit', 'status','invoice_name', 'invoiced_to_customer', 'invoice_amount', 'deposit_paid', 'payment_received', 'payment_date', 'payment_id','comments','fumigated'];

    /**
     * Change activity log event description
     *
     * @param string $eventName
     *
     * @return string
     */
    public function getDescriptionForEvent($eventName) {
        return __CLASS__ . " model has been {$eventName}";
    }

    public function shipping_container_products() {
        return $this->hasMany(ShippingContainerProduct::class, 'shipping_plan_id', 'id')->with('shipping_container_products_suppliers');
    }

    //for TotalCost
    public function setTotalCostAttribute($value) {
        $this->attributes['total_cost'] = str_replace(',', '', $value);
    }

    //for CustomerBuyPrice
    public function setCustomerBuyPriceAttribute($value) {
        $this->attributes['customer_buy_price'] = str_replace(',', '', $value);
    }

    //for Profit
    public function setProfitAttribute($value) {
        $this->attributes['profit'] = str_replace(',', '', $value);
    }

    //appended shipping container products mainly for order overview
    public function getShippingProductsAttribute() {

        $data = \App\ShippingContainerProduct::where('shipping_plan_id', $this->id)->pluck('product_name');
        $arr = [];
        $i = 1;
        foreach ($data as $k => $val) {
        $res = \App\Product::where('name', $val)->first();
        if ($res != '') {
        $prod_with_suplier = '(' . get_supplier_name_by_id($res->supplier_id) . ')';
        } else {
            $prod_with_suplier = '';
        }
        array_push($arr, '<b>'. $i .'.</b> '. $val . '' . $prod_with_suplier . '<br>');
        $i++;
        }
        $res = '<p style="color: #000; white-space: break-spaces;width: 250px;">' . implode('', $arr);
        return $res;
    }
    //appended supplier name mainly for order overview
    public function getShippingProductsSuppliersNameAttribute() {

        $data = \App\ShippingContainerProduct::where('shipping_plan_id', $this->id)->pluck('product_name');
        $arr = [];
        $i = 1;
        foreach ($data as $k => $val) {
        $res = \App\Product::where('name', $val)->first();
        if ($res != '') {
        $prod_with_suplier = get_supplier_name_by_id($res->supplier_id);
        } else {
            $prod_with_suplier = '';
        }
        if($prod_with_suplier != '')
        array_push($arr, $i .'. '. $prod_with_suplier . '<br>');
        $i++;
        }
        $res = '<p style="color: #000; white-space: break-spaces;width: 250px;">' . implode('', $arr);
        return $res;
    }

}
