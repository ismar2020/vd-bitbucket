<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Product extends Model
{
    use LogsActivity;
    use SoftDeletes;
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'category_id', 'type_id', 'pic_id', 'supplier_id', 'product_length', 'product_width', 'product_height', 'product_weight', 'product_material', 'product_cbm', 'product_price_per_unit', 'product_currency', 'shipping_port', 'notes', 'product_quantity_per_box', 'product_box_length', 'product_box_width', 'product_box_height', 'product_box_weight', 'product_box_cbm', 'product_box_material', 'boxes_per_stack', 'units_per_stack', 'stack_length', 'stack_width', 'stack_height', 'stack_weight', 'stack_cbm', 'product_box_material', 'product_quantity_per_20ft', 'product_quantity_per_40ft', 'product_quantity_per_40fthq', 'product_box_quantity_per_20ft', 'product_box_quantity_per_40ft', 'product_box_quantity_per_40fthq', 'active', 'user_id', 'item_number', 'supplier_item_number', 'item_code', 'supplier_item_code', 'barcode', 'outer_package_barcode'];

    

    /**
     * Change activity log event description
     *
     * @param string $eventName
     *
     * @return string
     */
    public function getDescriptionForEvent($eventName)
    {
        return __CLASS__ . " model has been {$eventName}";
    }
    
    public function setProductPricePerUnitAttribute($value){
        $this->attributes['product_price_per_unit'] = str_replace( ',','', $value);
    }
}
