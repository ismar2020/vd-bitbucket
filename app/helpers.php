<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function get_phone_prefix_list() {
    $phone_prefix_list = DB::table('countries')->pluck('calling_code', 'id');
//    $phone_prefix_list = array_unique($phone_prefix_list->toArray());
    //attaching country name to the phone prefix
    foreach ($phone_prefix_list as $key => $value):
        $phone_prefix_list[$key] = DB::table('countries')->where('id', $key)->first()->name . ' +' . $value;
    endforeach;

//    dd($phone_prefix_list);
    return $phone_prefix_list;
}

function get_phone_prefix_with_country_name_list() {
//    $phone_prefix_list = DB::table('countries')->pluck('calling_code', 'id');
//    $phone_prefix_list = array_unique($phone_prefix_list->toArray());
//
//    return $phone_prefix_list;
    $currency_list = DB::table('countries')->pluck('currency', 'id');
    $currency_list = array_unique($currency_list->toArray());
//            dd($currency_list);
    $currency_with_symbols = '';
    foreach ($currency_list as $key => $value):
        if ($value != ''):
            $currency_with_symbols .= "<option value=" . $key . ">" . $value . " (" . DB::table('countries')->where('id', $key)->first()->currency_symbol . ")</option>";
        endif;
    endforeach;
    return $currency_with_symbols;
}

function get_country_list() {
    $country_list = DB::table('countries')->pluck('name', 'id');
    return $country_list;
}

function get_country_name_by_id($id) {
    $country_list = DB::table('countries')->where('id', $id)->first();
    return ($country_list != null) ? $country_list->name : 'NA';
}

function get_currency_list() {
    $currency_list = DB::table('countries')->pluck('currency_symbol', 'currency', 'id');
    return $currency_list;
}

function get_currency_list_with_symbols() {

    $currency_list = DB::table('countries')->pluck('currency', 'id');
    $currency_list = array_unique($currency_list->toArray());
//            dd($currency_list);
    $currency_with_symbols = '';
    foreach ($currency_list as $key => $value):
        if ($value != ''):
            $currency_with_symbols .= "<option value=" . $key . ">" . $value . " (" . DB::table('countries')->where('id', $key)->first()->currency_symbol . ")</option>";
        endif;
    endforeach;
    return $currency_with_symbols;
}

function get_currency_list_with_symbols_through_id($id) {

    $currency_list = App\Country::whereIn('currency_code', array('USD', 'GBP', 'EUR', 'AUD'))->pluck('currency_code', 'id');
    $currency_list = array_unique($currency_list->toArray());
//            dd($currency_list);
    $currency_with_symbols = '';
    foreach ($currency_list as $key => $value):
        if ($value != ''):
            if (isset($id) && $key == $id) {
                $currency_with_symbols .= "<option value=" . $key . " selected>" . $value . " (" . DB::table('countries')->where('id', $key)->first()->currency_symbol . ")</option>";
            } else {
                $currency_with_symbols .= "<option value=" . $key . ">" . $value . " (" . DB::table('countries')->where('id', $key)->first()->currency_symbol . ")</option>";
            }

        endif;
    endforeach;
    return $currency_with_symbols;
}

function get_limited_currency_list_with_symbols_through_id() {

    $currency_list = App\Country::whereIn('currency_code', array('USD', 'GBP', 'EUR', 'AUD'))->pluck('currency_code', 'id');
    $currency_list = array_unique($currency_list->toArray());
//            dd($currency_list);
    $currency_with_symbols = '';
    foreach ($currency_list as $key => $value):
        if ($value != ''):

            $currency_with_symbols .= "<option value=" . $key . ">" . $value . " (" . DB::table('countries')->where('id', $key)->first()->currency_symbol . ")</option>";

        endif;
    endforeach;
    return $currency_with_symbols;
}

function get_limited_currency_list() {
    $currency_list = App\Country::whereIn('currency_code', array('USD', 'EUR', 'AUD', 'GBP'))->pluck('currency_code', 'id');
    $currency_list = array_unique($currency_list->toArray());
    return ($currency_list != '' && $currency_list != null) ? $currency_list : ['' => 'No Data currently'];
}

function get_limited_currency_code_list() {
    $currency_list = App\Country::whereIn('currency_code', array('USD', 'EUR', 'AUD', 'GBP'))->pluck('currency_code', 'id');
    $currency_list = array_unique($currency_list->toArray());
    return ($currency_list != '' && $currency_list != null) ? $currency_list : ['' => 'No Data currently'];
}

function get_limited_currency_list_with_selected_item($id) {
    $currency_list = App\Country::whereIn('currency_code', array('USD', 'GBP', 'EUR', 'AUD'))->pluck('currency_code', 'id');
    $currency_list = array_unique($currency_list->toArray());
//            dd($currency_list);
    $currency_with_symbols = '';
    foreach ($currency_list as $key => $value):
        if ($value != ''):
            if ($key == $id):
                $currency_with_symbols .= "<option value=" . $key . " selected>" . $value . " (" . DB::table('countries')->where('id', $key)->first()->currency_symbol . ")</option>";
            else:
                $currency_with_symbols .= "<option value=" . $key . ">" . $value . " (" . DB::table('countries')->where('id', $key)->first()->currency_symbol . ")</option>";
            endif;
        endif;
    endforeach;
    return $currency_with_symbols;
}

function get_suppliers_list() {
    if (\Auth::user()->roles->first()->name == 'super_admin') {
        $data = App\Supplier::latest()->pluck('name', 'id');
//        dd($data);
    } else if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
        $company_users = \App\User::where('company_id', \Auth::user()->company_id)->latest()->pluck('id')->toArray();
        $data = \App\Supplier::whereIn('user_id', $company_users)->latest()->pluck('name', 'id');
//        $supplier_users = \App\User::where('company_id', \Auth::user()->company_id)->where('supplier_id', '!=', null)->latest()->pluck('supplier_id')->toArray();
//        $data = \App\Supplier::whereIn('id', array_unique($supplier_users))->latest()->pluck('name', 'id');
    } else if (\Auth::user()->roles->first()->name == 'supplier' && \Auth::user()->supplier_id != null) {
//        $suppliersId = \App\User::where('supplier_id', \Auth::user()->supplier_id)->pluck('id');
//        $data = \App\Supplier::whereIn('user_id', $suppliersId)->latest()->pluck('name', 'id');
        $data = App\Supplier::where('id', \Auth::user()->supplier_id)->latest()->pluck('name', 'id');
//                dd($suppliersId);
    } else {
        $data = \App\Supplier::where('user_id', \Auth::id())->latest()->pluck('name', 'id');
    }
    return (App\Supplier::get()->isEmpty() != true) ? $data : ['' => 'No Suppliers currently'];
}

function get_supplier_name_by_id($id) {
    $supplier_list = DB::table('suppliers')->where('id', $id)->where('deleted_at', null)->first();
    return ($supplier_list != null) ? $supplier_list->name : 'N/A';
}

function get_product_name_by_id($id) {

    $product_list = \App\Product::where('id', $id)->first();
    return ($product_list != null) ? $product_list->name : 'NA';
}

function get_product_details_by_id($id) {
    $product_list = \App\Product::where('id', $id)->first();
    return ($product_list != null) ? $product_list : 'NA';
}

//    function get_sum_of_total_product_units_by_id($id,$shipping_id) {
//        $data = DB::table('shipping_container_products')->where('id', '!=' ,$id)->where('shipping_plan_id', '!=' ,$shipping_id)->sum('container_order_quantity')->first();
//        
//        return ($data != null)? $data:'NA';
//    }

function get_order_name_by_id($id) {
    $order = \App\Order::where('id', $id)->first();
    return ($order != null) ? $order->name : 'NA';
}

function get_order_name_by_id_using_in_order_overview($id) {
    $order = \App\Order::where('id', $id)->first();
    return ($order != null) ? $order->name : 'N/A';
}

function get_shipping_port_name_by_id($id) {
    $shipping_port = \App\ShippingPort::where('id', $id)->first();
    return ($shipping_port != null) ? $shipping_port->port : 'NA';
}

function get_shipping_port_name_by_id_using_in_order_overview($id) {
    $shipping_port = \App\ShippingPort::where('id', $id)->first();
    return ($shipping_port != null) ? $shipping_port->port : 'N/A';
}

function get_contacts_list() {
    if (\Auth::user()->roles->first()->name == 'super_admin') {
        $data = \App\Contact::latest()->pluck('first_name', 'id');
    } else if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
        $company_users = \App\User::where('company_id', \Auth::user()->company_id)->latest()->pluck('id')->toArray();
        $data = \App\Contact::whereIn('user_id', $company_users)->latest()->pluck('first_name', 'id');
    } else if (\Auth::user()->roles->first()->name == 'supplier' && \Auth::user()->supplier_id != null) {
        $suppliersId = \App\User::where('supplier_id', \Auth::user()->supplier_id)->pluck('id');
        $data = \App\Contact::whereIn('user_id', $suppliersId)->latest()->pluck('first_name', 'id');
//                dd($suppliersId);
    } else {
        $data = \App\Contact::where('user_id', \Auth::id())->latest()->pluck('first_name', 'id');
    }
    return (\App\Contact::get()->isEmpty() != true) ? $data : ['' => 'No Contacts currently'];
}

function get_contact_name_by_id($id) {
    $contact = DB::table('contacts')->where('id', $id)->first();
    return ($contact != null) ? $contact->first_name . ' ' . $contact->surname : 'NA';
}

function get_quote_cat_name_by_id($id) {
    $data = App\QuoteCategory::where('id', $id)->first();
    return ($data != null) ? $data->name : 'NA';
}

function get_latest_quote_comment_name_by_quote_id($id) {
    $data = App\Comment::where('quote_id', $id)->orderBy('id', 'DESC')->first();
    return ($data != null) ? $data->description : 'NA';
}

function get_products_list() {
    if (\Auth::user()->roles->first()->name == 'super_admin') {
        $data = App\Product::latest()->where('name', '!=', null)->pluck('name', 'id');
    } else if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
        $company_users = \App\User::where('company_id', \Auth::user()->company_id)->latest()->pluck('id')->toArray();
        $data = \App\Product::whereIn('user_id', $company_users)->latest()->pluck('name', 'id');
    } else if (\Auth::user()->roles->first()->name == 'supplier' && \Auth::user()->supplier_id != null) {
        $suppliersId = \App\User::where('supplier_id', \Auth::user()->supplier_id)->pluck('id');
        $data = \App\Product::whereIn('user_id', $suppliersId)->latest()->pluck('name', 'id');
//                dd($suppliersId);
    } else {
        $data = \App\Product::where('user_id', \Auth::id())->latest()->pluck('name', 'id');
    }
    return (App\Product::get()->isEmpty() != true) ? $data : ['' => 'No products currently'];
}

function get_quote_category_list() {
    if (\Auth::user()->roles->first()->name == 'super_admin') {
        $data = App\QuoteCategory::latest()->pluck('name', 'id');
    } else if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
        $company_users = \App\User::where('company_id', \Auth::user()->company_id)->latest()->pluck('id')->toArray();
        $data = \App\QuoteCategory::whereIn('user_id', $company_users)->latest()->pluck('name', 'id');
    } else if (\Auth::user()->roles->first()->name == 'supplier' && \Auth::user()->company_id != null) {
        $company_users = \App\User::where('company_id', \Auth::user()->company_id)->pluck('id');
        $data = \App\QuoteCategory::whereIn('user_id', $company_users)->latest()->pluck('name', 'id');
//        $data = App\QuoteCategory::latest()->pluck('name', 'id');
    } else {
        $data = \App\QuoteCategory::where('user_id', \Auth::id())->latest()->pluck('name', 'id');
    }
    return (App\QuoteCategory::get()->isEmpty() != true) ? $data : ['' => 'No Quote Categories'];
}

function get_supplier_category_list() {
    if (\Auth::user()->roles->first()->name == 'super_admin') {
        $data = App\Models\SupplierCategory::latest()->pluck('name', 'id');
    } else if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
        $company_users = \App\User::where('company_id', \Auth::user()->company_id)->latest()->pluck('id')->toArray();
        $data = \App\Models\SupplierCategory::whereIn('user_id', $company_users)->latest()->pluck('name', 'id');
    } else if (\Auth::user()->roles->first()->name == 'supplier' && \Auth::user()->company_id != null) {
        $company_users = \App\User::where('company_id', \Auth::user()->company_id)->pluck('id');
        $data = App\Models\SupplierCategory::whereIn('user_id', $company_users)->latest()->pluck('name', 'id');
//        $data = App\QuoteCategory::latest()->pluck('name', 'id');
    } else {
        $data = App\Models\SupplierCategory::where('user_id', \Auth::id())->latest()->pluck('name', 'id');
    }
    return (App\Models\SupplierCategory::get()->isEmpty() != true) ? $data : ['' => 'No Supplier Categories'];
}

function get_products_list_which_are_not_linked($template_id) {
    $template_products = \App\FisTemplateProducts::pluck('product_id');
    if (\Auth::user()->roles->first()->name == 'super_admin') {
        $data = \App\Product::whereNotIn('id', $template_products)->where('name', '!=', null)->latest()->pluck('name', 'id');
    } else if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
        $company_users = \App\User::where('company_id', \Auth::user()->company_id)->latest()->pluck('id')->toArray();
        $data = \App\Product::whereNotIn('id', $template_products)->whereIn('user_id', $company_users)->where('name', '!=', null)->latest()->pluck('name', 'id');
    } else if (\Auth::user()->roles->first()->name == 'supplier' && \Auth::user()->supplier_id != null) {
        $suppliersId = \App\User::where('supplier_id', \Auth::user()->supplier_id)->pluck('id');
        $data = \App\Product::whereNotIn('id', $template_products)->whereIn('user_id', $suppliersId)->where('name', '!=', null)->latest()->pluck('name', 'id');
//                dd($suppliersId);
    } else {
        $data = \App\Product::whereNotIn('id', $template_products)->where('user_id', \Auth::id())->where('name', '!=', null)->latest()->pluck('name', 'id');
    }
    return ($data != '') ? $data : ['' => 'No products currently'];
}

function get_shipping_port_list() {
//    if (\Auth::user()->roles->first()->name == 'super_admin') {
//        $data = App\ShippingPort::latest()->pluck('port', 'id');
//    } else if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
//        $company_users = \App\User::where('company_id', \Auth::user()->company_id)->latest()->pluck('id')->toArray();
//        $data = \App\ShippingPort::whereIn('user_id', $company_users)->latest()->pluck('port', 'id');
//    } else if (\Auth::user()->roles->first()->name == 'supplier' && \Auth::user()->supplier_id != null) {
//        $suppliersId = \App\User::where('supplier_id', \Auth::user()->supplier_id)->pluck('id');
//        $data = \App\ShippingPort::whereIn('user_id', $suppliersId)->latest()->pluck('port', 'id');
//    } else {
//        $data = \App\ShippingPort::where('user_id', \Auth::id())->latest()->pluck('port', 'id');
//    }
    $data = App\ShippingPort::latest()->pluck('port', 'id');
    return (App\ShippingPort::get()->isEmpty() != true) ? $data : ['' => 'No Shipping Port currently'];
}

function get_containers_list() {
//    if (\Auth::user()->roles->first()->name == 'super_admin') {
//        $data = App\Container::latest()->pluck('name', 'id');
//    } else if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
//        $company_users = \App\User::where('company_id', \Auth::user()->company_id)->latest()->pluck('id')->toArray();
//        $data = \App\Container::whereIn('user_id', $company_users)->latest()->pluck('name', 'id');
//    } else if (\Auth::user()->roles->first()->name == 'supplier' && \Auth::user()->supplier_id != null) {
//        $suppliersId = \App\User::where('supplier_id', \Auth::user()->supplier_id)->pluck('id');
//        $data = \App\Container::whereIn('user_id', $suppliersId)->latest()->pluck('name', 'id');
//    } else {
//        $data = \App\Container::where('user_id', \Auth::id())->latest()->pluck('name', 'id');
//    }
    $data = App\Container::latest()->pluck('name', 'id');
    return (\App\Container::get()->isEmpty() != true) ? $data : ['' => 'No Containers currently'];
}

function get_users_list() {
    $users = \App\User::whereHas('roles', function ($query) {
                $query->where('name', '!=', 'super_admin');
            })->get()->pluck('first_name', 'id');
    return $users;
}

function get_cost_name_by_id($cost_id) {
//            $cost = App\Cost::findOrFail($cost_id);
    $cost = DB::table('costs')->where('id', $cost_id)->first();
    if ($cost != null):
        return $cost->name;
    else:
        return 'NA';
    endif;
}

function get_currency_name_by_id($id) {
    $country_list = \App\Country::where('id', $id)->first();
    return ($country_list != null) ? $country_list->currency_code . ' (' . $country_list->currency_symbol . ')' : 'NA';
}

function get_container_list() {
    $container_list = DB::table('containers')->where('deleted_at', null)->pluck('name', 'id');
    return (DB::table('containers')->get()->isEmpty() != true) ? $container_list : ['' => 'No containers currently'];
}

function get_container_name_by_id($id) {
    $container_list = DB::table('containers')->where('id', $id)->first();
    return ($container_list != null) ? $container_list->name : 'NA';
}

function get_companies_list() {
    $companies_list = DB::table('companies')->where('deleted_at', null)->pluck('name', 'id');
    return (DB::table('companies')->get()->isEmpty() != true) ? $companies_list : ['' => 'No companies currently'];
}

function get_companies_name_by_id($id) {
    $companies_list = DB::table('companies')->where('id', $id)->first();
    return ($companies_list != null) ? $companies_list->name : 'NA';
}

function get_material_list() {
    if (\Auth::user()->roles->first()->name == 'super_admin') {
        $data = App\Material::latest()->pluck('name', 'id');
    } else if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
        $company_users = \App\User::where('company_id', \Auth::user()->company_id)->latest()->pluck('id')->toArray();
        $data = \App\Material::whereIn('user_id', $company_users)->latest()->pluck('name', 'id');
    } else if (\Auth::user()->roles->first()->name == 'supplier' && \Auth::user()->company_id != null) {
        $company_users = \App\User::where('company_id', \Auth::user()->company_id)->pluck('id');
        $data = \App\Material::whereIn('user_id', $company_users)->latest()->pluck('name', 'id');
//        $data = App\Material::latest()->pluck('name', 'id');
    } else {
        $data = \App\Material::where('user_id', \Auth::id())->latest()->pluck('name', 'id');
    }
    return (App\Material::get()->isEmpty() != true) ? $data : ['' => 'No Material currently'];
}

function get_material_name_by_id($id) {
    $companies_list = DB::table('materials')->where('id', $id)->first();
    return ($companies_list != null) ? $companies_list->name : 'NA';
}

function get_categories_list() {
    if (\Auth::user()->roles->first()->name == 'super_admin') {
        $data = App\Category::pluck('name', 'id');
    } else if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
        $company_users = \App\User::where('company_id', \Auth::user()->company_id)->pluck('id')->toArray();
        $data = \App\Category::whereIn('user_id', $company_users)->pluck('name', 'id');
    } else if (\Auth::user()->roles->first()->name == 'supplier' && \Auth::user()->company_id != null) {
        $company_users = \App\User::where('company_id', \Auth::user()->company_id)->pluck('id');
        $data = \App\Category::whereIn('user_id', $company_users)->pluck('name', 'id');
//        $data = App\Category::pluck('name', 'id');
    } else {
        $data = \App\Category::where('user_id', \Auth::id())->pluck('name', 'id');
    }
    return (App\Category::get()->isEmpty() != true) ? $data : ['' => 'No Category currently'];
}

function get_category_name_by_id($id) {
    $category_list = DB::table('categories')->where('id', $id)->where('deleted_at', null)->first();
    return ($category_list != null) ? $category_list->name : 'NA';
}

function get_order_status_list() {
//    if (\Auth::user()->roles->first()->name == 'super_admin') {
    $data = App\Models\OrderStatus::pluck('name', 'id');
//    } else if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
//        $company_users = \App\User::where('company_id', \Auth::user()->company_id)->pluck('id')->toArray();
//        $data = \App\Models\OrderStatus::whereIn('user_id', $company_users)->pluck('name', 'id');
//    } else if (\Auth::user()->roles->first()->name == 'supplier' && \Auth::user()->company_id != null) {
//        $company_users = \App\User::where('company_id', \Auth::user()->company_id)->pluck('id');
//        $data = \App\Models\OrderStatus::whereIn('user_id', $company_users)->pluck('name', 'id');
//    } else {
//        $data = \App\Models\OrderStatus::where('user_id', \Auth::id())->pluck('name', 'id');
//    }
    return (App\Models\OrderStatus::get()->isEmpty() != true) ? $data : ['' => 'No Order Status currently'];
}

function get_order_status_name_by_id($id) {
    $list = DB::table('order_statuses')->where('id', $id)->where('deleted_at', null)->first();
    return ($list != null) ? $list->name : 'NA';
}

function get_order_status_name_by_order_id($id) {
    $list = DB::table('order_statuses')->where('id', (\App\Order::where('id', $id)->first()) ? \App\Order::where('id', $id)->first()->confirmed : null)->where('deleted_at', null)->first();
    return ($list != null) ? $list->name : 'NA';
}

function get_order_status_name_by_order_id_using_in_overview($id) {
    $list = DB::table('order_statuses')->where('id', (\App\Order::where('id', $id)->first()) ? \App\Order::where('id', $id)->first()->confirmed : null)->where('deleted_at', null)->first();
    return ($list != null) ? $list->name : 'N/A';
}

function get_quote_status_list() {
//    if (\Auth::user()->roles->first()->name == 'super_admin') {
    $data = App\Models\QuoteStatus::pluck('name', 'id');
//    } else if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
//        $company_users = \App\User::where('company_id', \Auth::user()->company_id)->pluck('id')->toArray();
//        $data = \App\Models\QuoteStatus::whereIn('user_id', $company_users)->pluck('name', 'id');
//    } else if (\Auth::user()->roles->first()->name == 'supplier' && \Auth::user()->company_id != null) {
//        $company_users = \App\User::where('company_id', \Auth::user()->company_id)->pluck('id');
//        $data = \App\Models\QuoteStatus::whereIn('user_id', $company_users)->pluck('name', 'id');
//
//    } else {
//        $data = \App\Models\QuoteStatus::where('user_id', \Auth::id())->pluck('name', 'id');
//    }
    return (App\Models\QuoteStatus::get()->isEmpty() != true) ? $data : ['' => 'No Quote Status currently'];
}

function get_quote_status_name_by_id($id) {
    $list = DB::table('quote_statuses')->where('id', $id)->where('deleted_at', null)->first();
    return ($list != null) ? $list->name : 'NA';
}

function get_package_type_list() {
    $data = App\Models\PackageType::pluck('name', 'id');
    return (App\Models\PackageType::get()->isEmpty() != true) ? $data : ['' => 'No Package Type currently'];
}

function get_package_type_name_by_id($id) {
    $list = DB::table('package_types')->where('id', $id)->where('deleted_at', null)->first();
    return ($list != null) ? $list->name : 'NA';
}

function get_package_type_id_by_name($name) {
    $list = DB::table('package_types')->where('name', $name)->where('deleted_at', null)->first();
    return ($list != null) ? $list->id : 'NA';
}

function get_ethical_audits_list() {
    $data = App\Models\EthicalAudit::pluck('name', 'id');
    return (App\Models\PackageType::get()->isEmpty() != true) ? $data : ['' => 'No Types currently'];
}

function get_ethical_audit_name_by_id($id) {
    $list = DB::table('ethical_audits')->where('id', $id)->where('deleted_at', null)->first();
    return ($list != null) ? $list->name : 'NA';
}

function get_shipping_status_list() {
    $data = App\Models\ShippingStatus::pluck('name', 'id');
    return (App\Models\ShippingStatus::get()->isEmpty() != true) ? $data : ['' => 'No Status currently'];
}

function get_shipping_status_name_by_id($id) {
    $list = DB::table('shipping_statuses')->where('id', $id)->where('deleted_at', null)->first();
    return ($list != null) ? $list->name : 'NA';
}

function get_product_type_list() {
    if (\Auth::user()->roles->first()->name == 'super_admin') {
        $data = App\ProductType::latest()->pluck('name', 'id');
    } else if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
        $company_users = \App\User::where('company_id', \Auth::user()->company_id)->latest()->pluck('id')->toArray();
        $data = \App\ProductType::whereIn('user_id', $company_users)->latest()->pluck('name', 'id');
    } else if (\Auth::user()->roles->first()->name == 'supplier' && \Auth::user()->company_id != null) {
        $company_users = \App\User::where('company_id', \Auth::user()->company_id)->pluck('id');
        $data = \App\ProductType::whereIn('user_id', $company_users)->latest()->pluck('name', 'id');
//        $data = App\ProductType::latest()->pluck('name', 'id');
    } else {
        $data = \App\ProductType::where('user_id', \Auth::id())->latest()->pluck('name', 'id');
    }
    return (App\ProductType::get()->isEmpty() != true) ? $data : ['' => 'No Type currently'];
}

function get_product_type_name_by_id($id) {
    $product_types_list = DB::table('product_types')->where('id', $id)->where('deleted_at', null)->first();
    return ($product_types_list != null) ? $product_types_list->name : 'NA';
}

function get_user_count() {
    if (\Auth::user()->roles->first()->name == 'super_admin') {
        $data = \App\User::get()->toArray();
    } else if (\Auth::user()->roles->first()->name == 'company_user') {
        $company_users = \App\User::where('company_id', \Auth::user()->company_id)->pluck('id')->toArray();
//        $data = \App\User::whereIn('id', $company_users)->whereHas('roles', function ($query) {
//                    $query->where('name', '=', 'company_user');
//                })->get()->toArray();
        $data = \App\User::whereIn('id', $company_users)->latest()->get();
    } else {
        $data = \App\User::where('user_id', \Auth::id())->get()->toArray();
    }
    return count($data);
}

function get_new_users_count() {
    if (\Auth::user()->roles->first()->name == 'super_admin') {
        $data = App\User::whereDoesntHave('roles')->latest()->get()->toArray();
    } else {
        $data = App\User::whereDoesntHave('roles')->latest()->get()->toArray();
    }
    return count($data);
}

function get_user_name_by_id($id) {
    $user = DB::table('users')->where('id', $id)->first();
    return ($user != null) ? $user->username : 'NA';
}

function get_suppliers_count() {
    if (\Auth::user()->roles->first()->name == 'super_admin') {
        $data = App\Supplier::get()->toArray();
    } else if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
        $company_users = \App\User::where('company_id', \Auth::user()->company_id)->pluck('id')->toArray();
        $data = App\Supplier::whereIn('user_id', $company_users)->get()->toArray();
    } else if (\Auth::user()->roles->first()->name == 'supplier' && \Auth::user()->supplier_id != null) {
//        $suppliersId = \App\User::where('supplier_id', \Auth::user()->supplier_id)->pluck('id');
        $data = App\Supplier::where('id', \Auth::user()->supplier_id)->get();
//                dd($suppliersId);
    } else {
        $data = App\Supplier::where('user_id', \Auth::id())->get()->toArray();
    }
    return count($data);
}

function get_contacts_count() {
    if (\Auth::user()->roles->first()->name == 'super_admin') {
        $data = App\Contact::get()->toArray();
    } else if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
        $company_users = \App\User::where('company_id', \Auth::user()->company_id)->pluck('id')->toArray();
        $data = App\Contact::whereIn('user_id', $company_users)->get()->toArray();
    } else if (\Auth::user()->roles->first()->name == 'supplier' && \Auth::user()->supplier_id != null) {
        $suppliersId = \App\User::where('supplier_id', \Auth::user()->supplier_id)->pluck('id');
        $data = App\Contact::whereIn('user_id', $suppliersId)->latest()->get();
//                dd($suppliersId);
    } else {
        $data = App\Contact::where('user_id', \Auth::id())->get()->toArray();
    }
    return count($data);
}

function get_orders_count() {
    if (\Auth::user()->roles->first()->name == 'super_admin') {
        $data = App\Order::get()->toArray();
    } else if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
        $company_users = \App\User::where('company_id', \Auth::user()->company_id)->pluck('id')->toArray();
        $data = App\Order::whereIn('user_id', $company_users)->get()->toArray();
    } else {
        $data = App\Order::where('user_id', \Auth::id())->get()->toArray();
    }
    return count($data);
}

function get_product_count() {
    if (\Auth::user()->roles->first()->name == 'super_admin') {
        $data = App\Product::get()->toArray();
    } else if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
        $company_users = \App\User::where('company_id', \Auth::user()->company_id)->pluck('id')->toArray();
        $data = App\Product::whereIn('user_id', $company_users)->get()->toArray();
    } else if (\Auth::user()->roles->first()->name == 'supplier' && \Auth::user()->supplier_id != null) {
        $suppliersId = \App\User::where('supplier_id', \Auth::user()->supplier_id)->pluck('id');
        $data = App\Product::whereIn('user_id', $suppliersId)->latest()->get();
//                dd($suppliersId);
    } else {
        $data = App\Product::where('user_id', \Auth::id())->get()->toArray();
    }
    return count($data);
}

function get_quotes_count() {
    if (\Auth::user()->roles->first()->name == 'super_admin') {
        $data = App\Quote::get()->toArray();
    } else if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
        $company_users = \App\User::where('company_id', \Auth::user()->company_id)->pluck('id')->toArray();
        $data = App\Quote::whereIn('user_id', $company_users)->get()->toArray();
    } else if (\Auth::user()->roles->first()->name == 'supplier' && \Auth::user()->supplier_id != null) {
        $suppliersId = \App\User::where('supplier_id', \Auth::user()->supplier_id)->pluck('id');
        $data = App\Quote::whereIn('user_id', $suppliersId)->latest()->get();
//                dd($suppliersId);
    } else {
        $data = App\Quote::where('user_id', \Auth::id())->get()->toArray();
    }
    return count($data);
}

function get_order_count() {
    $data = App\Order::get()->toArray();
    return count($data);
}

function get_estimate_order_count() {
    $data = \App\EstimateOrder::get()->toArray();
    return count($data);
}

function get_margins_count() {
    if (\Auth::user()->roles->first()->name == 'super_admin') {
        $data = App\Margin::get()->toArray();
    } else if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
        $data = App\Margin::where('company_id', \Auth::user()->company_id)->get()->toArray();
    } else {
        $data = App\Margin::where('user_id', \Auth::id())->get()->toArray();
    }
    return count($data);
}

function get_costs_count() {

    if (\Auth::user()->roles->first()->name == 'super_admin') {
        $data = App\Cost::get()->toArray();
    } else if (\Auth::user()->roles->first()->name == 'company_user') {
        $company_users = \App\User::where('company_id', \Auth::user()->company_id)->pluck('id')->toArray();
        $data = App\Cost::whereIn('user_id', $company_users)->get()->toArray();
    } else {
        $data = App\Cost::where('user_id', \Auth::id())->get()->toArray();
    }
    return count($data);
}

function get_containers_count() {
    $data = \App\Container::get()->toArray();
    return count($data);
}

function get_materials_count() {
    if (\Auth::user()->roles->first()->name == 'super_admin') {
        $data = App\Material::get()->toArray();
    } else if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
        $company_users = \App\User::where('company_id', \Auth::user()->company_id)->pluck('id')->toArray();
        $data = App\Material::whereIn('user_id', $company_users)->get()->toArray();
    } else {
        $data = App\Material::where('user_id', \Auth::id())->get()->toArray();
    }
    return count($data);
}

function get_categories_count() {
    if (\Auth::user()->roles->first()->name == 'super_admin') {
        $data = \App\Category::get()->toArray();
    } else if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
        $company_users = \App\User::where('company_id', \Auth::user()->company_id)->pluck('id')->toArray();
        $data = \App\Category::whereIn('user_id', $company_users)->get()->toArray();
    } else if (\Auth::user()->roles->first()->name == 'supplier' && \Auth::user()->company_id != null) {
        $company_users = \App\User::where('company_id', \Auth::user()->company_id)->pluck('id');
        $data = App\Category::whereIn('user_id', $company_users)->latest()->get();
//                dd($data);
    } else {
        $data = \App\Category::where('user_id', \Auth::id())->get()->toArray();
    }
    return count($data);
}

function get_quote_categories_count() {
    if (\Auth::user()->roles->first()->name == 'super_admin') {
        $data = \App\QuoteCategory::get()->toArray();
    } else if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
        $company_users = \App\User::where('company_id', \Auth::user()->company_id)->pluck('id')->toArray();
        $data = \App\QuoteCategory::whereIn('user_id', $company_users)->get()->toArray();
    } else {
        $data = \App\QuoteCategory::where('user_id', \Auth::id())->get()->toArray();
    }
    return count($data);
}

function get_supplier_categories_count() {
    if (\Auth::user()->roles->first()->name == 'super_admin') {
        $data = \App\Models\SupplierCategory::get()->toArray();
    } else if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
        $company_users = \App\User::where('company_id', \Auth::user()->company_id)->pluck('id')->toArray();
        $data = \App\Models\SupplierCategory::whereIn('user_id', $company_users)->get()->toArray();
    } else {
        $data = \App\Models\SupplierCategory::where('user_id', \Auth::id())->get()->toArray();
    }
    return count($data);
}

function get_shipping_port_count() {
    if (\Auth::user()->roles->first()->name == 'super_admin') {
        $data = App\ShippingPort::get()->toArray();
    } else if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
        $company_users = \App\User::where('company_id', \Auth::user()->company_id)->pluck('id')->toArray();
        $data = App\ShippingPort::whereIn('user_id', $company_users)->get()->toArray();
    } else {
        $data = App\ShippingPort::where('user_id', \Auth::id())->get()->toArray();
    }
    return count($data);
}

function get_shipping_rates_count() {
    if (\Auth::user()->roles->first()->name == 'super_admin') {
        $data = App\ShippingRate::get()->toArray();
    } else if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
        $company_users = \App\User::where('company_id', \Auth::user()->company_id)->pluck('id')->toArray();
        $data = App\ShippingRate::whereIn('user_id', $company_users)->get()->toArray();
    } else {
        $data = App\ShippingRate::where('user_id', \Auth::id())->get()->toArray();
    }
    return count($data);
}

function get_fis_templates_count() {
    if (\Auth::user()->roles->first()->name == 'super_admin') {
        $data = App\FisTemplate::get()->toArray();
    } else if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
        $company_users = \App\User::where('company_id', \Auth::user()->company_id)->pluck('id')->toArray();
        $data = App\FisTemplate::whereIn('user_id', $company_users)->get()->toArray();
    } else {
        $data = App\FisTemplate::where('user_id', \Auth::id())->get()->toArray();
    }
    return count($data);
}

function get_currency_exchange_rate_count() {
    if (\Auth::user()->roles->first()->name == 'super_admin') {
        $data = App\CurrencyExchangeRate::get()->toArray();
    } else if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
        $company_users = \App\User::where('company_id', \Auth::user()->company_id)->pluck('id')->toArray();
        $data = App\CurrencyExchangeRate::whereIn('user_id', $company_users)->get()->toArray();
    } else {
        $data = App\CurrencyExchangeRate::where('user_id', \Auth::id())->get()->toArray();
    }
    return count($data);
}

function get_product_types_count() {
    if (\Auth::user()->roles->first()->name == 'super_admin') {
        $data = \App\ProductType::get()->toArray();
    } else if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
        $company_users = \App\User::where('company_id', \Auth::user()->company_id)->pluck('id')->toArray();
        $data = \App\ProductType::whereIn('user_id', $company_users)->get()->toArray();
    } else {
        $data = \App\ProductType::where('user_id', \Auth::id())->get()->toArray();
    }
    return count($data);
}

function get_companies_count() {
    $data = DB::table('companies')->get()->toArray();
    return count($data);
}

function get_supplier_contacts_count($supplier_id) {
//            $data = DB::table('supplier_contacts')->where('supplier_id',$supplier_id)->where('deleted_at',null)->get()->toArray();
//            return count($data);
    $data = App\SupplierContact::where('supplier_id', $supplier_id)->pluck('contact_id');
    $count = \App\Contact::whereIn('id', $data)->where('deleted_at', null)->count();
    return $count;
}

function get_supplier_products_count($supplier_id) {
    $data = \App\SupplierProduct::where('supplier_id', $supplier_id)->pluck('product_id');
    $count = \App\Product::whereIn('id', $data)->where('deleted_at', null)->count();
    return $count;
}

function get_contact_products_count($contact_id) {
    $data = App\ContactProduct::where('contact_id', $contact_id)->pluck('product_id');
    $count = \App\Product::whereIn('id', $data)->where('deleted_at', null)->count();
    return $count;
}

function get_suppliers_count_by_product_id($product_id) {
    $data = \App\SupplierProduct::where('product_id', $product_id)->pluck('supplier_id');
    $count = App\Supplier::whereIn('id', $data)->where('deleted_at', null)->count();
    return $count;
}

function get_supplierdetails_from_contactid($contactid) {
    $data = App\SupplierContact::where('contact_id', $contactid)->pluck('supplier_id');
    $count = App\Supplier::whereIn('id', $data)->where('deleted_at', null)->count();
//        dd($count);
    if ($count > '0') {
        $suppliers = App\Supplier::whereIn('id', $data)->get();

        return view('admin.suppliers.supplierbyid', compact('suppliers'));
    }
}

function get_productdetails_from_supplierid($supplierid) {
    $data = App\SupplierProduct::where('supplier_id', $supplierid)->pluck('product_id');
    $count = \App\Product::whereIn('id', $data)->where('deleted_at', null)->count();
//        dd($count);
    if ($count > '0') {
        $supplierproducts = \App\Product::whereIn('id', $data)->get();

        return view('admin.suppliers.viewsupplierproductsajax', compact('supplierproducts'));
    }
}

function get_contactdetails_from_supplierid($supplierid) {
    $data = App\SupplierContact::where('supplier_id', $supplierid)->pluck('contact_id');
    $count = App\Supplier::whereIn('id', $data)->where('deleted_at', null)->count();
//        dd($count);
    if ($count > '0') {
        $suppliercontacts = \App\Contact::whereIn('id', $data)->get();

        return view('admin.suppliers.viewsuppliercontactsajax', compact('suppliercontacts'));
    }
}

function get_supplier_name_by_contactid($contactid) {
    $data = App\SupplierContact::where('contact_id', $contactid)->pluck('supplier_id');
    $count = App\Supplier::whereIn('id', $data)->where('deleted_at', null)->count();
//        dd($count);
    if ($count > '0') {
        $suppliers = App\Supplier::whereIn('id', $data)->first();
//            dd($suppliers);
        return ($suppliers != null) ? $suppliers->name : 'NA';
    }
    return 'NA';
}

function get_supplier_id_by_contactid($contactid) {
    $data = App\SupplierContact::where('contact_id', $contactid)->pluck('supplier_id');
    $count = App\Supplier::whereIn('id', $data)->where('deleted_at', null)->count();
//        dd($count);
    if ($count > '0') {
        $suppliers = App\Supplier::whereIn('id', $data)->first();
//            dd($suppliers);
        return $suppliers->id;
    }
}

function get_wholesale_margin() {
    if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
        $wholesale_margin = App\Margin::where("name", "Whole Seller's Margin")->where('company_id', \Auth::user()->company_id)->first();
    } else {
        $wholesale_margin = App\Margin::where("name", "Whole Seller's Margin")->where('company_id', null)->first();
    }
    return ($wholesale_margin != null) ? $wholesale_margin->value : 'NA';
}

function get_retail_margin() {
    if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
        $retail_margin = App\Margin::where("name", "Retailer's Margin")->where('company_id', \Auth::user()->company_id)->first();
    } else {
        $retail_margin = App\Margin::where("name", "Retailer's Margin")->where('company_id', null)->first();
    }
    return ($retail_margin != null) ? $retail_margin->value : 'NA';
}

function get_shipping_container_product_count($id) {
    $data = \App\ShippingContainerProduct::where('shipping_plan_id', $id)->get()->toArray();
    return count($data);
}

function get_shipping_container_costs_count($id) {
    $data = \App\Cost::where('order_id', $id)->get()->toArray();
    return count($data);
}

//    function for checking whether value exist or template exist with value or not    
function checkValueExists($column_name, $details) {
//    dd($details);
    if ($details->$column_name != null) {
        return $details->$column_name;
    } else if ($details->template != null && $details->template->template_details != null) {
//        return 'template found';
        if ($details->template->template_details->$column_name != null) {

            return $details->template->template_details->$column_name;
        }
    } else if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
//        return 'company user';
        $company_users = \App\User::where('company_id', \Auth::user()->company_id)->pluck('id')->toArray();
        $template = \App\FisTemplate::whereIn('user_id', $company_users)->where('is_default', '1')->first();
        if ($template != null) {
            return $template->$column_name;
        }
    } else if (\Auth::user()->roles->first()->name == 'super_admin') {
//        return 0;
//        return '232';
        $template = App\FisTemplate::where('user_id', '1')->where('is_default', '1')->first();
        if ($template != null) {
            return $template->$column_name;
        }
    } else {
        return null;
    }
}

//function get_shipping_rate_and_currency_value($shipping_port, $shipping_port_id, $coloumn) {
//    $shipping_rate = App\ShippingRate::where('shipping_port_id', $shipping_port)->where('shipping_to_port_id', $shipping_port_id);
//    return ($shipping_rate->get()->isEmpty() == true) ? null : $shipping_rate->first()->$coloumn;
//}

function checkShippingValuesExists($shipping_port, $shipping_port_id, $details, $column_name) {

    if ($column_name != 'shipping_rate_currency' && $details->shipping_rate != null) {
        return $details->shipping_rate;
    } else if ($column_name == 'shipping_rate_currency' && $details->$column_name != null) {
        return $details->$column_name;
    } else {
        if (\Auth::user()->roles->first()->name == 'super_admin') {
            $shipping_rate = App\ShippingRate::where('shipping_port_id', $shipping_port)->where('shipping_to_port_id', $shipping_port_id);
//            Alternate to whereIn below sub-query
//             $shipping_rate = $shipping_rate->where(function($query) use ($request) {
//                    $query->whereNull('user_id')
//                            ->orWhere('user_id', \Auth::id());
//                });
//            dd($shipping_port,$shipping_port_id,$shipping_rate->first());
        } else if (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) {
            $company_users = \App\User::where('company_id', \Auth::user()->company_id)->pluck('id')->toArray();
            $shipping_rate = App\ShippingRate::whereIn('user_id', $company_users)->where('shipping_port_id', $shipping_port)->where('shipping_to_port_id', $shipping_port_id);
        } else if (\Auth::user()->roles->first()->name == 'supplier') {
            $supplier_users = \App\User::where('company_id', \Auth::user()->company_id)->where('supplier_id', '!=', null)->latest()->pluck('supplier_id')->toArray();
            $shipping_rate = App\ShippingRate::whereIn('user_id', $supplier_users)->where('shipping_port_id', $shipping_port)->where('shipping_to_port_id', $shipping_port_id);
        }
        return ($shipping_rate->get()->isEmpty() == true) ? null : $shipping_rate->first()->$column_name;
    }
}

function convertCurrentToBaseCurrency($base, $current, $value) {
    $converted_currency = \App\CurrencyExchangeRate::where('currency_id_from', $current)->where('currency_id_to', $base)->first();
    if ($converted_currency) {
        return $converted_currency->exchange_rate * $value;
    }
}

function getFile($file, $time = '+20 minutes') {
    //ASSESSING IMAGE FROM S3-PRIVATE
    return $file;

//    $client = Storage::disk('s3')->getDriver()->getAdapter()->getClient();
//    $bucket = Config::get('filesystems.disks.s3.bucket');
//
//    $command = $client->getCommand('GetObject', [
//        'Bucket' => $bucket,
//        'Key' => $file  // file name in s3 bucket which you want to access
//    ]);
//    $request = $client->createPresignedRequest($command, $time);    
    // Get the actual presigned-url
//    $presignedUrl = (string)$request->getUri();
//    return $presignedUrl;
}

function get_name_by_id_and_table_name($table_name, $id, $first_column, $second_column = null) {
    if ($table_name == '' || $id == '')
        return '';

    $row = DB::table($table_name)->where('id', $id)->where('deleted_at', null)->first();
    return ($row != null) ? ($second_column == null ? $row->$first_column : $row->$first_column . ' ' . $row->$second_column) : '';
}

function get_order_lines_product_name_as_string($order_id) {
    $data = App\OrderLine::where('order_id', $order_id)->pluck('product_id');
    if (\Auth::user()->roles->first()->name == 'supplier' && \Auth::user()->supplier_id != null) {
        $suppliersId = \App\User::where('supplier_id', \Auth::user()->supplier_id)->pluck('id');

        $getAssociatedProducts = \App\Product::where('supplier_id', \Auth::user()->supplier_id)->pluck('id');

        $data = \App\OrderLine::whereIn('product_id', $getAssociatedProducts)->where('order_id', $order_id)->pluck('product_id');
//            dd($data);
        return ($data != '') ? $data : 'NA';
    }
    return ($data != '') ? $data : 'NA';
}

function get_order_lines_shipping_plan_product_name_as_string($shipping_plan_id) {
    if (\Auth::user()->roles->first()->name == 'supplier' && \Auth::user()->supplier_id != null) {

        $data = App\ShippingContainerProduct::where('shipping_plan_id', $shipping_plan_id)->where('supplier_id', \Auth::user()->supplier_id)->pluck('product_name');
    } else {
        $data = App\ShippingContainerProduct::where('shipping_plan_id', $shipping_plan_id)->pluck('product_name');
    }


    $arr = [];
    $i = 1;
    foreach ($data as $k => $val) {
        $res = \App\Product::where('name', $val)->first();
        if ($res != '') {
            $prod_with_suplier = '/' . get_supplier_name_by_id($res->supplier_id);
        } else {
            $prod_with_suplier = '';
        }
        array_push($arr, '<b>' . $i . '.</b> ' . $val . '' . $prod_with_suplier . '<br>');
        $i++;
    }
    $res = '<p style="color: #000; white-space: break-spaces;width: 250px;">' . implode('', $arr);
    return $res;
//        return ($res != '') ? $res : 'NA';
}

//    function get_order_lines_shipping_plan_product_length($shipping_plan_id) {
////        dd($shipping_plan_id);  
//           if (\Auth::user()->roles->first()->name == 'supplier' && \Auth::user()->supplier_id != null)      {
//              
//        $data = App\ShippingContainerProduct::where('shipping_plan_id',$shipping_plan_id)->where('supplier_id',\Auth::user()->supplier_id)->count();
//        if($data < 0){
//            return true;
//        }else{
//            return false;
//        }
//        
//           }
//       
//        
//        
////        return ($res != '') ? $res : 'NA';
//    }
//     function get_order_lines_shipping_plan_product_length($shipping_plan_id) {
//////        dd($shipping_plan_id);  
//           if (\Auth::user()->roles->first()->name == 'supplier' && \Auth::user()->supplier_id != null)      {
//              $checkShipping = App\ShippingPlan::where('id',$shipping_plan_id)->first();
//              $checkShippingContainerPlan = App\ShippingContainerProduct::where('shipping_plan_id',$shipping_plan_id)->get();
////              dd($checkShipping);
//                if($checkShipping->user_id == \Auth::id() || $checkShippingContainerPlan->shipping_plan_id = $shipping_plan_id){
//                    $data = App\ShippingContainerProduct::where('supplier_id',\Auth::user()->supplier_id)->count();
//                    if($data > 0){
//                        return true;
//                    }else{
//                        return false;
//                    }
//                }
//        
//           }
//       
//        return false;
////        
////       return ($res != '') ? $res : 'NA';
//    }

function get_order_lines_shipping_plan_product_length($shipping_plan_id) {
    //       dd($shipping_plan_id);  
    if (\Auth::user()->roles->first()->name == 'supplier' && \Auth::user()->supplier_id != null) {

        $product_exist = App\ShippingContainerProduct::where('shipping_plan_id', $shipping_plan_id)->pluck('order_line_id');
//              dd($product_exists);

        $order_line = App\OrderLine::whereIn('id', $product_exist)->pluck('product_id');
        $product = App\Product::whereIn('id', $order_line)->first();
        $product_count = App\Product::whereIn('id', $order_line)->where('supplier_id', \Auth::user()->supplier_id)->count();

        if ($product != '' && $product != null) {

            if ($product->supplier_id == \Auth::user()->supplier_id || $product_count > 0) {
                $show = 'yes';
            } else {
                $show = 'no';
            }
            $product_exists = App\ShippingPlan::where('id', $shipping_plan_id)->where('user_id', \Auth::id())->first();

            if ($product_exists != '' && $product_exists != null || $show == 'yes') {
                $data = App\ShippingContainerProduct::where('supplier_id', \Auth::user()->supplier_id)->count();
                if ($data > 0 || $product_exists != '' && $product_exists != null) {
                    return true;
                } else {
                    return false;
                }
            }
        }


        $product_exists = App\ShippingPlan::where('id', $shipping_plan_id)->where('user_id', \Auth::id())->first();
//              if($product_exists != '' && $product_exists != null ){
//                    $data = App\ShippingContainerProduct::where('supplier_id',\Auth::user()->supplier_id)->count();
//                    if($data > 0){
//                         return true;
//                     }else{
//                         return false;
//                     }
//              }
//                      
//              return false;     
//        
//           }
        if ($product_exists != '' && $product_exists != null) {
            return true;
        } else {
            return false;
        }
    }

    return false;
}
