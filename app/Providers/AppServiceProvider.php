<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Request;

class AppServiceProvider extends ServiceProvider {

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        // dd(Request::getHttpHost());
       // if (Request::getHttpHost() != 'localhost' && Request::getHttpHost() != '3.219.184.206') {
          //  \URL::forceScheme('https');
//  $this->app['request']->server->set('HTTPS','on');
        //}
    }

}
