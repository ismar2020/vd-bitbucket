<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class OrderLine extends Model
{
    use LogsActivity;
    use SoftDeletes;
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'order_lines';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */

    
    protected $fillable = ['order_id', 'product_id', 'product_name', 'product_image', 'shipping_port_id', 'product_unit_price',  'product_unit_currency', 'currency_exchange_rate', 'pricing_currency', 'shipping_to_port_id', 'container_size','packing_quantity', 'order_quantity','landed_product_price', 'purchase_cost', 'wholesale_margin_percentage',  'wholesale_cost', 'retail_margin_percentage', 'retail_cost', 'confirmed', 'active', 'shipping_rate', 'shipping_rate_currency', 'transport', 'transport_currency', 'port_costs', 'port_costs_currency', 'insurance_cost', 'insurance_cost_currency', 'import_custom_duties_percentage', 'import_taxes_percentage', 'handing_costs_in_destination_terminal', 'handing_costs_currency', 'costs_of_shipping_after_destination_port', 'shipping_after_port_currency', 'import_custom_duties_value', 'import_taxes_value', 'total_landed_costs', 'order_pricing_costs_total_fob', 'costs_from_factory_fob', 'costs_from_factory_currency_fob', 'custom_and_other_costs_fob', 'custom_and_other_costs_currency_fob', 'gross_profit_value', 'domestic_transport', 'domestic_transport_currency', 'storage_overhead', 'storage_overhead_currency', 'merchandising', 'merchandising_currency', 'other_overheads', 'other_overheads_currency', 'total_landed_costs_fis'];

    

    /**
     * Change activity log event description
     *
     * @param string $eventName
     *
     * @return string
     */
    public function getDescriptionForEvent($eventName)
    {
        return __CLASS__ . " model has been {$eventName}";
    }
    public function template(){
        return $this->hasOne(FisTemplateProducts::class,'product_id', 'product_id')->with('template_details');
    }
    //Mutators
    //for UOM
    public function setOrderQuantityAttribute($value){
        $this->attributes['order_quantity'] = str_replace( ',','', $value);
    }
    //for GP%
    public function setWholesaleMarginPercentageAttribute($value){
        $this->attributes['wholesale_margin_percentage'] = str_replace( ',','', $value);
    }
    //for landed product price
    public function setLandedProductPriceAttribute($value){
        $this->attributes['landed_product_price'] = str_replace( ',','', $value);
    }
    //for gp$
    public function setGrossProfitValueAttribute($value){
        $this->attributes['gross_profit_value'] = str_replace( ',','', $value);
    }
    //for price to customer
    public function setWholesaleCostAttribute($value){
        $this->attributes['wholesale_cost'] = str_replace( ',','', $value);
    }
    //for RCM
    public function setRetailMarginPercentageAttribute($value){
        $this->attributes['retail_margin_percentage'] = str_replace( ',','', $value);
    }
    //for RRP
    public function setRetailCostAttribute($value){
        $this->attributes['retail_cost'] = str_replace( ',','', $value);
    }
    //for TotalLandedCostsFis
    public function setTotalLandedCostsFisAttribute($value){
        $this->attributes['total_landed_costs_fis'] = str_replace( ',','', $value);
    }
    //for TotalLandedCosts
    public function setTotalLandedCostsAttribute($value){
        $this->attributes['total_landed_costs'] = str_replace( ',','', $value);
    }
    //for PackingQuantity
    public function setPackingQuantityAttribute($value){
        $this->attributes['packing_quantity'] = str_replace( ',','', $value);
    }
    //for ShippingRate
    public function setShippingRateAttribute($value){
        $this->attributes['shipping_rate'] = str_replace( ',','', $value);
    }
    //for Transport
    public function setTransportAttribute($value){
        $this->attributes['transport'] = str_replace( ',','', $value);
    }
    //for PortCosts
    public function setPortCostsAttribute($value){
        $this->attributes['port_costs'] = str_replace( ',','', $value);
    }
    //for InsuranceCost
    public function setInsuranceCostAttribute($value){
        $this->attributes['insurance_cost'] = str_replace( ',','', $value);
    }
    //for ImportCustomDutiesPercentage
    public function setImportCustomDutiesPercentageAttribute($value){
        $this->attributes['import_custom_duties_percentage'] = str_replace( ',','', $value);
    }
    //for ImportTaxesPercentage
    public function setImportTaxesPercentageAttribute($value){
        $this->attributes['import_taxes_percentage'] = str_replace( ',','', $value);
    }
    //for HandingCostsInDestinationTerminal
    public function setHandingCostsInDestinationTerminalAttribute($value){
        $this->attributes['handing_costs_in_destination_terminal'] = str_replace( ',','', $value);
    }
    //for CostsOfShippingAfterDestinationPort
    public function setCostsOfShippingAfterDestinationPortAttribute($value){
        $this->attributes['costs_of_shipping_after_destination_port'] = str_replace( ',','', $value);
    }
    //for DomesticTransport
    public function setDomesticTransportAttribute($value){
        $this->attributes['domestic_transport'] = str_replace( ',','', $value);
    }
    //for StorageOverhead
    public function setStorageOverheadAttribute($value){
        $this->attributes['storage_overhead'] = str_replace( ',','', $value);
    }
    //for Merchandising
    public function setMerchandisingAttribute($value){
        $this->attributes['merchandising'] = str_replace( ',','', $value);
    }
    //for OtherOverheads
    public function setOtherOverheadsAttribute($value){
        $this->attributes['other_overheads'] = str_replace( ',','', $value);
    }
//    
    //code setting multiple mutators together for multiple fields
//    public function __set($key, $value)
//    {
//        if(in_array($key, ['order_quantity', 'wholesale_margin_percentage'])){
//            $this->attributes[$key] = str_replace( ',','', $value);
//        } else {
//            //do what Laravel normally does
//            $this->setAttribute($key, $value);
//        }
//    }
    
}
