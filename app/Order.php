<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Order extends Model
{
    use LogsActivity;
    use SoftDeletes;
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'orders';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'purchase_order', 'shiiping_port', 'product_unit_quantity', 'container', 'purchase_cost', 'wholesale_cost', 'retail_cost', 'total_number_of_containers', 'total_purchase_cost', 'total_wholesale_cost', 'total_retail_cost', 'confirmed', 'active', 'user_id', 'order_type', 'costs_from_factory', 'costs_from_factory_currency', 'custom_and_other_costs', 'custom_and_other_costs_currency', 'order_pricing_costs_fob_total', 'pricing_currency','po_type','po_number'];

    

    /**
     * Change activity log event description
     *
     * @param string $eventName
     *
     * @return string
     */
    public function getDescriptionForEvent($eventName)
    {
        return __CLASS__ . " model has been {$eventName}";
    }
    
    public function order_lines(){
        return $this->hasMany(OrderLine::class,'order_id', 'id');
    }
}
