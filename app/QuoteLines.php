<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class QuoteLines extends Model {

    use LogsActivity;
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'quote_lines';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    
    
//     protected $appends = ['packaging_options'];


    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['quote_id', 'product_id', 'product_name', 'product_length', 'product_width', 'product_height', 'product_weight', 'product_material', 'shipping_port', 'supplier', 'product_quantity_per_20ft', 'product_quantity_per_20fthq', 'product_quantity_per_40ft', 'product_quantity_per_40fthq', 'product_image', 'product_unit_price', 'product_unit_currency', 'wholesale_margin_percentage', 'wholesale_cost', 'retail_margin_percentage', 'retail_cost', 'confirmed', 'active', 'user_id', 'product_cbm', 'product_quantity_per_box', 'product_box_length', 'product_box_width', 'product_box_height', 'product_box_weight', 'product_box_cbm', 'boxes_per_stack'];

    /**
     * Change activity log event description
     *
     * @param string $eventName
     *
     * @return string
     */
    public function getDescriptionForEvent($eventName) {
        return __CLASS__ . " model has been {$eventName}";
    }
    
    public function packaging_options(){
        return $this->hasMany(Models\ProductPackagingOption::class,'product_id', 'product_id')->limit(5);
    }
    
    public function setProductUnitPriceAttribute($value){
        $this->attributes['product_unit_price'] = str_replace( ',','', $value);
    }

}
