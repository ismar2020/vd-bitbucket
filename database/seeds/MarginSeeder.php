<?php

use Illuminate\Database\Seeder;

class MarginSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $margin_data = [
            'name' => "Retailer's Margin",
            'value' => '10',
        ];
        $margin = \App\Margin::create($margin_data);
        $wholesale_data = [
            'name' => "Whole Seller's Margin",
            'value' => '5',
        ];
        $margin = \App\Margin::create($wholesale_data);
    }

}
