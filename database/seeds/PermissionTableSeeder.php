<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\Permission;

class PermissionTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');

        $collection = \Route::getRoutes();
        $routes = [];
        try {
//            $roleU = Role::findByName('super-admin');
            $roleU = Role::where('name', 'super_admin')->first();
            echo 'There is already role super_admin with ID ' . $roleU->id;
        } catch (\Exception $ex) {
            $roleU = Role::create(['name' => 'super_admin', 'label' => 'admin']);
        }
        foreach ($collection as $route) {
            try {
                Permission::create(['name' => $route->uri(), 'label' => 'admin']);
                $roleU->givePermissionTo($route->uri());
                $routes[] = $route->uri();
                echo '<br> Permistion granted to ' . $route->uri().'<br> ';
            } catch (\Exception $ex) {
                continue;
            }
        }
    }

}