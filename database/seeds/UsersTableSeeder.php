<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
      $data = [
          'username' => 'admin',
          'first_name' => 'super',
          'last_name' => 'admin',
          'email' => 'admin@stock.com',
          'active' => '1',
          'password' => Hash::make('12345678'),
          // 'phone' => '98166422',

      ];
      $user = \App\User::create($data);
      $user->assignRole('super_admin');
    }

}
