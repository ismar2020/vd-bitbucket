<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddContainerFieldsToShippingRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shipping_rates', function (Blueprint $table) {
            $table->string('date_valid_from')->nullable();
            $table->string('date_valid_until')->nullable();
            $table->double('shipping_rate_20ft')->nullable();
            $table->double('shipping_rate_20fthq')->nullable();
            $table->double('shipping_rate_40ft')->nullable();
            $table->double('shipping_rate_40fthq')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shipping_rates', function (Blueprint $table) {
            $table->string('date_valid_from')->nullable();
            $table->string('date_valid_until')->nullable();
            $table->double('shipping_rate_20ft')->nullable();
            $table->double('shipping_rate_20fthq')->nullable();
            $table->double('shipping_rate_40ft')->nullable();
            $table->double('shipping_rate_40fthq')->nullable();
        });
    }
}
