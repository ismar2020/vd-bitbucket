<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewFieldsToShippingPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shipping_plans', function (Blueprint $table) {
            $table->string('bl_number')->nullable();
            $table->string('description')->nullable();
            $table->string('port_of_discharge')->nullable();
            $table->string('cargo_ready_date')->nullable();
            $table->string('dispatched_date')->nullable();
            $table->string('eta_date')->nullable();
            $table->string('total_cost')->nullable();
            $table->string('customer_buy_price')->nullable();
            $table->string('profit')->nullable();
            $table->string('status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shipping_plans', function (Blueprint $table) {
            //
        });
    }
}
