<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEstimateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estimate_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('quick_container_type')->nullable();
            $table->double('number_of_containers')->nullable();
            $table->integer('cost_id')->nullable()->unsigned()->index();
            $table->foreign('cost_id')->nullable()->references('id')->on('costs')->onDelete('cascade');
            $table->double('product_price_per_unit')->nullable();
            $table->integer('product_currency')->nullable();
            $table->bigInteger('user_id')->nullable()->unsigned()->index();
            $table->foreign('user_id')->nullable()->references('id')->on('users')->onDelete('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('estimate_orders');
    }
}
