<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupplierCertificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier_certifications', function (Blueprint $table) {
            $table->id();
            $table->softDeletes();
            $table->integer('supplier_id')->nullable();
            $table->string('ethical_audit')->nullable();
            $table->string('audit_name')->nullable();
            $table->string('certificate_comment')->nullable();
            $table->string('user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_certifications');
    }
}
