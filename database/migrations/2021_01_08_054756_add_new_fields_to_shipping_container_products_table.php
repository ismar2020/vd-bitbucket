<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewFieldsToShippingContainerProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shipping_container_products', function (Blueprint $table) {
            $table->integer('product_id')->nullable();
            $table->integer('supplier_id')->nullable();
            $table->string('using')->nullable();
            $table->string('quantity')->nullable();
            $table->string('confirmed_quantity')->nullable();
            $table->string('total_cost')->nullable();
            $table->string('customer_buy_price')->nullable();
            $table->string('profit')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shipping_container_products', function (Blueprint $table) {
            //
        });
    }
}
