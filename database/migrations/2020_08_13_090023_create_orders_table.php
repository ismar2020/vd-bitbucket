<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('name')->nullable();
            $table->string('purchase_order')->nullable();
            $table->string('shiiping_port')->nullable();
            $table->double('product_unit_quantity')->nullable();
            $table->integer('container')->nullable()->unsigned()->index();
            $table->foreign('container')->nullable()->references('id')->on('containers')->onDelete('cascade');
            $table->double('purchase_cost')->nullable();
            $table->double('wholesale_cost')->nullable();
            $table->double('retail_cost')->nullable();
            $table->double('total_number_of_containers')->nullable();
            $table->double('total_purchase_cost')->nullable();
            $table->double('total_wholesale_cost')->nullable();
            $table->double('total_retail_cost')->nullable();
            $table->integer('confirmed')->nullable();
            $table->integer('active')->nullable();
            $table->bigInteger('user_id')->nullable()->unsigned()->index();
            $table->foreign('user_id')->nullable()->references('id')->on('users')->onDelete('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
