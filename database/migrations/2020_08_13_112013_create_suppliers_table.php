<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suppliers', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('name')->nullable();
            $table->text('street_address')->nullable();
            $table->string('suburb')->nullable();
            $table->string('postcode')->nullable();
            $table->string('city')->nullable();
            $table->integer('country')->nullable();
            $table->integer('phone_prefix')->nullable();
            $table->double('phone')->nullable();
            $table->string('email')->nullable();
            $table->text('picture_company')->nullable();
            $table->text('picture_card_front')->nullable();
            $table->text('picture_card_back')->nullable();
            $table->integer('contact_id')->nullable()->unsigned()->index();
            $table->foreign('contact_id')->nullable()->references('id')->on('contacts')->onDelete('cascade');
            $table->integer('product_id')->nullable()->unsigned()->index();
            $table->foreign('product_id')->nullable()->references('id')->on('products')->onDelete('cascade');
            $table->integer('active')->nullable();
            $table->string('scanned')->nullable();
            $table->integer('verified')->nullable();
            $table->bigInteger('user_id')->nullable()->unsigned()->index();
            $table->foreign('user_id')->nullable()->references('id')->on('users')->onDelete('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('suppliers');
    }
}
