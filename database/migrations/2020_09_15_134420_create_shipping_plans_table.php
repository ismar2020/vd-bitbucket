<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateShippingPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping_plans', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('order_id')->nullable();
            $table->integer('container_id')->nullable();
            $table->double('purchase_order_number')->nullable();
            $table->string('shipping_to_port')->nullable();
            $table->string('shipping_date')->nullable();
            $table->double('container_unit_quantity')->nullable();
            $table->double('container_purchase_cost')->nullable();
            $table->double('container_wholesale_price')->nullable();
            $table->double('container_costs')->nullable();
            $table->string('conatiner_costs_value')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shipping_plans');
    }
}
