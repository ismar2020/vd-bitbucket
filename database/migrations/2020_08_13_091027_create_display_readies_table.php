<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDisplayReadiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('display_readies', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->double('display_length')->nullable();
            $table->double('display_width')->nullable();
            $table->double('display_height')->nullable();
            $table->double('display_max_weight')->nullable();
            $table->double('number_of_displays')->nullable();
            $table->double('product_price_per_unit')->nullable();
            $table->integer('product_currency')->nullable();
            $table->bigInteger('user_id')->nullable()->unsigned()->index();
            $table->foreign('user_id')->nullable()->references('id')->on('users')->onDelete('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('display_readies');
    }
}
