<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateShippingContainerProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping_container_products', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('order_id')->nullable();
            $table->integer('order_line_id')->nullable();
            $table->integer('shipping_plan_id')->nullable();
            $table->string('product_name')->nullable();
            $table->text('product_image')->nullable();
            $table->double('number_of_units_that_fit')->nullable();
            $table->double('container_order_quantity')->nullable();
            $table->double('container_order_quantity_cbm')->nullable();
            $table->double('container_total_cost')->nullable();
            $table->double('container_total_wholesale_price')->nullable();
            $table->double('total_product_units_added_to_other_containers')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shipping_container_products');
    }
}
