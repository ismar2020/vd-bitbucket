<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSupplierProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier_products', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('supplier_id')->nullable()->unsigned()->index();

            $table->foreign('supplier_id')->nullable()->references('id')->on('suppliers')->onDelete('cascade');
            $table->integer('product_id')->nullable()->unsigned()->index();

            $table->foreign('product_id')->nullable()->references('id')->on('products')->onDelete('cascade');
            $table->bigInteger('user_id')->nullable()->unsigned()->index();
            $table->foreign('user_id')->nullable()->references('id')->on('users')->onDelete('cascade');
            }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('supplier_products');
    }
}
