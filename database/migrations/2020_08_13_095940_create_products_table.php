<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('name')->nullable();
            $table->integer('category_id')->nullable();
            $table->integer('type_id')->nullable();
            $table->text('pic_id')->nullable();
            $table->string('supplier_id')->nullable();
            $table->double('product_length')->nullable();
            $table->double('product_width')->nullable();
            $table->double('product_height')->nullable();
            $table->double('product_weight')->nullable();
            $table->string('product_material')->nullable();
            $table->double('product_cbm')->nullable();
            $table->double('product_price_per_unit')->nullable();
            $table->integer('product_currency')->nullable();
            $table->string('shipping_port')->nullable();
            $table->text('notes')->nullable();
            $table->double('product_quantity_per_box')->nullable();
            $table->double('product_box_length')->nullable();
            $table->double('product_box_width')->nullable();
            $table->double('product_box_height')->nullable();
            $table->double('product_box_weight')->nullable();
            $table->double('product_box_cbm')->nullable();
            $table->string('product_box_material')->nullable();
            $table->double('boxes_per_stack')->nullable();
            $table->double('units_per_stack')->nullable();
            $table->double('stack_length')->nullable();
            $table->double('stack_width')->nullable();
            $table->double('stack_height')->nullable();
            $table->double('stack_weight')->nullable();
            $table->double('stack_cbm')->nullable();
            $table->string('stack_material')->nullable();
            $table->double('product_quantity_per_20ft')->nullable();
            $table->double('product_quantity_per_40ft')->nullable();
            $table->double('product_quantity_per_40fthq')->nullable();
            $table->double('product_box_quantity_per_20ft')->nullable();
            $table->double('product_box_quantity_per_40ft')->nullable();
            $table->double('product_box_quantity_per_40fthq')->nullable();
            $table->integer('active')->nullable();
            $table->bigInteger('user_id')->nullable()->unsigned()->index();
            $table->foreign('user_id')->nullable()->references('id')->on('users')->onDelete('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
