<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuoteLinesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('quote_lines', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('quote_id')->nullable();
            $table->string('product_name')->nullable();
            $table->double('product_length')->nullable();
            $table->double('product_width')->nullable();
            $table->double('product_height')->nullable();
            $table->double('product_weight')->nullable();
            $table->string('product_material')->nullable();
            $table->string('shipping_port')->nullable();
            $table->string('supplier')->nullable();
            $table->double('product_quantity_per_20ft')->nullable();
            $table->double('product_quantity_per_20fthq')->nullable();
            $table->double('product_quantity_per_40ft')->nullable();
            $table->double('product_quantity_per_40fthq')->nullable();
            $table->text('product_image')->nullable();
            $table->double('product_unit_price')->nullable();
            $table->integer('product_unit_currency')->nullable();
            $table->string('wholesale_margin_percentage')->nullable();
            $table->double('wholesale_cost')->nullable();
            $table->string('retail_margin_percentage')->nullable();
            $table->double('retail_cost')->nullable();
            $table->integer('confirmed')->nullable();
            $table->integer('active')->nullable();
            $table->bigInteger('user_id')->nullable()->unsigned()->index();
            $table->foreign('user_id')->nullable()->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('quote_lines');
    }

}
