<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewFieldsToOrderLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_lines', function (Blueprint $table) {
            $table->integer('shipping_port_id')->nullable()->unsigned()->index()->after('product_image');
            $table->foreign('shipping_port_id')->nullable()->references('id')->on('shipping_ports')->onDelete('cascade');
            $table->double('currency_exchange_rate')->nullable()->after('product_unit_currency');
            $table->integer('pricing_currency')->nullable()->after('currency_exchange_rate');
            $table->integer('shipping_to_port_id')->nullable()->unsigned()->index()->after('pricing_currency');
            $table->foreign('shipping_to_port_id')->nullable()->references('id')->on('shipping_ports')->onDelete('cascade');
            $table->string('container_size')->nullable()->after('shipping_to_port_id');
            $table->string('packing_quantity')->nullable()->after('container_size');
            $table->double('landed_product_price')->nullable()->after('order_quantity');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_lines', function (Blueprint $table) {
            //
        });
    }
}
