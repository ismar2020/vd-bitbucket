<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFisTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fis_templates', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('template_name')->nullable();
            $table->enum('is_default', ['0', '1'])->default('0');
            $table->string('transport')->nullable();
            $table->integer('transport_currency')->nullable();
            $table->double('port_costs')->nullable();
            $table->integer('port_costs_currency')->nullable();
            $table->double('insurance_cost')->nullable();
            $table->integer('insurance_cost_currency')->nullable();
            $table->string('import_custom_duties_percentage')->nullable();
            $table->string('import_taxes_percentage')->nullable();
            $table->double('handing_costs_in_destination_terminal')->nullable();
            $table->integer('handing_costs_currency')->nullable();
            $table->double('costs_of_shipping_after_destination_port')->nullable();
            $table->integer('shipping_after_port_currency')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fis_templates');
    }
}
