<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductQuantityPerContainersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_quantity_per_containers', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('container_id')->nullable()->unsigned()->index();
            $table->foreign('container_id')->nullable()->references('id')->on('containers')->onDelete('cascade');
            $table->integer('product_id')->nullable()->unsigned()->index();
            $table->foreign('product_id')->nullable()->references('id')->on('products')->onDelete('cascade');
            $table->string('product_quantity')->nullable();
            $table->string('box_quantity')->nullable();
            $table->string('stack_quantity')->nullable();
            $table->integer('active')->nullable();
            $table->string('scanned')->nullable();
            $table->integer('verified')->nullable();
            $table->bigInteger('user_id')->nullable()->unsigned()->index();
            $table->foreign('user_id')->nullable()->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_container_per_containers');
    }
}
