<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUserIdFieldToAllTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('currency_exchange_rates', function (Blueprint $table) {
            $table->bigInteger('user_id')->nullable()->unsigned()->index();
            $table->foreign('user_id')->nullable()->references('id')->on('users')->onDelete('cascade');
        });
        Schema::table('fis_templates', function (Blueprint $table) {
            $table->bigInteger('user_id')->nullable()->unsigned()->index();
            $table->foreign('user_id')->nullable()->references('id')->on('users')->onDelete('cascade');
        });
        Schema::table('order_documents', function (Blueprint $table) {
            $table->bigInteger('user_id')->nullable()->unsigned()->index();
            $table->foreign('user_id')->nullable()->references('id')->on('users')->onDelete('cascade');
        });
        Schema::table('quotes_attachments', function (Blueprint $table) {
            $table->bigInteger('user_id')->nullable()->unsigned()->index();
            $table->foreign('user_id')->nullable()->references('id')->on('users')->onDelete('cascade');
        });
        Schema::table('quote_categories', function (Blueprint $table) {
            $table->bigInteger('user_id')->nullable()->unsigned()->index();
            $table->foreign('user_id')->nullable()->references('id')->on('users')->onDelete('cascade');
        });
        Schema::table('settings', function (Blueprint $table) {
            $table->bigInteger('user_id')->nullable()->unsigned()->index();
            $table->foreign('user_id')->nullable()->references('id')->on('users')->onDelete('cascade');
        });
        Schema::table('shipping_container_products', function (Blueprint $table) {
            $table->bigInteger('user_id')->nullable()->unsigned()->index();
            $table->foreign('user_id')->nullable()->references('id')->on('users')->onDelete('cascade');
        });
        Schema::table('shipping_plans', function (Blueprint $table) {
            $table->bigInteger('user_id')->nullable()->unsigned()->index();
            $table->foreign('user_id')->nullable()->references('id')->on('users')->onDelete('cascade');
        });
        Schema::table('shipping_ports', function (Blueprint $table) {
            $table->bigInteger('user_id')->nullable()->unsigned()->index();
            $table->foreign('user_id')->nullable()->references('id')->on('users')->onDelete('cascade');
        });
        Schema::table('shipping_rates', function (Blueprint $table) {
            $table->bigInteger('user_id')->nullable()->unsigned()->index();
            $table->foreign('user_id')->nullable()->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('currency_exchange_rates', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
        });
        Schema::table('fis_templates', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
        });
        Schema::table('order_documents', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
        });
        Schema::table('quotes_attachments', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
        });
        Schema::table('quote_categories', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
        });
        Schema::table('settings', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
        });
        Schema::table('shipping_container_products', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
        });
        Schema::table('shipping_plans', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
        });
        Schema::table('shipping_ports', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
        });
        Schema::table('shipping_rates', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
        });
    }

}
