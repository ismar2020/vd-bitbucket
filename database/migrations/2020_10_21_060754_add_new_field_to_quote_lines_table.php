<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewFieldToQuoteLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quote_lines', function (Blueprint $table) {
            $table->double('product_cbm')->nullable();
            $table->double('product_quantity_per_box')->nullable();
            $table->double('product_box_length')->nullable();
            $table->double('product_box_width')->nullable();
            $table->double('product_box_height')->nullable();
            $table->double('product_box_weight')->nullable();
            $table->double('product_box_cbm')->nullable();
            $table->double('boxes_per_stack')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quote_lines', function (Blueprint $table) {
            //
        });
    }
}
