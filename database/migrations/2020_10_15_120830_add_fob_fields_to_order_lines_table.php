<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFobFieldsToOrderLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_lines', function (Blueprint $table) {
            $table->double('order_pricing_costs_total_fob')->nullable();
            $table->double('costs_from_factory_fob')->nullable();
            $table->integer('costs_from_factory_currency_fob')->nullable();
            $table->double('custom_and_other_costs_fob')->nullable();
            $table->integer('custom_and_other_costs_currency_fob')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_lines', function (Blueprint $table) {
            //
        });
    }
}
