<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTrackingOrderFieldsToShippingPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shipping_plans', function (Blueprint $table) {
            $table->string('invoiced_to_customer')->nullable();
            $table->string('invoice_amount')->nullable();
            $table->string('deposit_paid')->nullable();
            $table->string('payment_received')->nullable();
            $table->string('payment_date')->nullable();
            $table->string('payment_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shipping_plans', function (Blueprint $table) {
            //
        });
    }
}
