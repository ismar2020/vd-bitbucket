<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFisFieldsToOrderLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_lines', function (Blueprint $table) {
            $table->double('shipping_rate')->nullable();
            $table->integer('shipping_rate_currency')->nullable();
            $table->string('transport')->nullable();
            $table->integer('transport_currency')->nullable();
            $table->double('port_costs')->nullable();
            $table->integer('port_costs_currency')->nullable();
            $table->double('insurance_cost')->nullable();
            $table->integer('insurance_cost_currency')->nullable();
            $table->string('import_custom_duties_percentage')->nullable();
            $table->string('import_taxes_percentage')->nullable();
            $table->double('handing_costs_in_destination_terminal')->nullable();
            $table->integer('handing_costs_currency')->nullable();
            $table->double('costs_of_shipping_after_destination_port')->nullable();
            $table->integer('shipping_after_port_currency')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_lines', function (Blueprint $table) {
            //
        });
    }
}
