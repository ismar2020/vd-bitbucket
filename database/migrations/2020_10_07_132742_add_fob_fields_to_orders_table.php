<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFobFieldsToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->double('costs_from_factory')->nullable();
            $table->integer('costs_from_factory_currency')->nullable();
            $table->double('custom_and_other_costs')->nullable();
            $table->integer('custom_and_other_costs_currency')->nullable();
            $table->double('order_pricing_costs_fob_total')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            //
        });
    }
}
