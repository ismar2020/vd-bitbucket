<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('first_name')->nullable();
            $table->string('surname')->nullable();
            $table->double('phone')->nullable();
            $table->integer('phone_prefix')->nullable();
            $table->double('mobile')->nullable();
            $table->integer('mobile_prefix')->nullable();
            $table->string('email')->nullable();
            $table->text('picture_supplier')->nullable();
            $table->text('picture_card')->nullable();
            $table->string('supplier_id')->nullable();
            $table->integer('Product_id')->nullable()->unsigned()->index();
            $table->foreign('Product_id')->nullable()->references('id')->on('products')->onDelete('cascade');
            $table->integer('active')->nullable();
            $table->string('scanned')->nullable();
            $table->integer('verified')->nullable();
            $table->bigInteger('user_id')->nullable()->unsigned()->index();
            $table->foreign('user_id')->nullable()->references('id')->on('users')->onDelete('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contacts');
    }
}
