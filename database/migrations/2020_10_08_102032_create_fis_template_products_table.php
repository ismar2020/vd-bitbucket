<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFisTemplateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fis_template_products', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('template_id')->nullable()->unsigned()->index();

            $table->foreign('template_id')->nullable()->references('id')->on('fis_templates')->onDelete('cascade');
            $table->integer('product_id')->nullable()->unsigned()->index();

            $table->foreign('product_id')->nullable()->references('id')->on('products')->onDelete('cascade');
            $table->bigInteger('user_id')->nullable()->unsigned()->index();
            $table->foreign('user_id')->nullable()->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fis_template_products');
    }
}
