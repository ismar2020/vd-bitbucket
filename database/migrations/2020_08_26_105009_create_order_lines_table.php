<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_lines', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('order_id')->nullable();
            $table->integer('product_id')->nullable();
            $table->string('product_name')->nullable();
            $table->text('product_image')->nullable();
            $table->double('product_unit_price')->nullable();
            $table->double('product_unit_currency')->nullable();
            $table->string('order_quantity')->nullable();
            $table->double('purchase_cost')->nullable();
            $table->string('wholesale_margin_percentage')->nullable();
            $table->double('wholesale_cost')->nullable();
            $table->string('retail_margin_percentage')->nullable();
            $table->double('retail_cost')->nullable();
            $table->integer('confirmed')->nullable();
            $table->integer('active')->nullable();
            $table->bigInteger('user_id')->nullable()->unsigned()->index();
            $table->foreign('user_id')->nullable()->references('id')->on('users')->onDelete('cascade');
            });  
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('order_lines');
    }
}
