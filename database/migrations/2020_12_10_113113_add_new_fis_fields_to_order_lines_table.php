<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewFisFieldsToOrderLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_lines', function (Blueprint $table) {
            $table->double('domestic_transport')->nullable();
            $table->integer('domestic_transport_currency')->nullable();
            $table->double('storage_overhead')->nullable();
            $table->integer('storage_overhead_currency')->nullable();
            $table->double('merchandising')->nullable();
            $table->integer('merchandising_currency')->nullable();
            $table->double('other_overheads')->nullable();
            $table->integer('other_overheads_currency')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_lines', function (Blueprint $table) {
            //
        });
    }
}
