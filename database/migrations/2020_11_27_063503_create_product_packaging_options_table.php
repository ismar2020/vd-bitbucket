<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductPackagingOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_packaging_options', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('product_id')->nullable();
            $table->string('packaging_type')->nullable();
            $table->string('quantity_per_page')->nullable();
            $table->double('packaging_length')->nullable();
            $table->double('packaging_width')->nullable();
            $table->double('packaging_height')->nullable();
            $table->string('gross_weight_including_packaging')->nullable();
            $table->double('packaging_cbm')->nullable();
            $table->string('floor_ready')->nullable();
            $table->string('stackable')->nullable();
            $table->string('nested_product')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('product_packaging_options');
    }
}
