#!/bin/sh
cd /var/app/staging
export COMPOSER_HOME=/root && /usr/bin/composer.phar self-update
if [ $? -eq 1 ]
then
  echo composer is already the latest version
fi
/usr/bin/composer.phar install -n
/usr/bin/composer.phar require anhskohbo/no-captcha -n

