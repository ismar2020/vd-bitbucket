$(document).ready(function () {
    $('#product_length,#product_width,#product_height').on('keyup', function () {
        //        alert('d');
        var l = $("#product_length").val();
        var w = $("#product_width").val();
        var h = $("#product_height").val();

        var cbm = l * w * h;
        $('#product_cbm').val(cbm);
    });
    $('#length,#width,#height').on('keypress', function () {
        //        alert('d');
        var l = $("#length").val();
        var w = $("#width").val();
        var h = $("#height").val();

        var cbm = l * w * h;
        $('#container_cbm').val(cbm);
    });


    //************************---onselect product bring values------***********************
    $(document).on('change', '.product_id', function (e) {
        e.preventDefault();
        var divId = $(this).closest('tr').attr('id');
        var form_data = new FormData();
        form_data.append("product_id", this.value);
        form_data.append("_token", $('meta[name="csrf-token"]').attr('content'));
        $.ajax({
            url: "{{url('admin/getproductdetails')}}",
            method: "POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                //                        Swal.showLoading();
            },
            success: function (response)
            {

                if (response) {
                    if (response.status) {
                        $('#' + divId + ' .shipping_port').val(response.product.shipping_port);
                        //                            swal("Good job!", "Product Added! Now you can select product from dropdown", "success");
                    } else {
                        swal("Error!", 'Something Went Wrong', "error");
                    }
                }
            }
        });
    });
    //************************----ends-----************************

    $(document).delegate('a.add-record', 'click', function (e) {
        e.preventDefault();

        var content = $('#sample_table tr'),
                size = $('#tbl_posts >tbody >tr').length + 1,
                name_count = $('#tbl_posts >tbody >tr').length + 1,
                element = null,
                element = content.clone();
        element.attr('id', 'rec-' + size);


        element.find('.delete-record').attr('data-id', size);
        element.appendTo('#tbl_posts_body');
        element.find('.sn').html(size);
        element.find('.sn').html(size);

        //struggler's code
        //code for adding name attribute in input in format of order_details[i][input_name]
        $('#rec-' + size + ' :input').each(function () {
            var input = $(this);
            var input_name = input.attr('name');
            input.attr('name', 'order_detail[' + name_count + '][' + input_name + ']');
        });
        //ends 
    });
    $(document).delegate('a.delete-record', 'click', function (e) {
        e.preventDefault();
        var didConfirm = confirm("Are you sure You want to delete");
        if (didConfirm == true) {
            var id = $(this).attr('data-id');
            var targetDiv = $(this).attr('targetDiv');
            $('#rec-' + id).remove();

            //regnerate index number on table
            $('#tbl_posts_body tr').each(function (index) {
                $(this).find('span.sn').html(index + 1);
            });
            return true;
        } else {
            return false;
        }
    });



    $('#addProductForm').on('submit', function (e) {

        e.preventDefault();

        var form_data = new FormData(this);

        form_data.append("_token", $('meta[name="csrf-token"]').attr('content'));
        $.ajax({
            url: "{{url('admin/addproduct')}}",
            method: "POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                //                        Swal.showLoading();
            },
            success: function (response)
            {

                if (response) {
                    if (response.status) {
                        $('#product_id').html(response.product_html);
                        $('#Product_id').html(response.product_html);
                        $('#modalCloseProduct').trigger('click');
                        swal("Good job!", "Product Added! Now you can select product from dropdown", "success");
                    } else {
                        swal("Error!", 'Something Went Wrong', "error");
                    }
                }
            }
        });
    });
    $('#addContactForm').on('submit', function (e) {

        e.preventDefault();

        var form_data = new FormData(this);

        form_data.append("_token", $('meta[name="csrf-token"]').attr('content'));
        $.ajax({
            url: "{{url('admin/addcontact')}}",
            method: "POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                //                        Swal.showLoading();
            },
            success: function (response)
            {

                if (response) {
                    if (response.status) {
                        $('#contact_id').html(response.contact_html);
                        $('#modalCloseContact').trigger('click');
                        swal("Good job!", "Contact Added! Now you can select contact from dropdown", "success");
                    } else {
                        swal("Error!", 'Something Went Wrong', "error");
                    }
                }
            }
        });
    });
    $('#addSupplierForm').on('submit', function (e) {

        e.preventDefault();

        var form_data = new FormData(this);

        form_data.append("_token", $('meta[name="csrf-token"]').attr('content'));
        $.ajax({
            url: "{{url('admin/addsupplier')}}",
            method: "POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                //                        Swal.showLoading();
            },
            success: function (response)
            {

                if (response) {
                    if (response.status) {
                        $('#supplier_id').html(response.supplier_html);
                        $('#modalCloseSupplier').trigger('click');
                        swal("Good job!", "New Supplier Added! Now you can select supplier from dropdown", "success");
                    } else {
                        swal("Error!", 'Something Went Wrong', "error");
                    }
                }
            }
        });
    });
});