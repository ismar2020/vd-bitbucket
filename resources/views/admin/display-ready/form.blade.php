<div class="form-group{{ $errors->has('display_length') ? 'has-error' : ''}}">
    {!! Form::label('display_length', 'Display Length', ['class' => 'control-label']) !!}
    {!! Form::number('display_length', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('display_length', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('display_width') ? 'has-error' : ''}}">
    {!! Form::label('display_width', 'Display Width', ['class' => 'control-label']) !!}
    {!! Form::number('display_width', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('display_width', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('display_height') ? 'has-error' : ''}}">
    {!! Form::label('display_height', 'Display Height', ['class' => 'control-label']) !!}
    {!! Form::number('display_height', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('display_height', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('display_max_weight') ? 'has-error' : ''}}">
    {!! Form::label('display_max_weight', 'Display Max Weight', ['class' => 'control-label']) !!}
    {!! Form::number('display_max_weight', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('display_max_weight', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('number_of_displays') ? 'has-error' : ''}}">
    {!! Form::label('number_of_displays', 'Number Of Displays', ['class' => 'control-label']) !!}
    {!! Form::number('number_of_displays', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('number_of_displays', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('product_price_per_unit') ? 'has-error' : ''}}">
    {!! Form::label('product_price_per_unit', 'Product Price Per Unit', ['class' => 'control-label']) !!}
    {!! Form::number('product_price_per_unit', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('product_price_per_unit', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('product_currency') ? 'has-error' : ''}}">
    {!! Form::label('product_currency', 'Product Currency', ['class' => 'control-label']) !!}
    <select class="form-control" id="product_currency" name="product_currency" size="5">
    {!!  get_currency_list_with_symbols() !!}
    </select>
    {!! $errors->first('product_currency', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
