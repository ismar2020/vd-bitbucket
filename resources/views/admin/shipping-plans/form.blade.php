<div class="form-group{{ $errors->has('order_id') ? 'has-error' : ''}}">
    {!! Form::label('order_id', 'Order Id', ['class' => 'control-label']) !!}
    {!! Form::number('order_id', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('order_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('container_id') ? 'has-error' : ''}}">
    {!! Form::label('container_id', 'Container Id', ['class' => 'control-label']) !!}
    {!! Form::number('container_id', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('container_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('purchase_order_number') ? 'has-error' : ''}}">
    {!! Form::label('purchase_order_number', 'Purchase Order Number', ['class' => 'control-label']) !!}
    {!! Form::number('purchase_order_number', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('purchase_order_number', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('shipping_to_port') ? 'has-error' : ''}}">
    {!! Form::label('shipping_to_port', 'Shipping To Port', ['class' => 'control-label']) !!}
    {!! Form::text('shipping_to_port', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('shipping_to_port', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('shipping_date') ? 'has-error' : ''}}">
    {!! Form::label('shipping_date', 'Shipping Date', ['class' => 'control-label']) !!}
    {!! Form::text('shipping_date', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('shipping_date', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('container_unit_quantity') ? 'has-error' : ''}}">
    {!! Form::label('container_unit_quantity', 'Container Unit Quantity', ['class' => 'control-label']) !!}
    {!! Form::number('container_unit_quantity', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('container_unit_quantity', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('container_purchase_cost') ? 'has-error' : ''}}">
    {!! Form::label('container_purchase_cost', 'Container Purchase Cost', ['class' => 'control-label']) !!}
    {!! Form::number('container_purchase_cost', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('container_purchase_cost', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('container_wholesale_price') ? 'has-error' : ''}}">
    {!! Form::label('container_wholesale_price', 'Container Wholesale Price', ['class' => 'control-label']) !!}
    {!! Form::number('container_wholesale_price', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('container_wholesale_price', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('container_costs') ? 'has-error' : ''}}">
    {!! Form::label('container_costs', 'Container Costs', ['class' => 'control-label']) !!}
    {!! Form::number('container_costs', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('container_costs', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('conatiner_costs_value') ? 'has-error' : ''}}">
    {!! Form::label('conatiner_costs_value', 'Conatiner Costs Value', ['class' => 'control-label']) !!}
    {!! Form::text('conatiner_costs_value', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('conatiner_costs_value', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
