@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Shipping rate</div>
                    <div class="card-body">
                        <a href="{{ url('/admin/shipping-rate/create') }}" class="btn btn-success btn-sm" title="Add New ShippingRate">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>

                        <br/>
                        <br/>
                        <div class="table-responsive suppliers_table_mobile">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Port of Origin</th>
                                        <th>Port of Discharge</th>
                                        <!--<th>Shipping Rate</th>-->
                                        <th>Shipping Rate Currency</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($shippingrate as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ get_shipping_port_name_by_id($item->shipping_port_id) }}</td>
                                        <td>{{ get_shipping_port_name_by_id($item->shipping_to_port_id) }}</td>
                                        
                                        <td>{{ get_currency_name_by_id($item->shipping_rate_currency) }}</td>
                                        <td>
                                            <a href="{{ url('/admin/shipping-rate/' . $item->id . '/edit') }}" title="Edit ShippingRate"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                            {!! Form::open([
                                                'method' => 'DELETE',
                                                'url' => ['/admin/shipping-rate', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                        'type' => 'submit',
                                                        'type' => 'submit',
'class' => 'btn btn-danger btn-sm deleteentry',
                                                        'title' => 'Delete ShippingRate'   
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                      
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
