@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-12">
                <div class="card">
                    
                    <!--<div class="card-header">Edit Shipping Rate {{ get_name_by_id_and_table_name('shipping_rates', $shippingrate->id, 'shipping_port_id', 'shipping_to_port_id') }}</div>-->
                    
                    <div class="card-header">Edit Shipping Rate #{{ $shippingrate->id }}</div>
                    
                    <div class="card-body">
                        <?php /*  <a href="{{ url('/admin/shipping-rate') }}" title="Back"><button class="btn btn-warning btn-sm" style="display: none;"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
<!--                        <br />
                        <br /> */ ?> 

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($shippingrate, [
                            'method' => 'PATCH',
                            'url' => ['/admin/shipping-rate', $shippingrate->id],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

                        @include ('admin.shipping-rate.form', ['formMode' => 'edit'])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
