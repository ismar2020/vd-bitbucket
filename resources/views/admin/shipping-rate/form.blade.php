<div class="row shipping_rate_width"> 
    <div class="col-md-4 form-group{{ $errors->has('shipping_port_id') ? 'has-error' : ''}}">
        {!! Form::label('shipping_port_id', 'Port of Origin', ['class' => 'control-label']) !!}
        <!--    {!! Form::number('shipping_port_id', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}-->
        {!! Form::select('shipping_port_id', get_shipping_port_list() , null, ('' == 'required') ? ['class' => 'form-control select2 shipping_select', 'required' => 'required'] : ['class' => 'form-control select2 shipping_select']) !!}
        {!! $errors->first('shipping_port_id', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-md-4 form-group{{ $errors->has('shipping_to_port_id') ? 'has-error' : ''}}">
        {!! Form::label('shipping_to_port_id', 'Port of Discharge', ['class' => 'control-label']) !!}
        <!--    {!! Form::number('shipping_to_port_id', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}-->
        {!! Form::select('shipping_to_port_id', get_shipping_port_list() , null, ('' == 'required') ? ['class' => 'form-control select2 shipping_select', 'required' => 'required'] : ['class' => 'form-control select2 shipping_select']) !!}
        {!! $errors->first('shipping_to_port_id', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-md-3 form-group{{ $errors->has('date_valid_from') ? 'has-error' : ''}}" style="display: none;">
        {!! Form::label('date_valid_from', 'Date Valid From', ['class' => 'control-label']) !!}
        {!! Form::date('date_valid_from', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('date_valid_from', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-md-3 form-group{{ $errors->has('date_valid_until') ? 'has-error' : ''}}" style="display: none;">
        {!! Form::label('date_valid_until', 'Date Valid until', ['class' => 'control-label']) !!}
        {!! Form::date('date_valid_until' , null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('date_valid_until', '<p class="help-block">:message</p>') !!}
    </div>

    <div class="col-md-4 form-group{{ $errors->has('shipping_rate_currency') ? 'has-error' : ''}}">
        {!! Form::label('shipping_rate_currency', 'Currency', ['class' => 'control-label']) !!}
        <!--    {!! Form::number('shipping_rate_currency', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}-->
        <select class="form-control" id="shipping_rate_currency" name="shipping_rate_currency">

            @if($formMode == 'edit')
            <?php $id = $shippingrate->shipping_rate_currency; ?>
            {!!  get_currency_list_with_symbols_through_id($shippingrate->shipping_rate_currency) !!}
            @else
            {!!  get_limited_currency_list_with_symbols_through_id() !!}
            @endif

        </select>
        {!! $errors->first('shipping_rate_currency', '<p class="help-block">:message</p>') !!}
    </div>
</div><br>
<div class="row border_class">
    <div class="col-md-3 form-group">
        {!! Form::label('shipping_rate', 'Container Size', ['class' => 'control-label']) !!}
        <hr>
        {!! Form::label('shipping_rate', 'Shipping Rate', ['class' => 'control-label']) !!}
    </div>
    <?php 
        foreach (\App\Container::get() as $container){ 
        // strip out all whitespace
        $container_name = preg_replace('/\s*/', '', $container->name);
        // convert the name to all lowercase
        $container_name = strtolower($container_name);    
    ?>
    <div class="col-md-2 form-group{{ $errors->has('shipping_rate_'.$container_name) ? 'has-error' : ''}}">
        {!! Form::label('shipping_rate_'.$container_name, $container->name, ['class' => 'control-label']) !!}
        <hr>
        {!! Form::text('shipping_rate_'.$container_name, null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('shipping_rate_'.$container_name, '<p class="help-block">:message</p>') !!}
    </div>
    <?php } ?>
</div>
<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
