<div class="form-group{{ $errors->has('first_name') ? 'has-error' : ''}}">
    {!! Form::label('first_name', 'First Name', ['class' => 'control-label']) !!}
    {!! Form::text('first_name', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('surname') ? 'has-error' : ''}}">
    {!! Form::label('surname', 'Surname', ['class' => 'control-label']) !!}
    {!! Form::text('surname', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('surname', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('phone_prefix') ? 'has-error' : ''}}">
    {!! Form::label('phone_prefix', 'Phone Prefix', ['class' => 'control-label']) !!}
    <div class="input-group">
        <span class="input-group-addon">
            <i class="fa fa-plus" aria-hidden="true"></i>
        </span>
        {!! Form::select('phone_prefix', get_phone_prefix_list(), null, ['class' => 'form-control']) !!}
        {!! $errors->first('phone_prefix', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('phone') ? 'has-error' : ''}}">
    {!! Form::label('phone', 'Phone', ['class' => 'control-label']) !!}
    {!! Form::number('phone', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('mobile_prefix') ? 'has-error' : ''}}">
    {!! Form::label('mobile_prefix', 'Mobile Prefix', ['class' => 'control-label']) !!}
    <div class="input-group">
        <span class="input-group-addon">
            <i class="fa fa-plus" aria-hidden="true"></i>
        </span>
        {!! Form::select('mobile_prefix', get_phone_prefix_list(), null, ['class' => 'form-control']) !!}
        {!! $errors->first('mobile_prefix', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('mobile') ? 'has-error' : ''}}">
    {!! Form::label('mobile', 'Mobile', ['class' => 'control-label']) !!}
    {!! Form::number('mobile', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('mobile', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group{{ $errors->has('email') ? 'has-error' : ''}}">
    {!! Form::label('email', 'Email', ['class' => 'control-label']) !!}
    {!! Form::email('email', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('picture_supplier') ? 'has-error' : ''}}">
    {!! Form::label('picture_supplier', 'Picture Supplier', ['class' => 'control-label']) !!}
    {!! Form::file('picture_supplier', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('picture_supplier', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('picture_card') ? 'has-error' : ''}}">
    {!! Form::label('picture_card', 'Picture Card', ['class' => 'control-label']) !!}
    {!! Form::file('picture_card', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('picture_card', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('supplier_id') ? 'has-error' : ''}}">
    {!! Form::label('supplier_id', 'Supplier Name', ['class' => 'control-label']) !!}
    {!! Form::select('supplier_id', get_suppliers_list(), null, ['class' => 'form-control']) !!}
    {!! $errors->first('supplier_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('Product_id') ? 'has-error' : ''}}">
    {!! Form::label('Product_id', 'Product Name', ['class' => 'control-label']) !!}
    {!! Form::select('Product_id', get_products_list(), null, ['class' => 'form-control']) !!}
    {!! $errors->first('Product_id', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group{{ $errors->has('active') ? 'has-error' : ''}}">
    {!! Form::label('active', 'Active', ['class' => 'control-label']) !!}
{!! Form::select('active', array('0' => 'Yes', '1' => 'No'), '1',['class' => 'form-control']) !!}

{!! $errors->first('active', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('scanned') ? 'has-error' : ''}}">
    {!! Form::label('scanned', 'Scanned', ['class' => 'control-label']) !!}
    {!! Form::select('scanned', array('Scanned' => 'Scanned', 'Manual' => 'Manual'), 'Manual',['class' => 'form-control']) !!}
    {!! $errors->first('scanned', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('verified') ? 'has-error' : ''}}">
    {!! Form::label('verified', 'Verified', ['class' => 'control-label']) !!}
    {!! Form::select('verified', array('0' => 'Yes', '1' => 'No', '2' => 'Pending'), '0',['class' => 'form-control']) !!}

    {!! $errors->first('verified', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
