@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-12">
                <div class="card">
                    <!--***********//SHOWING CONTACT NAME instead of ID*************-->
<!--                    <div class="card-header">Edit Contact {{ get_name_by_id_and_table_name('contacts', $contact->id, 'first_name', 'surname') }}</div>-->
                    <!--************************ENDS*********************************-->
                    <div class="card-header">Edit Contact #{{ $contact->id }}</div>
                    
                    <div class="card-body">
<!--                        <a href="{{ url('/admin/contacts') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />-->

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($contact, [
                            'method' => 'PATCH',
                            'url' => ['/admin/contacts', $contact->id],
                            'class' => 'form-horizontal',
                            'id' => 'submit_contact_form', 
                            'files' => true
                        ]) !!}

                        @include ('admin.contacts.form', ['formMode' => 'edit'])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection
