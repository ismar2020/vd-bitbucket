@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Scanned Contacts</div>
                    <div class="card-body">
                        <a href="{{ url(url()->previous()) }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br/><br />

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Name</th><th>Phone</th><th>Product</th><th>Supplier</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($scannedcontacts as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->first_name.' '.$item->surname }}</td><td>{{ $item->phone }}</td>
                                       
                                        <td><?php $product_name = get_product_name_by_id($item->Product_id); if($product_name != 'NA'){ ?><a href="{{ url('admin/showproduct',$item->Product_id) }}">{{ $product_name }}</a><?php }else{ echo $product_name; } ?>
                                        </td>
                                        <td><?php $supplier_name = get_supplier_name_by_id($item->supplier_id); if($supplier_name != 'NA'){ ?><a href="{{ url('admin/showsupplier',$item->supplier_id) }}">{{ $supplier_name }}</a><?php }else{ echo $supplier_name; } ?>
                                        </td>
                                        <td>
                                            <a href="{{ url('/admin/contacts/' . $item->id) }}" title="View Contact" style="display:none;"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                            <a href="{{ url('/admin/contacts/' . $item->id . '/editscannedcontacts') }}" title="Edit Contact"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                            {!! Form::open([
                                                'method' => 'DELETE',
                                                'url' => ['/admin/contacts', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                        'type' => 'submit',
                                                        'type' => 'submit',
'class' => 'btn btn-danger btn-sm deleteentry',
                                                        'title' => 'Delete Contact'   
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
