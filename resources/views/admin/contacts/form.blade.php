<!-- -----------------------****************tab code**************************---------------- -->
<ul class="nav nav-tabs">
    <li><a data-toggle="tab" href="#home"  id="home_active">Contact Home</a></li>
    @if (\Auth::user()->roles->first()->name != 'supplier')
    <li><a data-toggle="tab" href="#menu1">Supplier</a></li>
    @endif
    <li><a data-toggle="tab" href="#menu2">Products</a></li>
</ul>

<div class="tab-content">
    <div id="home" class="tab-pane fade in active">
        <br>

        <!-- ********************supplier's contact ID****************************************-->
        <?php if (Request::exists('supplierid')) { ?><input type="hidden" name="supplierid" class="form-control suppliers_contact" value="{{ Request::get('supplierid') }}"/><?php } ?>
        <!--******************************ends************************************************-->
        @if($formMode == 'edit')
        <input type="hidden" name="contact_id" value="{{ $contact->id }}"/>
        @endif
        <div class="row">
            <div class="col-md-6 form-group{{ $errors->has('first_name') ? 'has-error' : ''}}">
                {!! Form::label('first_name', 'First Name', ['class' => 'control-label']) !!}
                {!! Form::text('first_name', null, ('required' == 'required') ? ['class' => 'form-control contact_form', 'required' => 'required'] : ['class' => 'form-control contact_form']) !!}
                {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="col-md-6 form-group{{ $errors->has('surname') ? 'has-error' : ''}}">
                {!! Form::label('surname', 'Surname', ['class' => 'control-label']) !!}
                {!! Form::text('surname', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                {!! $errors->first('surname', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 form-group{{ $errors->has('phone_prefix') ? 'has-error' : ''}}">
                {!! Form::label('phone_prefix', 'Phone Prefix', ['class' => 'control-label']) !!}
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                    </span>
                    <select class="form-control phone_prefix select2" name="phone_prefix" style="WIDTH: MAX-CONTENT !important;"><?php $data = get_phone_prefix_list(); ?>
                        <option value="">Select Prefix</option>
                        @foreach($data as $key => $value)
                        @if($formMode == 'edit' && $contact->phone_prefix == $key)
                        <option value="{{$key}}" selected>{{$value}}</option>
                        @else
                        <option value="{{$key}}">{{$value}}</option>
                        @endif
                        @endforeach
                    </select>

                    {!! $errors->first('phone_prefix', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="col-md-6 form-group{{ $errors->has('phone') ? 'has-error' : ''}}">
                {!! Form::label('phone', 'Phone', ['class' => 'control-label']) !!}
                {!! Form::number('phone', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 form-group{{ $errors->has('mobile_prefix') ? 'has-error' : ''}}">
                {!! Form::label('mobile_prefix', 'Mobile Prefix', ['class' => 'control-label']) !!}
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                    </span>
                    <select class="form-control mobile_prefix select2" name="mobile_prefix" style="WIDTH: MAX-CONTENT !important;"><?php $data = get_phone_prefix_list(); ?>
                        <option value="">Select Prefix</option>
                        @foreach($data as $key => $value)
                        @if($formMode == 'edit' && $contact->mobile_prefix == $key)
                        <option value="{{$key}}" selected>{{$value}}</option>
                        @else
                        <option value="{{$key}}">{{$value}}</option>
                        @endif
                        @endforeach
                    </select>
                    {!! $errors->first('mobile_prefix', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="col-md-6 form-group{{ $errors->has('mobile') ? 'has-error' : ''}}">
                {!! Form::label('mobile', 'Mobile', ['class' => 'control-label']) !!}
                {!! Form::number('mobile', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                {!! $errors->first('mobile', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 form-group{{ $errors->has('email') ? 'has-error' : ''}}">
                {!! Form::label('email', 'Email', ['class' => 'control-label']) !!}
                {!! Form::email('email', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="col-md-6 form-group{{ $errors->has('active') ? 'has-error' : ''}}">
                {!! Form::label('active', 'Active', ['class' => 'control-label']) !!}
                {!! Form::select('active', array('0' => 'Yes', '1' => 'No'), '0',['class' => 'form-control']) !!}

                {!! $errors->first('active', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <?php
        if ($formMode == 'create') {
            $image = 'images/no_image.png';
            $backimage = 'images/no_image.png';
        } else if ($formMode == 'edit') {
            if ($contact->picture_supplier != null) {
                $image = getFile($contact->picture_supplier);
            } else {
                $image = 'images/no_image.png';
            }
            if ($contact->picture_card != null) {
                $backimage = getFile($contact->picture_card);
            } else {
                $backimage = 'images/no_image.png';
            }
        }
        ?>
        <div class="row">
            <div class="col-md-6 form-group{{ $errors->has('picture_supplier') ? 'has-error' : ''}}" >
                {!! Form::label('picture_supplier', 'Business Card - Front', ['class' => 'control-label']) !!}<br>
                <input type="file" class="hidden_quote_image_input_div edit_picture_supplier" name="picture_supplier" onchange="showImage(this, 'showPictureSupplier');"/>
                <img src="{{url("$image")}}" width="50" class="showPictureSupplier"/>
                <div class = "edit_delete">
                    <a href="javascript:void(0)"><span class="fa fa-pencil edit_pencil" aria-hidden="true" onclick = "uploadImageDynamically('edit_picture_supplier')"></span></a>


                    <a href="javascript:void(0)" id="showPictureSupplier" <?php if($formMode == 'edit'){ ?> onclick="removeImageFromTable(<?=$contact->id?>, 'picture_supplier', 'contacts', 'showPictureSupplier');" <?php } ?>><span class="fa fa-times remove_image" aria-hidden="true"></span></a>
                </div>



                {!! $errors->first('picture_supplier', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="col-md-6 form-group{{ $errors->has('picture_card') ? 'has-error' : ''}}">
                {!! Form::label('picture_card', 'Business Card - Back', ['class' => 'control-label']) !!}<br>

                <input type="file" class="hidden_quote_image_input_div edit_picture_card" name="picture_card" onchange="showImage(this, 'showPictureCard');"/>
                <img src="{{url("$backimage")}}" width="50" class="showPictureCard"/>
                <div class = "edit_delete">
                    <a href="javascript:void(0)"><span class="fa fa-pencil edit_pencil" aria-hidden="true" onclick = "uploadImageDynamically('edit_picture_card')"></span></a>


                    <a href="javascript:void(0)" id="showPictureCard" <?php if($formMode == 'edit'){ ?> onclick="removeImageFromTable(<?=$contact->id?>, 'picture_card', 'contacts', 'showPictureCard');" <?php } ?>><span class="fa fa-times remove_image" aria-hidden="true"></span></a>
                </div>


                {!! $errors->first('picture_card', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="row" style="display: none;">
            <div class="col-md-6 form-group{{ $errors->has('scanned') ? 'has-error' : ''}}">
                {!! Form::label('scanned', 'Scanned', ['class' => 'control-label']) !!}
                {!! Form::select('scanned', array('Scanned' => 'Scanned', 'Manual' => 'Manual') ,($formMode == 'edit') ? 'Scanned' : 'Manual',['class' => 'form-control','size' => '2']) !!}
                {!! $errors->first('scanned', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="col-md-6 form-group{{ $errors->has('verified') ? 'has-error' : ''}}">
                {!! Form::label('verified', 'Verified', ['class' => 'control-label']) !!}
                {!! Form::select('verified', array('0' => 'Yes', '1' => 'No', '2' => 'Pending'), ($formMode == 'edit') ? '2' : '0', ['class' => 'form-control','size' => '3']) !!}

                {!! $errors->first('verified', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
        </div>

    </div>
    @if (\Auth::user()->roles->first()->name != 'supplier')
    <div id="menu1" class="tab-pane fade"><br>
        <div class="form-group{{ $errors->has('supplier_id') ? 'has-error' : ''}}">
            {!! Form::label('supplier_id', 'Link to Existing', ['class' => 'control-label']) !!}
            &nbsp;
            <!--            <button type="button" class="btn btn-info btn-sm addSupplier" data-toggle="modal" data-target="#supplierModalLong"><i class="fa fa-plus" aria-hidden="true"></i>
                            Add Supplier
                        </button>-->
            <?php $suppliers = get_suppliers_list() ?>

            <select class="select2" name = "supplier_id" style="width: max-content !IMPORTANT;">
                <option value = "">  Select Supplier  </option>
                <?php
                foreach ($suppliers as $key => $val) {
                    if ($formMode == 'edit') {
                        $supplier_contact = \App\SupplierContact::where('contact_id', $contact->id)->first();
                        if ($supplier_contact != null && $key == $supplier_contact->supplier_id) {
                            ?>
                            <option value = "<?= $key ?>" selected>  <?= $val ?>  </option>
                        <?php } else { ?>
                            <option value = "<?= $key ?>">  <?= $val ?>  </option>
                            <?php
                        }
                    } else if ($formMode == 'create') {
                        if (Request::exists('supplierid') && $key == Request::get('supplierid')) {
                            ?>
                            <option value = "<?= $key ?>" selected>  <?= $val ?>  </option>
                            <?php
                        } else {
                            if (isset($supplier->id) && $key == $supplier->id) {
                                ?>
                                <option value = "<?= $key ?>" selected>  <?= $val ?>  </option>
                            <?php } else { ?>
                                <option value = "<?= $key ?>">  <?= $val ?>  </option>
                                <?php
                            }
                        }
                    } else {
                        ?>
                        <option value = "<?= $key ?>">  <?= $val ?>  </option>
                        <?php
                    }
                }
                ?>
            </select>
            {!! $errors->first('supplier_id', '<p class="help-block">:message</p>') !!}
            <br><br>



        </div>
    </div>
    @endif
    <div id="menu2" class="tab-pane fade"><br>


        <div class="form-group{{ $errors->has('Product_id') ? 'has-error' : ''}}">
            {!! Form::label('Product_id', 'Link to Existing', ['class' => 'control-label']) !!}
            <select class="form-control select2" name="Product_id" style="width: max-content !IMPORTANT;"><?php $data = get_products_list(); ?>
                <option value="">Select Product</option>
                @foreach($data as $key => $value)
                @if($formMode == 'create')
                <option value="{{$key}}">{{$value}}</option>
                @else
                @if($contact->Product_id == $key)
                <option value="{{$key}}" selected>{{$value}}</option>
                @else
                <option value="{{$key}}">{{$value}}</option>
                @endif
                @endif
                @endforeach
            </select>
            {!! $errors->first('Product_id', '<p class="help-block">:message</p>') !!}

        </div>
    </div>
</div>



<!-- --------------------****************tab code ends**************************---------------- -->
<script>
   $(document).ready(function(){
       $("#showPictureSupplier").click(function(){
           $(".edit_picture_supplier").val('');
           $(".showPictureSupplier").attr('src', APP_URL + '/images/no_image.png');
       });
       $("#showPictureCard").click(function(){
           $(".edit_picture_card").val('');
           $(".showPictureCard").attr('src', APP_URL + '/images/no_image.png');
       })
   })
</script>