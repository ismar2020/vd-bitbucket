@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

           <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Contact Products</div>
                    <div class="card-body">
                         <a href="{{ url('/admin/contact-products/create') }}" class="btn btn-success btn-sm" title="Add New Contact's Product">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New Contact's Product
                        </a>
                            <a href="{{ url(url()->previous()) }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>

                        {!! Form::open(['method' => 'GET', 'url' => '/admin/products', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Name</th><th>Category</th><th>Type</th><th>Image</th><th>Active</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($contactproducts as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->name }}</td><td>{{ get_category_name_by_id($item->category_id) }}</td><td>{{ get_product_type_name_by_id($item->type_id) }}</td>
                                        <td>
                                            <?php if($item->pic_id != null) { ?>
                                            <img width="60" src="<?= url($item->pic_id); ?>" style="margin-bottom: 4px;">
                                            <?php }else{ ?>
                                            <img width="60" src="{{ url('images/no_image.png') }}" style="margin-bottom: 4px;">
                                            <?php } ?>
                                        </td>
                                        <td><?=($item->active == '0')?'Yes':'No'?></td>
                                        <td>
<!--                                            <a href="{{ url('/admin/suppliersById/' . $item->id) }}" title="View Suppliers"><button class="btn btn-info btn-sm"><i class="fa fa-industry" aria-hidden="true"></i></button></a>-->
                                            <a href="{{ url('/admin/products/' . $item->id) }}" title="View Product" style="display:none;"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                            <a href="{{ url('/admin/products/' . $item->id . '/edit') }}" title="Edit Product"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                            {!! Form::open([
                                                'method' => 'DELETE',
                                                'url' => ['/admin/products', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                        'type' => 'submit',
                                                        'type' => 'submit',
'class' => 'btn btn-danger btn-sm deleteentry',
                                                        'title' => 'Delete Product'   
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
