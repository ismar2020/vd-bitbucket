@extends('layouts.backend')

@section('content')
<div class="container">
    <div class="row">
        @include('admin.sidebar')

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Contacts</div>
                <div class="card-body">
                    <a href="{{ url('/admin/contacts/create') }}" class="btn btn-success btn-sm" title="Add New Contact">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add New
                    </a>
                    <!--                        <a href="{{ url('admin/scannedcontacts') }}" class="btn btn-success btn-sm" title="Scanned Contacts">
                                                <i class="fa fa-eye" aria-hidden="true"></i> Scanned Contacts List
                                            </a>-->


                    <br/>
                    <br/>
                    <div class="table-responsive suppliers_table_mobile">
                        <table class="table table-borderless">
                            <thead>
                                <tr>
                                    <th>#</th><th>Name</th>
                                    <th>Phone</th>
                                    <th>Business Card Front</th>
                                    <th>Business Card Back</th>
                                    <th>Supplier</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($contacts as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->first_name.' '.$item->surname }}</td><td>{{ $item->phone }}</td>
                                    <td>
                                        <?php if ($item->picture_supplier != null) { ?>
                                            <img width="60" src="<?= getFile($item->picture_supplier); ?>" style="margin-bottom: 4px;">
                                        <?php } else { ?>
                                            <img width="60" src="{{ url('images/no_image.png') }}" style="margin-bottom: 4px;">
                                        <?php } ?>
                                    </td>
                                    <td>
                                        <?php if ($item->picture_card != null) { ?>
                                            <img width="60" src="<?= getFile($item->picture_card); ?>" style="margin-bottom: 4px;">
                                        <?php } else { ?>
                                            <img width="60" src="{{ url('images/no_image.png') }}" style="margin-bottom: 4px;">
                                        <?php } ?>
                                    </td>
<!--                                    <td>
                                        <a href="{{ url('admin/viewcontactproducts',$item->id) }}" class="btn btn-info btn-sm" title="click to view">Products ({!! get_contact_products_count($item->id) !!})</a>
                                    </td>-->
                                    <td>
                                        <?php
                                        $contact_supplier = App\SupplierContact::where('contact_id', $item->id);
                                        $contact_supplier_id = ($contact_supplier->get()->isEmpty() != true) ? $contact_supplier->first()->supplier_id : 'NA';
                                        if ($contact_supplier_id != 'NA') {
                                            $supplier_name = get_supplier_name_by_id($contact_supplier_id);
                                            if ($supplier_name != 'NA') {
                                                ?><a href="{{ url('admin/suppliers',$contact_supplier->first()->supplier_id) }}">{{ $supplier_name }}</a><?php
                                            } else {
                                                echo $supplier_name;
                                            }
                                        } else {
                                            echo 'NA';
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <a href="{{ url('/admin/contacts/' . $item->id) }}" title="View Contact" style="display:none;"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                        <a href="{{ url('/admin/contacts/' . $item->id . '/edit') }}" title="Edit Contact"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                        {!! Form::open([
                                        'method' => 'DELETE',
                                        'url' => ['/admin/contacts', $item->id],
                                        'style' => 'display:inline'
                                        ]) !!}
                                        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                        'type' => 'submit',
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-sm deleteentry',
                                        'title' => 'Delete Contact' 
                                        )) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.table').dataTable({
            "iDisplayLength": 50
        });
    })
</script>
@endsection
