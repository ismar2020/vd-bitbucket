@extends('layouts.backend')

@section('content')

<!-------------------------------------Shipping Plan Tab data--------------------------------->

<div class="container">
    <div class="row">
        @include('admin.sidebar')

        <div class="col-md-12">
            <div class="card">
                <?php $order_id = Request::get('order_id');?>
                <div class="card-header">Edit Order #<?php echo $order_id;?></div>
                <div class="card-body">
                    <ul class="nav nav-tabs">
                        
                        <li><a href="{{url("admin/orders/".$order_id."/edit")}}">Order Pricing</a></li>
                        <li class="active"><a href="{{url("admin/orders/".$order_id."/edit")}}#shipping_pan"><b><u>Shipping Plan > Container Costs</u></b></a></li>
                        <li><a href="{{url("admin/orders/".$order_id."/edit")}}#summary">Summary</a></li>
                    </ul>
                    <div id="container_products">
                        <br>
                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#shippingContainerProductModalLongnew" style="margin:12px;"><i class="fa fa-plus" aria-hidden="true"></i>
                            Add Costs into Container
                        </button>
                        <br><br>
                        <form method="post" name="shipping_container_products" action="javascript:void(0)" accept-charset="UTF-8" class="form-horizontal" id="shipping_container_products" enctype="multipart/form-data">
                        <div class="responsive" style="overflow-y:hidden">
                            <!-- ********************supplier's contact ID**********************************-->
    <?php if (Request::exists('order_id')) { ?><input type="hidden" name="order_id" class="form-control" id="container_products_order_id" value="{{ Request::get('order_id') }}"/><?php } ?>
    <?php if (Request::exists('shipping_plan_id')) { ?><input type="hidden" name="shipping_plan_id" class="form-control" id="container_products_shipping_plan_id" value="{{ Request::get('shipping_plan_id') }}"/><?php } ?>
    <?php if (Request::exists('shipping_plan_id')) { ?><input type="hidden" name="shipping_plan_id" class="form-control ship_container_id" id="" value="<?php echo \App\ShippingPlan::where('id',Request::get('shipping_plan_id'))->first()->container_id;
    ?>"/><?php } ?>
 <!--***************************************ends**********************************-->
                            <div id = "load_shipping_container_products">
 
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--add container products modal-->
<div class="modal fade" id="shippingContainerProductModalLongnew" tabindex="-1" role="dialog" aria-labelledby="shippingContainerProductModalLongnewTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background: #397657;">
                <h5 class="modal-title" id="orderProductModalLongNewTitle" style="color:#fff;">Add Products into Container</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="javascript:void(0)" accept-charset="UTF-8" class="form-horizontal" id="addOrderProductForm" enctype="multipart/form-data">
                    @csrf
                    @include ('admin.orders.ajaxshippingcontainerproductform', ['formMode' => 'create'])
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" id="modalCloseContainerProductModal" class="modalCloseProduct_u" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>
<!--end modal-->

<!-------------------------------------Summary Tab Data------------------------------------->
@endsection