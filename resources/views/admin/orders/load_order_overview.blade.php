@extends('layouts.backend')

@section('content')
<style>

    div#tbl_load_order_overview_filter {
        position: relative;
        bottom: -49px;
        left: -163px;
        margin-bottom: 50px;
        top: auto;
    }

    div.dataTables_wrapper div.dataTables_filter {
        text-align: left !important;
    }
    .empty_blocks {
        border: 0 !IMPORTANT;
        background-color: #fff;
    }
    tr.shipping_plan_header {
        background-color: #000;
        font-size: 12px;
    }
    tr.shipping_plan_header th {
        color : #fff;
    }
    tr.tracking_container_products_color {
        background-color: #f5f5f5;
        font-size: 14px;
    }
    .shipping_plan_tracking {
        background: transparent !IMPORTANT;
        border: 0;
        width: 120px;
    }
    .select_webkit_hide {
        -webkit-appearance: none;
        -moz-appearance: none;
        text-indent: 1px;
        text-overflow: '';
    }
    .product_image_append img {
        margin-left: 25px;
    }
    tfoot {
        display: table-header-group;
    }

    #tbl_load_order_overview_wrapper div.dt-button-collection {
        position: sticky;
        top: 0;
        width: 80%;
        margin-top: 3px;
        padding: 8px 8px 4px 8px;
        border: 1px solid #ccc;
        border: 1px solid rgba(0,0,0,0.4);
        background-color: white;
        overflow: auto;
        height: 570px;
        z-index: 999999;
        border-radius: 5px;
        box-shadow: 3px 3px 5px rgba(0,0,0,0.3);
        box-sizing: border-box;
        left: 0px !IMPORTANT;
        padding-top: 178px;
    }
    div.dt-button-collection button.dt-button, div.dt-button-collection div.dt-button, div.dt-button-collection a.dt-button {
        left: 0;
        right: 0;
        width:22%;
        display: block;
        float: left;
        margin-bottom: 15px;
        margin-right: 0;
        margin-left: 18px;
        margin-top: 10px;

        /*    background: #000 !important;
            color: #fff;*/
    }
    #tbl_load_order_overview_wrapper > div.dt-buttons > div.dt-button-collection > div > button:active {
        background: #fff;
    }
    #tbl_load_order_overview_wrapper > div.dt-buttons > div.dt-button-collection > div > button.active{
        box-sizing: border-box;
        font-size: 0.88em;
        line-height: 1.6em;
        color: white;
        white-space: nowrap;
        overflow: hidden;
        background-color: red;
        background: indianred;
        background: -moz-linear-gradient(top, rgba(230,230,230,0.1) 0%, rgba(0,0,0,0.1) 100%);
        background: -ms-linear-gradient(top, rgba(230,230,230,0.1) 0%, rgba(0,0,0,0.1) 100%);
        background: -o-linear-gradient(top, rgba(230,230,230,0.1) 0%, rgba(0,0,0,0.1) 100%);
        background: black;
        filter: progid:DXImageTransform.Microsoft.gradient(GradientType=0,StartColorStr='rgba(230, 230, 230, 0.1)', EndColorStr='rgba(0, 0, 0, 0.1)');
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        text-decoration: none;
        outline: none;
        text-overflow: ellipsis;
    }
    #tbl_load_order_overview_wrapper > div.dt-buttons > button{
        background: #000;
        color: #fff;
        font-weight: 600;
        height: 35px;
        border-radius: 7px;
        padding: 0.25rem 0.5rem;
        font-size: 0.875rem;
        line-height: 1.5;
        border-radius: 0.2rem;
        left: 332px;
        top: 43px;
    }

    #tbl_load_order_overview_wrapper > div#tbl_load_order_overview_info {
        top: 106px;
        left: 23px;
        position: absolute;
    }


    p {
        line-height: 1.6;
        margin-bottom: 0;
        margin: 0px -9px 0px 10px;

    }
    /*button.dt-button.buttons-collection.buttons-colvis {
        left: 500px;
        bottom: 0px;
        position: relative;
    }*/

    div.dt-buttons {
        position: relative;
        float: left;
        top: 6px;
        z-index: 999;
    }

    @media(min-width:1500px)
    {    div.dt-button-collection button.dt-button, div.dt-button-collection div.dt-button, div.dt-button-collection a.dt-button {
             position: relative;
             left: 0;
             right: 0;
             width: 18%;
             display: block;
             float: left;
             margin-bottom: 0;
             margin-right: -6px;
             margin-left: 13px;
             margin-top: 10px;
         }




         #tbl_load_order_overview_wrapper div.dt-button-collection {
             overflow: scroll;
             height: 406px !important;
             padding-top: 110px !Important;


         }
    }

</style>

<div class="container">
    <div class="row">
        @include('admin.sidebar')

        <div class="col-md-12">
            <div class="card overview_card">
                <div class="card-header">Orders Overview</div>
                <div class="card-body">
                    <!--button to collapse view totals-->
                    <div class="overview_summary_div">
                        <a href="javascript:void(0)" class="btn btn-info btn-sm show_hide_order_overview_collapse_icon" style="border-radius: 50%;" data-toggle="collapse" data-target="#viewOrderOverviewCollapse" onclick="load_order_overview_totals();">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                        </a>&nbsp;Click here to View Summary</div>
                    <div id="viewOrderOverviewCollapse" class="collapse">

                    </div>

                    <div class="responsive" style="overflow-y:hidden">
                        <table class="table table-bordered tbl_tracking_order display nowrap tbl_load_order_over" id="tbl_load_order_overview" cellpadding="4">
                            <button type="button" class="btn btn-primary btn-sm show_hide_order_overview" data-toggle="modal" data-target="#showHideorderOverviewModalLongnew" style="display :none;"><i class="fa fa-eye" aria-hidden="true"></i>
                                Show/hide columns
                            </button>     
                            <thead>
                                <tr>
                                    <th>Inside</th>
                                    <th>#</th>
                                    <th>Order Name</th>
                                    <th>Order Status</th>
                                    <th>Products</th>
                                    <th>Container Name</th>
                                    <th>PO Number</th>
                                    <th>BL Number</th>
                                    <th>Description</th>
                                    <th>Port of Discharge</th>
                                    <th>Cargo Ready Date</th>
                                    <th>Dispatched Date</th>
                                    <th>ETA Date</th>
                                    <th>Fumigated</th>
                                    <th>Total Cost</th>
                                    <th>Customer Buy Price</th>
                                    <th>Profit</th>
                                    <th>Status</th>
                                    <th>Invoiced to Customer</th>
                                    <th>Invoice name</th>
                                    <th>Invoice Amount</th>
                                    <th>Deposit Paid</th>
                                    <th>Payment Rec.</th>
                                    <th>Payment Date</th>
                                    <th>Payment Id</th>
                                    <th>Comments</th>
                                    
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th>#</th>
                                    <th>Order Name</th>
                                    <th>Order Status</th>
                                    <th>Products</th>
                                    <th>Container Name</th>
                                    <th>PO Number</th>
                                    <th>BL Number</th>
                                    <th>Description</th>
                                    <th>Port of Discharge</th>
                                    <th>Cargo Ready Date</th>
                                    <th>Dispatched Date</th>
                                    <th>ETA Date</th>
                                    <th>Fumigated</th>
                                    <th>Total Cost</th>
                                    <th>Customer Buy Price</th>
                                    <th>Profit</th>
                                    <th>Status</th>
                                    <th>Invoiced to Customer</th>
                                    <th>Invoice Amount</th>
                                    <th>Deposit Paid</th>
                                    <th>Payment Rec.</th>
                                    <th>Payment Date</th>
                                    <th>Payment Id</th>
                                    <th>Invoice name</th>
                                    <th>Comments</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="showHideorderOverviewModalLongnew" tabindex="-1" role="dialog" aria-labelledby="showHideModalLongNewTitle" aria-hidden="true">
        <div class="modal-dialog" role="document" style="
             max-width: 70%;
             margin-top: 50px;
             ">
            <div class="modal-content">
                <div class="modal-header" style="background: #397657;">
                    <h5 class="modal-title" id="showHideModalLongNewTitle" style="color:#fff;">Show / hide Columns</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="overflow-y: auto;">
                    <div id='control_sh'>
                        <div id="showhidecolumns">
                            <div class="show_hide">
                                <div class="row">
                                    <div class="col-md-4">
                                        <input type="checkbox" class="hide_show" checked><span>Products</span><br>
                                        <input type="checkbox" class="hide_show" checked><span>Id</span><br>
                                        <input type="checkbox" class="hide_show" checked><span>Order Name</span><br>
                                        <input type="checkbox" class="hide_show" checked><span>Order Status</span><br>
                                        <input type="checkbox" class="hide_show" checked><span>Container Name</span><br>
                                        <input type="checkbox" class="hide_show" checked><span>Products</span><br>
                                        <input type="checkbox" class="hide_show" checked><span>PO Number</span><br>
                                        <input type="checkbox" class="hide_show" checked><span>BL Number</span><br>
                                        <input type="checkbox" class="hide_show" checked><span>Description</span><br>

                                    </div>
                                    <div class="col-md-4">

                                        <input type="checkbox" class="hide_show" checked><span>Port of Discharge</span><br>
                                        <input type="checkbox" class="hide_show" checked><span>Cargo Ready Date</span><br>
                                        <input type="checkbox" class="hide_show" checked><span>Dispatched Date</span><br>
                                        
                                        <input type="checkbox" class="hide_show" checked><span>Fumigated</span><br>
                                        <input type="checkbox" class="hide_show" checked><span>Total Cost</span><br>
                                        <input type="checkbox" class="hide_show" checked><span>Customer Buy Price</span><br>
                                        <input type="checkbox" class="hide_show" checked><span>Profit</span><br>
                                        <input type="checkbox" class="hide_show" checked><span>Status</span><br>

                                    </div>
                                    <div class="col-md-4">
                                        <input type="checkbox" class="hide_show" checked><span>Invoiced to Customer</span><br>
                                        <input type="checkbox" class="hide_show" checked><span>Invoice Name</span><br>
                                        <input type="checkbox" class="hide_show" checked><span>Invoice Amount</span><br>
                                        <input type="checkbox" class="hide_show" checked><span>Deposit Paid</span><br>
                                        <input type="checkbox" class="hide_show" checked><span>Payment Rec.</span><br>
                                        <input type="checkbox" class="hide_show" checked><span>Payment Date</span><br>
                                        <input type="checkbox" class="hide_show" checked><span>Payment Id</span><br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" id="modalCloseProduct" data-dismiss="modal">Close</button>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
        var table = $('#tbl_load_order_overview').DataTable({
        dom: '<"top"i>rt<"bottom"flp><"clear">',
                dom: 'Blfrtip',
                language: {
                "info": "_END_ out of _TOTAL_ records",
                "infoFiltered": "",
                },
                columnDefs: [
                {
                targets: 0,
                        className: 'noVis'
                }
                ],
                buttons: [
                {
                extend: 'colvis',
                        columns: ':not(.noVis)',
                        text: '<i class="fa fa-eye" aria-hidden="true"></i> Show/hide columns'
                }
                ],
                initComplete: function () {
                // Apply the search at every keyup of search bar 
                this.api().columns().every(function () {
                var that = this;
                $('input', this.footer()).on('keyup change clear', function () {
                if (that.search() !== this.value) {
//                                if(this.value.length >= 3){
                that
                        .search(this.value)
                        .draw();
//                                    }
                // Ensure we clear the search if they backspace far enough
//                                if (this.value == "") {
//                                    that
//                                        .search("")
//                                        .draw();
//                                }

                }
                });
                });
                },
                pageLength: 10,
                processing: true,
                oLanguage: {
                sProcessing: '<img id="loading-image" src="{{url("assets/images/loader.gif")}}" alt="Loading..." />'
                },
                serverSide: false,
                ajax: "{{ route('orders.overview') }}",
                columns: [
                {data: '+', name: '+'},
<?php foreach ($rules as $rule): ?>
    <?php if ($rule == 'order_name'): ?>
                        {data: 'order_name', name: 'orders.name'},
    <?php elseif ($rule == 'order_status'): ?>
                        {data: 'order_status', name: 'order_statuses.name'},
    <?php elseif ($rule == 'container_name'): ?>
                        {data: 'container_name', name: 'containers.name'},
    <?php elseif ($rule == 'port_of_discharge'): ?>
                        {data: 'port_of_discharge', name: 'shipping_ports.port'},
    <?php elseif ($rule == 'status'): ?>
                        {data: 'status', name: 'shipping_statuses.name'},
    <?php elseif ($rule == 'shipping_products'): ?>
                        {data: 'shipping_products', name: 'shipping_container_products.product_name'},
    <?php elseif ($rule == 'shipping_products_suppliers_name'): ?>
                        {data: 'shipping_products_suppliers_name', name: 'shipping_products_suppliers_name'},
    <?php else: ?>
                        {data: "{{$rule}}", name: "{{$rule}}"},
    <?php endif; ?>
<?php endforeach; ?>
                ],
                //getting shipping plan id >>>>>>>>>>>
                'createdRow': function (row, data, dataIndex) {
                //                console.log($(row).find('td:nth-child(2)').text());
                $(row).attr('data-child-value', $(row).find('td:nth-child(2)').text());
                },
                //ends
                //adding N/A to empty fields
                "columnDefs": [{
                "defaultContent": "N/A",
                        "targets": "_all"
                }]
                //ends
        });
        //this code shifted to backend blade.php
//            table.on('page.dt', function () {
//                $('html, body').animate({
//                    scrollTop: $(".dataTables_wrapper").offset().top
//                }, 'slow');
//            });
        //adding search box
        $('#tbl_load_order_overview tfoot th:not(:lt(2), :first)').each(function () {
        var title = $(this).text();
//        console.log(title);
        if (title == 'Order Name') {
        $(this).html('<input type="text" placeholder="Search" style="width :300px;"/>');
        } else if (title == 'Products' || title == 'Description') {
        $(this).html('<input type="text" placeholder="Search" style="width :250px;"/>');
        } else{
        $(this).html('<input type="text" placeholder="Search" style="width :135px;"/>');
        }
        });
        // Add event listener for opening and closing rows
        $('#tbl_load_order_overview').on('click', 'td a.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var shipping_plan = tr.attr('data-child-value');
        if (row.child.isShown()) {
        // This row is already open - close it
        row.child.hide();
        tr.find('i').addClass('fa-plus');
        tr.find('i').removeClass('fa-minus');
        tr.removeClass('shown');
        } else {
        // Open this row
        tr.find('i').addClass('fa-minus');
        tr.find('i').removeClass('fa-plus');
        format(row.child, shipping_plan);
        tr.addClass('shown');
        }
        });
        //code for wait 3 keyup in laravel 
        $(".dataTables_filter input")
                .unbind()
                .bind("input", function (e) {

                if (this.value.length >= 3 || e.keyCode == 13) {
                // Call the API search function
                table.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                table.search("").draw();
                }
                return;
                });
        //adding inner table through this code
        function format(rowchild, shipping_plan) {
        $.ajax({
        url: "{{url('admin/load_shipping_container_products_for_tracking')}}",
                method: "POST",
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {shipping_plan_id: shipping_plan},
                beforeSend: function () {
                //                        Swal.showLoading();
                },
                onError: function () {
                swal("Error!", 'Something Went Wrong', "error");
                },
                success: function (response)
                {
                rowchild($(response)).show();
                }
        });
        }
        });
        //strugglers code
        $("#hide_show_all").on("change", function () {
        var hide = $(this).is(":checked");
        $(".hide_show").prop("checked", hide);
        if (hide) {
        $('.tbl_load_order_over tr th').hide(100);
        $('.tbl_load_order_over tr td').hide(100);
        } else {
        $('.tbl_load_order_over tr th').show(100);
        $('.tbl_load_order_over tr td').show(100);
        }
        });
        $(document).on("change", ".hide_show", function () {
        var hide = $(this).is(":checked");
        var all_ch = $(".hide_show:checked").length == $(".hide_show").length;
        $("#hide_show_all").prop("checked", all_ch);
        var ti = $(this).index(".hide_show");
        $('.tbl_load_order_over tr').each(function () {
        if (!hide) {
        $('td:eq(' + ti + ')', this).hide(100);
        $('th:eq(' + ti + ')', this).hide(100);
        } else {
        $('td:eq(' + ti + ')', this).show(100);
        $('th:eq(' + ti + ')', this).show(100);
        }
        });
        });
    </script>
    @endsection