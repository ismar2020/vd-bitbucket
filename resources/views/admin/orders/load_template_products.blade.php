<div class="card-body" style="overflow: auto;">
    <h4>List of Products Using the Template</h4>
    <div class="table-responsive">
        <table class="table table-borderless">
            <thead>
                <tr>
                    <th>#</th><th>Name</th><th>Category</th><th>Type</th><th>Image</th><th>Active</th><th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($template_products_details as $item)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $item->name }}</td><td>{{ get_category_name_by_id($item->category_id) }}</td><td>{{ get_product_type_name_by_id($item->type_id) }}</td>
                    <td>
                        <?php if ($item->pic_id != null) { ?>
                            <img width="60" src="<?= getFile($item->pic_id); ?>" style="margin-bottom: 4px;">
                        <?php } else { ?>
                            <img width="60" src="{{ url('images/no_image.png') }}" style="margin-bottom: 4px;">
                        <?php } ?>
                    </td>
                    <td><?= ($item->active == '0') ? 'Yes' : 'No' ?></td>
                    <td><a class="btn btn-xs" onclick="delete_template_products('fis_template_products',{{$template_id}},{{$item->id}});"><i class="fa fa-trash"></i></a></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

</div>
