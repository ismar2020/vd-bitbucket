<div class="responsive" style="overflow-y: scroll;">
    
    <table class="table table-bordered" id="tbl_particular_product_packaging_options">
    <thead>
        <tr>
            <th>#</th>
            <th>Packaging Type</th>
            <th>Quantity Per Package</th>
            <th>Packaging Length (cm)</th>
            <th>Packaging Width (cm)</th>
            <th>Packaging Height (cm)</th>
            <th>Gross Weight (kg) including Packaging</th>
            <th>Packaging CBM</th>
            <th>Floor Ready</th>
            <th>Stackable</th>
            <th>Nested Product</th>
        </tr>
    </thead>
    <tbody id="tbl_packaging_options_body">
        <?php $i = '1'; ?>
        @foreach($packaging_option as $details)
        <tr id="rec-<?= $i ?>">
            <td><span class="sn"></span><?= $i ?>.</td>
    <input type="hidden" value="{{$details->id}}" class="row_id" name="packaging_options[<?= $i ?>][row_id]" />
    <td>
        <select class="form-control packaging_options" name="packaging_options[<?= $i ?>][packaging_type]" style="width:140px;" disabled="">
            <?php
            $type = get_package_type_list();
            $select = ['1' => 'Yes', '2' => 'No'];
            ?>
            <option value="">Select</option>
            @foreach($type as $key => $value)
            @if($key == $details->packaging_type)
            <option value="{{$key}}" selected>{{$value}}</option>
            @else
            <option value="{{$key}}">{{$value}}</option>
            @endif
            @endforeach
        </select>
    </td>
    <td><input type="text" class="form-control quantity_per_pagckage packaging_options" name="packaging_options[<?= $i ?>][quantity_per_page]" id="quantity_per_pagckage_{{$details->id}}" value="{{$details->quantity_per_page}}" disabled=""/></td>
    <td><input type="text" class="form-control packaging_length packaging_options" name="packaging_options[<?= $i ?>][packaging_length]" id="packaging_length" value="{{$details->packaging_length}}" disabled=""/></td>
    <td><input type="text" class="form-control packaging_width packaging_options" name="packaging_options[<?= $i ?>][packaging_width]" id="packaging_width" value="{{$details->packaging_width}}" disabled=""/></td>
    <td><input type="text" class="form-control packaging_height packaging_options" name="packaging_options[<?= $i ?>][packaging_height]" id="packaging_height" value="{{$details->packaging_height}}" disabled=""/></td>
    <td><input type="text" class="form-control packaging_options" name="packaging_options[<?= $i ?>][gross_weight_including_packaging]" value="{{$details->gross_weight_including_packaging}}" disabled=""/></td>
    <td><input type="text" class="form-control packaging_cbm packaging_options" name="packaging_options[<?= $i ?>][packaging_cbm]" id="packaging_cbm" value="{{$details->packaging_cbm}}" disabled=""/></td>
    <td>
        <select class="form-control packaging_options" name="packaging_options[<?= $i ?>][floor_ready]" style="width:90px;" disabled="">
            <option value="">Select</option>
            @foreach($select as $key => $value)
            @if($key == $details->floor_ready)
            <option value="{{$key}}" selected>{{$value}}</option>
            @else
            <option value="{{$key}}">{{$value}}</option>
            @endif
            @endforeach
        </select>
    </td>
    <td>
        <select class="form-control packaging_options" name="packaging_options[<?= $i ?>][stackable]" style="width:150px;" disabled="">
            <option value="">Select</option>
            @foreach($select as $key => $value)
            @if($key == $details->stackable)
            <option value="{{$key}}" selected>{{$value}}</option>
            @else
            <option value="{{$key}}">{{$value}}</option>
            @endif
            @endforeach
        </select>
    </td>
    <td>
        <select class="form-control nested_product packaging_options" name="packaging_options[<?= $i ?>][nested_product]" style="width:150px;" disabled="">
            <option value="">Select</option>
            @foreach($select as $key => $value)
            @if($key == $details->nested_product)
            <option value="{{$key}}" selected>{{$value}}</option>
            @else
            <option value="{{$key}}">{{$value}}</option>
            @endif
            @endforeach
        </select>
    </td>
</tr>
<?php $i++; ?>
@endforeach
</tbody>
</table>
</div>