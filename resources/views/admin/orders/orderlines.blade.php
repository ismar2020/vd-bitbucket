@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Order Lines <?php $order_name = get_order_name_by_id($orderid); if($order_name != 'NA'){ ?>Of {{ $order_name }}<?php }else{ echo ''; } ?></div>
                    <div class="card-body">
                        <?php if(isset($previous)){ ?>
                            <a href="{{ url('admin/orders') }}" class="btn btn-success btn-sm">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                            </a>
                        <?php } ?>
                        {!! Form::open(['method' => 'GET', 'url' => '/admin/suppliers', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Order Name</th><th>Product</th><th>Purchase order</th><th>Port of Origin</th><th>Product Unit Quantity</th><th>Purchase $</th><th>Wholesale $</th><th>Retail $</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($orderlines as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td><?php $order_name = get_order_name_by_id($item->order_id); ?>{{ $order_name }}
                                        </td>
                                        <td><?php $product_name = get_product_name_by_id($item->product_id); ?>{{ $product_name }}
                                        </td>
                                        <td>{{ $item->purchase_order }}</td>
                                        <td>{{ $item->shipping_port }}</td>
                                        <td>{{ $item->product_unit_quantity }}</td>
                                        <td>{{ $item->purchase_cost }}</td>
                                        <td>{{ $item->wholesale_cost }}</td>
                                        <td>{{ $item->retail_cost }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
