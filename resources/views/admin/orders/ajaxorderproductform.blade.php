<div class="form-group{{ $errors->has('supplier_id') ? 'has-error' : ''}}">
    {!! Form::label('supplier_id', 'Select Supplier', ['class' => 'control-label']) !!}
<!--    {!! Form::select('order_supplier_id', get_suppliers_list(), null, ['class' => 'form-control select2','size' => '5']) !!}-->
    <?php $suppliers = get_suppliers_list() ?>
    <select class="order_supplier_id select2" name = "order_supplier_id" style = "width:180px">
        <option value="">Select Supplier</option>
        <?php foreach ($suppliers as $key => $val) { ?>
            <option value = "<?= $key ?>">  <?= $val ?>  </option>

        <?php } ?>

    </select>
    {!! $errors->first('supplier_id', '<p class="help-block">:message</p>') !!}
    <br><br>
</div>
<div class="form-group{{ $errors->has('product_id') ? 'has-error' : ''}}">
    {!! Form::label('product_id', 'Product Name', ['class' => 'control-label']) !!}
    
    <select class="order_product_id select2" name = "order_product_id" style = "width:180px">
        <?php $data = get_products_list(); ?>
        <option value="">Select Product</option>
        @foreach($data as $key => $value)
            <option value="{{$key}}">{{$value}}</option>
        @endforeach
    </select>
    {!! $errors->first('product_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="product_details">
    <div class="row">
    <div class="col-md-6 product_"><h4>Product Name : <span class="product_name"></span></h4></div>
    <div class="col-md-6"><span class="product_image"></span></div>
    </div>
</div>
<input type="hidden" class="form-control" id="order_product_line_id" name="order_product_line_id" value=""/>
<input type="hidden" class="form-control" name="order_id" value="{{$order->id}}"/>
<div class="form-group">
    <a href="javascript:void(0)" class="btn btn-primary btn-sm add-record order_line_create" onclick="order_line_create(),$('.modalCloseProduct_u').trigger('click');" data-added="0" style="margin:10px;"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Add</a>
</div>
