<table class="child_table_tracking">
    <tr class="tracking_container_products tracking_container_products_color">
        <th class="empty_blocks"></th>
        <th class="empty_blocks"></th>
        <th class="less_width">#</th>
        <th>Product Name</th>
        <th>Product Image</th>
        <th>Supplier Name</th>
        <th>Packaging Type</th>
        <th>Quantity</th>
        <th>Confirmed Quantity</th>
        <th>Total Cost</th>
        <th>Buy Price</th>
        <th>Profit</th>
        <!--<th><i class="fa fa-cogs"></i></th>-->
    </tr>
    <tbody>
        <?php $j = '1'; ?>
        @foreach($shipping_container_products as $details)
        <?php
        if ($details->product_image == null || $details->product_image == '') {
            $product_pic = 'images/no_image.png';
        } else {
            $product_pic = getFile($details->product_image);
        }
//getting product Id from order line
        $product_res = \App\OrderLine::where('id', $details->order_line_id)->first();
//getting details from product Id of product
        $product_id = null;
        if ($product_res) {
            $product = \App\Product::where('id', $product_res->product_id)->first();

            if ($product) {
                $product_id = $product->id;
            }
        }
//getting order line details
        $orderLine = \App\OrderLine::where('id', $details->order_line_id)->first();
//getting order type from order-id got from above
//        $order_type = App\Order::where('id', Request::get('order_id'))->first()->order_type;
        ?>
        <tr id="rec-<?= $j ?>" class="tracking_container_products">
            <td class="empty_blocks"></td>
            <td class="empty_blocks"></td>
            <td class="less_width"><span class="sn"></span><?= $j ?>.</td>
    <input type="hidden" value="{{$details->id}}" name="shipping_container_products[<?= $j ?>][row_id]" />
    <td>
        <input type="hidden" class="form-control product_id" name="shipping_container_products[<?= $j ?>][product_id]" value=""/>
        <?php /* <input type="text" class="form-control product_name" name="shipping_container_products[<?= $j ?>][product_name]" value="{{$details->product_name}}" style="width: 160px;" disabled=""/> */ ?>
        <span>{{$details->product_name}}</span>
    </td>
    <td class="product_image_append">
        <img src="{{ url("$product_pic") }}" width="50"/>
        <input type="hidden" class="form-control ship_order_line_id" value="{{$details->order_line_id}}"/>
    </td>
    <td>
        <?php /* <input type="text" class="form-control supplier_name" name="shipping_container_products[<?= $j ?>][supplier_id]" value="{{get_supplier_name_by_id($details->supplier_id)}}" style="width: 160px;" disabled=""/> */ ?>
        {{get_supplier_name_by_id($details->supplier_id)}}
    </td>
    <td>
        <?= ($details->using == '' || $details->using == null) ? 'NA' : $details->using ?>
    </td>

    <td>
        <?= ($details->quantity == '' || $details->quantity == null) ? 'NA' : $details->quantity ?>
    </td>
    <td>
        {{$details->confirmed_quantity}}
    </td>
    <td>
        <input type="text" class="form-control shipping_plan_tracking format_me_in_decimals" name="shipping_container_products" value="{{$details->total_cost}}" style="width: 160px;" disabled=""/>
        <?php /* {{$details->total_cost}} */ ?>
    </td>
    <td>
        <input type="text" class="form-control shipping_plan_tracking format_me_in_decimals" name="shipping_container_products" value="{{$details->customer_buy_price}}" style="width: 160px;" disabled=""/>
        <?php /* {{$details->customer_buy_price}} */ ?>
    </td>
    <td>
        <input type="text" class="form-control shipping_plan_tracking format_me_in_decimals" name="shipping_container_products" value="{{$details->profit}}" style="width: 160px;" disabled=""/>
        <?php /* {{$details->profit}} */ ?>
    </td>
    <td style="display :none;">
        <?php
        $data = DB::table("shipping_container_products")
                ->select(DB::raw("SUM(container_order_quantity) as count"))
                ->where('shipping_plan_id', '!=', $details->shipping_plan_id)
                ->where('order_line_id', $details->order_line_id)
                ->get();
        ?>
        <input type="text" class="form-control total_units_added_other_containers" name="shipping_container_products[<?= $j ?>][total_product_units_added_to_other_containers]" value="<?php echo $data[0]->count; ?>" disabled=""/>
    </td>
</tr>
<?php $j++; ?>
@endforeach
</tbody>
</table>
