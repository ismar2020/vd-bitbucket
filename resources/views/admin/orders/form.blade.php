<div class="form-group{{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('confirmed') ? 'has-error' : ''}}">
    {!! Form::label('confirmed', 'Order Status', ['class' => 'control-label']) !!}
    <?php /*{!! Form::select('confirmed', get_order_status_list(), '0',['class' => 'form-control']) !!}*/ ?>
    <select class="form-control confirmed" name="confirmed"><?php $data = get_order_status_list(); ?>
        <option value="">Select Order Status</option>
        @foreach($data as $key => $value)
        <option value="{{$key}}">{{$value}}</option>
        @endforeach
    </select>
    {!! $errors->first('confirmed', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('order_type') ? 'has-error' : ''}}">
    {!! Form::label('order_type', 'Order Type', ['class' => 'control-label']) !!}
    {!! Form::select('order_type', array('1' => 'FOB', '2' => 'FIS', '3' => 'FIW'), 'FOB',['class' => 'form-control']) !!}
    {!! $errors->first('order_type', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('pricing_currency') ? 'has-error' : ''}}">
    {!! Form::label('pricing_currency', 'Pricing Currency', ['class' => 'control-label']) !!}
    <select class="form-control" id="pricing_currency" name="pricing_currency">
        <option value="">Select Pricing Currency</option>
        @if(isset($order->pricing_currency) && $order->pricing_currency != null)
        {!!  get_limited_currency_list_with_selected_item($order->pricing_currency) !!}
        @else
        {!!  get_limited_currency_list_with_symbols_through_id() !!}
        @endif


    </select>
</div>
<div class="form-group{{ $errors->has('active') ? 'has-error' : ''}}">
    {!! Form::label('active', 'Active', ['class' => 'control-label']) !!}
    {!! Form::select('active', array('0' => 'Yes', '1' => 'No'), '0',['class' => 'form-control']) !!}
    {!! $errors->first('active', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group{{ $errors->has('po_type') ? 'has-error' : ''}}">
    {!! Form::label('po_type', 'PO Type', ['class' => 'control-label']) !!}
    <?php /*{!! Form::select('confirmed', get_order_status_list(), '0',['class' => 'form-control']) !!}*/ ?>
    <select class="form-control po_type" name="po_type" id="po_type"><?php $data = ['0' => 'Single PO', '1'=>'Multiple PO'] ?>
        <option>Select PO Type</option>
        @foreach($data as $key => $value)
        <option value="{{$key}}">{{$value}}</option>
        @endforeach
    </select>
    {!! $errors->first('po_type', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group d-none{{ $errors->has('name') ? 'has-error' : ''}}" id="po_number">
    {!! Form::label('name', 'PO Number', ['class' => 'control-label']) !!}
    {!! Form::text('po_number', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('po_number', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>

<script>
 $(document).ready(function(){
     $("#po_type").change(function(){
        if($(this).val() == 1){
            $("#po_number").addClass('d-none');
        }else if($(this).val() == 0){
            $("#po_number").removeClass('d-none');
        } else{
            $("#po_number").addClass('d-none');
        }
        
     });
 })   
 </script>