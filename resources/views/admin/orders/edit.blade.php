@extends('layouts.backend')

@section('content')
<style>
    /*    .shipping_plan_tracking{
            width: 120px;
        }*/
    .empty_blocks {
        border: 0 !IMPORTANT;
        background-color: #fff;
    }
    tr.shipping_plan_header {
        background-color: #000;
        font-size: 12px;
    }
    tr.shipping_plan_header th {
        color : #fff;
    }
    tr.tracking_container_products_color {
        background-color: #f5f5f5;
        font-size: 14px;
    }
    .shipping_plan_tracking {
        background: transparent !IMPORTANT;
        border: 0;
        /*width: 120px;*/
    }
    .select_webkit_hide {
        -webkit-appearance: none;
        -moz-appearance: none;
        text-indent: 1px;
        text-overflow: '';
    }
    .product_image_append img {
        margin-left: 25px;
    }

    .hide_inputs {
        background: transparent !IMPORTANT;
        border: 0;
        /*width: 120px !IMPORTANT;*/
    }
</style>

<div class="container">
    <div class="row">
        @include('admin.sidebar')

        <div class="col-md-12">
            <div class="card">

                <!--<div class="card-header">Edit Order {{ get_name_by_id_and_table_name('orders', $order->id, 'name') }}</div>-->

                <div class="card-header">Edit Order #{{ $order->id }}</div>

                <div class="card-body">
<!--                        <a href="{{ url('/admin/orders') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                    <br />
                    <br />-->

                    @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    @endif

                    <!--                        {!! Form::model($order, [
                                                'method' => 'PATCH',
                                                'url' => ['/admin/orders', $order->id],
                                                'class' => 'form-horizontal',
                                                'files' => true
                                            ]) !!}-->

                    @include ('admin.orders.editform', ['formMode' => 'edit'])

                    <!--{!! Form::close() !!}-->

                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="orderProductModalLongnew" tabindex="-1" role="dialog" aria-labelledby="orderProductModalLongNewTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background: #397657;">
                <h5 class="modal-title" id="orderProductModalLongNewTitle" style="color:#fff;">Add Product</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="javascript:void(0)" accept-charset="UTF-8" class="form-horizontal" id="addOrderProductForm" enctype="multipart/form-data">
                    @csrf
                    @include ('admin.orders.ajaxorderproductform', ['formMode' => 'create'])
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary modalCloseProduct_u" id="modalCloseProduct" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->

<?php
$route = Route::current()->uri;
if ($route == 'admin/order_pricing_costs_fis') {
    ?>
    <script>
        $(document).ready(function () {
            //    alert('s');
            $(".shipping_rate").trigger('click');

        });
    </script>
<?php } ?>
<script>

    var getAllCurrencyExhangeJson = '';
    getAllCurrencyExhange();
    function getAllCurrencyExhange() {
        $.ajax({
            url: "{{url('admin/getAllCurrencyExhange')}}",
            method: "POST",
            async: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            beforeSend: function () {
                //                        Swal.showLoading();
            },
            onError: function () {
                swal("Error!", 'Something Went Wrong', "error");
            },
            success: function (response)
            {

                getAllCurrencyExhangeJson = response;
            }
        });
    }
    //function for calculating base currency value from current currency
    function convertCurrentToBaseCurrency(base, current, value) {
        for (var i = 0; i < getAllCurrencyExhangeJson.getAllCurrencyExhange.length; i++) {
            if (getAllCurrencyExhangeJson.getAllCurrencyExhange[i].currency_id_to == base && getAllCurrencyExhangeJson.getAllCurrencyExhange[i].currency_id_from == current) {
                var newvalue = value * getAllCurrencyExhangeJson.getAllCurrencyExhange[i].exchange_rate;
                return newvalue;
            }
        }

        return 0;
    }

    function forGpCalculation() {
        $(".orderlinetable tbody tr").each(function () {
//        $(".select2_port").select2();
//        var quantity = $(this).val();
            var rowId = this.id;

//            return rowId;
            var currentRow = $("#" + rowId);
            var uom = currentRow.find(".order_quantity").val();
            if (typeof uom !== "undefined" && uom.length > 0) {
//                var uom = parseInt(uom.replace(",", ""));
                var uom = uom.replace(/,/g, '');
            }

            var fob_price = currentRow.find(".product_unit_price").val();
            if (typeof fob_price != "undefined") {
                fob_price = fob_price.replace(/,/g, '');
            }

            var gp_percentage = currentRow.find(".wholesale_margin").val();
            if (typeof gp_percentage != "undefined") {
                gp_percentage = gp_percentage.replace(/,/g, '');
            }

            var fob_currency = currentRow.find(".product_unit_currency").val();

            var pricing_currency = $("#pricing_currency").val();

            var packing_quantity = currentRow.find(".packing_quantity").val();
            if (typeof packing_quantity != "undefined") {
                packing_quantity = packing_quantity.replace(/,/g, '');
            }

            var total_costs_fiw = currentRow.find(".total_costs_fis").val();
            if (typeof total_costs_fiw != "undefined") {
                total_costs_fiw = total_costs_fiw.replace(/,/g, '');
            }

            var total_costs_fis = currentRow.find(".total_landed_costs_fis").val();
            if (typeof total_costs_fis != "undefined") {
                total_costs_fis = total_costs_fis.replace(/,/g, '');
            }

            var landed_product_price_db = currentRow.find(".landed_product_price").val();
            var order_type = $(".order_type").val();

            var retail_margin = currentRow.find(".retail_margin").val();
            if (typeof retail_margin != "undefined") {
                retail_margin = retail_margin.replace(/,/g, '');
            }

//        getting FOB price base currnecy value
            var converted_fob_price = 0;
//            console.log('pricing curr->' + pricing_currency, fob_currency, fob_price);
            converted_fob_price = convertCurrentToBaseCurrency(pricing_currency, fob_currency, fob_price);
//        console.log(order_type);
            //ends
            if (order_type === '1') {
                //calculate gp value for FOB order type
                var gross_profit_value = 0;
                gross_profit_value = ((converted_fob_price * gp_percentage) / 100) * uom;
                if (isNaN(gross_profit_value)) {
                    gross_profit_value = 0;
                }
                currentRow.find('.gross_profit_value').val(gross_profit_value.toFixed(2));
                //ends 


                //>>>>>>>>>---calcualting gp percentage ---->>>>>>>>
//                gp_percentage = ((gross_profit_value * 100) / (uom * converted_fob_price));
//                currentRow.find('.wholesale_margin').val(gp_percentage.toFixed(2));
                //ends


                //calculate price to customer for FOB order type
                var price_to_customer = 0;
                price_to_customer = converted_fob_price * (1 + (gp_percentage / 100)) * uom;

                if (isNaN(price_to_customer)) {
                    price_to_customer = 0;
                }

                currentRow.find('.wholesale_price').val(price_to_customer.toFixed(2));
                var rrp = price_to_customer * (1 + (retail_margin / 100));
                currentRow.find('.retail_price').val(rrp.toFixed(2));
                //ends
            } else if (order_type === '2' || order_type === '3') {

                //calculating landed_product_price for FIS and FIW order type
                var landed_product_price = 0;

                if (order_type === '2') { //landed price for FIS
                    landed_product_price = converted_fob_price + (total_costs_fis / packing_quantity);
                    console.log('landed product price->' + converted_fob_price, total_costs_fis, packing_quantity, landed_product_price);
                } else if (order_type === '3') { //landed price for FIW
                    landed_product_price = converted_fob_price + (total_costs_fiw / packing_quantity);
//                    console.log('landed product price->' + converted_fob_price, total_costs_fiw, packing_quantity, landed_product_price);
                }
//                landed_product_price = converted_fob_price + (total_costs_fis / packing_quantity);
//                console.log('landed product price->' + converted_fob_price, total_costs_fis, packing_quantity, landed_product_price);
                if (!isFinite(landed_product_price)) {
                    landed_product_price = 0;
                }
//            if(landed_product_price == Infinity && landed_product_price !== -Infinity){
//               landed_product_price = 0; 
//            }
                currentRow.find('.landed_product_price').val(landed_product_price.toFixed(2));
                //calculate gp value for FIS and FIW order type
                var gross_profit_value = 0;
                gross_profit_value = ((landed_product_price * gp_percentage) / 100) * uom;

                //code for creating objet of numberformat for formatting number
                var nf = new Intl.NumberFormat();
                //ends
//                gross_profit_value = gross_profit_value.toLocaleString();

                if (isNaN(gross_profit_value)) {
                    gross_profit_value = 0;
                }
                currentRow.find('.gross_profit_value').val(nf.format(gross_profit_value));

                //calculate price_to_customer for FIS and FIW order type
                var price_to_customer = 0;
                price_to_customer = landed_product_price * (1 + (gp_percentage / 100)) * uom;

                if (isNaN(price_to_customer)) {
                    price_to_customer = 0;
                }
//            alert(price_to_customer);
                currentRow.find('.wholesale_price').val(nf.format(price_to_customer.toFixed(2)));
                var rrp = price_to_customer * (1 + (retail_margin / 100));
                currentRow.find('.retail_price').val(nf.format(rrp.toFixed(2)));

            }
        });




    }

    $(document).on('keyup', '.gp_percentage_cal', function () {
        var rowId = $(this).closest('tr').attr('id');
        var currentRow = $("#" + rowId);
//        console.log(rowId);
        var uom = currentRow.find(".order_quantity").val();
        if (typeof uom !== "undefined" && uom.length > 0) {
//                var uom = parseInt(uom.replace(",", ""));
            var uom = uom.replace(/,/g, '');
        }

        var fob_price = currentRow.find(".product_unit_price").val();
        if (typeof fob_price != "undefined") {
            fob_price = fob_price.replace(/,/g, '');
        }

        var gp_percentage = currentRow.find(".wholesale_margin").val();
        if (typeof gp_percentage != "undefined") {
            gp_percentage = gp_percentage.replace(/,/g, '');
        }

        var gross_profit_value = currentRow.find(".gross_profit_value").val();
        if (typeof gross_profit_value != "undefined") {
            gross_profit_value = gross_profit_value.replace(/,/g, '');
        }

        var fob_currency = currentRow.find(".product_unit_currency").val();

        var pricing_currency = $("#pricing_currency").val();

        var packing_quantity = currentRow.find(".packing_quantity").val();
        if (typeof packing_quantity != "undefined") {
            packing_quantity = packing_quantity.replace(/,/g, '');
        }

        var total_costs_fiw = currentRow.find(".total_costs_fis").val();
        if (typeof total_costs_fiw != "undefined") {
            total_costs_fiw = total_costs_fiw.replace(/,/g, '');
        }

        var total_costs_fis = currentRow.find(".total_landed_costs_fis").val();
        if (typeof total_costs_fis != "undefined") {
            total_costs_fis = total_costs_fis.replace(/,/g, '');
        }

        var landed_product_price_db = currentRow.find(".landed_product_price").val();
        var order_type = $(".order_type").val();

        var retail_margin = currentRow.find(".retail_margin").val();
        if (typeof retail_margin != "undefined") {
            retail_margin = retail_margin.replace(/,/g, '');
        }

//        getting FOB price base currnecy value
        var converted_fob_price = 0;
//            console.log('pricing curr->' + pricing_currency, fob_currency, fob_price);
        converted_fob_price = convertCurrentToBaseCurrency(pricing_currency, fob_currency, fob_price);
//        console.log(order_type);
        if (order_type === '1') {
            var nf = new Intl.NumberFormat();
            //calculate gp value for FOB order type
//            var gross_profit_value = 0;
//            gross_profit_value = ((converted_fob_price * gp_percentage) / 100) * uom;
//            if (isNaN(gross_profit_value)) {
//                gross_profit_value = 0;
//            }
//            currentRow.find('.gross_profit_value').val(gross_profit_value.toFixed(2));
            //ends 


            //>>>>>>>>>---calcualting gp percentage ---->>>>>>>>
//                gp_percentage = ((gross_profit_value * 100) / (uom * converted_fob_price));
//                currentRow.find('.wholesale_margin').val(gp_percentage.toFixed(2));

            var gp_percentage_final = 0;
            gp_percentage_final = ((gross_profit_value * 100) / (uom * converted_fob_price));
            if (isNaN(gp_percentage_final)) {
                gp_percentage_final = 0;
            }
            currentRow.find('.wholesale_margin').val(nf.format(gp_percentage_final));
            //ends


            //calculate price to customer for FOB order type
            var price_to_customer = 0;
            price_to_customer = converted_fob_price * (1 + (gp_percentage / 100)) * uom;

            if (isNaN(price_to_customer)) {
                price_to_customer = 0;
            }

            currentRow.find('.wholesale_price').val(price_to_customer.toFixed(2));
            var rrp = price_to_customer * (1 + (retail_margin / 100));
            currentRow.find('.retail_price').val(rrp.toFixed(2));
            //ends
        } else if (order_type === '2' || order_type === '3') {

            //calculating landed_product_price for FIS and FIW order type
            var landed_product_price = 0;

            if (order_type === '2') { //landed price for FIS
                landed_product_price = converted_fob_price + (total_costs_fis / packing_quantity);
                console.log('landed product price->' + converted_fob_price, total_costs_fis, packing_quantity, landed_product_price);
            } else if (order_type === '3') { //landed price for FIW
                landed_product_price = converted_fob_price + (total_costs_fiw / packing_quantity);
//                    console.log('landed product price->' + converted_fob_price, total_costs_fiw, packing_quantity, landed_product_price);
            }
//                landed_product_price = converted_fob_price + (total_costs_fis / packing_quantity);
//                console.log('landed product price->' + converted_fob_price, total_costs_fis, packing_quantity, landed_product_price);
            if (!isFinite(landed_product_price)) {
                landed_product_price = 0;
            }
//            if(landed_product_price == Infinity && landed_product_price !== -Infinity){
//               landed_product_price = 0; 
//            }
            currentRow.find('.landed_product_price').val(landed_product_price.toFixed(2));
            //calculate gp value for FIS and FIW order type
//            var gross_profit_value = 0;
//            gross_profit_value = ((landed_product_price * gp_percentage) / 100) * uom;

            //code for creating objet of numberformat for formatting number
            var nf = new Intl.NumberFormat();
            //ends
//                gross_profit_value = gross_profit_value.toLocaleString();

//            if (isNaN(gross_profit_value)) {
//                gross_profit_value = 0;
//            }
//            currentRow.find('.gross_profit_value').val(nf.format(gross_profit_value));

            //calculate price_to_customer for FIS and FIW order type
            var price_to_customer = 0;

            var gp_percentage_final = 0;
            gp_percentage_final = ((gross_profit_value * 100) / (uom * landed_product_price));
            if (isNaN(gp_percentage_final)) {
                gp_percentage_final = 0;
            }
            currentRow.find('.wholesale_margin').val(nf.format(gp_percentage_final));


            price_to_customer = landed_product_price * (1 + (gp_percentage / 100)) * uom;

            if (isNaN(price_to_customer)) {
                price_to_customer = 0;
            }
//            alert(price_to_customer);
            currentRow.find('.wholesale_price').val(nf.format(price_to_customer.toFixed(2)));
            var rrp = price_to_customer * (1 + (retail_margin / 100));
            currentRow.find('.retail_price').val(nf.format(rrp.toFixed(2)));



        }
        setTimeout(function () {
            $('#order_pricing').submit();
        }, 3000);

    });


    $(document).ready(function () {

        Dropzone.autoDiscover = false;
        var url = "<?= url('admin/dropzoneStore?order_id=' . $order->id . '') ?>";
        $("#dZUpload").dropzone({
            url: url,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method: "POST",
            maxFilesize: 15,
            addRemoveLinks: true,
            success: function (file, response) {
                console.log(response);
                var imgName = response;
                file.previewElement.classList.add("dz-success");
                console.log("Successfully uploaded :" + imgName);
                load_order_documents_lines();
            },
            error: function (file, response) {
                file.previewElement.classList.add("dz-error");
            }
        });
    });


    function deleteImage(order_id, image_id) {
        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure you want to delete this doc?',
            buttons: {
                confirm: function () {
                    var url = '<?= url('/'); ?>';
                    $.ajax(url + '/admin/deleteOrderDocument?order->id=' + order_id + '&image_id=' + image_id + '', // request url
                            {
                                method: 'POST',
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                success: function (data, status, xhr) {// success callback function
                                    location.reload();
                                }
                            });
                },
                cancel: function () {
                    // $.alert('Canceled!');
                },

            }
        });


    }
    //strugglers code
    $("#hide_show_all").on("change", function () {
        var hide = $(this).is(":checked");
        $(".hide_show").prop("checked", hide);

        if (hide) {
            $('#tbl_posts tr th').hide(100);
            $('#tbl_posts tr td').hide(100);
        } else {
            $('#tbl_posts tr th').show(100);
            $('#tbl_posts tr td').show(100);
        }
    });
    $(document).on("change", ".hide_show", function () {
        var hide = $(this).is(":checked");

        var all_ch = $(".hide_show:checked").length == $(".hide_show").length;

        $("#hide_show_all").prop("checked", all_ch);

        var ti = $(this).index(".hide_show");

        $('#tbl_posts tr').each(function () {
            if (!hide) {
                $('td:eq(' + ti + ')', this).hide(100);
                $('th:eq(' + ti + ')', this).hide(100);
            } else {
                $('td:eq(' + ti + ')', this).show(100);
                $('th:eq(' + ti + ')', this).show(100);
            }
        });
    });

//    ends
    if (window.history.replaceState) {
        window.history.replaceState(null, null, window.location.href);
    }
    //calculation
//    $(document).on('keyup', '.order_pricing_cal', function () {
//        var rowId = $(this).closest('tr').attr('id');
//        var currentRow = $("#" + rowId);
//        var quantity = currentRow.find(".order_quantity").val();
//        var unit_price = currentRow.find(".product_unit_price").val();
//        var wholesale_margin = currentRow.find(".wholesale_margin").val();
//        var retail_margin = currentRow.find(".retail_margin").val();
//        var wholesale_price = unit_price * quantity * (wholesale_margin / 100 + 1);
//        var retail_price = wholesale_price * (1 + (retail_margin / 100));
//        currentRow.find('.wholesale_price').val(wholesale_price.toFixed(2));
//        currentRow.find('.retail_price').val(retail_price.toFixed(2));
//    });
    //ends

    $(document).ready(function () {
        $(document).on('click change keyup', '.order_pricing', function () {
            $('#order_pricing').submit();
        });
        $(document).on('change', '.select_order_pricing', function () {
            $('#order_pricing').submit();
        });
        $(document).on('change click keyup', '.shipping_plan', function () {
            $('#shipping_plan').submit();
            $('#tracking_order').submit();
        });
        $(document).on('change click keyup', '.tracking_order', function () {
            $('#tracking_order').submit();
        });
        $(document).on('change click keyup', '.order_documents', function () {
            $('#order_documents').submit();
        });

    });
    //for submitting order pricing data on keyup in orders
    $('#order_pricing').on('submit', function (e) {

        e.preventDefault();

        var form_data = new FormData(this);

        form_data.append("_token", $('meta[name="csrf-token"]').attr('content'));
        $.ajax({
            url: "{{url('admin/submit_order_pricing')}}",
            method: "POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                //                        Swal.showLoading();
            },
            onError: function () {
                swal("Error!", 'Something Went Wrong', "error");
            },
            success: function (response)
            {

                if (response) {
                    if (response.status) {
//                        console.log('order line saved');
                        forGpCalculation();
//                        getProducts();
                    }
                 }
            }
        });
    });
    
    
    //for submitting shipping plan data on keyup in orders
    $('#shipping_plan').on('submit', function (e) {

        e.preventDefault();

        var form_data = new FormData(this);

        form_data.append("_token", $('meta[name="csrf-token"]').attr('content'));
        $.ajax({
            url: "{{url('admin/submit_shipping_plan')}}",
            method: "POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                //                        Swal.showLoading();
            },
            onError: function () {
                swal("Error!", 'Something Went Wrong', "error");
            },
            success: function (response)
            {

                if (response) {
                    if (response.status) {
//                        console.log('shipping plan saved');
                    }
                }
            }
        });
    });
    //for submitting shipping plan data on keyup in orders
    $('#tracking_order').on('submit', function (e) {

        e.preventDefault();

        var form_data = new FormData(this);

        form_data.append("_token", $('meta[name="csrf-token"]').attr('content'));
        $.ajax({
            url: "{{url('admin/submit_shipping_plan')}}",
            method: "POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                //                        Swal.showLoading();
            },
            onError: function () {
                swal("Error!", 'Something Went Wrong', "error");
            },
            success: function (response)
            {

                if (response) {
                    if (response.status) {
//                        console.log('Tracking order saved');
                    }
                }
            }
        });
    });
    //for submitting order documents data on keyup in orders
    $('#order_documents').on('submit', function (e) {

        e.preventDefault();

        var form_data = new FormData(this);

        form_data.append("_token", $('meta[name="csrf-token"]').attr('content'));
        $.ajax({
            url: "{{url('admin/submit_order_documents')}}",
            method: "POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                //                        Swal.showLoading();
            },
            onError: function () {
                swal("Error!", 'Something Went Wrong', "error");
            },
            success: function (response)
            {

                if (response) {
                    if (response.status) {
//                        console.log('Order documents name saved');
                    }
                }
            }
        });
    });



    //code for order lines
    $(document).on('change', '.order_supplier_id', function () {
        var supplier_id = $('.order_supplier_id').val();
//        alert(supplier_id);
        $.ajax({
            url: "{{url('admin/getSupplierProductsForOrders')}}",
            method: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {supplier_id: supplier_id},

            beforeSend: function () {
                //                        Swal.showLoading();
            },
            success: function (response)
            {
                if (response) {
                    if (response.status) {
                        $('.order_product_id').html(response.supplier_html);
//                        $('#modalCloseSupplier').trigger('click');
//                        swal("Good job!", "New Supplier Added! Now you can select supplier from dropdown", "success");
                    }
                }
            }
        });
    });
    $(document).on('change', '.order_product_id', function () {
        var product_id = $('.order_product_id').val();
        $.ajax({
            url: "{{url('admin/getproductdetailsonorders')}}",
            method: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {product_id: product_id},

            beforeSend: function () {
                //                        Swal.showLoading();
            },
            success: function (response)
            {
                if (response) {
                    if (response.status) {

                        //appended data in supplier product popup on order.edit
                        $('.product_name').html(response.product.name);
                        $(".product_image img:last-child").remove();
                        $('.product_image').append(response.product_img);

                        $('#order_product_line_id').val(response.product.id);
                    }
                }
            }
        });
    });

    load_order_lines();
    load_shipping_plans();

    function load_order_lines() {
        var order_id = '<?= $order->id ?>';

        $.ajax({
            url: "{{url('admin/load_order_lines')}}",
            method: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {order_id: order_id},
            beforeSend: function () {
                //                        Swal.showLoading();
            },
            onError: function () {
                swal("Error!", 'Something Went Wrong', "error");
            },
            success: function (response)
            {
                $('#load_order_lines').html(response);
                //$('.gp_cal').click();
                setTimeout(function () {
                    //code for select2 in port of dispatch
                    $(".orderlinetable .select2_port_of_dispatch").each(function () {
                        $(this).select2();
                    });
                    //code for format fob price
                    $(".format_me").each(function () {
                        var n = parseInt($(this).val().replace(/\D/g, ''), 10);
                        if (isNaN(n)) {
                            n = '';
                        } else {
                            $(this).val(n.toLocaleString());
                        }
                    });
                    $(".format_me_in_decimals").each(function (evt) {
                        if (evt.which != 190) {//not a fullstop
                            var n = parseFloat($(this).val().replace(/\,/g, ''), 10);
                            if (isNaN(n)) {
                                n = '';
                            } else {
                                $(this).val(n.toLocaleString());
                            }
                        }
                    });
                    // Do something after 1 second 
                }, 1000);



                $('.currency_exchange').click();
                forGpCalculation();
            }
        });
    }
    load_order_documents_lines();
    function load_order_documents_lines() {
        var order_id = '<?= $order->id ?>';

        $.ajax({
            url: "{{url('admin/load_order_documents_lines')}}",
            method: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {order_id: order_id},
            beforeSend: function () {
                //                        Swal.showLoading();
            },
            onError: function () {
                swal("Error!", 'Something Went Wrong', "error");
            },
            success: function (response)
            {
                $('#load_order_documents_lines').html(response);

            }
        });
    }
    function order_line_create() {
        var order_id = '<?= $order->id ?>';
        var order_product_line_id = $('#order_product_line_id').val();
        if (order_product_line_id != null && order_product_line_id != '') {
            $.ajax({
                url: "{{url('admin/insert_order_lines')}}",
                method: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {order_id: order_id, order_product_line_id: order_product_line_id},
                beforeSend: function () {
                    //                        Swal.showLoading();
                },
                onError: function () {
                    swal("Error!", 'Something Went Wrong', "error");
                },
                success: function (response)
                {
                    load_order_lines();
                    load_shipping_plans();
                    //$('.gp_cal').click();
                    //$('#load_order_lines').html(response);

                }
            });
        } else {
            alert('Please try again');
        }
    }

    //code for shipping plans
    function load_shipping_plans() {
        var order_id = '<?= $order->id ?>';

        $.ajax({
            url: "{{url('admin/load_shipping_plans')}}",
            method: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {order_id: order_id},
            beforeSend: function () {
                 $("#loading").show();
                 // Swal.showLoading();
            },
            complete: function(){ 
                $("#loading").hide();
            },
            onError: function () {
                swal("Error!", 'Something Went Wrong', "error");
            },
            success: function (response)
            {
                $('#load_shipping_plans').html(response);

                setTimeout(function () {
                    $("#tbl_shipping_plan .select2_shipping_dispatch").each(function () {
                        $(this).select2();
                    });


                    if ($.fn.dataTable.isDataTable('#tbl_shipping_plan')) {
//                        table = $('#example').DataTable();
                    } else {
                        $('#tbl_shipping_plan').dataTable({
                            "bPaginate": false,
                            "bInfo": false,
                            "order": [],
                        });
                    }

                    $(".format_me").each(function () {
                        var n = parseInt($(this).val().replace(/\D/g, ''), 10);
                        if (isNaN(n)) {
                            n = '';
                        } else {
                            $(this).val(n.toLocaleString());
                        }
                    });
                    $(".format_me_in_decimals").each(function (evt) {
                        if (evt.which != 190) {//not a fullstop
                            var n = parseFloat($(this).val().replace(/\,/g, ''), 10);
                            if (isNaN(n)) {
                                n = '';
                            } else {
                                $(this).val(n.toLocaleString());
                            }
                        }
                    });
                    // Do something after 1 second 
                }, 1000);

            }
        });
    }




    $(document).ready(function () {
        $(".copy_container").click(function () {

            var id = $(this).closest("tr").find("input").val();
            $.ajax({
                url: "{{url('admin/copy_container')}}",
                method: "get",
                data: {
                    id: id
                },
                success: function (response)
                {
                    if (response) {
                        load_shipping_plans();
                     
                    }
                }
            });
        });
    });
    //code for tracking order
    load_tracking_order();
    function load_tracking_order() {
        var order_id = '<?= $order->id ?>';
        $.ajax({
            url: "{{url('admin/load_tracking_order')}}",
            method: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {order_id: order_id},
            beforeSend: function () {
                //                        Swal.showLoading();
            },
            onError: function () {
                swal("Error!", 'Something Went Wrong', "error");
            },
            success: function (response)
            {
                $('#load_tracking_order').html(response);
                setTimeout(function () {
//                    $('#example').dataTable({
//                        "bPaginate": false,
//                        "bInfo": false,
//  
//                        "columnDefs": [
//                            {
//                                "targets": [1],
//                                "createdCell": function (td, cellData, rowData, row, col) {
//                                    if(cellData == 'Shivam Is Greeate'){
//                                         $(td).attr('colspan','19');
//                                    }
//                                   
//                                }
//                            }
//                        ],


//                    });
                    $(".format_me").each(function () {
                        var n = parseInt($(this).val().replace(/\D/g, ''), 10);
                        if (isNaN(n)) {
                            n = '';
                        } else {
                            $(this).val(n.toLocaleString());
                        }
                    });
                    $(".format_me_in_decimals").each(function (evt) {
                        if (evt.which != 190) {//not a fullstop
                            var n = parseFloat($(this).val().replace(/\,/g, ''), 10);
                            if (isNaN(n)) {
                                n = '';
                            } else {
                                $(this).val(n.toLocaleString());
                            }
                        }
                    });

                    // Do something after 1 second 
                }, 1000);

            }
        });
    }
    function shipping_plans_create() {
        var order_id = '<?= $order->id ?>';
        $('.add_container_row').hide();
        $.ajax({
            url: "{{url('admin/insert_shipping_plans')}}",
            method: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {order_id: order_id},
            beforeSend: function () {
                $("#loading").show();
            },
            onError: function () {
                swal("Error!", 'Something Went Wrong', "error");
            },
            success: function (response)
            {

                load_order_lines();
                load_shipping_plans();
                $("#loading").hide();
                $('.add_container_row').show();
                //$('#load_order_lines').html(response);

            }
        });
    }





</script>
@endsection
