
<!-- ----------------------****************tab code**************************------------ --> 
 
</div><!-- comment -->
<ul class="nav nav-tabs">
    <li><a data-toggle="tab" href="#order_pricing_home" id="home_active">Order Pricing</a></li>
    <li><a data-toggle="tab"    href="#shipping_pan" onclick="load_shipping_plans();">Shipping Plan</a> </li>
    @if(\Auth::user()->roles->first()->name != 'supplier')
    <li><a data-toggle="tab" href="#summary" onclick="load_tracking_order({{ $order->id }});">Tracking Order</a></li>
    @endif
    <li><a data-toggle="tab" href="#documentation">Documentation</a></li>
</ul>

<div class="tab-content">
    <div id="order_pricing_home" class="tab-pane fade in active">
        <br>
        <?php  if(\Auth::user()->roles->first()->name == 'supplier' && \Auth::user()->supplier_id != null){
                    if($order->user_id == \Auth::id()){
                        $check = "yes";
                    }else{
                        $check = "no";
                    }
                }
                
                if(\Auth::user()->roles->first()->name == 'super_admin'){
                    $check = "yes";
                }
        ?>
        <form name="order_pricing" action="javascript:void(0)" accept-charset="UTF-8" class="form-horizontal" id="order_pricing" enctype="multipart/form-data" style="overflow-y: hidden;">
            <input type="hidden" class="form-horizontal order_id" name="order_id" value="{{ $order->id }}">
            <div class="row">    
                <div class="col-sm-6 col-md-2 form-group{{ $errors->has('name') ? 'has-error' : ''}}">
                    {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
                        <input type="text" class="form-control order_pricing @if($check == 'no') shipping_plan_tracking  @endif" name="name" id="name" value="{{ $order->name }}" @if($check == 'no') disabled  @endif>
                    
<!--                    {!! Form::text('name', null, ('' == 'required') ? ['class' => 'form-control order_pricing  @if($check == "no") shipping_plan_tracking  @endif', 'required' => 'required'] : ['class' => 'form-control order_pricing']) !!}-->
                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                </div>
                <?php
                $status = ['0' => 'Quote', '1' => 'Pending Approval', '2' => 'Approved', '3' => 'Shipping', '4' => 'Closed'];
                $order_type = ['1' => 'FOB', '2' => 'FIS', '3' => 'FIW'];
                ?>
                <div class="col-sm-6 col-md-2 form-group{{ $errors->has('confirmed') ? 'has-error' : ''}}">
                    {!! Form::label('confirmed', 'Order Status', ['class' => 'control-label']) !!}
                    <?php /* <select class="form-control order_pricing" name="confirmed">
                      @foreach($status as $key => $value)
                      <option value="<?php echo $key; ?>" <?php
                      if ($order->confirmed == $key) {
                      echo 'selected';
                      } else {
                      echo '';
                      }
                      ?>><?php echo $value; ?></option>
                      @endforeach
                      </select> */ ?>
                    <select class="form-control confirmed order_pricing @if($check == 'no') shipping_plan_tracking select_webkit_hide @endif" name="confirmed"  @if($check == 'no') disabled @endif><?php $data = get_order_status_list(); ?>
                        <option value="">Select Order Status</option>
                        @foreach($data as $key => $value)
                        @if(isset($formMode) && $formMode == 'edit' && $key == $order->confirmed)
                        <option value="{{$key}}" selected="">{{$value}}</option>

                        @else
                        <option value="{{$key}}">{{$value}}</option>
                        @endif
                        @endforeach
                    </select>
                    {!! $errors->first('confirmed', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="col-sm-6 col-md-2 form-group{{ $errors->has('order_type') ? 'has-error' : ''}}">
                    {!! Form::label('order_type', 'Order Type', ['class' => 'control-label']) !!}
                    <select class="form-control order_pricing order_type gp_cal @if($check == 'no') shipping_plan_tracking select_webkit_hide @endif" name="order_type" @if($check == 'no') disabled @endif>
                        <option value="">Select</option>
                        @foreach($order_type as $key => $value)
                        <option value="<?php echo $key; ?>" <?php
                        if ($order->order_type == $key) {
                            echo 'selected';
                        } else {
                            echo '';
                        }
                        ?>><?php echo $value; ?></option>
                        @endforeach
                    </select>
                    {!! $errors->first('order_type', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="col-sm-6 col-md-2 form-group">
                    {!! Form::label('pricing_currency', 'Pricing Currency', ['class' => 'control-label']) !!}

                        <select class="form-control pricing_currency order_pricing trigger_currency_exchange  @if($check == 'no') shipping_plan_tracking select_webkit_hide @endif" id="pricing_currency" name="pricing_currency" @if($check == 'no') disabled @endif>
                        @if(isset($order->pricing_currency) && $order->pricing_currency != null)
                        {!!  get_limited_currency_list_with_selected_item($order->pricing_currency) !!}
                        @else
                        {!!  get_limited_currency_list_with_symbols_through_id() !!}
                        @endif


                    </select>
                </div>

                <div class="col-sm-6 col-md-2 form-group{{ $errors->has('po_type') ? 'has-error' : ''}}">
                    {!! Form::label('po_type', 'PO Type', ['class' => 'control-label']) !!}

                    <select class="form-control order_pricing @if($check == 'no') shipping_plan_tracking select_webkit_hide @endif" name="po_type" id="po_type" @if($check == 'no') disabled @endif>
                        <option>Select PO Type</option>
                        <option value="0" <?php
                        if ($order->po_type == '0') {
                            echo 'selected';
                        } else {
                            echo '';
                        }
                        ?>>Single PO</option>
                        <option value="1" <?php
                        if ($order->po_type == '1') {
                            echo 'selected';
                        } else {
                            echo '';
                        }
                        ?>>Multiple PO</option>
                    </select>
                    {!! $errors->first('po_type', '<p class="help-block">:message</p>') !!}
                </div>   
                
            <div class="col-sm-6 col-md-1 form-group @if($order->po_type == '1') d-none @endif {{ $errors->has('name') ? 'has-error' : ''}}" id="po_number">
                {!! Form::label('name', 'PO Number', ['class' => 'control-label']) !!}
                
                <input class="form-control order_pricing @if($check == "no") shipping_plan_tracking  @endif"" name="po_number" type="text" value="{{ $order->po_number }}" @if($check == "no") disabled  @endif">
                
<!--                {!! Form::text('po_number', null, ('' == 'required') ? ['class' => 'form-control order_pricing', 'required' => 'required @if($check == "no") shipping_plan_tracking @endif'] : ['class' => 'form-control order_pricing']) !!}-->
                {!! $errors->first('po_number', '<p class="help-block">:message</p>') !!}
            </div>
                <div class="col-sm-6 col-md-1 form-group{{ $errors->has('active') ? 'has-error' : ''}}">
                    {!! Form::label('active', 'Active', ['class' => 'control-label']) !!}

                    <select class="form-control order_pricing @if($check == 'no') shipping_plan_tracking select_webkit_hide @endif" name="active" @if($check == 'no') disabled @endif>
                        <option value="0" <?php
                        if ($order->active == '1') {
                            echo '';
                        } else {
                            echo 'selected';
                        }
                        ?>>Yes</option>
                        <option value="1" <?php
                        if ($order->active == '1') {
                            echo 'selected';
                        } else {
                            echo '';
                        }
                        ?>>No</option>
                    </select>
                    {!! $errors->first('active', '<p class="help-block">:message</p>') !!}
                </div>
            </div>    

            <div id="header">
            </div>
            <div class="well clearfix">
                <button type="button" class="btn btn-primary btn-sm addContact" data-toggle="modal" data-target="#orderProductModalLongnew" style="margin:12px;float:left"><i class="fa fa-plus" aria-hidden="true"></i>
                    Add Product Row
                </button>
                <button type="button" class="btn btn-primary btn-sm " data-toggle="modal" data-target="#showHideModalLongnew" style="margin:12px;float:left"><i class="fa fa-eye" aria-hidden="true"></i>
                    Show/hide columns
                </button>

                @if($order->order_type == '2')
                <a href="{{url('admin/order_pricing_costs_fis/'.$order->id)}}" class="btn btn-primary btn-sm fob_costs" style="margin:12px;float:left"><i class="fa fa-pencil" aria-hidden="true"></i>
                    Calculate FIS Landed Costs
                </a>
                <a href="{{url('admin/order_pricing_costs_fiw/'.$order->id)}}" class="btn btn-primary btn-sm fis_costs" style="display:none; margin:12px;float:left"><i class="fa fa-pencil" aria-hidden="true"></i>
                    Calculate FIW Landed Costs
                </a>
                @elseif($order->order_type == '3')
                <a href="{{url('admin/order_pricing_costs_fiw/'.$order->id)}}" class="btn btn-primary btn-sm fis_costs" style="margin:12px;"><i class="fa fa-pencil" aria-hidden="true"></i>
                    Calculate FIW Landed Costs
                </a>
                <a href="{{url('admin/order_pricing_costs_fis/'.$order->id)}}" class="btn btn-primary btn-sm fob_costs" style="display:none; margin:12px;"><i class="fa fa-pencil" aria-hidden="true"></i>
                    Calculate FIS Landed Costs
                </a>
                @else
                <a href="{{url('admin/order_pricing_costs_fis/'.$order->id)}}" class="btn btn-primary btn-sm fob_costs" style="display:none; margin:12px;float:left"><i class="fa fa-pencil" aria-hidden="true"></i>
                    View FIS Landed Costs
                </a>
                <a href="{{url('admin/order_pricing_costs_fiw/'.$order->id)}}" class="btn btn-primary btn-sm fis_costs" style="display:none; margin:12px;float:left"><i class="fa fa-pencil" aria-hidden="true"></i>
                    View FIW Landed Costs
                </a>
                @endif

            </div>

            <div id = "load_order_lines">

            </div>  

        </form><br><br>    

    </div>

    <!-------------------------------------Shipping Plan Tab data--------------------------------->
    <div id="shipping_pan" class="tab-pane fade">
        <br>
<!--        <a href="javascript:void(0)" class="btn btn-primary btn-sm add_container_row shipping_plan" onclick="shipping_plans_create();"><i class="fa fa-plus" aria-hidden="true"></i>Add Container Row</a>-->
        <a href="javascript:void(0)" class="btn btn-primary btn-sm" style="display : none;"><i class="fa fa-tick" aria-hidden="true"></i>Update</a><br><br>
        <!--button to collapse view totals-->
        @if (\Auth::user()->roles->first()->name != 'supplier')
        <a href="javascript:void(0)" class="btn btn-info btn-sm show_hide_icon" style="border-radius: 50%;" data-toggle="collapse" data-target="#viewcollapse" onclick="load_shipping_totals({{ $order->id }});">
            <i class="fa fa-plus" aria-hidden="true" class=""></i>
        </a>&nbsp;Click here to View Shipping Plan Summary<br><br>
        <!--collapse data-->
        @endif
        <div id="viewcollapse" class="collapse">

        </div>
        <!--ends-->
        <br>
        <form method="post" name="order_pricing" action="javascript:void(0)" accept-charset="UTF-8" class="form-horizontal" id="shipping_plan" enctype="multipart/form-data">
            <input type="hidden" class="form-horizontal" name="order_id" value="{{ $order->id }}">
            <div class="responsive" style="overflow-y:hidden">
                <div id = "load_shipping_plans">

                </div>

            </div>
        </form>
    </div>

    <!-------------------------------------Summary Tab Data--------------------------->
    <div id="summary" class="tab-pane fade">
        <br>
        <!--button to collapse view totals-->
        <a href="javascript:void(0)" class="btn btn-info btn-sm show_hide_tracking_collapse_icon" style="border-radius: 50%;" data-toggle="collapse" data-target="#viewTrackingCollapse" onclick="load_shipping_totals({{ $order->id }});">
            <i class="fa fa-plus" aria-hidden="true"></i>
        </a>&nbsp;Click here to View Summary<br><br>
        <!--collapse data-->
        <div id="viewTrackingCollapse" class="collapse">

        </div>
        <!--ends-->
        <br>
        <form method="post" name="tracking_order" action="javascript:void(0)" accept-charset="UTF-8" class="form-horizontal" id="tracking_order" enctype="multipart/form-data">
            <input type="hidden" class="form-horizontal" name="order_id" value="{{ $order->id }}">
            <div class="responsive" style="overflow-y:hidden">
                <div id = "load_tracking_order">

                </div>

            </div>
        </form>
        <!--<div class="responsive" style="overflow-y:hidden">-->
        <?php /*
          <table class="table table-bordered" id="tbl_summary">
          <thead>
          <tr>
          <th>#</th>
          <th>Shipping Date</th>
          <th>Container Name</th>
          <th>Purchase Order Number</th>
          <th>Port of Origin</th>
          <th>Port of Discharge</th>
          <th>Product Name</th>
          <th>Product Image</th>
          <th># of Units that fit</th>
          <th>Order Quantity</th>
          <th>Container Status</th>
          <th>Total Cost</th>
          <th>Total Wholesale Price</th>
          <th><i class="fa fa-cogs"></i></th>
          </tr>
          </thead>
          <tbody id="tbl_summary_body">
          @if($formMode == 'edit')
          <?php $i = '1'; ?>
          <tr id="rec-<?= $i ?>">
          <input type="hidden" value="" name="order_detail[<?= $i ?>][row_id]" />
          <td><span class="sn"><?= $i ?></span></td>
          <td>
          <input type="text" class="form-control shipping_date" name="order_detail[<?= $i ?>][shipping_date]" value="" readonly/>
          </td>
          <td>
          <input type="text" class="form-control container_name" name="order_detail[<?= $i ?>][container_name]" value="" readonly/>
          </td>
          <td>
          <input type="text" class="form-control purchase_order_number" name="order_detail[<?= $i ?>][purchase_order_number]" value="" readonly/>
          </td>
          <td>
          <input type="text" class="form-control shipping_port" name="order_detail[<?= $i ?>][shipping_port]" value="" readonly/>
          </td>
          <td>
          <input type="text" class="form-control shipping_to_port" name="order_detail[<?= $i ?>][shipping_to_port]" value="" readonly/>
          </td>
          <td>
          <input type="text" class="form-control product_name" name="order_detail[<?= $i ?>][product_name]" value=""/>
          </td>
          <td>
          <input type="text" class="form-control product_image" name="order_detail[<?= $i ?>][product_image]" value="" readonly/>
          </td>
          <td>
          <input type="text" class="form-control number_of_units_that_fit" name="order_detail[<?= $i ?>][number_of_units_that_fit]" value="" readonly/>
          </td>
          <td>
          <input type="text" class="form-control order_quantity" name="order_detail[<?= $i ?>][order_quantity]" value="" readonly/>
          </td>
          <td>
          <input type="text" class="form-control container_status" name="order_detail[<?= $i ?>][container_status]" value="" readonly/>
          </td>
          <td>
          <input type="text" class="form-control total_costs" name="order_detail[<?= $i ?>][total_costs]" value="" readonly/>
          </td>
          <td>
          <input type="text" class="form-control total_wholesale_price" name="order_detail[<?= $i ?>][total_wholesale_price]" value="" readonly/>
          </td>

          <td><a class="btn btn-xs tbl_summary_delete" data-id="<?= $i ?>"><i class="fa fa-trash"></i></a></td>
          </tr>
          @endif
          </tbody>
          </table>
         * 
         */ ?>
        <!--</div>-->
    </div>
    <!-------------------------------------Documentation tab starts----------------------------------->
    <div id="documentation" class="tab-pane">
        <form name="order_documents" action="javascript:void(0)" accept-charset="UTF-8" class="form-horizontal" id="order_documents" enctype="multipart/form-data" style="overflow-y: hidden;">
            <input type="hidden" class="form-horizontal order_id" name="order_id" value="{{ $order->id }}"><br>
            <div id="dZUpload" class="dropzone">
                <div class="dz-default dz-message"><span>Drop Order Document Here</span></div>
                <meta name="csrf-token" content="{{ csrf_token() }}">

            </div><br>
            <div id="load_order_documents_lines">

            </div>
        </form><br>


    </div>    
    <!-------------------------------------Tab ends----------------------------------->
</div>
<!--

<div class = "row" id = "relatedImages">



</div>-->
<script>
 $(document).ready(function(){
     $("#po_type").change(function(){
        if($(this).val() == 1){
            $("#po_number").addClass('d-none');
        }else if($(this).val() == 0){
            $("#po_number").removeClass('d-none');
        } else{
            $("#po_number").addClass('d-none');
        }
        
     });
 })   
 </script>