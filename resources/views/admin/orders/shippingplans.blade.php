
@if(\Auth::user()->roles()->first()->name != 'supplier')
<a href="javascript:void(0)" class="btn btn-primary btn-sm add_container_row shipping_plan" onclick="shipping_plans_create();"><i class="fa fa-plus" aria-hidden="true"></i>Add Container</a>
@endif

<table class="table table-bordered" id="tbl_shipping_plan">
    <thead>
        <tr>
            @if(\Auth::user()->roles()->first()->name != 'supplier')
            <th></th>
            @endif
            <th>#</th>
            @if(\Auth::user()->roles()->first()->name != 'supplier')
            <th>Container Name</th>
            @endif
            <th>Purchase Order Number</th>
                <th>Port of Discharge</th>
             @if(\Auth::user()->roles()->first()->name != 'supplier')
            <th>Container Products</th>
            @endif
            <th>Products/Supplier</th>
            @if(\Auth::user()->roles()->first()->name != 'supplier')
            <th class="d-none">Total Container</th>
            @endif
            <th>Description</th>
            <th>Status</th>
            <th>Cargo Ready Date</th>
            <th>Dispatched Date</th>
            <th>ETA Date</th>
            <th>Fumigated</th>
            <th>BL Number</th>
            @if(\Auth::user()->roles()->first()->name != 'supplier')
            <th>Total Cost</th>
            <th>Customer Buy Price</th>
            <th>Profit</th>
            @endif
            @if(\Auth::user()->roles()->first()->name != 'supplier')
            <th><i class="fa fa-cogs"></i></th>
            @endif
        </tr>
    </thead>
                
    <tbody id="tbl_shipping_plan_body">
        <?php $i = '1'; ?>
        @foreach($shippingPlan as $details)
        <tr id="rec-<?= $i ?>">

    
    @if(\Auth::user()->roles()->first()->name == 'supplier' && get_order_lines_shipping_plan_product_length($details->id))
    <input type="hidden" value="{{$details->id}}" name="shipping_plan[<?= $i ?>][row_id]" class="shipping_rowId"/>
    @if(\Auth::user()->roles()->first()->name != 'supplier')
    <td value="{{$details->id}}" class="copy_container" style="cursor: pointer;"><i class="fa fa-clone" aria-hidden="true"></i></td>
    @endif
    
    <td><span class="sn">{{$details->id}}</span></td>

    @if(\Auth::user()->roles()->first()->name != 'supplier')
    <td data-search='<?= get_container_name_by_id($details->container_id) ?>' data-order='<?= get_container_name_by_id($details->container_id) ?>'  <?php if (get_shipping_container_product_count($details->id) > "0") { ?> onclick='swal({title: "Warning!", title : "Warning!! Not Allowed", text: "You need to remove Container Products first.", icon: "warning", button: "Ok!", dangerMode: true, });' <?php } ?>>
        <select class="form-control container_id shipping_plan c hide_unhide_container_select" name="shipping_plan[<?= $i ?>][container_id]" style="width: 180px;" <?php if (get_shipping_container_product_count($details->id) > "0") { ?> disabled="" <?php } ?>><?php $data = get_containers_list(); ?>
            <option value="">Select Container</option>
            @foreach($data as $key => $value)
            @if(isset($details->container_id) && $details->container_id == $key)
            <option value="{{$key}}" selected>{{$value}}</option>
            @else
            <option value="{{$key}}">{{$value}}</option>
            @endif
            @endforeach
        </select>
    </td>
@endif
    
    <td data-search='<?= $details->purchase_order_number ?>' data-order='<?= $details->purchase_order_number ?>'>
        <input type="text" class="form-control purchase_order_name shipping_plan @if(\App\Order::where('id', $order_id)->first()->po_type == '0') shipping_plan_tracking @endif" name="shipping_plan[<?= $i ?>][purchase_order_number]" value="{{$details->purchase_order_number}}" @if(\App\Order::where('id', $order_id)->first()->po_type == '0') disabled @endif style="width: 150px;"/>
    </td>

    <td data-search='<?= get_shipping_port_name_by_id($details->port_of_discharge) ?>' data-order='<?= get_shipping_port_name_by_id($details->port_of_discharge) ?>'>
        <select class="form-control port_of_discharge shipping_plan select2_shipping_dispatch" name="shipping_plan[<?= $i ?>][port_of_discharge]" style="width: 140px;"><?php $data = get_shipping_port_list(); ?>
            <option value="">Port of Discharge</option>
            @foreach($data as $key => $value)
            @if($key == $details->port_of_discharge)
            <option value="{{$key}}" selected>{{$value}}</option>
            @else
            <option value="{{$key}}">{{$value}}</option>
            @endif
            @endforeach
        </select>
    </td>
 @if(\Auth::user()->roles()->first()->name != 'supplier')
    <td>
        @if($details->container_id == '' || $details->container_id == null)
        <a href='javascript:void(0)' class="btn btn-success btn-sm container_first" style="display:block;" onclick='swal({
            title: "Warning!",
                    text: "You need to select a container name",
                    icon: "warning",
                    button: "Ok!",
            });'>Select Container First</a>
        <a href='{{url('admin/shipping_container_products?order_id='.$order_id.'&&shipping_plan_id='.$details->id)}}' class="btn btn-success btn-sm view_products view_product" style="display:none;">View Products ({{get_shipping_container_product_count($details->id)}})</a>
        @else
        <a href='{{url('admin/shipping_container_products?order_id='.$order_id.'&&shipping_plan_id='.$details->id)}}' class="btn btn-success btn-sm view_products view_product" style="display:block;">View Products ({{get_shipping_container_product_count($details->id)}})</a>
        <a href='javascript:void(0)' class="btn btn-success btn-sm container_first" style="display:none;" onclick='swal({
            title: "Warning!",
                    text: "You need to select a container name",
                    icon: "warning",
                    button: "Ok!",
            });'>Select Container First</a>
        @endif
    </td>
    @endif
    
    <td style="width: 45%;"><?=get_order_lines_shipping_plan_product_name_as_string($details->id)?></td>
     @if(\Auth::user()->roles()->first()->name != 'supplier')
     <td class="d-none">
        <?php
        echo DB::table('shipping_plans')
                ->where('order_id', $order_id)
                ->where('deleted_at', null)
                ->count('id');
        ?>
    </td>
   @endif
     <td data-search='<?= $details->description ?>' data-order='<?= $details->description ?>'>
        <input type="text" class="form-control description shipping_plan" name="shipping_plan[<?= $i ?>][description]" value="{{$details->description}}" style="width: 150px;"/>
    </td>
    <td data-search='<?= get_shipping_status_name_by_id($details->status) ?>' data-order='<?= get_shipping_status_name_by_id($details->status) ?>'>
        <select class="form-control shipping_plan status" name="shipping_plan[<?= $i ?>][status]" style="width:170px;">
            <?php
            $status = get_shipping_status_list();
            ?>
            <option value="">Select</option>
            @foreach($status as $key => $value)
            @if($key == $details->status)
            <option value="{{$key}}" selected>{{$value}}</option>
            @else
            <option value="{{$key}}">{{$value}}</option>
            @endif
            @endforeach
        </select>
    </td>
    <td data-search='<?= $details->cargo_ready_date ?>' data-order='<?= $details->cargo_ready_date ?>'>
        <input type="date" class="form-control cargo_ready_date shipping_plan" name="shipping_plan[<?= $i ?>][cargo_ready_date]" value="{{$details->cargo_ready_date}}"/>
    </td>
    <td data-search='<?= $details->dispatched_date ?>' data-order='<?= $details->dispatched_date ?>'>
        <input type="date" class="form-control dispatched_date shipping_plan" name="shipping_plan[<?= $i ?>][dispatched_date]" value="{{$details->dispatched_date}}"/>
    </td>
    <td data-search='<?= $details->eta_date ?>' data-order='<?= $details->eta_date ?>'>
        <input type="date" class="form-control eta_date shipping_plan" name="shipping_plan[<?= $i ?>][eta_date]" value="{{$details->eta_date}}"/>
    </td>
    <td data-search='<?= $details->fumigated ?>' data-order='<?= $details->fumigated ?>'>
        <select class="form-control fumigated shipping_plan " name="shipping_plan[<?= $i ?>][fumigated]" style="width: 140px;"><?php $data = ['0' => 'No','1'=>'Yes']; ?>
            <option value="">Select</option>
            @foreach($data as $key => $value)
            <option value="{{$key}}" <?php if($key == $details->fumigated && $details->fumigated != '' && $details->fumigated != null){ echo "selected"; }  ?> ><?= $value?></option>
            @endforeach
        </select>
    </td>
    <td data-search='<?= $details->bl_number ?>' data-order='<?= $details->bl_number ?>'>
        <input type="text" class="form-control bl_number shipping_plan" name="shipping_plan[<?= $i ?>][bl_number]" value="{{$details->bl_number}}" style="width: 150px;"/>
    </td>
    @if(\Auth::user()->roles()->first()->name != 'supplier')
    <td data-search='<?= $details->total_cost ?>' data-order='<?= $details->total_cost ?>' >
        <input type="text" class="form-control total_cost shipping_plan format_me_in_decimals hide_inputs" name="shipping_plan[<?= $i ?>][total_cost]" value="{{$details->total_cost}}" disabled style="width: 120px;"/>
    </td>

    <td data-search='<?= $details->customer_buy_price ?>' data-order='<?= $details->customer_buy_price ?>' >
        <input type="text" class="form-control customer_buy_price shipping_plan format_me_in_decimals hide_inputs" name="shipping_plan[<?= $i ?>][customer_buy_price]" value="{{$details->customer_buy_price}}" disabled style="width: 120px;"/>
    </td>
    <td data-search='<?= $details->profit ?>' data-order='<?= $details->profit ?>'>
        <input type="text" class="form-control profit shipping_plan format_me_in_decimals hide_inputs" name="shipping_plan[<?= $i ?>][profit]" value="{{$details->profit}}" disabled="" style="width: 120px;"/>
    </td>
  @endif

 @if(\Auth::user()->roles()->first()->name != 'supplier')
    <td><a class="btn btn-xs tbl_shipping_plan_delete" data-id="<?= $i ?>" onclick="deleteOrderDetail('shipping_plans',{{$details->id}});"><i class="fa fa-trash"></i></a></td>
 @endif
 @endif
 
 
 
 @if(\Auth::user()->roles()->first()->name == 'super_admin' || \Auth::user()->roles()->first()->name == 'company')
 <input type="hidden" value="{{$details->id}}" name="shipping_plan[<?= $i ?>][row_id]" class="shipping_rowId"/>
    @if(\Auth::user()->roles()->first()->name != 'supplier')
    <td value="{{$details->id}}" class="copy_container" style="cursor: pointer;"><i class="fa fa-clone" aria-hidden="true"></i></td>
    @endif
    
    <td><span class="sn">{{$details->id}}</span></td>

    @if(\Auth::user()->roles()->first()->name != 'supplier')
    <td data-search='<?= get_container_name_by_id($details->container_id) ?>' data-order='<?= get_container_name_by_id($details->container_id) ?>'  <?php if (get_shipping_container_product_count($details->id) > "0") { ?> onclick='swal({title: "Warning!", title : "Warning!! Not Allowed", text: "You need to remove Container Products first.", icon: "warning", button: "Ok!", dangerMode: true, });' <?php } ?>>
        <select class="form-control container_id shipping_plan c hide_unhide_container_select" name="shipping_plan[<?= $i ?>][container_id]" style="width: 180px;" <?php if (get_shipping_container_product_count($details->id) > "0") { ?> disabled="" <?php } ?>><?php $data = get_containers_list(); ?>
            <option value="">Select Container</option>
            @foreach($data as $key => $value)
            @if(isset($details->container_id) && $details->container_id == $key)
            <option value="{{$key}}" selected>{{$value}}</option>
            @else
            <option value="{{$key}}">{{$value}}</option>
            @endif
            @endforeach
        </select>
    </td>
@endif
    <?php //dd(\App\Order::all()); ?>
    <td data-search='<?= $details->purchase_order_number ?>' data-order='<?= $details->purchase_order_number ?>'>
        <input type="text" class="form-control purchase_order_name shipping_plan @if(\App\Order::where('id', $order_id)->first()->po_type == '0') shipping_plan_tracking @endif" name="shipping_plan[<?= $i ?>][purchase_order_number]" value="{{$details->purchase_order_number}}" @if(\App\Order::where('id', $order_id)->first()->po_type == '0') disabled @endif style="width: 150px;"/>
    </td>

    <td data-search='<?= get_shipping_port_name_by_id($details->port_of_discharge) ?>' data-order='<?= get_shipping_port_name_by_id($details->port_of_discharge) ?>'>
        <select class="form-control port_of_discharge shipping_plan select2_shipping_dispatch" name="shipping_plan[<?= $i ?>][port_of_discharge]" style="width: 140px;"><?php $data = get_shipping_port_list(); ?>
            <option value="">Port of Discharge</option>
            @foreach($data as $key => $value)
            @if($key == $details->port_of_discharge)
            <option value="{{$key}}" selected>{{$value}}</option>
            @else
            <option value="{{$key}}">{{$value}}</option>
            @endif
            @endforeach
        </select>
    </td>
 @if(\Auth::user()->roles()->first()->name != 'supplier')
    <td>
        @if($details->container_id == '' || $details->container_id == null)
        <a href='javascript:void(0)' class="btn btn-success btn-sm container_first" style="display:block;" onclick='swal({
            title: "Warning!",
                    text: "You need to select a container name",
                    icon: "warning",
                    button: "Ok!",
            });'>Select Container First</a>
        <a href='{{url('admin/shipping_container_products?order_id='.$order_id.'&&shipping_plan_id='.$details->id)}}' class="btn btn-success btn-sm view_products view_product" style="display:none;">View Products ({{get_shipping_container_product_count($details->id)}})</a>
        @else
        <a href='{{url('admin/shipping_container_products?order_id='.$order_id.'&&shipping_plan_id='.$details->id)}}' class="btn btn-success btn-sm view_products view_product" style="display:block;">View Products ({{get_shipping_container_product_count($details->id)}})</a>
        <a href='javascript:void(0)' class="btn btn-success btn-sm container_first" style="display:none;" onclick='swal({
            title: "Warning!",
                    text: "You need to select a container name",
                    icon: "warning",
                    button: "Ok!",
            });'>Select Container First</a>
        @endif
    </td>
    @endif
    
    <td style="width: 45%;"><?=get_order_lines_shipping_plan_product_name_as_string($details->id)?></td>
     @if(\Auth::user()->roles()->first()->name != 'supplier')
     <td class="d-none">
        <?php
        echo DB::table('shipping_plans')
                ->where('order_id', $order_id)
                ->where('deleted_at', null)
                ->count('id');
        ?>
    </td>
   @endif
     <td data-search='<?= $details->description ?>' data-order='<?= $details->description ?>'>
        <input type="text" class="form-control description shipping_plan" name="shipping_plan[<?= $i ?>][description]" value="{{$details->description}}" style="width: 150px;"/>
    </td>
    <td data-search='<?= get_shipping_status_name_by_id($details->status) ?>' data-order='<?= get_shipping_status_name_by_id($details->status) ?>'>
        <select class="form-control shipping_plan status" name="shipping_plan[<?= $i ?>][status]" style="width:170px;">
            <?php
            $status = get_shipping_status_list();
            ?>
            <option value="">Select</option>
            @foreach($status as $key => $value)
            @if($key == $details->status)
            <option value="{{$key}}" selected>{{$value}}</option>
            @else
            <option value="{{$key}}">{{$value}}</option>
            @endif
            @endforeach
        </select>
    </td>
    <td data-search='<?= $details->cargo_ready_date ?>' data-order='<?= $details->cargo_ready_date ?>'>
        <input type="date" class="form-control cargo_ready_date shipping_plan" name="shipping_plan[<?= $i ?>][cargo_ready_date]" value="{{$details->cargo_ready_date}}"/>
    </td>
    <td data-search='<?= $details->dispatched_date ?>' data-order='<?= $details->dispatched_date ?>'>
        <input type="date" class="form-control dispatched_date shipping_plan" name="shipping_plan[<?= $i ?>][dispatched_date]" value="{{$details->dispatched_date}}"/>
    </td>
    <td data-search='<?= $details->eta_date ?>' data-order='<?= $details->eta_date ?>'>
        <input type="date" class="form-control eta_date shipping_plan" name="shipping_plan[<?= $i ?>][eta_date]" value="{{$details->eta_date}}"/>
    </td>
    <td data-search='<?= $details->fumigated ?>' data-order='<?= $details->fumigated ?>'>
        <select class="form-control fumigated shipping_plan " name="shipping_plan[<?= $i ?>][fumigated]" style="width: 140px;"><?php $data = ['0' => 'No','1'=>'Yes']; ?>
            <option value="">Select</option>
            @foreach($data as $key => $value)
            <option value="{{$key}}" <?php if($key == $details->fumigated && $details->fumigated != '' && $details->fumigated != null){ echo "selected"; }  ?> ><?= $value?></option>
            @endforeach
        </select>
    </td>
    <td data-search='<?= $details->bl_number ?>' data-order='<?= $details->bl_number ?>'>
        <input type="text" class="form-control bl_number shipping_plan" name="shipping_plan[<?= $i ?>][bl_number]" value="{{$details->bl_number}}" style="width: 150px;"/>
    </td>
    @if(\Auth::user()->roles()->first()->name != 'supplier')
    <td data-search='<?= $details->total_cost ?>' data-order='<?= $details->total_cost ?>' >
        <input type="text" class="form-control total_cost shipping_plan format_me_in_decimals hide_inputs" name="shipping_plan[<?= $i ?>][total_cost]" value="{{$details->total_cost}}" disabled style="width: 120px;"/>
    </td>

    <td data-search='<?= $details->customer_buy_price ?>' data-order='<?= $details->customer_buy_price ?>' >
        <input type="text" class="form-control customer_buy_price shipping_plan format_me_in_decimals hide_inputs" name="shipping_plan[<?= $i ?>][customer_buy_price]" value="{{$details->customer_buy_price}}" disabled style="width: 120px;"/>
    </td>
    <td data-search='<?= $details->profit ?>' data-order='<?= $details->profit ?>'>
        <input type="text" class="form-control profit shipping_plan format_me_in_decimals hide_inputs" name="shipping_plan[<?= $i ?>][profit]" value="{{$details->profit}}" disabled="" style="width: 120px;"/>
    </td>
  @endif

 @if(\Auth::user()->roles()->first()->name != 'supplier')
    <td><a class="btn btn-xs tbl_shipping_plan_delete" data-id="<?= $i ?>" onclick="deleteOrderDetail('shipping_plans',{{$details->id}});"><i class="fa fa-trash"></i></a></td>
 @endif
 @endif
  
</tr>
<?php $i++; ?>
@endforeach

</tbody>
</table>
<script>
$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
</script>
<script>
$( document ).ready(function() {
    $(".copy_container").click(function(){

        var id = $(this).closest("tr").find("input").val();
//        if (confirm("Are you sure you want to copy?")) {
        swal({
            title: "Are you sure?",
            text: "Do you really want to copy ?",
//            icon: "warning",
            type: 'warning',
            buttons: true,
//            dangerMode: true,
        })
        .then((result) => {
            if (result) {
                $.ajax({
                    url: "{{url('admin/copy_container')}}",
                    method: "get",
                    data: {
                    id: id
                    },
                    success: function (response)
                    {
                        if (response) {
                            load_shipping_plans();
                        }
                    }
                });
            }
        });
    });
});

</script>
