@extends('layouts.backend')

@section('content')

<!-------------------------------------Shipping Plan Tab data--------------------------------->
<style>
    table {
        border-collapse: separate;
        border-spacing: 0 2em;
    }
    td input{
        border-radius: 10%;
    }

</style>
<div class="container">
    <div class="row">
        @include('admin.sidebar')

        <div class="col-md-12">
            <div class="card">
                <?php
                $order_id = Request::get('order_id');
                ?>
                <div class="card-header">Edit Order #<?php echo $order_id; ?></div>
                <div class="card-body">
                    <ul class="nav nav-tabs">

                        <li class="active"><a href="{{url("admin/orders/".$order_id."/edit")}}">Back to Order Pricing</a></li>
                        <li><a href="{{url("admin/orders/".$order_id."/edit")}}#shipping_pan"><b><u>Shipping Plan</u></b></a></li>
                        <li><a href="{{url("admin/orders/".$order_id."/edit")}}#summary">Summary</a></li>
                        <li><a href="{{url("admin/orders/".$order_id."/edit")}}#documentation">Documentation</a></li>
                    </ul>
                    <div id=""><br>
                        <form method="post" name="" action="javascript:void(0)" accept-charset="UTF-8" class="form-horizontal" id="order_pricing_costs_fob" enctype="multipart/form-data">
                            <div class="responsive" style="overflow-y:hidden">
                                <!-- ********************supplier's contact ID**********************************-->
                                <?php if (Request::exists('order_id')) { ?><input type="hidden" name="order_id" class="form-control" id="order_id" value="{{ Request::get('order_id') }}"/><?php } ?>
                                <!--***************************************ends**********************************-->
                                <div id="order_pricing_costs_fob">
                                    <input type="hidden" value="{{$order_id}}" name="order_id" />

                                    <table class="table fob_table">
                                        <thead>
                                        <th class="thicon">FOB</th>
                                        <?php
                                        $i = '1';
                                        foreach ($orderDetails as $details):
                                            $product_data = get_product_details_by_id($details->product_id);
                                            ?>
                                            <th class="thicon">{{$product_data->name}}</th>
                                            <?php
                                            $i++;
                                        endforeach;
                                        ?>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <div class="m1">
                                                        <div class="m1a x"><p>FOB Free on Board</p></div>
                                                        <div class="m1a x fob_factory_port"><p>Costs from factory to the port</p></div>
                                                        <div class="m1a x fob_custom"><p>Custom and other associated cost in orignation port	</p></div></div>
                                                </td>

                                                <?php
                                                $i = '1';
                                                foreach ($orderDetails as $details):
                                                    $product_data = get_product_details_by_id($details->product_id);
//                                                    dd(json_encode($details));
                                                    ?>
                                                    <td id="<?php echo $i; ?>">
                                                        <input type="hidden" value="{{$details->id}}" name="order_pricing[<?= $i ?>][row_id]" />
                                                        <input type="hidden" class="form-control product_unit_price" value="{{$details->product_unit_price}}" name="product_unit_price" />
                                                        <input type="hidden" class="form-control packing_quantity" value="{{$details->packing_quantity}}" name="packing_quantity" />
                                                        <input type="hidden" class="form-control pricing_currency" value="{{$details->pricing_currency}}" name="pricing_currency" />
                                                        <input type="hidden" class="form-control product_currency" value="{{$product_data->product_currency}}" name="product_currency" />
                                                        <div class="m2">
                                                            <div class="m2a" style="visibility: hidden;"><input type="text" class="form-control" name="product_name" value="{{$details->product_name}}" disabled/></div>
                                                            <div class="m2a">
                                                                <input type="text" class="form-control order_pricing_costs_fob order_pricing_costs_total_fob" name="order_pricing[<?= $i ?>][order_pricing_costs_total_fob]" value="{{$details->order_pricing_costs_total_fob}}" disabled=""/>
                                                            </div>
                                                            <div class="m2a fob_from_factory">
                                                                <input type="text" class="form-control order_pricing_costs_fob costs_from_factory_fob" name="order_pricing[<?= $i ?>][costs_from_factory_fob]" value="{{$details->costs_from_factory_fob}}"/>
                                                                <select class="form-control order_pricing_costs_fob costs_from_factory_currency_fob" id="costs_from_factory_currency_fob" name="order_pricing[<?= $i ?>][costs_from_factory_currency_fob]" style="width: auto;">
                                                                    <option value="">Select Currency</option>
                                                                    @if($details->costs_from_factory_currency_fob != null)
                                                                    {!!  get_limited_currency_list_with_selected_item($details->costs_from_factory_currency_fob) !!}
                                                                    @else
                                                                    {!!  get_limited_currency_list_with_symbols_through_id() !!}
                                                                    @endif
                                                                </select>
                                                            </div>
                                                            <div class="m2a fob_other_costs">
                                                                <input type="text" class="form-control order_pricing_costs_fob custom_and_other_costs_fob" name="order_pricing[<?= $i ?>][custom_and_other_costs_fob]" value="{{$details->custom_and_other_costs_fob}}"/>
                                                                <select class="form-control order_pricing_costs_fob custom_and_other_costs_currency_fob" id="custom_and_other_costs_currency_fob" name="order_pricing[<?= $i ?>][custom_and_other_costs_currency_fob]"  style="width: auto;">
                                                                    <option value="">Select Currency</option>
                                                                    @if($details->custom_and_other_costs_currency_fob != null)
                                                                    {!!  get_limited_currency_list_with_selected_item($details->custom_and_other_costs_currency_fob) !!}
                                                                    @else
                                                                    {!!  get_limited_currency_list_with_symbols_through_id() !!}
                                                                    @endif
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <?php
                                                    $i++;
                                                endforeach;
                                                ?>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection