<div class="row view_totals_row_table">
    <div class="col-lg-12 col-md-12 ">
        <table class="view_totals_table">

            <tr>
                <th>Total Containers</th>
                <th>Total Cost</th>
                <th>Total Buy Price</th>
                <th>Total Profit</th>
            </tr>
            <tr>
                <td>
                    <?php
                    echo number_format(DB::table('shipping_plans')
//                            ->where('order_id', $order_id)
                            ->where('deleted_at', null)
                            ->count('id'));
                    ?>
                </td>
                <td>
                    <?php
                    echo number_format(DB::table('shipping_plans')
//                            ->where('order_id', $order_id)
                            ->where('deleted_at', null)
                            ->sum('total_cost'),2);
                    ?>
                </td>


                <td>
                    <?php
                    echo number_format(DB::table('shipping_plans')
//                            ->where('order_id', $order_id)
                            ->where('deleted_at', null)
                            ->sum('customer_buy_price'),2);
                    ?>
                </td>


                <td>
                    <?php
                    echo number_format(DB::table('shipping_plans')
//                            ->where('order_id', $order_id)
                            ->where('deleted_at', null)
                            ->sum('profit'),2);
                    ?>
                </td>
            </tr>
        </table>
    </div>
    <br>
    <br>
    <div class="col-lg-12 col-md-12 m257">
        <table class="view_totals_table order_overview_shipping_totals">
            <thead>
                <tr>
                    <th>Product</th>
                    <!--<th>Containers</th>-->
                    <th>Total Qty (Confirmed)</th>
                    <th>Total Cost</th>
                    <th>Customer Buy Price</th>
                    <th>Profit</th>
                </tr>
            </thead>
            <?php foreach (App\OrderLine::get() as $order_lines) { ?>
                <tr>
                    <td><p style="white-space: break-spaces;width: 240px;">{{get_product_name_by_id($order_lines->product_id)}}</p></td>
                    <td><?php
                        echo number_format(DB::table('shipping_container_products')
//                            ->where('order_id', $order_id)
                                ->where('order_line_id', $order_lines->id)
                                ->where('deleted_at', null)
                                ->sum('confirmed_quantity'));
                        ?>
                    </td>
                    <td><?php
                        echo number_format(DB::table('shipping_container_products')
//                            ->where('order_id', $order_id)
                                ->where('order_line_id', $order_lines->id)
                                ->where('deleted_at', null)
                                ->sum('total_cost'),2);
                        ?>
                    </td>
                    <td><?php
                        echo number_format(DB::table('shipping_container_products')
//                            ->where('order_id', $order_id)
                                ->where('order_line_id', $order_lines->id)
                                ->where('deleted_at', null)
                                ->sum('customer_buy_price'),2);
                        ?>
                    </td>
                    <td><?php
                        echo number_format(DB::table('shipping_container_products')
//                            ->where('order_id', $order_id)
                                ->where('order_line_id', $order_lines->id)
                                ->where('deleted_at', null)
                                ->sum('profit'),2);
                        ?>
                    </td>
                </tr>
            <?php } ?>
        </table>
    </div>
    <!--<div class="col-md-4">-->
<!--                    <table class="view_totals_table">
            <tr>
                <td>Costs Include:</td>
                <td></td>
            </tr>
            <tr>
                <td><Cost name><use><% or Value)><total applied></td>
                    <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                    </table>-->
    <!--</div>-->
</div>