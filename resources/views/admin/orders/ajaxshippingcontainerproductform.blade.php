<div class="responsive" style="overflow-y:hidden">
    <table class="table table-bordered" id="tbl_add_container_products">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Product Name</th>
                    <th>Product Image</th>
                    <th><i class="fa fa-cogs"></i></th>
                </tr>
            </thead>
            <tbody id="tbl_add_container_products_body">
                <?php $i = '1'; 
                $order_lines = App\OrderLine::where('order_id',Request::get('order_id'))->get();
                foreach($order_lines as $details):
                ?>
                <tr id="rec-<?= $i ?>">
            <input type="hidden" value="{{$details->id}}" name="order_pricing[<?= $i ?>][row_id]" />
            <td><span class="sn"><?= $i ?></span></td>
            
            <?php
            $product_data = get_product_details_by_id($details->product_id);

//            if ($product_data->pic_id == null || $product_data->pic_id == '') {
//                $product_pic = 'images/no_image.png';
//            } else {
//                $product_pic = getFile($product_data->pic_id);
//            }
            
//*******Alternate code for jumping loop iteration if product is deleted from products module
            if($product_data != 'NA'){
                if ($product_data->pic_id == null || $product_data->pic_id == '') {
                    $product_pic = 'images/no_image.png';
                } else {
                    $product_pic = getFile($product_data->pic_id);
                }
            }
            ?>
            <td>
                <input type="hidden" class="form-control shipping_product_id" name="shipping_product_id" value="{{$product_data->id}}"/>
                <input type="text" class="form-control shipping_product_name" name="shipping_product_name" value="{{$product_data->name}}" disabled/>
            </td>
            <td>
                <img src="{{ url("$product_pic") }}" width="100"/>
            </td>
            <td>
                <a href="javascript:void(0)" class="btn btn-primary btn-sm" onclick="shipping_container_products_create({{$details->id}},{{ Request::get('order_id') }},{{ Request::get('shipping_plan_id') }});"  data-added="0" style="margin:10px;">&nbsp;Add</a>
            </td>
            </tr>
            <?php $i++; endforeach; ?>
            </tbody>
        </table>
</div>
            
