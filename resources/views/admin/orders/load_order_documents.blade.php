<table class="table table-bordered suppliers_table_mobile" id="tbl_order_documents">
    <thead>
        <tr>
            <th>#</th>
            <th>Original Name</th>
            <th>Preview</th>
            <th>File Name</th>
            <th>Date/Time</th>
            <th><i class="fa fa-cogs"></i></th>
        </tr>
    </thead>
    <tbody id="tbl_order_documents_body">
        <?php $i = '1'; ?>
        @foreach($order_documents as $details)
        <tr id="rec-<?= $i ?>">

            <td><span class="sn"></span><?= $i ?>.</td>
    <input type="hidden" value="{{$details->id}}" name="order_documents[<?= $i ?>][row_id]" />
    
    <td>
        <input type="text" class="form-control order_documents" name="original_file_name" value="{{ $details->original_file_name }}" disabled=""/>
    </td>
    <?php $img =  $details->original_file_name; ?>
    <td><a href="{{getFile($details->original_file_name)}}" target="_blank">View</a></td>
    <td>
        <input type="text" class="form-control order_documents" name="order_documents[<?= $i ?>][file_name]" value="{{$details->file_name}}"/>
    </td>
    <td>
        <h5>{{$details->created_at}}</h5>
    </td>

    <td><a class="btn btn-xs btn-danger order_documents_delete" style="color:white;" data-id="<?= $i ?>" onclick="deleteOrderDetail('order_documents',{{$details->id}});"><i class="fa fa-trash"></i></a>
        <a class="btn btn-xs btn-success download_order" href="<?= getFile("$details->original_file_name") ?>" download=""><i class="fa fa-download"></i></a>
    </td>
</tr>
<?php $i++; ?>
@endforeach
</tbody>
</table>