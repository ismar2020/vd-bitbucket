<table class="table table-bordered table_shipping_container_products" id="tbl_shipping_container_products">
    <thead>
        <tr>
            <th>#</th>
            <th>Product Name</th>
            <th>Product Image</th>
            <th>Supplier Name</th>
            <th>Packaging Type</th>
            <th>Quantity</th>
            <th>Confirmed Quantity</th>
            <th>Total Cost</th>
            <th>Customer Buy Price</th>
            <th>Profit</th>
<!--            <th>Total Product Units added to other Containers</th>
            
            <th>#</th>
            <th>Product Name</th>
            <th>Product Image</th>
            <th>Number of Units that fit</th>
            <th>Container Order Quantity</th>
            <th>Container Order Quantity CBM</th>
            <th>Container Total Cost</th>
            <th>Container Total Wholesale Price</th>
            <th>Total Product Units added to other Containers</th>-->
            
            <th><i class="fa fa-cogs"></i></th>
        </tr>
    </thead>
    <tbody id="tbl_shipping_container_products_body">
        <?php $i = '1'; ?>
        @foreach($shipping_container_products as $details)
        <?php
            if ($details->product_image == null || $details->product_image == '') {
                $product_pic = 'images/no_image.png';
            } else {
                $product_pic = getFile($details->product_image);
            } 
            //getting product Id from order line
            $product_res = \App\OrderLine::where('id',$details->order_line_id)->first(); 
            //getting details from product Id of product
            $product_id = null;  
            if($product_res){
                $product = \App\Product::where('id',$product_res->product_id)->first(); 

                if($product){ 
                    $product_id = $product->id;  
                }   
            }
            //getting order line details
            $orderLine = \App\OrderLine::where('id',$details->order_line_id)->first();
            //getting order type from order-id got from above
            $order_type = App\Order::where('id',Request::get('order_id'))->first()->order_type;
        ?>
        <tr id="rec-<?= $i ?>">
            
            <td><span class="sn"></span><?= $i ?>.</td>
            <input type="hidden" value="{{$details->id}}" name="shipping_container_products[<?= $i ?>][row_id]" />
            <td>
                <input type="hidden" class="form-control product_id shipping_plan_tracking" name="shipping_container_products[<?= $i ?>][product_id]" value=""/>
                <input type="text" class="form-control product_name shipping_plan_tracking" name="shipping_container_products[<?= $i ?>][product_name]" value="{{$details->product_name}}" style="width: 160px;" disabled=""/>
            </td>
            <td class="product_image_append">
                 <input type="hidden" class="form-control ship_order_type" value="{{$order_type}}"/><img src="{{ url("$product_pic") }}" width="100"/>
                 <input type="hidden" class="form-control ship_order_line_id" value="{{$details->order_line_id}}"/>
            </td>
            
        <!--***************previous fields which are removed******************-->
          <?php /* 
            <td>
                <input type="hidden" class="form-control ship_product_id" value="{{$product_id}}"/>
                <input type="hidden" class="form-control ship_container_cbm" value=""/>
                
                <select class="form-control number_of_units_that_fit" name="shipping_container_products[<?= $i ?>][number_of_units_that_fit]" style="width:150px;">
                    <option value="">No Formula selected</option>
                    <option value="1">Box Dimensions</option>
                    <option value="2">Stack Dimensions</option>
                    <option value="3">Product Dimension</option>
                    <option value="4">Stack Opt Dimension</option>
                    <option value="5">Product Opt Dimension</option>
                    <option value="6">Box Opt Dimension</option>
                    <option value="7">Box Quantity</option>
                    <option value="8">Stack Quantity</option>
                    <option value="9">Product Quantity</option>
                </select>
            </td>
            <td>
                <input type="hidden" class="form-control ship_product_unit_price" value="{{$product->product_price_per_unit}}"/>
                <input type="hidden" class="form-control ship_orderline_wholesale_margin" value="{{$orderLine->wholesale_margin_percentage}}"/>
                <input type="text" class="form-control ship_container_order_quantity container_order_quantity shipping_container_products" name="shipping_container_products[<?= $i ?>][container_order_quantity]" value="{{$details->container_order_quantity}}"/>
            </td>
            <td>
                <input type="text" class="form-control container_order_quantity_cbm" name="shipping_container_products[<?= $i ?>][container_order_quantity_cbm]" value="{{$details->container_order_quantity_cbm}}" disabled/>
            </td>
            <td>
                <input type="text" class="form-control container_total_costs ship_container_total_costs" name="shipping_container_products[<?= $i ?>][container_total_cost]" value="{{$details->container_total_cost}}" disabled/>
            </td>
            <td>
                <input type="text" class="form-control container_total_wholesale_price" name="shipping_container_products[<?= $i ?>][container_total_wholesale_price]" value="{{$details->container_total_wholesale_price}}" disabled/>
            </td>
            <td>
                <?php  $data = DB::table("shipping_container_products")
                            ->select(DB::raw("SUM(container_order_quantity) as count"))
                            ->where('shipping_plan_id', '!=' ,$details->shipping_plan_id)
                            ->where('order_line_id', $details->order_line_id)
                            ->get(); ?>
                <input type="text" class="form-control total_units_added_other_containers" name="shipping_container_products[<?= $i ?>][total_product_units_added_to_other_containers]" value="<?php echo $data[0]->count; ?>" disabled/>
            </td>
*/ ?> 
        <!--**********previous fields section which are removed ends************-->
            <td>
                <input type="text" class="form-control supplier_name shipping_plan_tracking" name="shipping_container_products[<?= $i ?>][supplier_id]" value="{{get_supplier_name_by_id($details->supplier_id)}}" style="width: 160px;" disabled=""/>
            </td>
            <td>
                <input type="text" class="form-control using shipping_container_products shipping_plan_tracking" name="shipping_container_products[<?= $i ?>][using]" value="<?=($details->using == '' || $details->using == null)?'N/A':$details->using ?>" style="width: 160px;" disabled/>
            </td>
            
            <td>
                <input type="text" class="form-control quantity shipping_container_products shipping_plan_tracking" name="shipping_container_products[<?= $i ?>][quantity]" value="<?=($details->quantity == '' || $details->quantity == null)?'NA':$details->quantity ?>" style="width: 120px;" disabled/>
            </td>
            <td>
                <input type="text" class="form-control shipping_container_products confirmed_quantity shipping_plan_cal format_me" name="shipping_container_products[<?= $i ?>][confirmed_quantity]" value="{{$details->confirmed_quantity}}" onkeypress="return isNumber(event)" />
            </td>
            <td>
                <input type="text" class="form-control shipping_container_products total_cost ship_total_cost format_me_in_decimals shipping_plan_tracking" name="shipping_container_products[<?= $i ?>][total_cost]" value="{{$details->total_cost}}" style="width: 120px;" disabled/>
            </td>
            <td>
                <input type="text" class="form-control shipping_container_products customer_buy_price ship_customer_buy_price format_me_in_decimals shipping_plan_tracking" name="shipping_container_products[<?= $i ?>][customer_buy_price]" value="{{$details->customer_buy_price}}" style="width: 120px;" disabled/>
            </td>
            <td>
                <input type="text" class="form-control shipping_container_products profit ship_profit format_me_in_decimals shipping_plan_tracking" name="shipping_container_products[<?= $i ?>][profit]" value="{{$details->profit}}" style="width: 120px;" disabled/>
            </td>
            <td style="display :none;">
                <?php  $data = DB::table("shipping_container_products")
                            ->select(DB::raw("SUM(container_order_quantity) as count"))
                            ->where('shipping_plan_id', '!=' ,$details->shipping_plan_id)
                            ->where('order_line_id', $details->order_line_id)
                            ->get(); ?>
                <input type="text" class="form-control total_units_added_other_containers shipping_plan_tracking" name="shipping_container_products[<?= $i ?>][total_product_units_added_to_other_containers]" value="<?php echo $data[0]->count; ?>" disabled/>
            </td>
            
           <td style="display :flex;"><a class="btn btn-xs" title="View Packaging type details" href="javascript:void(0)" <?php if($details->packaging_id != null && $details->packaging_id != ''){ ?> data-toggle="modal" data-target="#view_particular_shipping_product_packaging_option" onclick="load_shipping_packaging_option({{$details->packaging_id}});"<?php }else{ ?>onclick='swal({
            title: "Warning!",
            text: "Packaging Type not found",
            icon: "warning",
            button: "Ok!",
          });' <?php } ?>><i class="fa fa-eye"></i></a><a class="btn btn-xs shipping_container_product_delete" data-id="<?= $i ?>" onclick="deleteOrderDetail('shipping_container_products',{{$details->id}});"><i class="fa fa-trash"></i></a></td>
        </tr>
        <?php $i++; ?>
        @endforeach
    </tbody>
</table>