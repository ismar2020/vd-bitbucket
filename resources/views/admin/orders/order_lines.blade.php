<!-------------------------- Modal for show hide column code-------------------------->
<style>
    #tbl_posts td .order_pricing{
        width :140px;
    }
    #tbl_posts td .select_order_pricing{
        width :140px;
    }
    .inner_table input {
        width: 120px;
    }
</style>
<?php $order_type = App\Order::where('id', $order_id)->first()->order_type; ?>

<div class="modal fade" id="showHideModalLongnew" tabindex="-1" role="dialog" aria-labelledby="showHideModalLongNewTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background: #397657;">
                <h5 class="modal-title" id="showHideModalLongNewTitle" style="color:#fff;">Show / hide Columns</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="overflow-y: auto;">
                <div id='control_sh'>
                    <div id="showhidecolumns">
                        <?php
//                        $i = '1';
//                        $columns = Schema::getColumnListing('order_lines'); 
//                        $column_exc = ['created_at', 'updated_at', 'deleted_at', 'order_id', 'product_id', 'purchase_cost', 'confirmed', 'active', 'user_id'];
//                        $columns_fresh = array_diff($columns, $column_exc);
//                        foreach ($columns_fresh as $key => $name): 
                        ?>

                        <!--<input type="checkbox" class="hide_show" checked><span>-->
                        <?php //echo ucfirst(str_replace('_', ' ', $name)); ?>
                        <!--</span>-->
                        <?php //$i++; endforeach; ?>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <div class="show_hide">
                            <div class="row">
                                <div class="col-md-4"><input type="checkbox" class="hide_show" checked><span>Id</span><br>
                                    <input type="checkbox" class="hide_show" checked><span>Product Name</span><br>
                                    <input type="checkbox" class="hide_show" checked><span>Product Image</span><br>
                                    <input type="checkbox" class="hide_show" checked><span>Port of Origin</span><br>
                                    <input type="checkbox" class="hide_show"><span>Category</span><br>
                                    <input type="checkbox" class="hide_show"><span>Product Type</span><br>
                                    <input type="checkbox" class="hide_show"><span>Product Length</span><br>
                                    <input type="checkbox" class="hide_show"><span>Product Width</span><br>
                                    <input type="checkbox" class="hide_show"><span>Product Height</span><br>
                                    <input type="checkbox" class="hide_show"><span>Product Weight</span><br>
                                    <input type="checkbox" class="hide_show"><span>Product Material</span><br>
                                    <input type="checkbox" class="hide_show"><span>Notes</span><br>
                                    <input type="checkbox" class="hide_show"><span>Supplier Name</span><br>
                                </div>
                                <div class="col-md-4">
                                    <input type="checkbox" class="hide_show"><span>Item Code</span><br>
                                    <?php /* <input type="checkbox" class="hide_show"><span>Product quantity per box</span><br>
                                      <input type="checkbox" class="hide_show"><span>Product Box Material</span><br>
                                      <input type="checkbox" class="hide_show"><span>Boxes per stack</span><br>
                                      <input type="checkbox" class="hide_show"><span>Units per stack</span><br>
                                      <input type="checkbox" class="hide_show"><span>Stack Material</span><br> */ ?>
                                    <?php foreach (App\Container::get() as $data) { ?>
                                        <input type="checkbox" class="hide_show"><span>Product qty per <?= $data->name ?></span><br>
                                    <?php } ?>

                                    <input type="checkbox" class="hide_show"><span>Packaging Type 1</span><br>
                                    <input type="checkbox" class="hide_show"><span>Packaging Type 2</span><br>
                                    <input type="checkbox" class="hide_show"><span>Packaging Type 3</span><br>
                                    <input type="checkbox" class="hide_show"><span>Packaging Type 4</span><br>
                                    <input type="checkbox" class="hide_show"><span>Packaging Type 5</span><br>
                                    <input type="checkbox" class="hide_show" checked><span>FOB Price</span><br>
                                    <input type="checkbox" class="hide_show" checked><span>FOB Currency</span><br>
                                    <?php if ($order_type != '1') { ?>
                                    <input type="checkbox" class="hide_show" checked><span>FOB Currency Exchange Rate</span><br>
                                    <?php /* <input type="checkbox" class="hide_show" checked><span>Pricing Currency</span><br> */ ?>
                                    <input type="checkbox" class="hide_show" checked><span>Port of Dispatch</span><br>
                                    <input type="checkbox" class="hide_show" checked><span>Container Size</span><br>
                                    <?php } ?>
                                    <?php /* <input type="checkbox" class="hide_show" checked><span>Products Units in the container</span><br> */ ?>
                                </div>
                                <div class="col-md-4">
                                    <input type="checkbox" class="hide_show" checked><span>Unit of Measure (UOM)</span><br>
                                    <?php if ($order_type != '1') { ?>
                                    <input type="checkbox" class="hide_show" checked><span>Landed Product Price</span><br>
                                    <?php } ?>
                                    <input type="checkbox" class="hide_show" checked><span>GP%</span><br>
                                    <input type="checkbox" class="hide_show" checked><span>GP$</span><br>
                                    <input type="checkbox" class="hide_show" checked><span>Price to Customer</span><br>
                                    <input type="checkbox" class="hide_show" checked><span>RCM</span><br>
                                    <input type="checkbox" class="hide_show" checked><span>RRP</span>    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" id="modalCloseProduct" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->

<!--------------------------------show hide column code ends-------------------------->
<?php /* $order_type = App\Order::where('id', $order_id)->first()->order_type; */ ?>
<table class="table table-bordered orderlinetable" id="tbl_posts" width="100%" cellspacing="0">
    <thead>
        <tr>
            @if(\Auth::user()->roles->first()->name == 'supplier')
            <th>#</th>
            <th>Product Name</th>
            <th>Product Image</th>
            <th>Port of Origin</th>
            <th style="display:none;">Category</th>
            <th style="display:none;">Product Type</th>
            <th style="display:none;">Product Length</th>
            <th style="display:none;">Product Width</th>
            <th style="display:none;">Product Height</th>
            <th style="display:none;">Product Weight</th>
            <th style="display:none;">Product Material</th>
            <th style="display:none;">Notes</th>
            <th style="display:none;">Supplier Name</th>
            <th style="display:none;">Item Code</th>
            <?php /* <th style="display:none;">Product quantity per box</th>
              <th style="display:none;">Product Box Material</th>
              <th style="display:none;">Boxes per stack</th>
              <th style="display:none;">Units per stack</th>
              <th style="display:none;">Stack Material</th> */ ?>
            <?php foreach (App\Container::get() as $data) { ?>
                <th style="display:none;">Product quantity per <?= $data->name ?></th>
            <?php } ?>
            <th style="display:none; text-align: center;">Packaging Type 1</th>
            <th style="display:none; text-align: center;">Packaging Type 2</th>
            <th style="display:none; text-align: center;">Packaging Type 3</th>
            <th style="display:none; text-align: center;">Packaging Type 4</th>
            <th style="display:none; text-align: center;">Packaging Type 5</th>
            <th>FOB Price</th>
            <th>FOB Currency</th>
            @else
            <th>#</th>
            <th>Product Name</th>
            <th>Product Image</th>
            <th>Port of Origin</th>
            <th style="display:none;">Category</th>
            <th style="display:none;">Product Type</th>
            <th style="display:none;">Product Length</th>
            <th style="display:none;">Product Width</th>
            <th style="display:none;">Product Height</th>
            <th style="display:none;">Product Weight</th>
            <th style="display:none;">Product Material</th>
            <th style="display:none;">Notes</th>
            <th style="display:none;">Supplier Name</th>
            <th style="display:none;">Item Code</th>
            <?php /* <th style="display:none;">Product quantity per box</th>
              <th style="display:none;">Product Box Material</th>
              <th style="display:none;">Boxes per stack</th>
              <th style="display:none;">Units per stack</th>
              <th style="display:none;">Stack Material</th> */ ?>
            <?php foreach (App\Container::get() as $data) { ?>
                <th style="display:none;">Product quantity per <?= $data->name ?></th>
            <?php } ?>
            <th style="display:none; text-align: center;">Packaging Type 1</th>
            <th style="display:none; text-align: center;">Packaging Type 2</th>
            <th style="display:none; text-align: center;">Packaging Type 3</th>
            <th style="display:none; text-align: center;">Packaging Type 4</th>
            <th style="display:none; text-align: center;">Packaging Type 5</th>
            <th>FOB Price</th>
            <th>FOB Currency</th>
            <?php if ($order_type != '1') { ?>
            <th>FOB Currency Exchange Rate</th>
            <th>Port of Dispatch</th>
            <th>Container Size</th>
            
            <?php } ?>
            <th>Unit of Measure (UOM)</th>
            <?php if ($order_type != '1') { ?>
                <th class="hide_show_landed_product_price">Landed Product Price</th>
            <?php } ?>
            <th title="Gross Profit Percentage">GP%</th>
            <th title="Gross Profit Value">GP$</th>
            <th title="FOB Price to Customer">Price to Customer</th>
            <th title="Recommended Customer Margin">RCM</th>
            <th title="Recommended Retail Price">RRP</th>
            @endif
            <th><i class="fa fa-cogs"></i></th>
        </tr>
    </thead>
    <tbody id="tbl_posts_body">

        <?php $i = '1'; ?>
        @foreach($orderDetails as $details)
        <tr id="rec-<?= $i ?>">
    <input type="hidden" value="{{$details->id}}" name="order_pricing[<?= $i ?>][row_id]" class="order_line_id" />
    @if(\Auth::user()->roles->first()->name == 'supplier')
    <?php
    $product_data = get_product_details_by_id($details->product_id);

    if($product_data != 'NA'){
        if ($product_data->pic_id == null || $product_data->pic_id == '') {
            $product_pic = 'images/no_image.png';
        } else {
            $product_pic = getFile($product_data->pic_id);
        }
    }
    ?>
    <td><span class="sn"><?= $i ?></span></td>
    <td>
        <input type="hidden" class="form-control product_id" name="order_pricing[<?= $i ?>][product_id]" value="{{$product_data->id}}"/>
        <input type="text" class="form-control product_name hide_inputs" name="order_pricing[<?= $i ?>][product_name]"   title="{{$product_data->name}}" value="{{$product_data->name}}" style="width: 200px;" disabled/>
    </td>
    <td>
        <img src="{{ url("$product_pic") }}" width="100"/>
    </td>
    <td>
        <select class="form-control order_pricing shipping_plan_tracking select_webkit_hide" name="order_pricing[<?= $i ?>][shipping_port]" style="width: 140px;" disabled=""><?php $data = get_shipping_port_list(); ?>
            <option value="">Port of Origins</option>
            @foreach($data as $key => $value)
            @if($key == $product_data->shipping_port)
            <option value="{{$key}}" selected>{{$value}}</option>
            @else
            <option value="{{$key}}">{{$value}}</option>
            @endif
            @endforeach
        </select>

    </td>
    <td style="display:none;">
        <input type="text" class="form-control category_id shipping_plan_tracking" name="order_pricing[<?= $i ?>][category_id]" value="{{get_category_name_by_id($product_data->category_id)}}" disabled/>
    </td> 
    <td style="display:none;">
        <input type="text" class="form-control type_id shipping_plan_tracking" name="order_pricing[<?= $i ?>][type_id]" value="{{get_product_type_name_by_id($product_data->type_id)}}" disabled/>
    </td> 
    <td style="display:none;">
        <input type="text" class="form-control product_length shipping_plan_tracking" name="order_pricing[<?= $i ?>][product_length]" value="{{$product_data->product_length}}" disabled/>
    </td> 
    <td style="display:none;">
        <input type="text" class="form-control product_width shipping_plan_tracking" name="order_pricing[<?= $i ?>][product_width]" value="{{$product_data->product_width}}" disabled/>
    </td> 
    <td style="display:none;">
        <input type="text" class="form-control product_height shipping_plan_tracking" name="order_pricing[<?= $i ?>][product_height]" value="{{$product_data->product_height}}" disabled/>
    </td> 
    <td style="display:none;">
        <input type="text" class="form-control product_weight shipping_plan_tracking" name="order_pricing[<?= $i ?>][product_weight]" value="{{$product_data->product_weight}}" disabled/>
    </td> 
    <td style="display:none;">
        <input type="text" class="form-control product_material shipping_plan_tracking" name="order_pricing[<?= $i ?>][product_material]" value="{{$product_data->product_material}}" disabled/>
    </td> 
</td> 
<td style="display:none;">
    <input type="text" class="form-control notes shipping_plan_tracking" name="order_pricing[<?= $i ?>][notes]" value="{{$product_data->notes}}" disabled/>
</td> 
<td style="display:none;">
    <input type="text" class="form-control supplier_id shipping_plan_tracking" name="order_pricing[<?= $i ?>][supplier_id]" value="{{get_supplier_name_by_id($product_data->supplier_id)}}" disabled/>
</td> 
<td style="display:none;">
    <input type="text" class="form-control product_quantity_per_box shipping_plan_tracking" name="order_pricing[<?= $i ?>][item_code]" value="{{$product_data->item_code}}" style="width: 78px;" disabled/>
</td> 
<?php /* <td style="display:none;">
  <input type="text" class="form-control product_quantity_per_box" name="order_pricing[<?= $i ?>][product_quantity_per_box]" value="{{$product_data->product_quantity_per_box}}" disabled/>
  </td>
  <td style="display:none;">
  <input type="text" class="form-control product_box_material" name="order_pricing[<?= $i ?>][product_box_material]" value="{{$product_data->product_box_material}}" disabled/>
  </td>
  <td style="display:none;">
  <input type="text" class="form-control boxes_per_stack" name="order_pricing[<?= $i ?>][boxes_per_stack]" value="{{$product_data->boxes_per_stack}}" disabled/>
  </td>
  <td style="display:none;">
  <input type="text" class="form-control units_per_stack" name="order_pricing[<?= $i ?>][units_per_stack]" value="{{$product_data->units_per_stack}}" disabled/>
  </td>
  <td style="display:none;">
  <input type="text" class="form-control stack_material" name="order_pricing[<?= $i ?>][stack_material]" value="{{$product_data->stack_material}}" disabled/>
  </td> */ ?>
<?php
foreach (App\Container::get() as $data) {

    $product_quantity = \App\ProductQuantityPerContainers::where('container_id', $data->id)->where('product_id', $product_data->id)->first();
    // dd($product_quantity->product_quantity);
    ?>
     <!--<th style="display:none;">Product quantllity per <?php //echo $data->name ?></th>-->
    <td style="display:none;">
        <input type="text" class="form-control format_me_in_decimals shipping_plan_tracking product_quantity_per_<?= $data->name ?>" name="order_pricing[<?= $i ?>][product_quantity_per_<?= $data->name ?>]" value="<?php
        if ($product_quantity != null) {
            echo $product_quantity->product_quantity;
        }
        ?>" disabled/>
    </td> 
<?php } ?>
<?php 
//    $packaging_type = App\Models\ProductPackagingOption::where('product_id', $details->product_id)->orderBy('id', 'ASC')->get()->toArray();
    
    $product_id = $details->product_id;
    $packaging_type = \App\Models\ProductPackagingOption::where('product_id', $product_id)->where('nested_id', '0')->get();

    $packaging_options_nested = \App\Models\ProductPackagingOption::where('product_id', $product_id)->where('nested_id', '!=', '0')->pluck('nested_id');

    $packaging_options_nested = \App\Models\ProductPackagingOption::whereIn('id', $packaging_options_nested)->get();

    $packaging_type = $packaging_type->merge($packaging_options_nested);
    
    
    
//    dd($packaging_type);
    for($k = 0; $k < 5; $k++){
        $j = $k+1;
 ?>
    <td style="display:none;">
        <table class="inner_table">
            <tr>
                <th>Type</th>
                <th>Quantity</th>
                <th>Length (cm)</th>
                <th>Width (cm)</th>
                <th>Height (cm)</th>
                <th>Gross Weight (kg)</th>
                <th>Packaging CBM</th>
                <th>Floor Ready</th>
                <th>Stackable</th>
                <th>Nested Product</th>
            </tr>
            <tr>
                <th>
                    <?php
                        $type = get_package_type_list();
                        $select = ['1' => 'Yes', '2' => 'No'];
                        $typeValue = 'NA';
                        foreach($type as $key => $value):
                            if(isset($packaging_type[$k]['packaging_type']) && $key == $packaging_type[$k]['packaging_type']):
                                $typeValue = $value;
                            endif;
                        endforeach;
                        
                    ?>
                    <input type="text" class="form-control shipping_plan_tracking packaging_type_<?= $j ?>" name="order_pricing[<?= $i ?>][packaging_type_<?= $j ?>]" value="<?= $typeValue ?>" disabled=""/>
                </th>
                <th>
                <input type="text" class="form-control shipping_plan_tracking packaging_type_<?= $j ?>" name="order_pricing[<?= $i ?>][packaging_type_<?= $j ?>]" value="<?php
                if (isset($packaging_type[$k]['quantity_per_page'])) {
                    echo $packaging_type[$k]['quantity_per_page'];
                }else{
                    echo 'NA';
                }
                ?>" disabled=""/>
                </th>
                <th>
                <input type="text" class="form-control shipping_plan_tracking packaging_type_<?= $j ?>" name="order_pricing[<?= $i ?>][packaging_type_<?= $j ?>]" value="<?php
                if (isset($packaging_type[$k]['packaging_length'])) {
                    echo $packaging_type[$k]['packaging_length'];
                }else{
                    echo 'NA';
                }
                ?>" disabled=""/>
                </th>
                <th>
                <input type="text" class="form-control shipping_plan_tracking packaging_type_<?= $j ?>" name="order_pricing[<?= $i ?>][packaging_type_<?= $j ?>]" value="<?php
                if (isset($packaging_type[$k]['packaging_width'])) {
                    echo $packaging_type[$k]['packaging_width'];
                }else{
                    echo 'NA';
                }
                ?>" disabled=""/>
                </th>
                <th>
                <input type="text" class="form-control shipping_plan_tracking packaging_type_<?= $j ?>" name="order_pricing[<?= $i ?>][packaging_type_<?= $j ?>]" value="<?php
                if (isset($packaging_type[$k]['packaging_height'])) {
                    echo $packaging_type[$k]['packaging_height'];
                }else{
                    echo 'NA';
                }
                ?>" disabled=""/>
                </th>
                <th>
                <input type="text" class="form-control shipping_plan_tracking packaging_type_<?= $j ?>" name="order_pricing[<?= $i ?>][packaging_type_<?= $j ?>]" value="<?php
                if (isset($packaging_type[$k]['gross_weight_including_packaging'])) {
                    echo $packaging_type[$k]['gross_weight_including_packaging'];
                }else{
                    echo 'NA';
                }
                ?>" style="width : 140px;" disabled=""/>
                </th>
                <th>
                <input type="text" class="form-control shipping_plan_tracking packaging_type_<?= $j ?>" name="order_pricing[<?= $i ?>][packaging_type_<?= $j ?>]" value="<?php
                if (isset($packaging_type[$k]['packaging_cbm'])) {
                    echo $packaging_type[$k]['packaging_cbm'];
                }else{
                    echo 'NA';
                }
                ?>" style="width : 140px;" disabled=""/>
                </th>
                <th>
                <input type="text" class="form-control shipping_plan_tracking packaging_type_<?= $j ?>" name="order_pricing[<?= $i ?>][packaging_type_<?= $j ?>]" value="<?php
                if (isset($packaging_type[$k]['floor_ready'])) {
                    if($packaging_type[$k]['floor_ready'] == '1'){
                        echo 'Yes';
                    }else if($packaging_type[$k]['floor_ready'] == '2'){
                        echo 'No';
                    }else{
                        echo 'NA';
                    }
                }else{
                    echo 'NA';
                }
                ?>" disabled=""/>
                </th>
                <th>
                <input type="text" class="form-control shipping_plan_tracking packaging_type_<?= $j ?>" name="order_pricing[<?= $i ?>][packaging_type_<?= $j ?>]" value="<?php
                if (isset($packaging_type[$k]['stackable'])) {
                    if($packaging_type[$k]['stackable'] == '1'){
                        echo 'Yes';
                    }else if($packaging_type[$k]['stackable'] == '2'){
                        echo 'No';
                    }else{
                        echo 'NA';
                    }
                }else{
                    echo 'NA';
                }
                ?>" disabled=""/>
                </th>
                <th>
                <input type="text" class="form-control shipping_plan_tracking  packaging_type_<?= $j ?>" name="order_pricing[<?= $i ?>][packaging_type_<?= $j ?>]" value="<?php
                if (isset($packaging_type[$k]['nested_product'])) {
                    if($packaging_type[$k]['nested_product'] == '1'){
                        echo 'Yes';
                    }else if($packaging_type[$k]['nested_product'] == '2'){
                        echo 'No';
                    }else{
                        echo 'NA';
                    }
                }else{
                    echo 'NA';
                }
                ?>" disabled=""/>
                </th>
            </tr>
        </table>
    </td>
<?php } ?>

<td>
    <input type="text" class="form-control order_pricing product_unit_price format_me_in_decimals hide_inputs shipping_plan_tracking" name="order_pricing[<?= $i ?>][product_unit_price]" value="{{$product_data->product_price_per_unit}}" disabled/>
</td>
<td>
    <input type="hidden" class="form-control product_unit_currency currency_exchange" name="order_pricing[<?= $i ?>][product_unit_currency]" data-id="{{$product_data->product_currency}}" value="{{$product_data->product_currency}}"/>
    <input type="hidden" class="form-control pricing_currency" name="" value="{{$details->pricing_currency}}"/>
    <input type="text" class="form-control order_pricing hide_inputs shipping_plan_tracking"  name="" value="{{get_currency_name_by_id($product_data->product_currency)}}" disabled/>
</td>
@else

    
    <?php
    $product_data = get_product_details_by_id($details->product_id);

    if($product_data != 'NA'){
        if ($product_data->pic_id == null || $product_data->pic_id == '') {
            $product_pic = 'images/no_image.png';
        } else {
            $product_pic = getFile($product_data->pic_id);
        }
    }
    ?>
    <td><span class="sn"><?= $i ?></span></td>
    <td>
        <input type="hidden" class="form-control product_id shipping_plan_tracking" name="order_pricing[<?= $i ?>][product_id]" value="{{$product_data->id}}"/>
        <input type="text" class="form-control product_name hide_inputs" name="order_pricing[<?= $i ?>][product_name]"   title="{{$product_data->name}}" value="{{$product_data->name}}" style="width: 200px;" disabled/>
    </td>
    <td>
        <img src="{{ url("$product_pic") }}" width="100"/>
    </td>
    <td>
        <select class="form-control order_pricing shipping_plan_tracking select_webkit_hide" name="order_pricing[<?= $i ?>][shipping_port]" style="width: 140px;" disabled=""><?php $data = get_shipping_port_list(); ?>
            <option value="">Port of Origins</option>
            @foreach($data as $key => $value)
            @if($key == $product_data->shipping_port)
            <option value="{{$key}}" selected>{{$value}}</option>
            @else
            <option value="{{$key}}">{{$value}}</option>
            @endif
            @endforeach
        </select>

    </td>
    <td style="display:none;">
        <input type="text" class="form-control hide_inputs category_id shipping_plan_tracking" name="order_pricing[<?= $i ?>][category_id]" value="{{get_category_name_by_id($product_data->category_id)}}" disabled/>
    </td> 
    <td style="display:none;">
        <input type="text" class="form-control hide_inputs type_id shipping_plan_tracking" name="order_pricing[<?= $i ?>][type_id]" value="{{get_product_type_name_by_id($product_data->type_id)}}" disabled/>
    </td> 
    <td style="display:none;">
        <input type="text" class="form-control hide_inputs product_length shipping_plan_tracking" name="order_pricing[<?= $i ?>][product_length]" value="{{$product_data->product_length}}" disabled/>
    </td> 
    <td style="display:none;">
        <input type="text" class="form-control hide_inputs product_width shipping_plan_tracking" name="order_pricing[<?= $i ?>][product_width]" value="{{$product_data->product_width}}" disabled/>
    </td> 
    <td style="display:none;">
        <input type="text" class="form-control hide_inputs product_height shipping_plan_tracking" name="order_pricing[<?= $i ?>][product_height]" value="{{$product_data->product_height}}" disabled/>
    </td> 
    <td style="display:none;">
        <input type="text" class="form-control hide_inputs product_weight shipping_plan_tracking" name="order_pricing[<?= $i ?>][product_weight]" value="{{$product_data->product_weight}}" disabled/>
    </td> 
    <td style="display:none;">
        <input type="text" class="form-control hide_inputs product_material shipping_plan_tracking" name="order_pricing[<?= $i ?>][product_material]" value="{{$product_data->product_material}}" disabled/>
    </td> 
</td> 
<td style="display:none;">
    <input type="text" class="form-control hide_inputs notes shipping_plan_tracking" name="order_pricing[<?= $i ?>][notes]" value="{{$product_data->notes}}" disabled/>
</td> 
<td style="display:none;">
    <input type="text" class="form-control hide_inputs supplier_id shipping_plan_tracking" name="order_pricing[<?= $i ?>][supplier_id]" value="{{get_supplier_name_by_id($product_data->supplier_id)}}" disabled/>
</td> 
<td style="display:none;">
    <input type="text" class="form-control hide_inputs product_quantity_per_box shipping_plan_tracking" name="order_pricing[<?= $i ?>][item_code]" value="{{$product_data->item_code}}" style="width: 78px;" disabled/>
</td> 
<?php /* <td style="display:none;">
  <input type="text" class="form-control product_quantity_per_box" name="order_pricing[<?= $i ?>][product_quantity_per_box]" value="{{$product_data->product_quantity_per_box}}" disabled/>
  </td>
  <td style="display:none;">
  <input type="text" class="form-control product_box_material" name="order_pricing[<?= $i ?>][product_box_material]" value="{{$product_data->product_box_material}}" disabled/>
  </td>
  <td style="display:none;">
  <input type="text" class="form-control boxes_per_stack" name="order_pricing[<?= $i ?>][boxes_per_stack]" value="{{$product_data->boxes_per_stack}}" disabled/>
  </td>
  <td style="display:none;">
  <input type="text" class="form-control units_per_stack" name="order_pricing[<?= $i ?>][units_per_stack]" value="{{$product_data->units_per_stack}}" disabled/>
  </td>
  <td style="display:none;">
  <input type="text" class="form-control stack_material" name="order_pricing[<?= $i ?>][stack_material]" value="{{$product_data->stack_material}}" disabled/>
  </td> */ ?>
<?php
foreach (App\Container::get() as $data) {

    $product_quantity = \App\ProductQuantityPerContainers::where('container_id', $data->id)->where('product_id', $product_data->id)->first();
    // dd($product_quantity->product_quantity);
    ?>
     <!--<th style="display:none;">Product quantllity per <?php //echo $data->name ?></th>-->
    <td style="display:none;">
        <input type="text" class="form-control hide_inputs format_me_in_decimals shipping_plan_tracking product_quantity_per_<?= $data->name ?>" name="order_pricing[<?= $i ?>][product_quantity_per_<?= $data->name ?>]" value="<?php
        if ($product_quantity != null) {
            echo $product_quantity->product_quantity;
        }
        ?>" disabled/>
    </td> 
<?php } ?>
<?php 
//    $packaging_type = App\Models\ProductPackagingOption::where('product_id', $details->product_id)->orderBy('id', 'ASC')->get()->toArray();
    
    $product_id = $details->product_id;
    $packaging_type = \App\Models\ProductPackagingOption::where('product_id', $product_id)->where('nested_id', '0')->get();

    $packaging_options_nested = \App\Models\ProductPackagingOption::where('product_id', $product_id)->where('nested_id', '!=', '0')->pluck('nested_id');

    $packaging_options_nested = \App\Models\ProductPackagingOption::whereIn('id', $packaging_options_nested)->get();

    $packaging_type = $packaging_type->merge($packaging_options_nested);
    
    
    
//    dd($packaging_type);
    for($k = 0; $k < 5; $k++){
        $j = $k+1;
 ?>
    <td style="display:none;">
        <table class="inner_table">
            <tr>
                <th>Type</th>
                <th>Quantity</th>
                <th>Length (cm)</th>
                <th>Width (cm)</th>
                <th>Height (cm)</th>
                <th>Gross Weight (kg)</th>
                <th>Packaging CBM</th>
                <th>Floor Ready</th>
                <th>Stackable</th>
                <th>Nested Product</th>
            </tr>
            <tr>
                <th>
                    <?php
                        $type = get_package_type_list();
                        $select = ['1' => 'Yes', '2' => 'No'];
                        $typeValue = 'NA';
                        foreach($type as $key => $value):
                            if(isset($packaging_type[$k]['packaging_type']) && $key == $packaging_type[$k]['packaging_type']):
                                $typeValue = $value;
                            endif;
                        endforeach;
                        
                    ?>
                    <input type="text" class="form-control hide_inputs packaging_type_<?= $j ?>" name="order_pricing[<?= $i ?>][packaging_type_<?= $j ?>]" value="<?= $typeValue ?>" disabled=""/>
                </th>
                <th>
                <input type="text" class="form-control hide_inputs packaging_type_<?= $j ?>" name="order_pricing[<?= $i ?>][packaging_type_<?= $j ?>]" value="<?php
                if (isset($packaging_type[$k]['quantity_per_page'])) {
                    echo $packaging_type[$k]['quantity_per_page'];
                }else{
                    echo 'NA';
                }
                ?>" disabled=""/>
                </th>
                <th>
                <input type="text" class="form-control hide_inputs packaging_type_<?= $j ?>" name="order_pricing[<?= $i ?>][packaging_type_<?= $j ?>]" value="<?php
                if (isset($packaging_type[$k]['packaging_length'])) {
                    echo $packaging_type[$k]['packaging_length'];
                }else{
                    echo 'NA';
                }
                ?>" disabled=""/>
                </th>
                <th>
                <input type="text" class="form-control hide_inputs packaging_type_<?= $j ?>" name="order_pricing[<?= $i ?>][packaging_type_<?= $j ?>]" value="<?php
                if (isset($packaging_type[$k]['packaging_width'])) {
                    echo $packaging_type[$k]['packaging_width'];
                }else{
                    echo 'NA';
                }
                ?>" disabled=""/>
                </th>
                <th>
                <input type="text" class="form-control hide_inputs packaging_type_<?= $j ?>" name="order_pricing[<?= $i ?>][packaging_type_<?= $j ?>]" value="<?php
                if (isset($packaging_type[$k]['packaging_height'])) {
                    echo $packaging_type[$k]['packaging_height'];
                }else{
                    echo 'NA';
                }
                ?>" disabled=""/>
                </th>
                <th>
                <input type="text" class="form-control hide_inputs packaging_type_<?= $j ?>" name="order_pricing[<?= $i ?>][packaging_type_<?= $j ?>]" value="<?php
                if (isset($packaging_type[$k]['gross_weight_including_packaging'])) {
                    echo $packaging_type[$k]['gross_weight_including_packaging'];
                }else{
                    echo 'NA';
                }
                ?>" style="width : 140px;" disabled=""/>
                </th>
                <th>
                <input type="text" class="form-control hide_inputs packaging_type_<?= $j ?>" name="order_pricing[<?= $i ?>][packaging_type_<?= $j ?>]" value="<?php
                if (isset($packaging_type[$k]['packaging_cbm'])) {
                    echo $packaging_type[$k]['packaging_cbm'];
                }else{
                    echo 'NA';
                }
                ?>" style="width : 140px;" disabled=""/>
                </th>
                <th>
                <input type="text" class="form-control hide_inputs packaging_type_<?= $j ?>" name="order_pricing[<?= $i ?>][packaging_type_<?= $j ?>]" value="<?php
                if (isset($packaging_type[$k]['floor_ready'])) {
                    if($packaging_type[$k]['floor_ready'] == '1'){
                        echo 'Yes';
                    }else if($packaging_type[$k]['floor_ready'] == '2'){
                        echo 'No';
                    }else{
                        echo 'NA';
                    }
                }else{
                    echo 'NA';
                }
                ?>" disabled=""/>
                </th>
                <th>
                <input type="text" class="form-control hide_inputs packaging_type_<?= $j ?>" name="order_pricing[<?= $i ?>][packaging_type_<?= $j ?>]" value="<?php
                if (isset($packaging_type[$k]['stackable'])) {
                    if($packaging_type[$k]['stackable'] == '1'){
                        echo 'Yes';
                    }else if($packaging_type[$k]['stackable'] == '2'){
                        echo 'No';
                    }else{
                        echo 'NA';
                    }
                }else{
                    echo 'NA';
                }
                ?>" disabled=""/>
                </th>
                <th>
                <input type="text" class="form-control hide_inputs packaging_type_<?= $j ?>" name="order_pricing[<?= $i ?>][packaging_type_<?= $j ?>]" value="<?php
                if (isset($packaging_type[$k]['nested_product'])) {
                    if($packaging_type[$k]['nested_product'] == '1'){
                        echo 'Yes';
                    }else if($packaging_type[$k]['nested_product'] == '2'){
                        echo 'No';
                    }else{
                        echo 'NA';
                    }
                }else{
                    echo 'NA';
                }
                ?>" disabled=""/>
                </th>
            </tr>
        </table>
    </td>
<?php } ?>

<td>
    <input type="text" class="form-control order_pricing product_unit_price format_me_in_decimals hide_inputs" name="order_pricing[<?= $i ?>][product_unit_price]" value="{{$product_data->product_price_per_unit}}" disabled/>
</td>
<td>
    <input type="hidden" class="form-control product_unit_currency currency_exchange" name="order_pricing[<?= $i ?>][product_unit_currency]" data-id="{{$product_data->product_currency}}" value="{{$product_data->product_currency}}"/>
    <input type="hidden" class="form-control pricing_currency" name="" value="{{$details->pricing_currency}}"/>
    <input type="text" class="form-control order_pricing hide_inputs" name="" value="{{get_currency_name_by_id($product_data->product_currency)}}" disabled/>
</td>



<?php if ($order_type != '1') { ?>
<td>
    <input type="text" class="form-control currency_exchange_rate order_pricing gp_cal hide_inputs" name="order_pricing[<?= $i ?>][currency_exchange_rate]" value="{{$details->currency_exchange_rate}}" disabled=""/>
</td>
<td>
    <select class="form-control select_order_pricing port_of_dispatch select2_port_of_dispatch refresh_shipping_rate" name="order_pricing[<?= $i ?>][shipping_to_port_id]"><?php $data = get_shipping_port_list(); ?>
        <option value="">Port of Dispatch</option>
        @foreach($data as $key => $value)
        @if($key == $details->shipping_to_port_id)
        <option value="{{$key}}" selected>{{$value}}</option>
        @else
        <option value="{{$key}}">{{$value}}</option>
        @endif
        @endforeach
    </select>
</td>
<td>
    <select class="form-control select_order_pricing container_size refresh_shipping_rate" name="order_pricing[<?= $i ?>][container_size]"><?php $data = get_containers_list(); ?>
        <option value="">Container Size</option>
        @foreach($data as $key => $value)
        @if($key == $details->container_size)
        <option value="{{$key}}" selected>{{$value}}</option>
        @else
        <option value="{{$key}}">{{$value}}</option>
        @endif
        @endforeach
    </select>
</td>

<?php } ?>
<td>
    <input type="hidden" class="form-control packing_quantity products_units_in_the_container" name="order_pricing[<?= $i ?>][packing_quantity]" value="{{$details->packing_quantity}}" disabled/>
    <!--this is total landed costs for fiw basically but in db it is total_landed_costs-->
    <input type="hidden" class="form-control total_costs_fis" name="total_landed_costs" value="{{$details->total_landed_costs}}" disabled/>
    <!--this is total landed costs for fis basically but in db it is total_landed_costs_fis-->
    <input type="hidden" class="form-control total_landed_costs_fis" name="total_landed_costs_fis" value="{{$details->total_landed_costs_fis}}" disabled/>
    <input type="text" class="form-control order_quantity order_pricing order_pricing_cal gp_cal format_me" name="order_pricing[<?= $i ?>][order_quantity]" value="<?= ($details->order_quantity != null) ? $details->order_quantity : 1 ?>"/>
</td>
<?php if ($order_type != '1') { ?>
    <td class="hide_show_landed_product_price">
        <input type="text" class="form-control landed_product_price order_pricing format_me_in_decimals hide_inputs" name="order_pricing[<?= $i ?>][landed_product_price]" value="{{$details->landed_product_price}}" disabled=""/>
    </td>
<?php } ?>
<td>
    <input type="text" class="form-control wholesale_margin order_pricing order_pricing_cal gp_cal format_me_in_decimals" name="order_pricing[<?= $i ?>][wholesale_margin_percentage]" value="<?= ($details->wholesale_margin_percentage == null) ? get_wholesale_margin() : $details->wholesale_margin_percentage ?>"/>
</td>
<td>
    <input type="text" class="form-control gross_profit_value gp_percentage_cal format_me_in_decimals" name="order_pricing[<?= $i ?>][gross_profit_value]" value="{{$details->gross_profit_value}}"/>
</td>
<td>
    <input type="text" class="form-control order_pricing wholesale_price format_me_in_decimals hide_inputs" name="order_pricing[<?= $i ?>][wholesale_cost]" value="{{$details->wholesale_cost}}" disabled/>
</td>
<td>
    <input type="text" class="form-control retail_margin order_pricing order_pricing_cal format_me_in_decimals" name="order_pricing[<?= $i ?>][retail_margin_percentage]" value="<?= ($details->retail_margin_percentage == null) ? get_retail_margin() : $details->retail_margin_percentage ?>"/>
</td>
<td>
    <input type="text" class="form-control order_pricing retail_price format_me_in_decimals hide_inputs" name="order_pricing[<?= $i ?>][retail_cost]" value="{{$details->retail_cost}}" disabled/>
</td>
@endif
<td><a class="btn btn-xs delete-record" data-id="<?= $i ?>" onclick="deleteOrderDetail('order_lines',{{$details->id}});"><i class="fa fa-trash"></i></a></td>
</tr>
<?php $i++; ?>
@endforeach

</tbody>
</table>