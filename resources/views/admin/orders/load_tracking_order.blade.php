<!--<table id="example" class="display nowrap" cellspacing="0" width="100%">-->
<table class="table table-bordered tbl_tracking_order display nowrap" id="tbl_load_tracking_order" cellpadding="4">
    <thead>
        <tr class="shipping_plan_header">
            <th></th>
            <th>#</th>
            <th>Container Name</th>
            <th>PO Number</th>
            <th>Port of Discharge</th>
            <th>Dispatched Date</th>
            <th>ETA Date</th>
            <th>Customer Buy Price</th>
            <th>Status</th>
            <th>Invoice name</th>
            <th>Invoiced to Customer</th>
            <th>Invoice Amount</th>
            <th>Deposit Paid</th>
            <th>Payment Rec.</th>
            <th>Payment Date</th>
            <th>Payment Id</th>
            <th>Comments</th>
            <!--<th>BL Number</th>
            <th>Description</th>          
            <th>Cargo Ready Date</th>                        
            <th>Total Cost</th>            
            <th>Profit</th>-->
            
            
            
            
            
            <!--<th><i class="fa fa-cogs"></i></th>-->
        </tr>
    </thead>
    <tbody>
<?php $i = '1'; ?>
        @foreach($shippingPlanDetails as $details)
        <tr id="rec-<?= $i ?>" data-child-value="{{$details->id}}">
            <td>
                @if(count($details->shipping_container_products) > 0)
                <a href="javascript:void(0)" class="btn btn-info btn-sm details-control" style="border-radius: 50%;">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                </a>
                @endif
                <input type="hidden" value="{{$details->id}}" name="shipping_plan[<?= $i ?>][row_id]" />
            </td> 

            <td><span class="sn">{{$details->id}}</span></td>

            <td data-search='<?= get_container_name_by_id($details->container_id) ?>' data-order='<?= $details->container_id ?>'>
                <select class="form-control container_id shipping_plan c hide_unhide_container_select shipping_plan_tracking select_webkit_hide" name="shipping_plan[<?= $i ?>][container_id]" style="width: 150px;" disabled=""><?php $data = get_containers_list(); ?>
                    <option value="">N/A</option>
                    @foreach($data as $key => $value)
                    @if(isset($details->container_id) && $details->container_id == $key)
                    <option value="{{$key}}" selected>{{$value}}</option>
                    @else
                    <option value="{{$key}}">{{$value}}</option>
                    @endif
                    @endforeach
                </select>
                <?php /* {{get_container_name_by_id($details->container_id)}} */ ?>
            </td>
            <td data-search='<?= $details->purchase_order_number ?>' data-order='<?= $details->purchase_order_number ?>'>
                <input type="text" class="form-control purchase_order_name shipping_plan shipping_plan_tracking" name="shipping_plan[<?= $i ?>][purchase_order_number]" value="<?= ($details->purchase_order_number == '') ? 'N/A' : $details->purchase_order_number ?>" disabled=""/>
                <?php /* {{$details->purchase_order_number}} */ ?>
            </td>            
             <td data-search='<?= get_shipping_port_name_by_id($details->port_of_discharge) ?>' data-order='<?= $details->port_of_discharge ?>'>
                <select class="form-control port_of_discharge shipping_plan shipping_plan_tracking select_webkit_hide" name="shipping_plan[<?= $i ?>][port_of_discharge]" style="width: 167px;" disabled=""><?php $data = get_shipping_port_list(); ?>
                    <option value="">N/A</option>
                    @foreach($data as $key => $value)
                    @if($key == $details->port_of_discharge)
                    <option value="{{$key}}" selected>{{$value}}</option>
                    @else
                    <option value="{{$key}}">{{$value}}</option>
                    @endif
                    @endforeach
                </select>
                <?php /* {{get_shipping_port_name_by_id($details->port_of_discharge)}} */ ?>
            </td>
            <td data-search='<?= $details->dispatched_date ?>' data-order='<?= $details->dispatched_date ?>'>
                <input type="date" class="form-control dispatched_date shipping_plan shipping_plan_tracking" name="shipping_plan[<?= $i ?>][dispatched_date]" value="<?= ($details->dispatched_date == '') ? 'N/A' : $details->dispatched_date ?>" disabled="" style="width :170px;"/>
                <?php /* {{$details->dispatched_date}} */ ?>
            </td>
            <td data-search='<?= $details->eta_date ?>' data-order='<?= $details->eta_date ?>'>
                <input type="date" class="form-control eta_date shipping_plan shipping_plan_tracking" name="shipping_plan[<?= $i ?>][eta_date]" value="<?= ($details->eta_date == '') ? 'N/A' : $details->eta_date ?>" disabled="" style="width :170px;"/>
                <?php /* {{$details->eta_date}} */ ?>
            </td>
            <td data-search='<?= $details->customer_buy_price ?>' data-order='<?= $details->customer_buy_price ?>'>
                <input type="text" class="form-control customer_buy_price shipping_plan shipping_plan_tracking format_me_in_decimals" name="shipping_plan[<?= $i ?>][customer_buy_price]" value="<?= ($details->customer_buy_price == '') ? 'N/A' : $details->customer_buy_price ?>" disabled=""/>
                <?php /* {{$details->customer_buy_price}} */ ?>
            </td>
            <td data-search='<?= get_shipping_status_name_by_id($details->status) ?>' data-order='<?= $details->status ?>'>
                <select class="form-control shipping_plan status shipping_plan_tracking select_webkit_hide" name="shipping_plan[<?= $i ?>][status]" style="width:210px;" disabled="">
                    <?php
                    $status = get_shipping_status_list();
                    ?>
                    <option value="">N/A</option>
                    @foreach($status as $key => $value)
                    @if($key == $details->status)
                    <option value="{{$key}}" selected>{{$value}}</option>
                    @else
                    <option value="{{$key}}">{{$value}}</option>
                    @endif
                    @endforeach
                </select>
                <?php /* <?php
                  $status = ['Not scheduled' => '1', 'Scheduled' => '2', 'Shipped' => '3', 'Arrived' => '4'];
                  ?>
                  <?=array_search($details->status,$status); ?> */ ?>
            </td>
            <td data-search='<?= $details->invoiced_name ?>' data-order='<?= $details->invoiced_name ?>'>
                <input type="text" class="form-control invoice_name tracking_order" name="shipping_plan[<?= $i ?>][invoice_name]" value="{{$details->invoice_name}}" style="width :200px;"/>
            </td>
            <td data-search='<?= $details->invoiced_to_customer ?>' data-order='<?= $details->invoiced_to_customer ?>'>
                <input type="date" class="form-control invoiced_to_customer tracking_order" name="shipping_plan[<?= $i ?>][invoiced_to_customer]" value="{{$details->invoiced_to_customer}}" style="width :160px;"/>
            </td>
            <td data-search='<?= $details->invoice_amount ?>' data-order='<?= $details->invoice_amount ?>'>
                <input type="text" class="form-control invoice_amount tracking_order" name="shipping_plan[<?= $i ?>][invoice_amount]" value="{{$details->invoice_amount}}" style="width :120px;" onkeypress="return isNumber(event)"/>
            </td>
            <td data-search='<?= $details->deposit_paid ?>' data-order='<?= $details->deposit_paid ?>'>
                <input type="text" class="form-control deposit_paid tracking_order" name="shipping_plan[<?= $i ?>][deposit_paid]" value="{{$details->deposit_paid}}" style="width :120px;" onkeypress="return isNumber(event)"/>
            </td>
            <td data-search='<?= $details->payment_received ?>' data-order='<?= $details->payment_received ?>'>
                <?php /* <input type="text" class="form-control payment_recieved tracking_order shipping_plan_tracking" name="shipping_plan[<?= $i ?>][payment_received]" value="{{$details->payment_received}}"/> */ ?>
                <select class="form-control payment_recieved tracking_order" name="shipping_plan[<?= $i ?>][payment_received]" style="width :100px;">
                    <?php $receive_arr = ['1' => 'Yes', '2' => 'No']; ?>
                    <option value="">Select</option>
                    @foreach($receive_arr as $key => $value)
                    @if($key == $details->payment_received)
                    <option value="{{$key}}" selected="">{{$value}}</option>

                    @else
                    <option value="{{$key}}">{{$value}}</option>
                    @endif
                    @endforeach
                </select>

            </td>
            <td data-search='<?= $details->payment_date ?>' data-order='<?= $details->payment_date ?>'>
                <input type="date" class="form-control payment_date tracking_order" name="shipping_plan[<?= $i ?>][payment_date]" value="{{$details->payment_date}}" style="width :170px;"/>
            </td>

            <td data-search='<?= $details->payment_id ?>' data-order='<?= $details->payment_id ?>'>
                <input type="text" class="form-control payment_id tracking_order" name="shipping_plan[<?= $i ?>][payment_id]" value="{{$details->payment_id}}" style="width :120px;" onkeypress="return isNumber(event)"/>
            </td>
            <td data-search='<?= $details->comments?>' data-order='<?= $details->comments ?>'>
                <input type="text" class="form-control comments tracking_order" name="shipping_plan[<?= $i ?>][comments]" value="{{$details->comments}}" style="width :200px;"/>
            </td>
            
            
            
<!--            <td data-search='<?= $details->bl_number ?>' data-order='<?= $details->bl_number ?>'>
                <input type="text" class="form-control bl_number shipping_plan shipping_plan_tracking" name="shipping_plan[<?= $i ?>][bl_number]" value="<?= ($details->bl_number == '') ? 'N/A' : $details->bl_number ?>" disabled=""/>
                <?php /* {{$details->bl_number}} */ ?>
            </td>
            <td data-search='<?= $details->description ?>' data-order='<?= $details->description ?>'>
                <textarea rows="1" class="form-control description shipping_plan shipping_plan_tracking" name="shipping_plan[<?= $i ?>][description]" disabled=""><?= ($details->description == '') ? 'N/A' : $details->description ?></textarea>
                <?php /* {{$details->description}} */ ?>
            </td>
           
            <td data-search='<?= $details->cargo_ready_date ?>' data-order='<?= $details->cargo_ready_date ?>'>
                <input type="date" class="form-control cargo_ready_date shipping_plan shipping_plan_tracking" name="shipping_plan[<?= $i ?>][cargo_ready_date]" value="<?= ($details->cargo_ready_date == '') ? 'N/A' : $details->cargo_ready_date ?>" disabled="" style="width :170px;"/> 
                <?php /* {{$details->cargo_ready_date}} */ ?>
            </td>
            
            
            <td data-search='<?= $details->total_cost ?>' data-order='<?= $details->total_cost ?>'>
                <input type="text" class="form-control total_cost shipping_plan shipping_plan_tracking format_me_in_decimals" name="shipping_plan[<?= $i ?>][total_cost]" value="<?= ($details->total_cost == '') ? 'N/A' : $details->total_cost ?>" disabled=""/>
                <?php /* {{$details->total_cost}} */ ?>
            </td>
            
            <td data-search='<?= $details->profit ?>' data-order='<?= $details->profit ?>'>
                <input type="text" class="form-control profit shipping_plan shipping_plan_tracking format_me_in_decimals" name="shipping_plan[<?= $i ?>][profit]" value="<?= ($details->profit == '') ? 'N/A' : $details->profit ?>" disabled=""/>
                <?php /* {{$details->profit}} */ ?>
            </td>-->

            
            
            
            
            
            

        </tr>

        <?php $i++; ?>
        @endforeach
    </tbody>
</table>
<script>
$(document).ready(function () {
    var table = $('#tbl_load_tracking_order').DataTable({
        "bSort" : false,
        "bPaginate": false,
        "bInfo": false,
    });

    // Add event listener for opening and closing rows
    $('#tbl_load_tracking_order').on('click', 'td a.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var shipping_plan = tr.attr('data-child-value');
        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.find('i').addClass('fa-plus');
            tr.find('i').removeClass('fa-minus');
            tr.removeClass('shown');
        } else {
            // Open this row
            tr.find('i').addClass('fa-minus');
            tr.find('i').removeClass('fa-plus');
            format(row.child, shipping_plan);
            tr.addClass('shown');
        }
    });

    function format(rowchild, shipping_plan) {
        $.ajax({
            url: "{{url('admin/load_shipping_container_products_for_tracking')}}",
            method: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {shipping_plan_id: shipping_plan},
            beforeSend: function () {
                //                        Swal.showLoading();
            },
            onError: function () {
                swal("Error!", 'Something Went Wrong', "error");
            },
            success: function (response)
            {
                rowchild($(response)).show();
            }
        });
//            var thead = '',  tbody = '';
//            thead = '<th>jkjh</th>';
//            tbody = '<tr><td>oo</td><td>pp</td></tr>';
//            callback($('<table>' + thead + tbody + '</table>')).show();
    }
});    
</script>