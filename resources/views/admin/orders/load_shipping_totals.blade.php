<div class="row view_totals_row_table">
    <div class="col-lg-6 col-md-12 ">
        <table class="view_totals_table">

            <tr>
                <td>Total Containers</td>
                <td>
                    <?php
                    echo DB::table('shipping_plans')
                            ->where('order_id', $order_id)
                            ->where('deleted_at', null)
                            ->count('id');
                    ?>
                </td>
            </tr>
            <tr>
                <td>Total Container Order Cost</td>
                <td>
                    <?php
                    echo DB::table('shipping_plans')
                            ->where('order_id', $order_id)
                            ->sum('total_cost');
                    ?>
                </td>
            </tr>
            <tr>
                <td>Total Customer Buy Price</td>
                <td>
                    <?php
                    echo DB::table('shipping_plans')
                            ->where('order_id', $order_id)
                            ->sum('customer_buy_price');
                    ?>
                </td>
            </tr>
            <tr>
                <td>Total Profit</td>
                <td>
                    <?php
                    echo DB::table('shipping_plans')
                            ->where('order_id', $order_id)
                            ->sum('profit');
                    ?>
                </td>
            </tr>
        </table>
    </div>
    <div class="col-lg-6 col-md-12 ">
        <table class="view_totals_table">
            <tr>
                <td>Product</td>
                <td>Total Qty (Confirmed)</td>
                <td>Total Cost</td>
                <td>Customer Buy Price</td>
                <td>Profit</td>
            </tr>
            <?php foreach (App\OrderLine::where('order_id', $order_id)->get() as $order_lines) { ?>
                <tr>
                    <td>{{get_product_name_by_id($order_lines->product_id)}}</td>
                    <td><?php
                        echo DB::table('shipping_container_products')
                                ->where('order_id', $order_id)
                                ->where('order_line_id', $order_lines->id)
                                ->where('deleted_at', null)
                                ->sum('confirmed_quantity');
                        ?>
                    </td>
                    <td><?php
                        echo DB::table('shipping_container_products')
                                ->where('order_id', $order_id)
                                ->where('order_line_id', $order_lines->id)
                                ->where('deleted_at', null)
                                ->sum('total_cost');
                        ?>
                    </td>
                    <td><?php
                        echo DB::table('shipping_container_products')
                                ->where('order_id', $order_id)
                                ->where('order_line_id', $order_lines->id)
                                ->where('deleted_at', null)
                                ->sum('customer_buy_price');
                        ?>
                    </td>
                    <td><?php
                        echo DB::table('shipping_container_products')
                                ->where('order_id', $order_id)
                                ->where('order_line_id', $order_lines->id)
                                ->where('deleted_at', null)
                                ->sum('profit');
                        ?>
                    </td>
                </tr>
            <?php } ?>
        </table>
    </div>
    <!--<div class="col-md-4">-->
<!--                    <table class="view_totals_table">
            <tr>
                <td>Costs Include:</td>
                <td></td>
            </tr>
            <tr>
                <td><Cost name><use><% or Value)><total applied></td>
                    <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                    </table>-->
    <!--</div>-->
</div>