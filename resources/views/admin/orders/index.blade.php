
@extends('layouts.backend')

@section('content')
<div class="container">
    <div class="row">
        @include('admin.sidebar')

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Orders</div>
                <div class="card-body">
                    <a href="{{ url('/admin/orders/create') }}" class="btn btn-success btn-sm" title="Add New Order">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add New
                    </a>
                    @if(\Auth::user()->roles->first()->name != 'supplier')
                    <a href="{{ url('/admin/orders/order/overview') }}" class="btn btn-success btn-sm" title="View Order Report">
                        <i class="fa fa-eye" aria-hidden="true"></i> Order Overview
                    </a>
                    @endif
                    {!! Form::open(['method' => 'GET', 'url' => '/admin/orders', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
                    <div class="input-group">
                        <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                        <span class="input-group-append">
                            <button class="btn btn-secondary" type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                    {!! Form::close() !!}

                    <br/>
                    <br/>
                    <div class="table-responsive suppliers_table_mobile">
                        <table class="table table-borderless">
                            <thead>
                                <tr>
                                    <th>#</th><th>Order Name</th><th>Order Status</th><th>Products</th><th>Active</th><th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $status = get_order_status_list(); ?>
                                @foreach($orders as $item)
                                <tr>
                                    <td style="width: 4%;">{{ $item->id }}</td>
                                    <td style="width: 26%;">{{ $item->name }}</td>
                                    <td style="width: 10%;">{{ get_order_status_name_by_id($item->confirmed) }}</td>
                                    
                                    <td style="width: 45%;"><?php
                                        $k = 0;
                                        foreach (get_order_lines_product_name_as_string($item->id) as $key => $val) {
                                            echo (($k != 0) ? ', ' : '') . '' . get_product_name_by_id($val);
                                            $k++;
                                        }
                                        ?></td>
                                    <td style="width: 7%;"><?= ($item->active == '0') ? 'Yes' : 'No' ?></td>
                                    <td style="width: 7%;">
                                        <a href="{{ url('/admin/orderlinesbyorderid/' . $item->id) }}" title="View Order Lines" style="display: none"><button class="btn btn-info btn-sm"><i class="fa fa-industry" aria-hidden="true"></i></button></a>
                                        <a href="{{ url('/admin/orders/' . $item->id) }}" title="View Order" style="display:none;"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                        <a href="{{ url('/admin/orders/' . $item->id . '/edit') }}" title="Edit Order"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                        @if(\Auth::user()->roles->first()->name != 'supplier')
                                        {!! Form::open([
                                        'method' => 'DELETE',
                                        'url' => ['/admin/orders', $item->id],
                                        'style' => 'display:inline'
                                        ]) !!}
                                        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                        'type' => 'submit',
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-sm deleteentry',
                                        'title' => 'Delete Order' 
                                        )) !!}
                                        {!! Form::close() !!}
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
</script>
<script>

    $(document).ready(function () {
        $('.table').dataTable({
            "iDisplayLength": 50
        });
    });
    
    //change order status
    $('.order_status').on('change', function () {
        var status_id = $(this).attr('status_id');
        var val = $(this).val();
        
        $.ajax({
            url: "{{url('admin/update_order_status')}}",
            method: "get",
            data: {"id": status_id,"val":val},
            beforeSend: function () {
                //                        Swal.showLoading();
            },
            onError: function () {
                swal("Error!", 'Something Went Wrong', "error");
            },
            success: function (response)
            {

                if (response) {
                    if (response.status) {
//                      console.log('order line saved');
                        forGpCalculation();
//                        getProducts();
                    }
                }
            }
        });
    });
</script>
@endsection
