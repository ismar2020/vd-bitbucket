@extends('layouts.backend')

@section('content')
<div class="container">
    <div class="row">
        @include('admin.sidebar')

        <div class="col-md-12">
            <div class="card">
                
                <div class="card-header">Edit Order #<?php echo $order_id; ?></div>
                <div class="card-body">
                    <ul class="nav nav-tabs">

                        <li class="active"><a href="{{url("admin/orders/".$order_id."/edit")}}">Back to Order Pricing</a></li>
                        <li><a href="{{url("admin/orders/".$order_id."/edit")}}#shipping_pan"><b>Shipping Plan</b></a></li>
                        <li><a href="{{url("admin/orders/".$order_id."/edit")}}#summary">Tracking Order</a></li>
                        <li><a href="{{url("admin/orders/".$order_id."/edit")}}#documentation">Documentation</a></li>
                    </ul>
                    <div id="">

                        <br><br>
                        <form method="post" name="" action="javascript:void(0)" accept-charset="UTF-8" class="form-horizontal" id="order_pricing_costs_fis" enctype="multipart/form-data">
                            <div class="responsive" style="overflow: auto;">
                                <!-- ********************supplier's contact ID**********************************-->
                                <input type="hidden" name="order_id" class="form-control" id="order_id" value="{{ $order_id }}"/>
                                <!--***************************************ends**********************************-->

                                <table class="table fis_table">
                                    <thead>
                                        <tr>
                                            <th class="thicon">FIS</th>
                                            <?php
                                            $i = '1';
                                            foreach ($orderDetails as $details):
                                                $product_data = get_product_details_by_id($details->product_id);
//                                            dd($product_data->name);
                                                ?>
                                                <th class="tricon thborders">
                                                    <div class="div_fis_5">
                                                        <div class="form-group fleft">
                                                            {{$product_data->name}}
                                                        </div>
                                                        <!--<div class="form-group fright">-->
                                                            <!--<input type="text" class="form-control" value="<?php //echo get_currency_name_by_id(\App\Order::where('id',$order_id)->first()->pricing_currency); ?>" disabled=""/>-->
                                                        <!--</div>-->
                                                    </div>
                                                </th>
                                                <?php
                                                $i++;
                                            endforeach;
                                            ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div class="m1">
                                                    <hr style="
                                                        position: absolute;
                                                        width: 100%;
                                                        border-top: 1px solid #d6d7d8;
                                                        margin-top: 52px;
                                                        left: 0;
                                                        ">
                                                    <div class="m1a"><p>TOTAL LANDED COST</p></div>
                                                    <hr style="
                                                        position: absolute;
                                                        width: 100%;
                                                        border-top: 1px solid #d6d7d8;
                                                        margin-top: 127px;
                                                        left: 0;
                                                        ">
                                                    <div class="m1a left_ml"><p>Shipping Cost&nbsp;<i class="fa fa-eye" aria-hidden="true" title="*Note - if you are using a shipping quote that includes below charges, Leave below fields blank"></i></p></div>
                                                    <hr style="
                                                        position: absolute;
                                                        width: 100%;
                                                        border-top: 1px solid #d6d7d8;
                                                        margin-top: 323px;
                                                        left: 0;
                                                        ">
                                                    <div class="m2a left_ml left_f"><p>From factory to<br>port of origin</p></div>
                                                    <hr style="
                                                        position: absolute;
                                                        width: 100%;
                                                        border-top: 1px solid #d6d7d8;
                                                        margin-top: 570px;
                                                        left: 0;
                                                        ">
                                                    <div class="m3a left_ml"><p>From port of dispatch<br>to warehouse (FIW)</p></div>
                                                    <hr style="
                                                        position: absolute;
                                                        width: 100%;
                                                        border-top: 1px solid #d6d7d8;
                                                        margin-top: 958px;
                                                        left: 0;
                                                        ">
                                                    <div class="m4a left_ml"><p>From warehouse<br>to store (FIS)&nbsp;<i class="fa fa-eye" aria-hidden="true" title="*Note - all costs in this section should be allocated per individual product. For example, it costs $30 to send a single unit of the product to the store/customer"></i></p></div>
                                                </div>
                                            </td>
                                            <?php
                                            $i = '1';
                                            foreach ($orderDetails as $details):
                                                $product_data = get_product_details_by_id($details->product_id);
//                                                    dd(json_encode($details));
                                                ?>
                                            <td id="<?php echo $i; ?>" class="fis_table_td">
                                                    <input type="hidden" value="{{$details->id}}" name="order_pricing[<?= $i ?>][row_id]" />
                                                    <input type="hidden" class="form-control product_unit_price" value="{{$details->product_unit_price}}" name="product_unit_price" />
                                                    <input type="hidden" class="form-control packing_quantity" value="{{$details->packing_quantity}}" name="packing_quantity" />
                                                    <input type="hidden" class="form-control pricing_currency" value="{{$details->pricing_currency}}" name="pricing_currency" />
                                                    <input type="hidden" class="form-control product_currency" value="{{$product_data->product_currency}}" name="product_currency" />

                                                    <div class="form-group fleft">
                                                       <input type="text" class="form-control total_landed_costs_fis totals_of_landed format_me_in_decimals" name="order_pricing[<?= $i ?>][total_landed_costs_fis]" value="{{$details->total_landed_costs_fis}}" disabled=""/>
                                                       
                                                    </div>
                                                    <div class="form-group fright">
                                                       
                                                        <input type="text" class="form-control" value="<?php echo get_currency_name_by_id(\App\Order::where('id',$order_id)->first()->pricing_currency); ?>" disabled=""/>
                                                    </div>
                                                    <div class="div_fis_top">
                                                    <div class="form-group fleft">
                                                        <label class="control-label">Container</label>
                                                        <select class="form-control container_size" name="order_pricing[<?= $i ?>][container_size]" disabled><?php $data = get_containers_list(); ?>
                                                            <option value="">Container Size</option>
                                                            @foreach($data as $key => $value)
                                                            @if($key == $details->container_size)
                                                            <option value="{{$key}}" selected>{{$value}}</option>
                                                            @else
                                                            <option value="{{$key}}">{{$value}}</option>
                                                            @endif
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                        <div class="form-group fright">
                                                        <label class="control-label">Total Units</label>
                                                        <input type="text" class="form-control packing_quantity order_pricing_costs_fis" name="order_pricing[<?= $i ?>][packing_quantity]" value="{{$details->packing_quantity}}" disabled/></div>
                                                    </div>

                                                    <div class="" style="visibility: hidden;"><input type="text" class="form-control" name="product_name" value="{{$details->product_name}}" disabled/></div>
                                                    <div class="div_fis1">
                                                        <div class="form-group fleft">
                                                            <label class="control-label">Port of Origin</label>
                                                            <select class="form-control order_pricing_costs_fis" name="order_pricing[<?= $i ?>][shipping_port_id]" style="width: 140px;"  disabled=""><?php $data = get_shipping_port_list(); ?>
                                                                <option value="">Port of Origin</option>
                                                                @foreach($data as $key => $value)
                                                                @if($key == $product_data->shipping_port)
                                                                <option value="{{$key}}" selected>{{$value}}</option>
                                                                @else
                                                                <option value="{{$key}}">{{$value}}</option>
                                                                @endif
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="form-group fright">
                                                            <label class="control-label">Port of Dispatch</label>
                                                            <select class="form-control order_pricing_costs_fis" name="order_pricing[<?= $i ?>][shipping_to_port_id]" style="width: 140px;" disabled=""><?php $data = get_shipping_port_list(); ?>
                                                                <option value="">Port of Dispatch</option>
                                                                @foreach($data as $key => $value)
                                                                @if($key == $details->shipping_to_port_id)
                                                                <option value="{{$key}}" selected>{{$value}}</option>
                                                                @else
                                                                <option value="{{$key}}">{{$value}}</option>
                                                                @endif
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="form-group fleft">
                                                            <?php
                                                            $rate_currency = checkShippingValuesExists($product_data->shipping_port, $details->shipping_to_port_id, $details, 'shipping_rate_currency');
                                                            
                                                            
//Creating shipping rate field name using container ID as now rate as per container saved in db earliar only shipping_rate is a single field
                                                            
    $container = \App\Container::where('id', $details->container_size)->first();
//    dd($name);
    // strip out all whitespace
    $shipping_rate_field = null;
    if($container != ''){
        $name = $container->name;
        $container_name = preg_replace('/\s*/', '', $name);
        // convert the string to all lowercase
        $container_name = strtolower($container_name);
        $shipping_rate_field = 'shipping_rate_' . $container_name;
    }
//    dd($product_data->shipping_port,$details->shipping_to_port_id,$details,$shipping_rate_field,checkShippingValuesExists($product_data->shipping_port,$details->shipping_to_port_id, $details,$shipping_rate_field));
//ends
                                                           
                                                            ?>
                                                            <label class="control-label">Shipping Rate<?php //echo $product_data->shipping_port.'-'.$details->shipping_to_port_id.'-'.$shipping_rate_field.'-'.$details; ?></label>
                                                            <input type="text" class="form-control order_pricing_costs_fis shipping_rate format_me_in_decimals" onkeypress="return isNumber(event)" name="order_pricing[<?= $i ?>][shipping_rate]" value="{{ checkShippingValuesExists($product_data->shipping_port,$details->shipping_to_port_id, $details,$shipping_rate_field)}}"/>
                                                        </div>
                                                        <div class="form-group fright">
                                                        <label class="control-label">Shipping Rate Currency</label>
                                                        <select class="form-control order_pricing_costs_fis shipping_rate_currency" id="shipping_rate_currency" name="order_pricing[<?= $i ?>][shipping_rate_currency]">
                                                            <option value="">Select Currency</option>
                                                            @if($rate_currency != null)
                                                            {!!  get_limited_currency_list_with_selected_item($rate_currency) !!}
                                                            @elseif($details->shipping_rate_currency != null)
                                                            {!!  get_limited_currency_list_with_selected_item($details->shipping_rate_currency) !!}    
                                                            @else
                                                            {!!  get_limited_currency_list_with_symbols_through_id() !!}
                                                            @endif
                                                        </select>
                                                    </div>
                                                    </div>
                                                    <div class="div_fis2">
                                                    <?php //dd(checkValueExists('transport',$details)); ?>
                                                        <div class="form-group fleft">
                                                            <label class="control-label">Transport</label>
                                                            <input type="text" class="form-control order_pricing_costs_fis transport format_me_in_decimals" onkeypress="return isNumber(event)" name="order_pricing[<?= $i ?>][transport]" value="{{ checkValueExists('transport',$details)}}"/>
                                                        </div>
                                                        <div class="form-group fright">
                                                            <label class="control-label">Transport Currency</label>
                                                            <select class="form-control order_pricing_costs_fis transport_currency" id="transport_currency" name="order_pricing[<?= $i ?>][transport_currency]">
                                                                <option value="">Select Currency</option>
                                                                <?php $t_value = checkValueExists('transport_currency', $details); ?>
                                                                @if(checkValueExists('transport_currency',$details) != null)
                                                                {!!  get_limited_currency_list_with_selected_item($t_value) !!}
                                                                @else
                                                                {!!  get_limited_currency_list_with_symbols_through_id() !!}
                                                                @endif
                                                            </select>
                                                        </div>
                                                        <div class="form-group fleft">
                                                            <label class="control-label">Port Costs</label>
                                                            <input type="text" class="form-control order_pricing_costs_fis port_costs format_me_in_decimals" onkeypress="return isNumber(event)" name="order_pricing[<?= $i ?>][port_costs]" value="{{ checkValueExists('port_costs',$details)}}"/>   
                                                        </div>
                                                        <div class="form-group fright">
                                                            <label class="control-label">Port Costs Currency</label>
                                                            <select class="form-control order_pricing_costs_fis port_costs_currency" id="port_costs_currency" name="order_pricing[<?= $i ?>][port_costs_currency]">
                                                                <?php $p_c_value = checkValueExists('port_costs_currency', $details); ?>
                                                                <option value="">Select Currency</option>
                                                                @if(checkValueExists('port_costs_currency',$details) != null)
                                                                {!!  get_limited_currency_list_with_selected_item($p_c_value) !!}
                                                                @else
                                                                {!!  get_limited_currency_list_with_symbols_through_id() !!}
                                                                @endif
                                                            </select>  
                                                        </div>
                                                        <div class="form-group fleft">
                                                            <label class="control-label">Insurance Costs</label>
                                                            <input type="text" class="form-control order_pricing_costs_fis insurance_cost format_me_in_decimals" onkeypress="return isNumber(event)" name="order_pricing[<?= $i ?>][insurance_cost]" value="{{ checkValueExists('insurance_cost',$details)}}"/>  
                                                        </div>
                                                        <div class="form-group fright">
                                                        <label class="control-label">Insurance Costs Currency</label>
                                                        <select class="form-control order_pricing_costs_fis insurance_cost_currency" id="insurance_cost_currency" name="order_pricing[<?= $i ?>][insurance_cost_currency]">
                                                            <?php $i_c_value = checkValueExists('insurance_cost_currency', $details); ?>
                                                            <option value="">Select Currency</option>
                                                            @if(checkValueExists('insurance_cost_currency',$details) != null)
                                                            {!!  get_limited_currency_list_with_selected_item($i_c_value) !!}
                                                            @else
                                                            {!!  get_limited_currency_list_with_symbols_through_id() !!}
                                                            @endif
                                                        </select>  
                                                    </div> 
                                                    </div>
                                                    <div class="div_fis3">
                                                        <div class="form-group fleft">
                                                            <label class="control-label">Import Custom Duties %</label>
                                                            <input type="text" class="form-control order_pricing_costs_fis import_custom_duties_percentage format_me_in_decimals" onkeypress="return isNumber(event)" name="order_pricing[<?= $i ?>][import_custom_duties_percentage]" value="{{ checkValueExists('import_custom_duties_percentage',$details)}}"/>
                                                        </div>
                                                        <div class="form-group fright">
                                                            <label class="control-label" style="visibility :hidden;">Value</label>
                                                            <input type="text" class="form-control order_pricing_costs_fis import_custom_duties_value" name="order_pricing[<?= $i ?>][import_custom_duties_value]" value="{{ $details->import_custom_duties_value }}" disabled=""/>
                                                        </div>
                                                        <div class="form-group fleft">
                                                            <label class="control-label">Import Taxes %</label>
                                                            <input type="text" class="form-control order_pricing_costs_fis import_taxes_percentage format_me_in_decimals" onkeypress="return isNumber(event)" name="order_pricing[<?= $i ?>][import_taxes_percentage]" value="{{ checkValueExists('import_taxes_percentage',$details)}}"/>  
                                                        </div>
                                                        <div class="form-group fright">
                                                            <label class="control-label" style="visibility :hidden;">Value</label>
                                                            <input type="text" class="form-control import_taxes_value import_taxes_value" name="order_pricing[<?= $i ?>][import_taxes_value]" value="{{ $details->import_taxes_value }}" disabled=""/>   
                                                        </div>
                                                        <div class="form-group fleft">
                                                            <label class="control-label">Handling Costs in<br>port of discharge</label>
                                                            <input type="text" class="form-control order_pricing_costs_fis handing_costs_in_destination_terminal format_me_in_decimals" onkeypress="return isNumber(event)" name="order_pricing[<?= $i ?>][handing_costs_in_destination_terminal]" value="{{ checkValueExists('handing_costs_in_destination_terminal',$details)}}"/>   
                                                        </div>
                                                        <div class="form-group fright">
                                                            <label class="control-label" style="min-height:50px;">Handing Costs Currency</label>
                                                            <select class="form-control order_pricing_costs_fis handing_costs_currency" id="handing_costs_currency" name="order_pricing[<?= $i ?>][handing_costs_currency]">
                                                                <?php $h_c_value = checkValueExists('handing_costs_currency', $details); ?>
                                                                <option value="">Select Currency</option>
                                                                @if(checkValueExists('handing_costs_currency',$details) != null)
                                                                {!!  get_limited_currency_list_with_selected_item($h_c_value) !!}
                                                                @else
                                                                {!!  get_limited_currency_list_with_symbols_through_id() !!}
                                                                @endif
                                                            </select>   
                                                        </div>
                                                        <div class="form-group fleft">
                                                            <label class="control-label">Cost of shipping after<br>port of discharge</label>
                                                            <input type="text" class="form-control order_pricing_costs_fis costs_of_shipping_after_destination_port format_me_in_decimals" onkeypress="return isNumber(event)" name="order_pricing[<?= $i ?>][costs_of_shipping_after_destination_port]" value="{{ checkValueExists('costs_of_shipping_after_destination_port',$details)}}"/> 
                                                        </div>
                                                        <div class="form-group fright">
                                                        <label class="control-label" style="min-height:50px;">Shipping after Port Currency</label>
                                                        <select class="form-control order_pricing_costs_fis shipping_after_port_currency" id="shipping_after_port_currency" name="order_pricing[<?= $i ?>][shipping_after_port_currency]">
                                                            <?php $s_c_p_value = checkValueExists('shipping_after_port_currency', $details); ?>
                                                            <option value="">Select Currency</option>
                                                            @if(checkValueExists('shipping_after_port_currency',$details) != null)
                                                            {!!  get_limited_currency_list_with_selected_item($s_c_p_value) !!}
                                                            @else
                                                            {!!  get_limited_currency_list_with_symbols_through_id() !!}
                                                            @endif
                                                        </select>  
                                                    </div>
                                                    </div>
   <!--*******************---fis part which differs from FIW--***************------>
                                                    <div class="div_fis4 div_last">
                                                    <div class="form-group fleft">
                                                        <label class="control-label">Domestic Transport</label>
                                                        <input type="text" class="form-control order_pricing_costs_fis domestic_transport format_me_in_decimals" onkeypress="return isNumber(event)" name="order_pricing[<?= $i ?>][domestic_transport]" value="{{ checkValueExists('domestic_transport',$details)}}"/> 
                                                        <label class="control-label margin_top_new_fields">Transport value</label>
                                                        
                                                    </div>
                                                    <div class="form-group fright">
                                                        <label class="control-label currency_class">Currency</label>
                                                        <select class="form-control order_pricing_costs_fis domestic_transport_currency" id="domestic_transport_currency" name="order_pricing[<?= $i ?>][domestic_transport_currency]">
                                                            <?php $d_t_c_value = checkValueExists('domestic_transport_currency', $details); ?>
                                                            <option value="">Select Currency</option>
                                                            @if(checkValueExists('domestic_transport_currency',$details) != null)
                                                            {!!  get_limited_currency_list_with_selected_item($d_t_c_value) !!}
                                                            @else
                                                            {!!  get_limited_currency_list_with_symbols_through_id() !!}
                                                            @endif
                                                        </select> 
                                                        <input type="text" class="form-control domestic_transport_included_qty margin_top_new_fields" onkeypress="return isNumber(event)" name="domestic_transport_included_qty" value="" disabled=""/> 
                                                    </div>
                                                    <div class="form-group fleft">
                                                        <label class="control-label">Storage Overhead</label>
                                                        <input type="text" class="form-control order_pricing_costs_fis storage_overhead format_me_in_decimals" onkeypress="return isNumber(event)" name="order_pricing[<?= $i ?>][storage_overhead]" value="{{ checkValueExists('storage_overhead',$details)}}"/> 
                                                        <label class="control-label margin_top_new_fields">Storage value</label>
                                                        
                                                    </div>
                                                    <div class="form-group fright">
                                                        <label class="control-label currency_class">Currency</label>
                                                        <select class="form-control order_pricing_costs_fis storage_overhead_currency" id="storage_overhead_currency" name="order_pricing[<?= $i ?>][storage_overhead_currency]">
                                                            <?php $s_o_c_value = checkValueExists('storage_overhead_currency', $details); ?>
                                                            <option value="">Select Currency</option>
                                                            @if(checkValueExists('storage_overhead_currency',$details) != null)
                                                            {!!  get_limited_currency_list_with_selected_item($s_o_c_value) !!}
                                                            @else
                                                            {!!  get_limited_currency_list_with_symbols_through_id() !!}
                                                            @endif
                                                        </select>
                                                        <input type="text" class="form-control storage_overhead_included_qty margin_top_new_fields format_me_in_decimals" onkeypress="return isNumber(event)" name="storage_overhead_included_qty" value="" disabled=""/> 
                                                    </div>
                                                    <div class="form-group fleft">
                                                        <label class="control-label currency_class">Merchandising</label>
                                                        <input type="text" class="form-control order_pricing_costs_fis merchandising format_me_in_decimals" onkeypress="return isNumber(event)" name="order_pricing[<?= $i ?>][merchandising]" value="{{ checkValueExists('merchandising',$details)}}"/> 
                                                        <label class="control-label margin_top_new_fields">Merchandising Value</label>
                                                    </div>
                                                    <div class="form-group fright">
                                                        <label class="control-label currency_class">Currency</label>
                                                        <select class="form-control order_pricing_costs_fis merchandising_currency" id="merchandising_currency" name="order_pricing[<?= $i ?>][merchandising_currency]">
                                                            <?php $m_c_value = checkValueExists('merchandising_currency', $details); ?>
                                                            <option value="">Select Currency</option>
                                                            @if(checkValueExists('merchandising_currency',$details) != null)
                                                            {!!  get_limited_currency_list_with_selected_item($m_c_value) !!}
                                                            @else
                                                            {!!  get_limited_currency_list_with_symbols_through_id() !!}
                                                            @endif
                                                        </select>
                                                        <input type="text" class="form-control merchandising_included_qty margin_top_new_fields format_me_in_decimals" onkeypress="return isNumber(event)" name="merchandising_included_qty" value="" disabled=""/> 
                                                    </div>
                                                    <div class="form-group fleft">
                                                        <label class="control-label currency_class">Other Overheads</label>
                                                        <input type="text" class="form-control order_pricing_costs_fis other_overheads format_me_in_decimals" onkeypress="return isNumber(event)" name="order_pricing[<?= $i ?>][other_overheads]" value="{{ checkValueExists('other_overheads',$details)}}"/> 
                                                        <label class="control-label margin_top_new_fields">Overhead Value</label>
                                                        
                                                    </div>
                                                    <div class="form-group fright">
                                                        <label class="control-label currency_class">Currency</label>
                                                        <select class="form-control order_pricing_costs_fis other_overheads_currency" id="other_overheads_currency" name="order_pricing[<?= $i ?>][other_overheads_currency]">
                                                            <?php $o_o_c_value = checkValueExists('other_overheads_currency', $details); ?>
                                                            <option value="">Select Currency</option>
                                                            @if(checkValueExists('other_overheads_currency',$details) != null)
                                                            {!!  get_limited_currency_list_with_selected_item($o_o_c_value) !!}
                                                            @else
                                                            {!!  get_limited_currency_list_with_symbols_through_id() !!}
                                                            @endif
                                                        </select>  
                                                        <input type="text" class="form-control other_overheads_included_qty margin_top_new_fields format_me_in_decimals" onkeypress="return isNumber(event)" name="other_overheads_included_qty" value="" disabled=""/> 
                                                    </div>
                                                    </div>
                                                    <!--</div>-->
                                                </td>
                                                <?php
                                                $i++;
                                            endforeach;
                                            ?>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection