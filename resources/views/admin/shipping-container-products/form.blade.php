<div class="form-group{{ $errors->has('order_id') ? 'has-error' : ''}}">
    {!! Form::label('order_id', 'Order Id', ['class' => 'control-label']) !!}
    {!! Form::number('order_id', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('order_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('order_line_id') ? 'has-error' : ''}}">
    {!! Form::label('order_line_id', 'Order Line Id', ['class' => 'control-label']) !!}
    {!! Form::number('order_line_id', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('order_line_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('shipping_plan_id') ? 'has-error' : ''}}">
    {!! Form::label('shipping_plan_id', 'Shipping Plan Id', ['class' => 'control-label']) !!}
    {!! Form::number('shipping_plan_id', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('shipping_plan_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('product_name') ? 'has-error' : ''}}">
    {!! Form::label('product_name', 'Product Name', ['class' => 'control-label']) !!}
    {!! Form::text('product_name', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('product_name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('product_image') ? 'has-error' : ''}}">
    {!! Form::label('product_image', 'Product Image', ['class' => 'control-label']) !!}
    {!! Form::text('product_image', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('product_image', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('number_of_units_that_fit') ? 'has-error' : ''}}">
    {!! Form::label('number_of_units_that_fit', 'Number Of Units That Fit', ['class' => 'control-label']) !!}
    {!! Form::number('number_of_units_that_fit', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('number_of_units_that_fit', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('container_order_quantity') ? 'has-error' : ''}}">
    {!! Form::label('container_order_quantity', 'Container Order Quantity', ['class' => 'control-label']) !!}
    {!! Form::number('container_order_quantity', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('container_order_quantity', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('container_order_quantity_cbm') ? 'has-error' : ''}}">
    {!! Form::label('container_order_quantity_cbm', 'Container Order Quantity Cbm', ['class' => 'control-label']) !!}
    {!! Form::number('container_order_quantity_cbm', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('container_order_quantity_cbm', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('container_total_cost') ? 'has-error' : ''}}">
    {!! Form::label('container_total_cost', 'Container Total Cost', ['class' => 'control-label']) !!}
    {!! Form::number('container_total_cost', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('container_total_cost', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('container_total_wholesale_price') ? 'has-error' : ''}}">
    {!! Form::label('container_total_wholesale_price', 'Container Total Wholesale Price', ['class' => 'control-label']) !!}
    {!! Form::number('container_total_wholesale_price', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('container_total_wholesale_price', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('total_product_units_added_to_other_containers') ? 'has-error' : ''}}">
    {!! Form::label('total_product_units_added_to_other_containers', 'Total Product Units Added To Other Containers', ['class' => 'control-label']) !!}
    {!! Form::number('total_product_units_added_to_other_containers', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('total_product_units_added_to_other_containers', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
