@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">ShippingContainerProduct {{ $shippingcontainerproduct->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/shipping-container-products') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/admin/shipping-container-products/' . $shippingcontainerproduct->id . '/edit') }}" title="Edit ShippingContainerProduct"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/shippingcontainerproducts', $shippingcontainerproduct->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'type' => 'submit',
'class' => 'btn btn-danger btn-sm deleteentry',
                                    'title' => 'Delete ShippingContainerProduct',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $shippingcontainerproduct->id }}</td>
                                    </tr>
                                    <tr><th> Order Id </th><td> {{ $shippingcontainerproduct->order_id }} </td></tr><tr><th> Order Line Id </th><td> {{ $shippingcontainerproduct->order_line_id }} </td></tr><tr><th> Shipping Plan Id </th><td> {{ $shippingcontainerproduct->shipping_plan_id }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
