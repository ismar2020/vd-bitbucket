@extends('layouts.backend')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="container-fluid content-top-gap">

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb my-breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/home')}}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Admin</li>
                    </ol>
                </nav>
                <!--                <div class="welcome-msg pt-3 pb-4">
                                    <h1>Hi <span class="text-primary">{{ Auth::user()->username}}</span>, Welcome back</h1>
                
                                </div>-->

                <!-- statistics data -->
                <div class="statistics">
                    <div class="row">
                        <div ss class="col-xl-12 pr-xl-2">
                            <div class="row">
                                <div class="col-sm-6 pr-sm-2 statistics-grid">
                                    <a href = "{{url('admin/users')}}">
                                        <div class="card card_border border-primary-top p-4">
                                            <i class="lnr lnr-user"> </i>
                                            <h3 class="number">@php echo get_user_count(); @endphp</h3>
                                            <p class="stat-text"><?=(\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null)?get_companies_name_by_id(\Auth::user()->company_id):'Total' ?> Users</p>

                                        </div>
                                    </a>
                                </div>

                                <?php if (Auth::check() && (Auth::user()->hasRole('super_admin'))) { ?>
                                    <div class="col-sm-6 pr-sm-2 statistics-grid">
                                        <a href = "{{url('admin/company')}}">
                                            <div class="card card_border border-primary-top p-4">
                                                <i class="lnr lnr-layers"> </i>
                                                <h3 class="number">@php echo get_companies_count(); @endphp</h3>
                                                <p class="stat-text">Total Company</p>

                                            </div>
                                        </a>
                                    </div>
                                
                                <div class="col-sm-6 pr-sm-2 statistics-grid">
                                    <a href = "{{url('admin/currency-exchange-rate')}}">
                                        <div class="card card_border border-primary-top p-4">
                                            <i class="lnr lnr-indent-increase"> </i>
                                            <h3 class="number">@php echo get_currency_exchange_rate_count(); @endphp</h3>
                                            <p class="stat-text">Currency Exchange rate</p>

                                        </div>
                                    </a>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div> 
                <!-- //statistics data -->



            </div> 
        </div>
    </div>
</div>
@endsection
