<div class="form-group{{ $errors->has('quote_id') ? 'has-error' : ''}}">
    {!! Form::label('quote_id', 'Quote Id', ['class' => 'control-label']) !!}
    {!! Form::number('quote_id', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('quote_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('original_file_name') ? 'has-error' : ''}}">
    {!! Form::label('original_file_name', 'Original File Name', ['class' => 'control-label']) !!}
    {!! Form::text('original_file_name', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('original_file_name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('file_name') ? 'has-error' : ''}}">
    {!! Form::label('file_name', 'File Name', ['class' => 'control-label']) !!}
    {!! Form::text('file_name', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('file_name', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
