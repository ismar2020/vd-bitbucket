<div class="form-group{{ $errors->has('username') ? ' has-error' : ''}}">
    {!! Form::label('username', 'Username: ', ['class' => 'control-label']) !!}
    {!! Form::text('username', null, ['class' => 'form-control', 'required' => 'required']) !!}
    {!! $errors->first('username', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('first_name') ? ' has-error' : ''}}">
    {!! Form::label('first_name', 'First Name: ', ['class' => 'control-label']) !!}
    {!! Form::text('first_name', null, ['class' => 'form-control', 'required' => 'required']) !!}
    {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('last_name') ? ' has-error' : ''}}">
    {!! Form::label('last_name', 'Surname: ', ['class' => 'control-label']) !!}
    {!! Form::text('last_name', null, ['class' => 'form-control', 'required' => 'required']) !!}
    {!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
    {!! Form::label('email', 'Email: ', ['class' => 'control-label']) !!}
    {!! Form::email('email', null, ['class' => 'form-control', 'required' => 'required']) !!}
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('password') ? ' has-error' : ''}}">
    {!! Form::label('password', 'Password: ', ['class' => 'control-label']) !!}
    @php
    $passwordOptions = ['class' => 'form-control'];
    if ($formMode === 'create') {
    $passwordOptions = array_merge($passwordOptions, ['required' => 'required']);
    }
    @endphp
    {!! Form::password('password', $passwordOptions) !!}
    {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('roles') ? ' has-error' : ''}}">
    {!! Form::label('role', 'User Type: ', ['class' => 'control-label']) !!}

    <select class="form-control user_role" name="roles[]">
         <option value="">Select User Type</option>
        <?php foreach ($roles as $key => $value) { ?>
           
            <option value="<?= $key ?>" <?php if(isset($user_roles[0]) && $user_roles[0] == $key ){ echo 'selected'; }?>><?= $value ?></option>
        <?php } ?>


    </select>
    
</div>
<div class="form-group{{ $errors->has('supplier_id') ? 'has-error' : ''}} add_supplier_div" style="display:none;">
    {!! Form::label('supplier_id', 'Link to Existing', ['class' => 'control-label']) !!}

    <select class="form-control select2 user_supplier" name="supplier_id"><?php $data = get_suppliers_list(); ?>
        <option value="">Select Supplier</option>
        @foreach($data as $key => $value)
        @if(isset($formMode) && $formMode == 'edit' && $user->supplier_id == $key)
        <option value="{{$key}}" selected>{{$value}}</option>
        @else
        <option value="{{$key}}">{{$value}}</option>
        @endif
        @endforeach
    </select>
    @if($formMode == 'create')
    <button type="button" class="btn btn-info btn-sm add_supplier" data-toggle="modal" data-target="#supplierModalLongnew" style="margin:12px;"><i class="fa fa-plus" aria-hidden="true"></i>
        Add Supplier</button> 
    @endif
    {!! $errors->first('supplier_id', '<p class="help-block">:message</p>') !!}
</div>
<?php if (\Auth::user()->roles->first()->name == 'super_admin') { ?>
<div class="form-group{{ $errors->has('company_id') ? 'has-error' : ''}} company_div" style="display:none;">
        {!! Form::label('company_id', 'Company', ['class' => 'control-label']) !!}
        <select class="form-control company_id" name="company_id">
            <?php $data = get_companies_list(); ?>
            <option value="">Select Company</option>
            @foreach($data as $key => $value)
            @if(isset($formMode) && $formMode == 'edit' && $user->company_id == $key)
            <option value="{{$key}}" selected>{{$value}}</option>
            @else
            @if(Auth::user()->company_id != null && Auth::user()->company_id == $key)
            <option value="{{$key}}" selected>{{$value}}</option>
            @else
            <option value="{{$key}}">{{$value}}</option>
            @endif
            @endif
            @endforeach
        </select>
        {!! $errors->first('company_id', '<p class="help-block">:message</p>') !!}
    </div>
<?php } ?>
<div class="form-group{{ $errors->has('mobile_prefix') ? 'has-error' : ''}}">
    {!! Form::label('mobile_prefix', 'Mobile Prefix', ['class' => 'control-label']) !!}
    <div class="input-group">
        <span class="input-group-addon">
            <i class="fa fa-plus" aria-hidden="true"></i>
        </span>
        {!! Form::select('mobile_prefix', get_phone_prefix_list(), null, ['class' => 'form-control select2']) !!}


        {!! $errors->first('mobile_prefix', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('mobile') ? 'has-error' : ''}}">
    {!! Form::label('mobile', 'Mobile', ['class' => 'control-label']) !!}
    {!! Form::number('mobile', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('mobile', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group{{ $errors->has('active') ? 'has-error' : ''}}">
    {!! Form::label('active', 'Active', ['class' => 'control-label']) !!}
    {!! Form::select('active', array('0' => 'Yes', '1' => 'No'), '0',['class' => 'form-control']) !!}
    {!! $errors->first('active', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
