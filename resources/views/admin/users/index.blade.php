@extends('layouts.backend')

@section('content')
<div class="container">
    <div class="row">
        @include('admin.sidebar')

        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><?= (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) ? get_companies_name_by_id(\Auth::user()->company_id) : 'Total' ?> Users</div>
                <div class="card-body">
                    <?php if (\Auth::user()->roles->first()->name == 'super_admin') { ?>
                        <a href="{{ url('admin/users/new_users') }}" class="btn btn-success btn-sm" title="New Users">
                            <i class="fa fa-users" aria-hidden="true"></i> New Users
                        </a>
                    <?php } ?>
                    <a href="{{ url('/admin/users/create') }}" class="btn btn-success btn-sm" title="Add New User">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add New
                    </a>
                    <br/>
                    <br/>


                    <!--                    {!! Form::open(['method' => 'GET', 'url' => '/admin/users', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="search" placeholder="Search...">
                                            <span class="input-group-append">
                                                <button class="btn btn-secondary" type="submit">
                                                    <i class="fa fa-search"></i>
                                                </button>
                                            </span>
                                        </div>
                                        {!! Form::close() !!}-->


                    <div class="table-responsive suppliers_table_mobile">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>ID</th><th>Username</th><th>Email</th>
                                    <th>Company</th>
                                    <?php if (\Auth::user()->roles->first()->name != 'supplier') { ?>
                                        <th>Role</th>
                                    <?php } ?>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>
                                        <a href="{{ url('/admin/users', $item->id) }}">{{ $item->username }}</a>
                                    </td>
                                    <td>{{ $item->email }}</td>
                                    <td><?php
                                        $company_name = get_companies_name_by_id($item->company_id);
                                        if ($company_name != 'NA') {
                                            ?><a href="{{ url('admin/usercompany',$item->company_id) }}">{{ $company_name }}</a><?php
                                        } else {
                                            echo $company_name;
                                        }
                                        ?>
                                    </td>
                                    <?php if (\Auth::user()->roles->first()->name != 'supplier') { ?>
                                        <td>
                                            <?php
                                            $user = App\User::where('id', $item->id)->with('roles')->first();
                                            if ($user->roles->first() != '') {
                                                echo ucwords($user->roles->first()->label);
                                            } else {
                                                echo 'NA';
                                            }
                                            ?>
                                        </td>
                                    <?php } ?>
                                    <td>
                                        <a href="{{ url('/admin/users/' . $item->id) }}" title="View User" style="display:none;"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>

                                        <a href="{{ url('/admin/users/' . $item->id . '/edit') }}" title="Edit User"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                        <?php //if (\Auth::user()->roles->first()->name == 'super_admin') { ?>
                                        {!! Form::open([
                                        'method' => 'DELETE',
                                        'url' => ['/admin/users', $item->id],
                                        'style' => 'display:inline'
                                        ]) !!}
                                        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                        'type' => 'submit',
                                        'type' => 'submit',
'class' => 'btn btn-danger btn-sm deleteentry',
                                        'title' => 'Delete User' 
                                        )) !!}
                                        {!! Form::close() !!}
                                        <?php //} ?>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
