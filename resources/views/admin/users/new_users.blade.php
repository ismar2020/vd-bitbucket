@extends('layouts.backend')

@section('content')
<div class="container">
    <div class="row">
        @include('admin.sidebar')

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">New Users</div>
                <div class="card-body">
                    <div class="table-responsive suppliers_table_mobile">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>ID</th><th>Username</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($new_users as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>
                                        <a href="{{ url('/admin/users', $item->id) }}">{{ $item->username }}</a>
                                    </td>
                                    <td>{{ $item->email }}</td>
                                    <td>{{ $item->mobile }}</td>
                                    <td>
                                        <a href="{{ url('/admin/users/' . $item->id . '/edit') }}" title="Edit User"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                        <?php //if (\Auth::user()->roles->first()->name == 'super_admin') { ?>
                                        {!! Form::open([
                                        'method' => 'DELETE',
                                        'url' => ['/admin/users', $item->id],
                                        'style' => 'display:inline'
                                        ]) !!}
                                        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                        'type' => 'submit',
                                        'type' => 'submit',
'class' => 'btn btn-danger btn-sm deleteentry',
                                        'title' => 'Delete User' 
                                        )) !!}
                                        {!! Form::close() !!}
                                        <?php //} ?>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
