@extends('layouts.backend')

@section('content')
<div class="container">
    <div class="row">
        @include('admin.sidebar')

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">User</div>
                <div class="card-body">

                    <a href="{{ url('/admin/users') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                    <a href="{{ url('/admin/users/' . $user->id . '/edit') }}" title="Edit User"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                    <?php if (\Auth::user()->roles->first()->name == 'super_admin') { ?>
                        {!! Form::open([
                        'method' => 'DELETE',
                        'url' => ['/admin/users', $user->id],
                        'style' => 'display:inline'
                        ]) !!}
                        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                        'type' => 'submit',
                        'type' => 'submit',
'class' => 'btn btn-danger btn-sm deleteentry',
                        'title' => 'Delete User',
                        'onclick'=>'return confirm("Confirm delete?")'
                        ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>
                    <?php } ?>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>ID.</th>
                                    <td>{{ $user->id }}</td>
                                </tr><tr>    
                                    <th>Username</th>
                                    <td> {{ $user->username }} </td>
                                </tr> <tr>   
                                    <th>First name</th>
                                    <td> {{ $user->first_name }} </td>
                                </tr> <tr>   
                                    <th>Last name</th>
                                    <td> {{ $user->last_name }} </td>
                                </tr>  <tr>  
                                    <th>Email</th>
                                    <td> {{ $user->email }} </td>
                                </tr><tr>  
                                    <th>Mobile</th>
                                    <td> {{ '+'.$user->mobile_prefix.' '.$user->mobile }} </td>
                                </tr>
                                <tr>  
                                    <th>Active</th>
                                    <td><?= ($user->active == '0') ? 'Yes' : 'No' ?></td>
                                </tr>
                                <tr>  
                                    <th>Company</th>
                                    <td><?php $company_name = get_companies_name_by_id($user->company_id);
                    if ($company_name != 'NA') { ?><a href="{{ url('admin/company',$user->company_id) }}">{{ $company_name }}</a><?php } else {
                        echo $company_name;
                    } ?>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>

                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
