@extends('layouts.backend')

@section('content')
<div class="container">
    <div class="row">
        @include('admin.sidebar')

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Company {{ $company->id }}</div>
                <div class="card-body">
                    <?php if (isset($previous)) { ?>
                        <a href="{{ url('/admin/users') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                    <?php }else{ ?>
                     <a href="{{ url('/admin/company') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                    <?php } ?>
                    <a href="{{ url('/admin/company/' . $company->id . '/edit') }}" title="Edit Company"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                    {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['admin/company', $company->id],
                    'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                    'type' => 'submit',
                    'type' => 'submit',
'class' => 'btn btn-danger btn-sm deleteentry',
                    'title' => 'Delete Company',
                    'onclick'=>'return confirm("Confirm delete?")'
                    ))!!}
                    {!! Form::close() !!}
                    <br/>
                    <br/>

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th>ID</th><td>{{ $company->id }}</td>
                                </tr>
                                <tr><th> Name </th><td> {{ $company->name }} </td></tr><tr><th> Street Address </th><td> {{ $company->street_address }} </td></tr><tr><th> Suburb </th><td> {{ $company->suburb }} </td></tr><tr><th> Postcode </th><td> {{ $company->postcode }} </td></tr><tr><th> Country </th><td> {{ get_country_name_by_id($company->country) }} </td></tr><tr><th> Phone </th><td> {{ '+'.$company->phone_prefix.' '.$company->phone }} </td></tr><tr><th> Email </th><td> {{ $company->email }} </td></tr><tr><th> Subscription Type </th><td> {{ $company->subscription_type }} </td></tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
