<div class="form-group{{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('company_logo') ? 'has-error' : ''}}">
    {!! Form::label('company_logo', 'Upload Company Logo', ['class' => 'control-label']) !!}
    {!! Form::file('company_logo', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required', 'multiple' => true] : ['class' => 'form-control', 'multiple' => true]) !!}
    {!! $errors->first('company_logo', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('street_address') ? 'has-error' : ''}}">
    {!! Form::label('street_address', 'Street Address', ['class' => 'control-label']) !!}
    {!! Form::textarea('street_address', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required', 'rows' => 4] : ['class' => 'form-control', 'rows' => 4]) !!}
    {!! $errors->first('street_address', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('suburb') ? 'has-error' : ''}}">
    {!! Form::label('suburb', 'Suburb', ['class' => 'control-label']) !!}
    {!! Form::text('suburb', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('suburb', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('postcode') ? 'has-error' : ''}}">
    {!! Form::label('postcode', 'Postcode', ['class' => 'control-label']) !!}
    {!! Form::text('postcode', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('postcode', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('city') ? 'has-error' : ''}}">
    {!! Form::label('city', 'City', ['class' => 'control-label']) !!}
    {!! Form::text('city', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('city', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('country') ? 'has-error' : ''}}">
    {!! Form::label('country', 'Country', ['class' => 'control-label']) !!}
    {!! Form::select('country', get_country_list(), null, ['class' => 'form-control','size' => '6']) !!}
    {!! $errors->first('country', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('phone_prefix') ? 'has-error' : ''}}">
    {!! Form::label('phone_prefix', 'Phone Prefix', ['class' => 'control-label']) !!}
    <div class="input-group">
    <span class="input-group-addon">
        <i class="fa fa-plus" aria-hidden="true"></i>
    </span>
    {!! Form::select('phone_prefix', get_phone_prefix_list(), null, ['class' => 'form-control']) !!}
    {!! $errors->first('phone_prefix', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('phone') ? 'has-error' : ''}}">
    {!! Form::label('phone', 'Phone', ['class' => 'control-label']) !!}
    {!! Form::number('phone', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('email') ? 'has-error' : ''}}">
    {!! Form::label('email', 'Email', ['class' => 'control-label']) !!}
    {!! Form::email('email', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>
<!--<div class="form-group{{ $errors->has('user_id') ? 'has-error' : ''}}">
    {!! Form::label('user_id', 'Users', ['class' => 'control-label']) !!}
    {!! Form::select('user_id', get_users_list(), null, ['class' => 'form-control']) !!}
    {!! $errors->first('user_id', '<p class="help-block">:message</p>') !!}
</div>-->
<div class="form-group{{ $errors->has('subscription_type') ? 'has-error' : ''}}">
    {!! Form::label('subscription_type', 'Subscription Type', ['class' => 'control-label']) !!}
    {!! Form::select('subscription_type', array('Basic' => 'Basic', 'Wholesale' => 'Wholesale'), 'Basic',['class' => 'form-control']) !!}

    {!! $errors->first('subscription_type', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('active') ? 'has-error' : ''}}">
    {!! Form::label('active', 'Active', ['class' => 'control-label']) !!}
    {!! Form::select('active', array('0' => 'Yes', '1' => 'No'), '0',['class' => 'form-control']) !!}
    {!! $errors->first('active', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
