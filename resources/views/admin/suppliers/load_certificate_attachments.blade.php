<table class="table table-bordered table-responsive" id="tbl_certificate_attachments">
    <thead>
        <tr>
            <th>#</th>
            <th>Notes</th>
            <th>Preview</th>
            <th>File name</th>
            <th>Date/Time</th>
            <th><i class="fa fa-cogs"></i></th>
        </tr>
    </thead>
    <tbody id="tbl_certificate_attachments_body">
        <?php $i = '1'; //dd($certificate_attachments->toArray()); ?>
        @foreach($certificate_attachments as $details)
        <tr id="{{$details->id}}">

            <td><span class="sn"></span><?= $i ?>.</td>
    <input type="hidden" value="{{$details->id}}" class="attachment_id" name="certificate_attachments[<?= $i ?>][row_id]" />
    <?php
//    $audits = ['1' => 'BSCI', '2' => 'SEDEX', '3' => 'SEMTA', '4' => 'other'];
//    $audit_name = '';
//    $audit_array = array('1','2','3');
//  code for checking value is 1,2,3 if 4 then audit_name if none then simply assign ethical_audit
//    if ($details->ethical_audit == '4') {
//        $audit_name = $details->audit_name;
//    } else if(in_array($details->ethical_audit, $audit_array)){
//        foreach ($audits as $key => $value):
//        if ($details->ethical_audit == $key):
//        $audit_name = $value;
//        endif;
//        endforeach;
//    } else{
//       $audit_name = $details->ethical_audit; 
//    }
    ?>
    <td>
        <input type="text" class="form-control supplier_certifications" name="certificate_name" value="{{$details->notes}}"/>
    </td> 
    <td>
        <a href="{{getFile($details->file_name)}}" target="_blank">View</a>
    </td>
    <td>
        <input type="text" class="form-control certificate_attachments" name="file_name" value="{{$details->file_name}}" disabled=""/>
    </td>
    <td>{{$details->created_at}}</td>
    <td><a class="btn btn-xs btn-danger certificate_attachments_delete" style="color:white;" data-id="<?= $i ?>" onclick="deleteOrderDetail('certificate_attachments',{{$details->id}});"><i class="fa fa-trash"></i></a>
    </td>
</tr>
<?php $i++; ?>
@endforeach
</tbody>
</table>