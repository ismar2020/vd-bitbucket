
<div class="card-body">
    <!--    <a href="{{ url('/admin/products/create?supplierid='.request()->segment(3)) }}" class="btn btn-success btn-sm" title="Add New Supplier Product">
            <i class="fa fa-plus" aria-hidden="true"></i> Add New Supplier's Product
        </a>-->
    <!--                            <a href="{{ url(url()->previous()) }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>-->

    <!--    {!! Form::open(['method' => 'GET', 'url' => '/admin/products', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
        <div class="input-group">
            <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
            <span class="input-group-append">
                <button class="btn btn-secondary" type="submit">
                    <i class="fa fa-search"></i>
                </button>
            </span>
        </div>
        {!! Form::close() !!}-->

    <br/>
    <br/>
    <div class="table-responsive">
        <table class="table table-borderless">
            <thead>
                <tr>
                    <th>#</th><th>Name</th><th>Category</th><th>Type</th><th>Image</th><th>Active</th><th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($supplierproducts as $item)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $item->name }}</td><td>{{ get_category_name_by_id($item->category_id) }}</td><td>{{ get_product_type_name_by_id($item->type_id) }}</td>
                    <td>
                        <?php if ($item->pic_id != null) { ?>
                            <img width="60" src="<?= getFile($item->pic_id); ?>" style="margin-bottom: 4px;">
                        <?php } else { ?>
                            <img width="60" src="{{ url('images/no_image.png') }}" style="margin-bottom: 4px;">
                        <?php } ?>
                    </td>
                    <td><?= ($item->active == '0') ? 'Yes' : 'No' ?></td>
                    <td>
<!--                                            <a href="{{ url('/admin/suppliersById/' . $item->id) }}" title="View Suppliers"><button class="btn btn-info btn-sm"><i class="fa fa-industry" aria-hidden="true"></i></button></a>-->
                        <a href="{{ url('/admin/products/' . $item->id) }}" title="View Product" style="display:none;"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                        <!--<a href="{{ url('/admin/products/' . $item->id . '/edit?supplierid='.request()->segment(3)) }}" title="Edit Product"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a> -->

                        <a   class="btn btn-danger btn-sm" href = "#" onclick="deleteData(<?= $item->id ?>, 'SupplierProduct', 'getProducts',<?= $supplier_id ?>)"> <i class="fa fa-trash-o" aria-hidden="true"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

</div>
