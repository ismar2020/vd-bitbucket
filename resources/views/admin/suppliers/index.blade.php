@extends('layouts.backend')

@section('content')
<div class="container">
    <div class="row">
        @include('admin.sidebar')

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Suppliers</div>
                <div class="card-body">
                    <?php if (Auth::check() && Auth::user()->hasRole('super_admin') || \Auth::user()->roles->first()->name == 'company_user') { ?>
                        <a href="{{ url('/admin/suppliers/create') }}" class="btn btn-success btn-sm" title="Add New Supplier">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
                        <br/>
                        <br/>
                    <?php } ?>
                    <div class="table-responsive suppliers_table_mobile">
                        <table class="table table-borderless">
                            <thead>
                                <tr>
                                    <th>#</th><th>Name</th><th>Business Card Front</th><th>Business Card Back</th><th>Contact</th><th>Product</th><th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($suppliers as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>
                                        <?php if ($item->picture_card_front != null) { ?>
                                            <img width="60" src="{{ getFile($item->picture_card_front) }}" style="margin-bottom: 4px;">
                                        <?php } else { ?>
                                            <img width="60" src="{{ url('images/no_image.png') }}" style="margin-bottom: 4px;">
                                        <?php } ?>
                                    </td>
                                    <td>
                                        <?php if ($item->picture_card_back != null) { ?>
                                            <img width="60" src="{{ getFile($item->picture_card_back) }}" style="margin-bottom: 4px;">
                                        <?php } else { ?>
                                            <img width="60" src="{{ url('images/no_image.png') }}" style="margin-bottom: 4px;">
                                        <?php } ?>
                                    </td>

                                    <td>
                                        <a href="{{ url('admin/viewsuppliercontacts',$item->id) }}" class="btn btn-info btn-sm">View Contacts ({!! get_supplier_contacts_count($item->id) !!})</a>
                                    </td>
                                    <td>
                                        <a href="{{ url('admin/viewsupplierproducts',$item->id) }}" class="btn btn-info btn-sm">View products ({!! get_supplier_products_count($item->id) !!})</a>
                                    </td>
                                    <td>
                                        <?php if (Auth::check() && Auth::user()->hasRole('super_admin')) { ?>
                                            <a href="{{ url('/admin/suppliers/' . $item->id) }}" title="View Supplier" style="display:none;"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                        <?php } ?>
                                        <a href="{{ url('/admin/suppliers/' . $item->id . '/edit') }}" title="Edit Supplier"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                        <?php if (Auth::check() && (Auth::user()->hasRole('super_admin') || Auth::user()->hasRole('company_user'))) { ?>
                                            {!! Form::open([
                                            'method' => 'DELETE',
                                            'url' => ['/admin/suppliers', $item->id],
                                            'style' => 'display:inline'
                                            ]) !!}
                                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                            'type' => 'submit',
                                            'type' => 'submit',
'class' => 'btn btn-danger btn-sm deleteentry',
                                            'title' => 'Delete Supplier'  
                                            )) !!}
                                            {!! Form::close() !!}
                                        <?php } ?>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.table').dataTable({
            "iDisplayLength": 50
        });
    })
</script>
@endsection
