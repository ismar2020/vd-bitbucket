<div class="form-group{{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('street_address') ? 'has-error' : ''}}">
    {!! Form::label('street_address', 'Street Address', ['class' => 'control-label']) !!}
    {!! Form::textarea('street_address', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required', 'rows' => 4] : ['class' => 'form-control', 'rows' => 4]) !!}
    {!! $errors->first('street_address', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('suburb') ? 'has-error' : ''}}">
    {!! Form::label('suburb', 'Suburb', ['class' => 'control-label']) !!}
    {!! Form::text('suburb', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('suburb', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('postcode') ? 'has-error' : ''}}">
    {!! Form::label('postcode', 'Postcode', ['class' => 'control-label']) !!}
    {!! Form::text('postcode', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('postcode', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('city') ? 'has-error' : ''}}">
    {!! Form::label('city', 'City', ['class' => 'control-label']) !!}
    {!! Form::text('city', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('city', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('country') ? 'has-error' : ''}}">
    {!! Form::label('country', 'Country', ['class' => 'control-label']) !!}
    {!! Form::select('country', get_country_list(), null, ['class' => 'form-control']) !!}
    {!! $errors->first('country', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('phone_prefix') ? 'has-error' : ''}}">
    {!! Form::label('phone_prefix', 'Phone Prefix', ['class' => 'control-label']) !!}
    <div class="input-group">
    <span class="input-group-addon">
        <i class="fa fa-plus" aria-hidden="true"></i>
    </span>
    {!! Form::select('phone_prefix', get_phone_prefix_list(), null, ['class' => 'form-control']) !!}
    {!! $errors->first('phone_prefix', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('phone') ? 'has-error' : ''}}">
    {!! Form::label('phone', 'Phone', ['class' => 'control-label']) !!}
    {!! Form::number('phone', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('email') ? 'has-error' : ''}}">
    {!! Form::label('email', 'Email', ['class' => 'control-label']) !!}
    {!! Form::email('email', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('picture_company') ? 'has-error' : ''}}">
    {!! Form::label('picture_company', 'Picture Company', ['class' => 'control-label']) !!}
    {!! Form::file('picture_company', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('picture_company', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('picture_card_front') ? 'has-error' : ''}}">
    {!! Form::label('picture_card_front', 'Picture Card Front', ['class' => 'control-label']) !!}
    {!! Form::file('picture_card_front', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('picture_card_front', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('picture_card_back') ? 'has-error' : ''}}">
    {!! Form::label('picture_card_back', 'Picture Card Back', ['class' => 'control-label']) !!}
    {!! Form::file('picture_card_back', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('picture_card_back', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('contact_id') ? 'has-error' : ''}}">
    {!! Form::label('contact_id', 'Contact Name', ['class' => 'control-label']) !!}
{!! Form::select('contact_id', get_contacts_list() ?? '', null, ['class' => 'form-control']) !!}
    {!! $errors->first('contact_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('product_id') ? 'has-error' : ''}}">
    {!! Form::label('product_id', 'Product Name', ['class' => 'control-label']) !!}
    {!! Form::select('product_id', get_products_list() ?? '', null, ['class' => 'form-control']) !!}
    {!! $errors->first('product_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('active') ? 'has-error' : ''}}">
    {!! Form::label('active', 'Active', ['class' => 'control-label']) !!}
    {!! Form::select('active', array('0' => 'Yes', '1' => 'No'), '0',['class' => 'form-control']) !!}

    {!! $errors->first('active', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('scanned') ? 'has-error' : ''}}">
    {!! Form::label('scanned', 'Scanned', ['class' => 'control-label']) !!}
    {!! Form::select('scanned', array('Scanned' => 'Scanned', 'Manual' => 'Manual'), 'Manual',['class' => 'form-control']) !!}

    {!! $errors->first('scanned', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('verified') ? 'has-error' : ''}}">
    {!! Form::label('verified', 'Verified', ['class' => 'control-label']) !!}
    {!! Form::select('verified', array('0' => 'Yes', '1' => 'No', '2' => 'Pending'), '0',['class' => 'form-control']) !!}
    {!! $errors->first('verified', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
