<!-- -----------------------****************tab code**************************---------------- -->
<ul class="nav nav-tabs">
    <li><a data-toggle="tab" href="#supplier_home"  id="home_active">Supplier Home</a></li>
    <?php if ($formMode == 'edit') { ?>
        <li><a data-toggle="tab" href="#certifications">Certifications</a></li>
        <li><a data-toggle="tab" href="#supplier_contact">Contacts</a></li>
        <li><a data-toggle="tab" href="#supplier_product">Products</a></li>
    <?php } ?>
</ul>

<div class="tab-content suppliers_m25">
    <div id="supplier_home" class="tab-pane fade in active">
        <br>
        <!-- ********************supplier's contact ID****************************************-->
        <?php if (Request::exists('productid')) { ?><input type="hidden" name="productid" class="form-control productid" value="{{ Request::get('productid') }}"/><?php } ?>
        <!--******************************ends************************************************-->
        <div class="row">
            <div class="col-md-6 form-group{{ $errors->has('name') ? 'has-error' : ''}}">
                {!! Form::label('name', 'Supplier Name', ['class' => 'control-label']) !!}
                {!! Form::text('name', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="col-md-6 form-group{{ $errors->has('street_address') ? 'has-error' : ''}}">
                {!! Form::label('street_address', 'Street Address', ['class' => 'control-label']) !!}
                {!! Form::textarea('street_address', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required', 'rows' => 3] : ['class' => 'form-control', 'rows' => 3]) !!}
                {!! $errors->first('street_address', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-6  form-group{{ $errors->has('phone_prefix') ? 'has-error' : ''}}">
                {!! Form::label('phone_prefix', 'Phone Prefix', ['class' => 'control-label']) !!}
                <div class="input-group">
<!--                    <span class="input-group-addon">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                    </span>-->


                    <select class="form-control phone_prefix supplier_select2" name="phone_prefix" style="WIDTH: 100% !important;
    padding-left: 31px;"><?php $data = get_phone_prefix_list(); ?>
                        <option value="">Select Prefix</option>
                        @foreach($data as $key => $value)
                        @if($formMode == 'edit' && $supplier->phone_prefix == $key)
                        <option value="{{$key}}" selected>{{$value}}</option>
                        @else
                        <option value="{{$key}}">{{$value}}</option>
                        @endif
                        @endforeach
                    </select>
                    {!! $errors->first('phone_prefix', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="col-md-4 col-sm-3  form-group{{ $errors->has('phone') ? 'has-error' : ''}}">
                {!! Form::label('phone', 'Phone', ['class' => 'control-label']) !!}
                {!! Form::number('phone', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="col-md-4 form-group{{ $errors->has('suburb') ? 'has-error' : ''}}">
                {!! Form::label('suburb', 'Suburb', ['class' => 'control-label']) !!}
                {!! Form::text('suburb', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                {!! $errors->first('suburb', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-sm-3   form-group{{ $errors->has('email') ? 'has-error' : ''}}">
                {!! Form::label('email', 'Email', ['class' => 'control-label']) !!}
                {!! Form::email('email', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="col-md-6 form-group{{ $errors->has('postcode') ? 'has-error' : ''}}">
                {!! Form::label('postcode', 'Postcode', ['class' => 'control-label']) !!}
                {!! Form::text('postcode', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                {!! $errors->first('postcode', '<p class="help-block">:message</p>') !!}
            </div>

        </div>
        <div class="row">
            <div class="col-md-6 form-group">
                <div class="form-group{{ $errors->has('supplier_category_id') ? 'has-error' : ''}}">
                    {!! Form::label('supplier_category_id', 'Supplier Category', ['class' => 'control-label']) !!}
                    <select class="form-control supplier_category_id supplier_select2" name="supplier_category_id"><?php $data = get_supplier_category_list(); ?>
                        <option value="">Select Supplier Category</option>
                        @foreach($data as $key => $value)
                        @if($formMode == 'edit' && $supplier->supplier_category_id == $key)
                        <option value="{{$key}}" selected>{{$value}}</option>
                        @else
                        <option value="{{$key}}">{{$value}}</option>
                        @endif
                        @endforeach
                    </select>
                    {!! $errors->first('supplier_category_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="col-md-6 form-group{{ $errors->has('city') ? 'has-error' : ''}}">
                {!! Form::label('city', 'City', ['class' => 'control-label']) !!}
                {!! Form::text('city', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                {!! $errors->first('city', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 form-group{{ $errors->has('active') ? 'has-error' : ''}}">
                {!! Form::label('active', 'Active', ['class' => 'control-label']) !!}
                {!! Form::select('active', array('0' => 'Yes', '1' => 'No'), '0',['class' => 'form-control']) !!}

                {!! $errors->first('active', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="col-md-6 form-group{{ $errors->has('country') ? 'has-error' : ''}}">
                {!! Form::label('country', 'Country', ['class' => 'control-label']) !!}

                <select class="form-control country supplier_select2" name="country"><?php $data = get_country_list(); ?>
                    <option value="">Select Country</option>
                    @foreach($data as $key => $value)
                    @if($formMode == 'edit' && $supplier->country == $key)
                    <option value="{{$key}}" selected>{{$value}}</option>
                    @else
                    <option value="{{$key}}">{{$value}}</option>
                    @endif
                    @endforeach
                </select>

                {!! $errors->first('country', '<p class="help-block">:message</p>') !!}
            </div>
        </div>


        <div class="form-group{{ $errors->has('picture_company') ? 'has-error' : ''}}" style="display: none;">
            {!! Form::label('picture_company', 'Picture Company', ['class' => 'control-label']) !!}
            {!! Form::file('picture_company', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
            {!! $errors->first('picture_company', '<p class="help-block">:message</p>') !!}
        </div>
        <?php
        if ($formMode == 'create') {
            $image = 'images/no_image.png';
            $backimage = 'images/no_image.png';
        } else if ($formMode == 'edit') {
            if ($supplier->picture_card_front != null) {
                $image = getFile($supplier->picture_card_front);
            } else {
                $image = 'images/no_image.png';
            }
            if ($supplier->picture_card_back != null) {
                $backimage = getFile($supplier->picture_card_back);
            } else {
                $backimage = 'images/no_image.png';
            }
        }
        ?>
        <div class="row">
            <div class="col-md-6 form-group{{ $errors->has('picture_card_front') ? 'has-error' : ''}}">
                {!! Form::label('picture_card_front', 'Business Card - Front', ['class' => 'control-label']) !!}<br>

                <input type="file" class="hidden_quote_image_input_div edit_picture_card_front" name="picture_card_front" onchange="showImage(this, 'showPictureFront');"/>
                <img src="{{url("$image")}}" width="50" class="showPictureFront"/>
                <div class = "edit_delete">
                    <a href="javascript:void(0)"><span class="fa fa-pencil edit_pencil" aria-hidden="true" onclick = "uploadImageDynamically('edit_picture_card_front')"></span></a>


                    <a href="javascript:void(0)" <?php if($formMode == 'edit'){ ?> onclick="removeImageFromTable(<?=$supplier->id?>, 'picture_card_front', 'suppliers', 'showPictureFront');" <?php } ?>><span class="fa fa-times remove_image" aria-hidden="true"></span></a>
                </div>
                {!! $errors->first('picture_card_front', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="col-md-6 form-group{{ $errors->has('picture_card_back') ? 'has-error' : ''}}">
                {!! Form::label('picture_card_back', 'Business Card - Back', ['class' => 'control-label']) !!}<br>

                <input type="file" class="hidden_quote_image_input_div edit_picture_card_back" name="picture_card_back" onchange="showImage(this, 'showPictureBack');"/>
                <img src="{{url("$backimage")}}" width="50" class="showPictureBack"/>
                <div class = "edit_delete">
                    <a href="javascript:void(0)"><span class="fa fa-pencil edit_pencil" aria-hidden="true" onclick = "uploadImageDynamically('edit_picture_card_back')"></span></a>


                    <a href="javascript:void(0)"  <?php if($formMode == 'edit'){ ?> onclick="removeImageFromTable(<?=$supplier->id?>, 'picture_card_back', 'suppliers', 'showPictureBack');" <?php } ?>><span class="fa fa-times remove_image" aria-hidden="true"></span></a>
                </div>

                {!! $errors->first('picture_card_back', '<p class="help-block">:message</p>') !!}
            </div>
        </div>


        <div class="row" style="display: none;">
            <div class="col-md-6 form-group{{ $errors->has('scanned') ? 'has-error' : ''}}">
                {!! Form::label('scanned', 'Scanned', ['class' => 'control-label']) !!}
                {!! Form::select('scanned', array('Scanned' => 'Scanned', 'Manual' => 'Manual'), 'Manual',['class' => 'form-control']) !!}

                {!! $errors->first('scanned', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="col-md-6 form-group{{ $errors->has('verified') ? 'has-error' : ''}}">
                {!! Form::label('verified', 'Verified', ['class' => 'control-label']) !!}
                {!! Form::select('verified', array('0' => 'Yes', '1' => 'No', '2' => 'Pending'), '0',['class' => 'form-control']) !!}
                {!! $errors->first('verified', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary createentry']) !!}
        </div>

    </div>
    <?php if ($formMode == 'edit') { ?>
        <div id="certifications" class="tab-pane fade">
            <div class="container"><br>



                <?php
//                $ethical_audit = ['1' => 'BSCI', '2' => 'SEDEX', '3' => 'SEMTA', '4' => 'Other'];
                ?>
                <div class="row form-group{{ $errors->has('ethical_audit') ? 'has-error' : ''}}">
                    {!! Form::label('ethical_audit', 'Ethical Audit', ['class' => 'control-label col-md-4']) !!}

                    <select class="form-control  col-md-4 ethical_audit ethical_audit_ajax certification_ajax" name="ethical_audit">
                        <option value="">Select Ethical Audit</option>
                        @foreach(get_ethical_audits_list() as $key => $value)
                        @if($formMode == 'edit' && $supplier->ethical_audit == $key)
                        <option value="{{$key}}" selected>{{$value}}</option>
                        @else
                        <option value="{{$key}}">{{$value}}</option>
                        @endif
                        @endforeach
                    </select>
                    {!! $errors->first('ethical_audit', '<p class="help-block">:message</p>') !!}
                </div>

                <span class="audit_other_name_div" style="display :none;">
                    <div class="row form-group{{ $errors->has('audit_name') ? 'has-error' : ''}}">
                        {!! Form::label('audit_name', 'Audit Name (if other)', ['class' => 'control-label col-md-4']) !!}
                        {!! Form::text('audit_name', null, ['class' => 'form-control col-md-4 audit_name_ajax certification_ajax']) !!}
                        {!! $errors->first('audit_name', '<p class="help-block">:message</p>') !!}
                    </div>
                </span>
                <div class="row form-group{{ $errors->has('certificate_comment') ? 'has-error' : ''}}">
                    {!! Form::label('certificate_comment', 'Comments', ['class' => 'control-label col-md-4']) !!}
                    {!! Form::textarea('certificate_comment', null, ['class' => 'form-control col-md-4 certificate_comment_ajax certification_ajax', 'rows' => '5']) !!}
                    {!! $errors->first('certificate_comment', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <br>
            <form name="certifications" action="javascript:void(0)" accept-charset="UTF-8" class="form-horizontal" id="supplier_certifications" enctype="multipart/form-data" style="overflow-y: hidden;">
                <input type="hidden" class="form-horizontal supplier_id" name="supplier_id" value="{{ $supplier->id }}"><br>
                <div id="dZUpload" class="dropzone">
                    <div class="dz-default dz-message"><span>Drop Attachment Here</span></div>
                    <meta name="csrf-token" content="{{ csrf_token() }}">

                </div><br>
                <div id="load_certificate_attachment_lines">

                </div>
            </form><br>
        </div>
    <?php } ?>
    <div id="supplier_contact" class="tab-pane fade">
        <div class="form-group{{ $errors->has('contact_id') ? 'has-error' : ''}}">
            <div class="supplercontactdiv">
                <div class="one">
                    <label>Link to Existing</label> &nbsp;
                    <select class="form-control supplier_contact supplier_select2" name="contact_id" style="width: max-content !IMPORTANT;"><?php $data = get_contacts_list(); ?>
                        <option value="">Select Contact</option>
                        @foreach($data as $key => $value)
                        <option value="{{$key}}">{{$value}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="two">
                    {!! Form::label('contact_id', 'Add new', ['class' => 'control-label']) !!}
                    <button type="button" class="btn btn-info btn-sm addContact" data-toggle="modal" data-target="#contactModalLongnew" style="margin:12px;"><i class="fa fa-plus" aria-hidden="true"></i>
                        Add Contact</button>
                </div>
            </div>

            </br>




            <?php if ($formMode === 'edit') { ?>
                <button class="btn btn-success btn-sm" id = "saveContactFromList"> Save</button><br><br>
                {!! $errors->first('contact_id', '<p class="help-block">:message</p>') !!}
                <input type="hidden" class="supplieridforajax" value="{{$supplier->id}}">
                <div class="suppliercontactdatabyajax"></div>
            <?php } elseif ($formMode === 'create') { ?>
                <button class="btn btn-success btn-sm" id = "saveContactFromListOnCreate"> Click to Add</button><br><br>
                {!! $errors->first('contact_id', '<p class="help-block">:message</p>') !!}
                <div class="table-responsive">

                    <table class="table table-borderless supplierContact">
                        <thead>
                            <tr>
                                <th>#</th><th>Name</th><th>Phone</th><th>Email</th><th>Actions</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            <?php } ?>
        </div>
    </div>
    <div id="supplier_product" class="tab-pane fade">
        <div class="form-group{{ $errors->has('product_id') ? 'has-error' : ''}}">
            <div class="supplercontactdiv">
                <div class="one">
                    <label>Link to Existing</label>
                    <select class="form-control supplier_product supplier_select2" name="product_id" style="width: max-content !IMPORTANT;"><?php $data = get_products_list(); ?>
                        <option value="">Select Product</option>
                        @foreach($data as $key => $value)
                        <option value="{{$key}}">{{$value}}</option>
                        @endforeach
                    </select>

                    {!! $errors->first('product_id', '<p class="help-block">:message</p>') !!}
                </div>

                <div class="two">
                    {!! Form::label('product_id', 'Add new', ['class' => 'control-label']) !!}
                    <button type="button" class="btn btn-info btn-sm addProduct" data-toggle="modal" data-target="#exampleModalLongnew"><i class="fa fa-plus" aria-hidden="true"></i>
                        Add Product
                    </button>
                </div>
            </div>

            <?php if ($formMode === 'edit') { ?>
                <button class="btn btn-success btn-sm" id = "saveProductFromList"> Save</button><br><br>
                <input type="hidden" class="supplieridforajax" value="{{$supplier->id}}">
                <div class="supplierproductdatabyajax"></div>
            <?php } elseif ($formMode === 'create') { ?>
                <button class="btn btn-success btn-sm" id = "saveProductFromListOnCreate"> Click to Add</button><br><br>
                <div class="table-responsive">

                    <table class="table table-borderless supplierProduct">
                        <thead>
                            <tr>
                                <th>#</th><th>Name</th><th>length</th><th>Width</th><th>height</th><th>Actions</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<!-- --------------------****************tab code ends**************************------ -->

