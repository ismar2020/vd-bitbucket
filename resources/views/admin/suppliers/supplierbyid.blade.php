<div class="table-responsive">
    <table class="table table-borderless">
        <thead>
            <tr>
                <th>#</th><th>Name</th><th>Supplier</th><th>Card (Front,Back)</th><th>Contact</th><th>Product</th><th>Actions</th>
            </tr>
        </thead>
        <tbody>
        
        @foreach($suppliers as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $item->name }}</td>
            <td>
                <?php if ($item->picture_company != null) { ?>
                    <img width="60" src="<?= url($item->picture_company); ?>" style="margin-bottom: 4px;">
                <?php } else { ?>
                    <img width="60" src="{{ url('images/no_image.png') }}" style="margin-bottom: 4px;">
                <?php } ?>
            </td>
            <td>
                <?php if ($item->picture_card_front != null) { ?>
                    <img width="60" src="<?= url($item->picture_card_front); ?>" style="margin-bottom: 4px;">
                <?php } else { ?>
                    <img width="60" src="{{ url('images/no_image.png') }}" style="margin-bottom: 4px;">
                <?php } ?>
                <?php if ($item->picture_card_back != null) { ?>
                    <img width="60" src="<?= url($item->picture_card_back); ?>" style="margin-bottom: 4px;">
                <?php } else { ?>
                    <img width="60" src="{{ url('images/no_image.png') }}" style="margin-bottom: 4px;">
                <?php } ?>
            </td>

            <td>
                <a href="{{ url('admin/viewsuppliercontacts',$item->id) }}" class="btn btn-info btn-sm">View Contacts ({!! get_supplier_contacts_count($item->id) !!})</a>
            </td>
            <td>
                <a href="{{ url('admin/viewsupplierproducts',$item->id) }}" class="btn btn-info btn-sm">View products ({!! get_supplier_products_count($item->id) !!})</a>
            </td>
            <td>
                <a href="{{ url('/admin/suppliers/' . $item->id) }}" title="View Supplier" style="display:none;"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                <a href="{{ url('/admin/suppliers/' . $item->id . '/edit') }}" title="Edit Supplier"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                {!! Form::open([
                'method' => 'DELETE',
                'url' => ['/admin/suppliers', $item->id],
                'style' => 'display:inline'
                ]) !!}
                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                'type' => 'submit',
                'type' => 'submit',
'class' => 'btn btn-danger btn-sm deleteentry',
                'title' => 'Delete Supplier',
                'onclick'=>'return confirm("Confirm delete?")'
                )) !!}
                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
        </tbody>

    </table>
</div>
</div>



