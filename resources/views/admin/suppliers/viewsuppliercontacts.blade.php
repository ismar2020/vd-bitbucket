@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Supplier's Contacts</div>
                    <div class="card-body">
                        <a href="{{ url('/admin/contacts/create?supplierid='.request()->segment(3)) }}" class="btn btn-success btn-sm" title="Add New Supplier Contact">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New Supplier's Contact
                        </a>
<!--                        <a href="{{ url(url()->previous()) }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a> -->
                        <br /><br />
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Name</th><th>Phone</th><th>Picture Supplier</th><th>Picture Card</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($suppliercontacts as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->first_name.' '.$item->surname }}</td><td>{{ $item->phone }}</td>
                                        <td>
                                            <?php if($item->picture_supplier != null) { ?>
                                            <img width="60" src="<?= getFile($item->picture_supplier); ?>" style="margin-bottom: 4px;">
                                            <?php }else{ ?>
                                            <img width="60" src="{{ url('images/no_image.png') }}" style="margin-bottom: 4px;">
                                            <?php } ?>
                                        </td>
                                        <td>
                                            <?php if($item->picture_card != null) { ?>
                                            <img width="60" src="<?= getFile($item->picture_card); ?>" style="margin-bottom: 4px;">
                                            <?php }else{ ?>
                                            <img width="60" src="{{ url('images/no_image.png') }}" style="margin-bottom: 4px;">
                                            <?php } ?>
                                        </td>
                                       
                                        <td>
                                            <a href="{{ url('/admin/contacts/' . $item->id) }}" title="View Contact" style="display:none;"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                            <a href="{{ url('/admin/contacts/' . $item->id . '/edit?supplierid='.request()->segment(3)) }}" title="Edit Contact"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                            {!! Form::open([
                                                'method' => 'DELETE',
                                                'url' => ['/admin/contacts', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                             <?php //if (request()->route('id')) { ?><input type="hidden" name="product_contact_id" class="form-control product_contact_id" value="{{ Request::segment(3) }}"/><?php //} ?>
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                        'type' => 'submit',
                                                        'type' => 'submit',
'class' => 'btn btn-danger btn-sm deleteentry',
                                                        'title' => 'Delete Contact'   
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
