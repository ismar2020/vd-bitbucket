@extends('layouts.backend')

@section('content')
<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">-->
<div class="container select2_max_content">
    <div class="row">
        @include('admin.sidebar')

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Create New Supplier</div>
                <div class="card-body">
<!--                        <a href="{{ url('/admin/suppliers') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>-->

                    <!--                        <br />
                                            <br />-->

                    @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    @endif

                    {!! Form::open(['url' => '/admin/suppliers', 'class' => 'form-horizontal', 'files' => true]) !!}

                    @include ('admin.suppliers.form', ['formMode' => 'create'])

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="contactModalLongnew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Add Contact</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="javascript:void(0)" accept-charset="UTF-8" class="form-horizontal" id="addContactForm" enctype="multipart/form-data">
                    @csrf

                    @include ('admin.contacts.ajaxform', ['formMode' => 'create'])
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" id="modalCloseContact" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModalLongnew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Add Supplier Product</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="javascript:void(0)" accept-charset="UTF-8" class="form-horizontal" id="addSupplierProductForm" enctype="multipart/form-data">
                    @csrf

                    @include ('admin.products.form', ['formMode' => 'create'])
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" id="modalCloseProduct" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>




<script>
    
    $('#addSupplierProductForm').on('submit', function (e) {

        e.preventDefault();

        var form_data = new FormData(this);

        form_data.append("_token", $('meta[name="csrf-token"]').attr('content'));
        $.ajax({
            url: "{{url('admin/addproduct')}}",
            method: "POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                //                        Swal.showLoading();
            },
            onError: function () {
                swal("Error!", 'Something Went Wrong', "error");
            },
            success: function (response)
            {

                if (response) {
                    if (response.status) {
                        $('#product_id').html(response.product_html);
                        $('#Product_id').html(response.product_html);
                        $('#modalCloseProduct').trigger('click');
                        swal("Good job!", "Product Added!", "success");
                        $('.supplierProduct tr:last').after('<tr><td>' + response.product.id + '<input type = "hidden" name = "product_array[]" value = "' + response.product.id + '"></td><td>' + response.product.name + '</td><td>' + response.product.product_length + '</td><td>' + response.product.product_width + '</td><td>' + response.product.product_height + '</td><td><a   class="btn btn-danger btn-sm deleteRow" href = "#"> <i class="fa fa-trash-o" aria-hidden="true"></i></a></td></tr>');
//                        getProducts();
                    }
                }
            }
        });
    });



    $('#saveProductFromListOnCreate').on('click', function (e) {

        e.preventDefault();

        var product_id = $('#product_id').val();

        $.ajax({
            url: "{{url('admin/getProductRowByProductId')}}",
            method: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {product_id: product_id},

            beforeSend: function () {
                //                        Swal.showLoading();
            },
            success: function (response)
            {
                swal("Good job!", "Product Added!", "success");
                $('.supplierProduct tr:last').after(response.product_html);
            }
        });

    });

    $('#addContactForm').on('submit', function (e) {

        e.preventDefault();

        var form_data = new FormData(this);

        form_data.append("_token", $('meta[name="csrf-token"]').attr('content'));
        $.ajax({
            url: "{{url('admin/addcontact')}}",
            method: "POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                //                        Swal.showLoading();
            },
            onError: function () {
                swal("Error!", 'Something Went Wrong', "error");
            },
            success: function (response)
            {
                $('#contact_id').html(response.contact_html);
                $('#modalCloseContact').trigger('click');
                swal("Good job!", "Contact Added!", "success");
                $('.supplierContact tr:last').after('<tr><td>' + response.contact.id + '<input type = "hidden" name = "contact_array[]" value = "' + response.contact.id + '"></td><td>' + response.contact.first_name + '</td><td>' + response.contact.phone + '</td><td>' + response.contact.email + '</td><td><a   class="btn btn-danger btn-sm deleteRow" href = "#"> <i class="fa fa-trash-o" aria-hidden="true"></i></a></td></tr>');

                // getContacts();


            }
        });
    });

    $('#saveContactFromListOnCreate').on('click', function (e) {

        e.preventDefault();

        var contact_id = $('#contact_id').val();

        $.ajax({
            url: "{{url('admin/getContactRowByContactId')}}",
            method: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {contact_id: contact_id},

            beforeSend: function () {
                //                        Swal.showLoading();
            },
            success: function (response)
            {
                swal("Good job!", "Contact Added!", "success");
                $('.supplierContact tr:last').after(response.contact_html);
            }
        });

    });

</script>
@endsection
