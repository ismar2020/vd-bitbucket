@extends('layouts.backend')

@section('content')
<div class="container select2_max_content">
    <div class="row">
        @include('admin.sidebar')

        <div class="col-md-12">
            <div class="card">
                
                <!--<div class="card-header">Edit Supplier {{ get_name_by_id_and_table_name('suppliers', $supplier->id, 'name') }}</div>-->
                
                <div class="card-header">Edit Supplier #{{ $supplier->id }}</div>
                
                
                <div class="card-body">
<!--                        <a href="{{ url('/admin/suppliers') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                    <br />
                    <br />-->

                    @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    @endif

                    {!! Form::model($supplier, [
                    'method' => 'PATCH',
                    'url' => ['/admin/suppliers', $supplier->id],
                    'class' => 'form-horizontal',
                    'files' => true
                    ]) !!}

                    @include ('admin.suppliers.form', ['formMode' => 'edit'])

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade supplier_contact_modal_in_supplier" id="contactModalLongnew" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Add Contact</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="javascript:void(0)" accept-charset="UTF-8" class="form-horizontal" id="addContactForm" enctype="multipart/form-data">
                    @csrf

                    <input type="hidden" value="{{ $supplier->id }}" name = "supplierId">
                    @include ('admin.contacts.form', ['formMode' => 'create'])
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" id="modalCloseContact" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade supplier_product_modal_in_supplier" id="exampleModalLongnew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Add Supplier Product</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="javascript:void(0)" accept-charset="UTF-8" class="form-horizontal" id="addProductForm" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" value="{{ $supplier->id }}" name = "supplierId">
                    @include ('admin.products.form', ['formMode' => 'create'])
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" id="modalCloseProduct" class="modalCloseProduct_u" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>




<script>
    $(document).ready(function () {
        Dropzone.autoDiscover = false;
        var url = "<?= url('admin/dropzoneCertificateAttachment?supplier_id=' . $supplier->id . '') ?>";
        $("#dZUpload").dropzone({
            url: url,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method: "POST",
            maxFilesize: 15,
            addRemoveLinks: true,
            success: function (file, response) {
                console.log(response);
                var imgName = response;
                file.previewElement.classList.add("dz-success");
                console.log("Successfully uploaded :" + imgName);
                load_certificate_attachments_lines();
            },
            error: function (file, response) {
                file.previewElement.classList.add("dz-error");
            }
        });
    });
    load_certificate_attachments_lines();
    function load_certificate_attachments_lines() {
        var supplier_id = '<?= $supplier->id ?>';

        $.ajax({
            url: "{{url('admin/load_certificate_attachments_lines')}}",
            method: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {supplier_id: supplier_id},
            beforeSend: function () {
                //                        Swal.showLoading();
            },
            onError: function () {
                swal("Error!", 'Something Went Wrong', "error");
            },
            success: function (response)
            {
                $('#load_certificate_attachment_lines').html(response);

            }
        });
    }

    $(document).on('change click keyup', '.supplier_certifications', function () {
        var notes = $(this).val();
        var rowId = $(this).closest('tr').attr('id');
        var currentRow = $("#" + rowId);
        var attachment_id = currentRow.find('.attachment_id').val();
        var supplier_id = '<?= $supplier->id ?>';
//        console.log(ethical_audit,certificate_id,supplier_id);

        $.ajax({
            url: "{{url('admin/submit_supplier_certifications')}}",
            method: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {notes: notes, attachment_id: attachment_id, supplier_id: supplier_id},
            beforeSend: function () {
                //                        Swal.showLoading();
            },
            onError: function () {
                swal("Error!", 'Something Went Wrong', "error");
            },
            success: function (response)
            {
                if (response) {
                    if (response.status) {
//                        load_certificate_attachments_lines();
                        console.log('Note saved');
                    }
                }

            }
        });
    });
    getContacts();
    getProducts();
    function getContacts() {
        var supplier_id = '{{ $supplier->id }}';

        $.ajax({
            url: "{{url('admin/getSupplierConatcts')}}",
            method: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {supplier_id: supplier_id},

            beforeSend: function () {
                //                        Swal.showLoading();
            },
            success: function (response)
            {

                $('.suppliercontactdatabyajax').html(response);
            }
        });

    }
    function getProducts() {
        var supplier_id = '{{ $supplier->id }}';
//        alert(supplier_id);
        $.ajax({
            url: "{{url('admin/getSupplierProducts')}}",
            method: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {supplier_id: supplier_id},

            beforeSend: function () {
                //                        Swal.showLoading();
            },
            success: function (response)
            {

                $('.supplierproductdatabyajax').html(response);
            }
        });

    }


    $('#addContactForm').on('submit', function (e) {

        e.preventDefault();

        var form_data = new FormData(this);

        form_data.append("_token", $('meta[name="csrf-token"]').attr('content'));
        $.ajax({
            url: "{{url('admin/addcontact')}}",
            method: "POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                //                        Swal.showLoading();
            },
            success: function (response)
            {

                if (response) {
                    if (response.status) {

                        $('#modalCloseContact').trigger('click');
                        swal("Good job!", "Contact Added!", "success");
                        getContacts();
                    } else {
                        swal("Error!", 'Something Went Wrong', "error");
                    }
                }
            }
        });
    });
    $(document).on('click select keyup', '.certification_ajax', function () {
        var supplier_id = '{{ $supplier->id }}';
        var ethical_audit = $('.ethical_audit_ajax').val();
        var audit_name = $('.audit_name_ajax').val();
        var certificate_comment = $('.certificate_comment_ajax').val();
//        alert(supplier_id);
        $.ajax({
            url: "{{url('admin/submit_certification_fields')}}",
            method: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {supplier_id: supplier_id, ethical_audit:ethical_audit, audit_name:audit_name, certificate_comment:certificate_comment},

            beforeSend: function () {
                //                        Swal.showLoading();
            },
            success: function (response)
            {
                if (response) {
                    if (response.status) {
                        console.log('submit certification fields saved');
                    }
                }
            }
        });
    });
</script>
@endsection
