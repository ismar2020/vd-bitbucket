<div class="form-group{{ $errors->has('contact_id') ? 'has-error' : ''}}">
    {!! Form::label('contact_id', 'Contacts', ['class' => 'control-label']) !!}
    {!! Form::select('contact_id', get_contacts_list(), null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required','size' => '5'] : ['class' => 'form-control','size' => '5']) !!}
    {!! $errors->first('contact_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('product_id') ? 'has-error' : ''}}">
    {!! Form::label('product_id', 'Products', ['class' => 'control-label']) !!}
    {!! Form::select('product_id', get_products_list(), null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required','size' => '5'] : ['class' => 'form-control','size' => '5']) !!}
    {!! $errors->first('product_id', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
