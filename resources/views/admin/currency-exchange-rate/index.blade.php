@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Currency Exchange Rate</div>
                    <div class="card-body">
                        <a href="{{ url('/admin/currency-exchange-rate/create') }}" class="btn btn-success btn-sm" title="Add New CurrencyExchangeRate">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>

                        {!! Form::open(['method' => 'GET', 'url' => '/admin/currency-exchange-rate', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        <div class="table-responsive suppliers_table_mobile">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Currency From</th><th>Currency To</th><th>Exchange Rate</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($currencyexchangerate as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ get_currency_name_by_id($item->currency_id_from) }}</td><td>{{ get_currency_name_by_id($item->currency_id_to) }}</td><td>{{ $item->exchange_rate }}</td>
                                        <td>
                                            
                                            <a href="{{ url('/admin/currency-exchange-rate/' . $item->id . '/edit') }}" title="Edit CurrencyExchangeRate"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                            {!! Form::open([
                                                'method' => 'DELETE',
                                                'url' => ['/admin/currency-exchange-rate', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                        'type' => 'submit',
                                                        'type' => 'submit',
'class' => 'btn btn-danger btn-sm deleteentry',
                                                        'title' => 'Delete CurrencyExchangeRate'   
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                    
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
