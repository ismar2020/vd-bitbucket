<div class="form-group{{ $errors->has('currency_id_from') ? 'has-error' : ''}}">
    {!! Form::label('currency_id_from', 'Currency From', ['class' => 'control-label']) !!}
    {!! Form::select('currency_id_from', get_limited_currency_code_list() , null, ('' == 'required') ? ['class' => 'form-control select2', 'required' => 'required'] : ['class' => 'form-control select2']) !!}
    {!! $errors->first('currency_id_from', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('currency_id_to') ? 'has-error' : ''}}">
    {!! Form::label('currency_id_to', 'Currency To', ['class' => 'control-label']) !!}
    {!! Form::select('currency_id_to', get_limited_currency_code_list(), null, ('required' == 'required') ? ['class' => 'form-control select2', 'required' => 'required'] : ['class' => 'form-control select2']) !!}
    {!! $errors->first('currency_id_to', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('exchange_rate') ? 'has-error' : ''}}">
    {!! Form::label('exchange_rate', 'Exchange Rate', ['class' => 'control-label']) !!}
    {!! Form::text('exchange_rate', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('exchange_rate', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
