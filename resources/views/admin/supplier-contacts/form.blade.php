<div class="form-group{{ $errors->has('supplier_id') ? 'has-error' : ''}}">
    {!! Form::label('supplier_id', 'Supplier', ['class' => 'control-label']) !!}
    {!! Form::select('supplier_id', get_suppliers_list() , null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required', 'size' => '5'] : ['class' => 'form-control','size' => '5']) !!}
    {!! $errors->first('supplier_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('contact_id') ? 'has-error' : ''}}">
    {!! Form::label('contact_id', 'Contact', ['class' => 'control-label']) !!}
    {!! Form::select('contact_id', get_contacts_list() , null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required', 'size' => '5'] : ['class' => 'form-control','size' => '5']) !!}
    {!! $errors->first('contact_id', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
