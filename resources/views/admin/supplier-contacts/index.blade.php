@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Supplier contacts</div>
                    <div class="card-body">
                        <a href="{{ url('/admin/supplier-contacts/create') }}" class="btn btn-success btn-sm" title="Add New SupplierContact">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>

                        {!! Form::open(['method' => 'GET', 'url' => '/admin/supplier-contacts', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Supplier Id</th><th>Contact Id</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($suppliercontacts as $item)
                                    <tr>
                                        <td>{{ $loop->iteration or $item->id }}</td>
                                        <td><?php $supplier_name = get_supplier_name_by_id($item->supplier_id); echo $supplier_name; ?>
                                        </td>
                                        <td><?php $contact_name = get_contact_name_by_id($item->contact_id); echo $contact_name; ?>
                                        </td>
                                        <td>
                                            <a href="{{ url('/admin/supplier-contacts/' . $item->id) }}" title="View SupplierContact"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                            <a href="{{ url('/admin/supplier-contacts/' . $item->id . '/edit') }}" title="Edit SupplierContact"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                            {!! Form::open([
                                                'method' => 'DELETE',
                                                'url' => ['/admin/supplier-contacts', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                        'type' => 'submit',
                                                        'type' => 'submit',
'class' => 'btn btn-danger btn-sm deleteentry',
                                                        'title' => 'Delete SupplierContact'   
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $suppliercontacts->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
