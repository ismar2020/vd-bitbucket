@extends('layouts.backend')

@section('content')
<div class="container">
    <div class="row">
        @include('admin.sidebar')

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Edit Landed Cost Template #{{ $fistemplate->id }}</div>
                <div class="card-body">
                    @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    @endif

                    {!! Form::model($fistemplate, [
                    'method' => 'PATCH',
                    'url' => ['/admin/fis-template', $fistemplate->id],
                    'class' => 'form-horizontal',
                    'files' => true
                    ]) !!}

                    @include ('admin.fis-template.form', ['formMode' => 'edit'])

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
<script>
    load_template_products();
    function load_template_products() {
        var template_id = '<?= $fistemplate->id ?>';

        $.ajax({
            url: "{{url('admin/load_template_products')}}",
            method: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {template_id: template_id},
            beforeSend: function () {
                //                        Swal.showLoading();
            },
            onError: function () {
                swal("Error!", 'Something Went Wrong', "error");
            },
            success: function (response)
            {
                $('#load_template_products').html(response);

            }
        });
    }
    function fis_template_product_relation_create() {
        var fistemplate_id = '<?= $fistemplate->id ?>';
        var product_id = $('.product_fis').val();
        $(".product_fis option[value='" + product_id + "']").remove();
        $.ajax({
            url: "{{url('admin/insert_fis_template_product_relation')}}",
            method: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {fistemplate_id: fistemplate_id, product_id: product_id},
            beforeSend: function () {
                //                        Swal.showLoading();
            },
            onError: function () {
                swal("Error!", 'Something Went Wrong', "error");
            },
            success: function (response)
            {
                load_template_products();
            }
        });
    }
    function set_default_template() { 
        
        var template_id = '<?= $fistemplate->id ?>';
        var is_default = $('#is_default').val();
        $.ajax({
            url: "{{url('admin/set_default_template')}}",
            method: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {template_id: template_id, is_default: is_default},
            beforeSend: function () {
                //                        Swal.showLoading();
            },
            onError: function () {
                swal("Error!", 'Something Went Wrong', "error");
            },
            success: function (response)
            {
                if(response.status == 'true'){
                    if(is_default == '0'){
                        swal("Success!", "Default Template unset", "success");
                    }else{
                        swal("Success!", "Default Template set", "success");
                    }
                    
                }
            }
        });
    }
    //delete relation data from template products
    function delete_template_products(table_name, template_id, product_id) {
        //alert('s');
        var didConfirm = confirm("Are you sure You want to delete this relation");
        if (didConfirm == true) {
            $.ajax({
                url: "{{url('admin/delete_template_products')}}",
                method: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    table_name: table_name,
                    template_id: template_id,
                    product_id: product_id
                },

                beforeSend: function () {
                    //                        Swal.showLoading();
                },
                success: function (response)
                {
                    load_template_products();
                }
            });
            return true;
        } else {
            return false;
        }


    }
</script>
@endsection
