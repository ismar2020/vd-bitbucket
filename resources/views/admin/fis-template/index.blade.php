@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Landed Cost Templates</div>
                    <div class="card-body">
                        <a href="{{ url('/admin/fis-template/create') }}" class="btn btn-success btn-sm" title="Add New FisTemplate">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>

                        <br/>
                        <br/>
                        <div class="table-responsive suppliers_table_mobile">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Template Name</th><th>Is Default</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($fistemplate as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->template_name }}</td>
                                        <td><?= ($item->is_default == '0') ? 'No' : 'Yes' ?></td>
                                        <td>
                                            <a href="{{ url('/admin/fis-template/' . $item->id . '/edit') }}" title="Edit FisTemplate"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                            {!! Form::open([
                                                'method' => 'DELETE',
                                                'url' => ['/admin/fis-template', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                        'type' => 'submit',
                                                        'type' => 'submit',
'class' => 'btn btn-danger btn-sm deleteentry',
                                                        'title' => 'Delete FisTemplate'   
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                     
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
