
<div class="row">
    <!--    <div class="col-md-12">-->
    <div class="col-md-7">
        <div class="form-group{{ $errors->has('template_name') ? 'has-error' : ''}}">
            {!! Form::label('template_name', 'Template Name', ['class' => 'control-label']) !!}
            {!! Form::text('template_name', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
            {!! $errors->first('template_name', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group{{ $errors->has('is_default') ? 'has-error' : ''}}">
            {!! Form::label('is_default', 'Is Default', ['class' => 'control-label']) !!}
            {!! Form::select('is_default', array('0' => 'No', '1' => 'Yes'),null,['class' => 'form-control', 'onchange' => 'set_default_template();']) !!}
            {!! $errors->first('is_default', '<p class="help-block">:message</p>') !!}
        </div>

        <!--button for linking product with template-->
        @if($formMode == 'edit')
        <button type="button" class="btn btn-primary btn-sm fis_product" data-toggle="modal" data-target="#fisProductModalLongnew" style="margin:12px;"><i class="fa fa-plus" aria-hidden="true"></i>
            Add Product
        </button><br><br>
        @endif
        <table class="table table-responsive">
            <thead>
                <tr>
                    <th class="">FIS</th>
                    <th class="">COST</th>
                    <th class="">Details</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <div class="m1">
                            <div class="m2a left_ml_fis"><p>From factory to<br>port of origin</p></div><hr class="m_media_query" style="
                                    position: absolute;
                                    width: 100%;
                                    border-top: 1px solid #d6d7d8;
                                    margin-top: 210px;
                                    left: 0;
                                                                                                                      ">
                            <div class="m3a left_ml_fis"><p>From port of dispatch<br>to warehouse (FIW)</p></div><hr class="m_media_query2" style="
                                    position: absolute;
                                    width: 100%;
                                    border-top: 1px solid #d6d7d8;
                                    margin-top: 420px;
                                    left: 0;
                                                                                                                      ">
                            <div class="m4a left_ml_fis"><p>From warehouse<br>to store (FIS)</p></div>
                        </div>
                    </td>
                    <td>
                        <!--second row-->
                        <div class="m3">
                            <div class="m3a x b_bottom"><p>Transport</p></div>
                            <div class="m3a x b_bottom"><p>Transport Currency</p></div>
                            <div class="m3a x b_bottom"><p>Port Costs</p></div>
                            <div class="m3a x b_bottom"><p>Port Costs Currency</p></div>
                            <div class="m3a x b_bottom"><p>Insurance Cost</p></div>
                            <div class="m3a x b_bottom"><p>Insurance Cost Currency</p></div>
                        </div>
                        <!--third row-->
                        <div class="m4">
                            <div class="m4a x m4_bottom"><p>Import Custom Duties %</p></div>
                            <div class="m4a x m4_bottom z_bottom_fis"><p>Import Taxes %</p></div>
                            <div class="m4a x m4_bottom z_bottom_1_fis"><p>Handing Costs in destination terminal (DTHC)</p></div>
                            <div class="m4a x m4_bottom m4_bottom_fis"><p>Handing Costs Currency</p></div>
                            <div class="m4a x m4_bottom m4_bottom_fis"><p>Costs of Shipping after destination port</p></div>
                            <div class="m4a x m4_bottom m4_bottom_fis"><p>Shipping after Port Currency</p></div>
                        </div>
                        <div class="m5">
                            <div class="m5a x m5_bottom"><p>Domestic Transport</p></div>
                            <div class="m5a x m5_bottom"><p>Currency</p></div>
                            <div class="m5a x m5_bottom"><p>Storage Overhead</p></div>
                            <div class="m5a x m5_bottom"><p>Currency</p></div>
                            <div class="m5a x m5_bottom"><p>Merchandising</p></div>
                            <div class="m5a x m5_bottom"><p>Currency</p></div>
                            <div class="m5a x m5_bottom"><p>Other Overheads</p></div>
                            <div class="m5a x m5_bottom"><p>Currency</p></div>
                        </div>
                    </td>
                    <td>
                        <div class="m2">
                            <div class="m2a">
                                <div class="form-group{{ $errors->has('transport') ? 'has-error' : ''}}">
                                    {!! Form::text('transport', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required','onkeypress' => 'return isNumber(event)'] : ['class' => 'form-control','onkeypress' => 'return isNumber(event)']) !!}
                                    {!! $errors->first('transport', '<p class="help-block">:message</p>') !!}
                                </div>

                            </div>
                            <div class="m2a">
                                <div class="form-group{{ $errors->has('transport_currency') ? 'has-error' : ''}}">
                                    {!! Form::select('transport_currency', get_limited_currency_list(), null, ['class' => 'form-control']) !!}
                                    {!! $errors->first('transport_currency', '<p class="help-block">:message</p>') !!}
                                </div>


                            </div>
                            <div class="m2a">
                                <div class="form-group{{ $errors->has('port_costs') ? 'has-error' : ''}}">
                                    {!! Form::number('port_costs', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required','onkeypress' => 'return isNumber(event)'] : ['class' => 'form-control','onkeypress' => 'return isNumber(event)']) !!}
                                    {!! $errors->first('port_costs', '<p class="help-block">:message</p>') !!}
                                </div>

                            </div>
                            <div class="m2a">
                                <div class="form-group{{ $errors->has('port_costs_currency') ? 'has-error' : ''}}">

                                    {!! Form::select('port_costs_currency', get_limited_currency_list(), null, ['class' => 'form-control']) !!}
                                    {!! $errors->first('port_costs_currency', '<p class="help-block">:message</p>') !!}
                                </div>

                            </div>
                            <div class="m2a">
                                <div class="form-group{{ $errors->has('insurance_cost') ? 'has-error' : ''}}">
                                    {!! Form::number('insurance_cost', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required','onkeypress' => 'return isNumber(event)'] : ['class' => 'form-control','onkeypress' => 'return isNumber(event)']) !!}
                                    {!! $errors->first('insurance_cost', '<p class="help-block">:message</p>') !!}
                                </div>

                            </div>
                            <div class="m2a z">
                                <div class="form-group{{ $errors->has('insurance_cost_currency') ? 'has-error' : ''}}">
                                    {!! Form::select('insurance_cost_currency', get_limited_currency_list(), null, ['class' => 'form-control']) !!}
                                    {!! $errors->first('insurance_cost_currency', '<p class="help-block">:message</p>') !!}
                                </div>

                            </div> 
                            <div class="m2a">
                                <div class="form-group{{ $errors->has('import_custom_duties_percentage') ? 'has-error' : ''}}">
                                    {!! Form::text('import_custom_duties_percentage', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                                    {!! $errors->first('import_custom_duties_percentage', '<p class="help-block">:message</p>') !!}
                                </div>

                            </div>
                            <div class="m2a">
                                <div class="form-group{{ $errors->has('import_taxes_percentage') ? 'has-error' : ''}}">

                                    {!! Form::text('import_taxes_percentage', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                                    {!! $errors->first('import_taxes_percentage', '<p class="help-block">:message</p>') !!}
                                </div>

                            </div>
                            <div class="m2a">
                                <div class="form-group{{ $errors->has('handing_costs_in_destination_terminal') ? 'has-error' : ''}}">
                                    {!! Form::number('handing_costs_in_destination_terminal', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required','onkeypress' => 'return isNumber(event)'] : ['class' => 'form-control','onkeypress' => 'return isNumber(event)']) !!}
                                    {!! $errors->first('handing_costs_in_destination_terminal', '<p class="help-block">:message</p>') !!}
                                </div>

                            </div>
                            <div class="m2a">
                                <div class="form-group{{ $errors->has('handing_costs_currency') ? 'has-error' : ''}}">

                                    {!! Form::select('handing_costs_currency', get_limited_currency_list(), null, ['class' => 'form-control']) !!}
                                    {!! $errors->first('handing_costs_currency', '<p class="help-block">:message</p>') !!}
                                </div>

                            </div>
                            <div class="m2a">
                                <div class="form-group{{ $errors->has('costs_of_shipping_after_destination_port') ? 'has-error' : ''}}">
                                    {!! Form::number('costs_of_shipping_after_destination_port', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required','onkeypress' => 'return isNumber(event)'] : ['class' => 'form-control','onkeypress' => 'return isNumber(event)']) !!}
                                    {!! $errors->first('costs_of_shipping_after_destination_port', '<p class="help-block">:message</p>') !!}
                                </div>

                            </div>
                            <div class="m2a h_cost">
                                <div class="form-group{{ $errors->has('shipping_after_port_currency') ? 'has-error' : ''}}">
                                    {!! Form::select('shipping_after_port_currency', get_limited_currency_list(), null, ['class' => 'form-control']) !!}
                                    {!! $errors->first('shipping_after_port_currency', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="m2a">

                            </div>
                            <div class="m2a s_port"> 
                            </div>
                            <div class="m2a">
                                <div class="form-group{{ $errors->has('domestic_transport') ? 'has-error' : ''}}">
                                    {!! Form::text('domestic_transport', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required','onkeypress' => 'return isNumber(event)'] : ['class' => 'form-control','onkeypress' => 'return isNumber(event)']) !!}
                                    {!! $errors->first('domestic_transport', '<p class="help-block">:message</p>') !!}
                                </div>

                            </div>
                            <div class="m2a">
                                <div class="form-group{{ $errors->has('domestic_transport_currency') ? 'has-error' : ''}}">
                                    {!! Form::select('domestic_transport_currency', get_limited_currency_list(), null, ['class' => 'form-control']) !!}
                                    {!! $errors->first('domestic_transport_currency', '<p class="help-block">:message</p>') !!}
                                </div>


                            </div>
                            <div class="m2a">
                                <div class="form-group{{ $errors->has('storage_overhead') ? 'has-error' : ''}}">
                                    {!! Form::text('storage_overhead', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required','onkeypress' => 'return isNumber(event)'] : ['class' => 'form-control','onkeypress' => 'return isNumber(event)']) !!}
                                    {!! $errors->first('storage_overhead', '<p class="help-block">:message</p>') !!}
                                </div>

                            </div>
                            <div class="m2a">
                                <div class="form-group{{ $errors->has('storage_overhead_currency') ? 'has-error' : ''}}">
                                    {!! Form::select('storage_overhead_currency', get_limited_currency_list(), null, ['class' => 'form-control']) !!}
                                    {!! $errors->first('storage_overhead_currency', '<p class="help-block">:message</p>') !!}
                                </div>


                            </div>
                            <div class="m2a">
                                <div class="form-group{{ $errors->has('merchandising') ? 'has-error' : ''}}">
                                    {!! Form::text('merchandising', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required','onkeypress' => 'return isNumber(event)'] : ['class' => 'form-control','onkeypress' => 'return isNumber(event)']) !!}
                                    {!! $errors->first('merchandising', '<p class="help-block">:message</p>') !!}
                                </div>

                            </div>
                            <div class="m2a">
                                <div class="form-group{{ $errors->has('merchandising_currency') ? 'has-error' : ''}}">
                                    {!! Form::select('merchandising_currency', get_limited_currency_list(), null, ['class' => 'form-control']) !!}
                                    {!! $errors->first('merchandising_currency', '<p class="help-block">:message</p>') !!}
                                </div>


                            </div>
                            <div class="m2a">
                                <div class="form-group{{ $errors->has('other_overheads') ? 'has-error' : ''}}">
                                    {!! Form::text('other_overheads', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required','onkeypress' => 'return isNumber(event)'] : ['class' => 'form-control','onkeypress' => 'return isNumber(event)']) !!}
                                    {!! $errors->first('other_overheads', '<p class="help-block">:message</p>') !!}
                                </div>

                            </div>
                            <div class="m2a">
                                <div class="form-group{{ $errors->has('other_overheads_currency') ? 'has-error' : ''}}">
                                    {!! Form::select('other_overheads_currency', get_limited_currency_list(), null, ['class' => 'form-control']) !!}
                                    {!! $errors->first('other_overheads_currency', '<p class="help-block">:message</p>') !!}
                                </div>


                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>

        <div class="form-group">
            {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
        </div>    
    </div>
    <div class="col-md-5">
        <div id="load_template_products">

        </div>
    </div>
    <!--    </div>-->
</div>
<!--Modal for linking products with template-->
@if($formMode == 'edit')
<div class="modal fade" id="fisProductModalLongnew" tabindex="-1" role="dialog" aria-labelledby="fisProductModalLongNewTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background: #397657;">
                <h5 class="modal-title" id="orderProductModalLongNewTitle" style="color:#fff;">Link Product to Template</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group{{ $errors->has('product_id') ? 'has-error' : ''}}">
                    {!! Form::label('product_fis', 'Product Name', ['class' => 'control-label']) !!}

                    <select class="product_fis select2" name = "product_fis" style = "width:180px">
                        <?php $data = get_products_list_which_are_not_linked($fistemplate->id); ?>
                        <option value="">Select Product</option>
                        @foreach($data as $key => $value)
                        <option value="{{$key}}">{{$value}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <a href="javascript:void(0)" class="btn btn-primary btn-sm add_product_fis" onclick="fis_template_product_relation_create(), $('#modalCloseFISTemplateProduct').trigger('click');" data-added="0" style="margin:10px;"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Add</a>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" id="modalCloseFISTemplateProduct" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>
@endif
<!-- Modal ends -->