<ul class="nav nav-tabs">
    <li><a data-toggle="tab" href="#quote_details_home"  id="home_active">Quote Details</a></li>
    <li><a data-toggle="tab" href="#chat">Comments</a></li>
    <li><a data-toggle="tab" href="#attachments">Attachments</a></li>
</ul>

<div class="tab-content">
    <div id="quote_details_home" class="tab-pane fade in active">
        <br>
         <?php  if(\Auth::user()->roles->first()->name == 'supplier' && \Auth::user()->supplier_id != null){
                    if($quote->user_id == \Auth::id()){
                        $check = "yes";
                    }else{
                        $check = "no";
                    }
                }
                
                if(\Auth::user()->roles->first()->name == 'super_admin'){
                    $check = "yes";
                }
        ?> 
        <form name="quote_lines" action="javascript:void(0)" accept-charset="UTF-8" class="form-horizontal" id="quote_lines" enctype="multipart/form-data" style="overflow-y: hidden;">
            <input type="hidden" class="form-horizontal quote_id" name="quote_id" value="{{ $quote->id }}">
            <div class="row quotes_new">
            <div class="col-sm-6 col-md-3 form-group{{ $errors->has('name') ? 'has-error' : ''}}">
                {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
                <input class="form-control quote_lines triggered_quote @if($check == 'no') shipping_plan_tracking  @endif" name="name" type="text" value="{{ $quote->name }}" id="name" @if($check == "no") disabled  @endif>
<!--                {!! Form::text('name', null, ('' == 'required') ? ['class' => 'form-control quote_lines triggered_quote', 'required' => 'required'] : ['class' => 'form-control quote_lines triggered_quote']) !!}-->
                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
            </div>
            <?php
            $quote_status = ['0' => 'Quote', '1' => 'Pending Approval', '2' => 'Approved', '3' => 'Shipping', '4' => 'Closed'];
            $quote_category = ['0' => 'CAT 1', '1' => 'CAT 2'];
            ?>
            <div class="col-sm-6 col-md-3 form-group{{ $errors->has('quote_status') ? 'has-error' : ''}}">
                {!! Form::label('quote_status', 'Quote Status', ['class' => 'control-label']) !!}
                <?php /*<select class="form-control quote_lines" name="quote_status">
                    @foreach($quote_status as $key => $value)
                    <option value="<?php echo $key; ?>" <?php
                    if ($quote->quote_status == $key) {
                        echo 'selected';
                    } else {
                        echo '';
                    }
                    ?>><?php echo $value; ?></option>
                    @endforeach
                </select>*/?>
                <select class="form-control quote_lines @if($check == 'no') shipping_plan_tracking select_webkit_hide @endif" name="quote_status " @if($check == 'no') disabled  @endif><?php $data = get_quote_status_list(); ?>
                    <option value="">Select Quote Status</option>
                    @foreach($data as $key => $value)
                    @if(isset($formMode) && $formMode == 'edit' && $key == $quote->quote_status)
                    <option value="{{$key}}" selected="">{{$value}}</option>

                    @else
                    <option value="{{$key}}">{{$value}}</option>
                    @endif
                    @endforeach
                </select>
                {!! $errors->first('quote_status', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="col-sm-6 col-md-3 form-group{{ $errors->has('quote_category') ? 'has-error' : ''}}">
                {!! Form::label('quote_category', 'Quote Category', ['class' => 'control-label']) !!}
                <select class="form-control quote_category quote_lines @if($check == 'no') shipping_plan_tracking select_webkit_hide @endif" name="quote_category " @if($check == 'no') disabled @endif"><?php $data = get_quote_category_list(); ?>
                    <option value="">Select Quote Category</option>
                    @foreach($data as $key => $value)
                    @if($quote->quote_category == $key)
                    <option value="{{$key}}" selected>{{$value}}</option>
                    @else
                    <option value="{{$key}}">{{$value}}</option>
                    @endif
                    @endforeach
                </select>
                {!! $errors->first('quote_category', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="col-sm-6 col-md-3 form-group{{ $errors->has('active') ? 'has-error' : ''}}">
                {!! Form::label('active', 'Active', ['class' => 'control-label']) !!}

                <select class="form-control quote_lines @if($check == 'no') shipping_plan_tracking select_webkit_hide @endif" name="active " @if($check == 'no') disabled @endif">
                    <option value="0" <?php
                    if ($quote->active == '1') {
                        echo '';
                    } else {
                        echo 'selected';
                    }
                    ?>>Yes</option>
                    <option value="1" <?php
                    if ($quote->active == '1') {
                        echo 'selected';
                    } else {
                        echo '';
                    }
                    ?>>No</option>
                </select>
                {!! $errors->first('active', '<p class="help-block">:message</p>') !!}
            </div>
            </div>

            <div id="header">
            </div>
            <div class="well clearfix">
                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#quoteLineModalLongnew" style="margin:12px;float:left"><i class="fa fa-plus" aria-hidden="true"></i>
                    Add Product Row
                </button>
                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#showQuoteHideModalLongnew" style="margin:12px;float:left"><i class="fa fa-eye" aria-hidden="true"></i>
                    Show/hide columns
                </button>
                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#importExcelModalLongnew" style="margin:12px;float:left"><i class="fa fa-arrow-up" aria-hidden="true"></i>
                    Upload From Excel
                </button>
                <a href="{{ url('admin/quote_lines_export/'.$quote->id)}}" class="btn btn-primary btn-sm" style="margin:12px;float:left"><i class="fa fa-pencil" aria-hidden="true"></i>
                    Export to Excel
                </a>


            </div>

            <div id = "load_quote_lines">

            </div>  

        </form><br><br>    

    </div>

    <!-------------------------------------Chat Tab data--------------------------------->
    <div id="chat" class="tab-pane fade">
        <div class="container"><br>
            <h3 class="panel-title">Add Comment</h3>
            <form method="POST" action="javascript:void(0)" accept-charset="UTF-8" class="form-horizontal" id="commentForm" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="quote_id" value="<?= $quote->id ?>"/>
                <div class="form-group{{ $errors->has('title') ? 'has-error' : ''}}" style="display:none;">
                    {!! Form::label('title', 'Title', ['class' => 'control-label']) !!}
                    {!! Form::text('title', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group{{ $errors->has('description') ? 'has-error' : ''}}">
                    {!! Form::label('description', 'Comment', ['class' => 'control-label']) !!}
                    {!! Form::textarea('description', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required', 'rows' => '4'] : ['class' => 'form-control', 'rows' => '4']) !!}
                    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group">
                    {!! Form::button('Post comment', ['class' => 'btn btn-primary comment_submit']) !!}
                </div>
            </form>
            <div id="comments_data">
            </div>
        </div>
    </div>

    <!-------------------------------------Attachments Tab Data--------------------------->
    <div id="attachments" class="tab-pane fade">
        <br>
        <form name="quote_" action="javascript:void(0)" accept-charset="UTF-8" class="form-horizontal" id="quote_attachments" enctype="multipart/form-data" style="overflow-y: hidden;">
            <input type="hidden" class="form-horizontal quote_id" name="quote_id" value="{{ $quote->id }}"><br>
            <div id="dZUpload" class="dropzone">
                <div class="dz-default dz-message"><span>Drop Quote Attachment Here</span></div>
                <meta name="csrf-token" content="{{ csrf_token() }}">

            </div><br>
            <div id="load_quote_attachment_lines">

            </div>
        </form><br>
    </div>    
    <!-------------------------------------Tab ends----------------------------------->
</div>



<!--
<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>-->
