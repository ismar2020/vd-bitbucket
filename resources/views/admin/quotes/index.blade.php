@extends('layouts.backend')

@section('content')
<div class="container">
    <div class="row">
        @include('admin.sidebar')

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Quotes</div>
                <div class="card-body">
                    <a href="{{ url('/admin/quotes/create') }}" class="btn btn-success btn-sm" title="Add New Quote">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add New
                    </a>
<!--                    <a href="javascript:void(0)" class="btn btn-success btn-sm" title="Compare Quotes">
                        <i class="fa fa-quote-left" aria-hidden="true"></i> Compare Quotes
                    </a>-->

                    <br/>
                    <br/>
                    <div class="table-responsive suppliers_table_mobile">
                        <table class="table table-borderless">
                            <thead>
                                <tr>
                                    <th>#</th><th>Name</th><th>Quote Status</th><th>Quote Category</th><th>Last Comment</th><th>Active</th><th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
//                                $status = ['0' => 'Quote', '1' => 'Pending Approval', '2' => 'Approved', '3' => 'Shipping', '4' => 'Closed'];
                                $cat = ['0' => 'CAT 1', '1' => 'CAT 2'];
                                ?>
                                @foreach($quotes as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>{{get_quote_status_name_by_id($item->quote_status)}}</td>
                                    <td>{{get_quote_cat_name_by_id($item->quote_category)}}</td>
                                    <td>{{get_latest_quote_comment_name_by_quote_id($item->id)}}</td>
                                    <td><?= ($item->active == '0') ? 'Yes' : 'No' ?></td>
                                    <td>
                                        <a href="{{ url('/admin/quotes/' . $item->id) }}" title="View Quote" style="display: none;"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                        <a href="{{ url('/admin/quotes/' . $item->id . '/edit') }}" title="Edit Quote"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                        {!! Form::open([
                                        'method' => 'DELETE',
                                        'url' => ['/admin/quotes', $item->id],
                                        'style' => 'display:inline'
                                        ]) !!}
                                        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                        'type' => 'submit',
                                        'type' => 'submit',
'class' => 'btn btn-danger btn-sm deleteentry',
                                        'title' => 'Delete Quote' 
                                        )) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                      
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script>

    $(document).ready(function () {
        $('.table').dataTable({
            "iDisplayLength": 50
        });
    });
</script>
@endsection
