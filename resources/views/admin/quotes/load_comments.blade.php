<div class="row">
    <div class="panel panel-default widget">
        <div class="panel-heading">
            <span class="glyphicon glyphicon-comment"></span>
            <h3 class="panel-title">
                <?php if(count($comments) > 0 ){ echo 'Recent Comments'; } ?></h3>
            <span class="label label-info">
            </span>
        </div>
        <div class="panel-body">
            <ul class="list-group">
                @foreach($comments as $data)
                <li class="list-group-item">

                    <div class="row">
                        <div class="col-xs-2 col-md-2">
                            <img src="{{url('images/user.png')}}" class="img-circle img-responsive" alt="" / style="border-radius: 50%;"></div>
                        <div class="col-xs-10 col-md-10">
                            <div>
                                <a href="javascript:void(0)" title="Title">
                                    {{$data->description}}</a>
                                <div class="mic-info">
                                    By: {{get_user_name_by_id($data->user_id)}} on {{($data->created_at)->format('d F Y')}}
                                </div>
                            </div>
                            <!--<div class="comment-text">-->
                               
                            <!--</div>-->

                        </div>
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>