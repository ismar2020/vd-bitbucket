<div class="row">
<div class="col-md-3 form-group{{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-3 form-group{{ $errors->has('quote_status') ? 'has-error' : ''}}">
    {!! Form::label('quote_status', 'Quote Status', ['class' => 'control-label']) !!}
    <?php /*{!! Form::select('quote_status', array('0' => 'Quote', '1' => 'Pending Approval', '2' => 'Approved', '3' => 'Shipping', '4' => 'Closed'), '0',['class' => 'form-control']) !!}*/?>
    <select class="form-control" name="quote_status"><?php $data = get_quote_status_list(); ?>
        <option value="">Select Quote Status</option>
        @foreach($data as $key => $value)
        <option value="{{$key}}">{{$value}}</option>
        @endforeach
    </select>
    {!! $errors->first('quote_status', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-3 form-group{{ $errors->has('quote_category') ? 'has-error' : ''}}">
    {!! Form::label('quote_category', 'Quote Category', ['class' => 'control-label']) !!}
    
    <select class="form-control quote_category" name="quote_category"><?php $data = get_quote_category_list(); ?>
        <option value="">Select Quote Category</option>
        @foreach($data as $key => $value)
        <option value="{{$key}}">{{$value}}</option>
        @endforeach
    </select>
    {!! $errors->first('quote_category', '<p class="help-block">:message</p>') !!}
</div>
<div class="col-md-3 form-group{{ $errors->has('active') ? 'has-error' : ''}}">
    {!! Form::label('active', 'Active', ['class' => 'control-label']) !!}
    {!! Form::select('active', array('0' => 'Yes', '1' => 'No'), '0',['class' => 'form-control']) !!}
    {!! $errors->first('active', '<p class="help-block">:message</p>') !!}
</div>
</div>

<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
