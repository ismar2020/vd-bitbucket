@extends('layouts.backend')

@section('content')
<style>
    .swal-text {
        font-size: 19px;
        text-align: center;
        font-weight: 700;
    }
</style>
<style>
        .progress { position:relative; width:100%; border: 1px solid #7F98B2; padding: 1px; border-radius: 3px; }
        .bar { background-color: #B4F5B4; width:0%; height:25px; border-radius: 3px; z-index:0px}
        .percent { position:absolute; display:inline-block; top:-2px; left:48%; color: #20be80;}
    </style>    
    <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>
<div class="container">
    <div class="row">
        @include('admin.sidebar')

        <div class="col-md-12">
            <div class="card">
                
                <!--<div class="card-header">Edit Quote {{ get_name_by_id_and_table_name('quotes', $quote->id, 'name') }}</div>-->
                
                <div class="card-header">Edit Quote #{{ $quote->id }}</div>
                
                <div class="card-body" style="overflow-y: hidden;">

                    @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    @endif

                    <!--                    {!! Form::model($quote, [
                                        'method' => 'PATCH',
                                        'url' => ['/admin/quotes', $quote->id],
                                        'class' => 'form-horizontal',
                                        'files' => true
                                        ]) !!}-->

                    @include ('admin.quotes.edit_quote', ['formMode' => 'edit'])

                    <!--{!! Form::close() !!}-->

                </div>
            </div>
        </div>
    </div>
</div>
<!-------------------------------Modal for Add product row-----------------------> 
<div class="modal fade" id="quoteLineModalLongnew" tabindex="-1" role="dialog" aria-labelledby="quoteLineModalLongnewTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background: #397657;">
                <h5 class="modal-title" id="quoteLineModalLongnewTitle" style="color:#fff;">Add Product</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="javascript:void(0)" accept-charset="UTF-8" class="form-horizontal" id="addOrderProductForm" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group{{ $errors->has('supplier_id') ? 'has-error' : ''}}">
                        {!! Form::label('supplier_id', 'Select Supplier', ['class' => 'control-label']) !!}
                        <!--    {!! Form::select('order_supplier_id', get_suppliers_list(), null, ['class' => 'form-control select2','size' => '5']) !!}-->
                        <?php $suppliers = get_suppliers_list(); ?>
                        <select class="order_supplier_id select2_quote_line_modal" name = "order_supplier_id" style = "width:180px">
                            <option value="">Select Supplier</option>
                            <?php foreach ($suppliers as $key => $val) { ?>
                                <option value = "<?= $key ?>">  <?= $val ?>  </option>

                            <?php } ?>

                        </select>
                        {!! $errors->first('supplier_id', '<p class="help-block">:message</p>') !!}
                        <br><br>
                    </div>
                    <div class="form-group{{ $errors->has('product_id') ? 'has-error' : ''}}">
                        {!! Form::label('product_id', 'Product Name', ['class' => 'control-label']) !!}

                        <select class="order_product_id select2_quote_line_modal" name = "order_product_id" style = "width:180px">
                            <?php $data = get_products_list(); ?>
                            <option value="">Select Product</option>
                            @foreach($data as $key => $value)
                            <option value="{{$key}}">{{$value}}</option>
                            @endforeach
                        </select>
                        {!! $errors->first('product_id', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="product_details">
                        <div class="row">
                            <div class="col-md-6 product_"><h4>Product Name : <span class="product_name"></span></h4></div>
                            <div class="col-md-6"><span class="product_image"></span></div>
                        </div>
                    </div>
                    <input type="hidden" class="form-control" id="order_product_line_id" name="quote_line_id" value=""/>
                    <input type="hidden" class="form-control" name="quote_id" value="{{$quote->id}}"/>
                    <div class="form-group">
                        <a href="javascript:void(0)" class="btn btn-primary btn-sm add-record order_line_create" onclick="quote_line_create(), $('.modalCloseQuoteProduct').trigger('click');" data-added="0" style="margin:10px;"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Add</a>
                    </div>

                    <div class="progress d-none">
                        <div class="bar"></div >
                        <div class="percent"></div >
                    </div>

                    <div id="status"></div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary modalCloseQuoteProduct" id="modalCloseProduct" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>

<!-----------------------------Modal for Import from Excel-----------------------> 
<div class="modal fade" id="importExcelModalLongnew" tabindex="-1" role="dialog" aria-labelledby="importExcelModalLongnewTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background: #397657;">
                <h5 class="modal-title" id="importExcelModalLongnewTitle" style="color:#fff;">Import From Excel</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{url('admin/import_excel/import')}}" accept-charset="UTF-8" class="form-horizontal" id="importExcelProductForm" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" class="form-control" name="quote_id" value="{{$quote->id}}"/>
                    
                    <div class="form-group">
                        <table>
                            <tr>
                                <td>{!! Form::label('supplier_id', 'Select Supplier*', ['class' => 'control-label']) !!}</td>
                                <td><?php $suppliers = get_suppliers_list(); ?>
                                    <select class="supplier_id select2 supplier_id_import" id="supplier_id_import" name = "supplier_id" style = "width:190px">
                            
                            <option value="">Select Supplier</option>
                            <?php foreach ($suppliers as $key => $val) { ?>
                            <?php if (\Auth::user()->roles->first()->name == 'supplier'){ ?>
                            <option value = "<?= $key ?>" selected>  <?= $val ?>  </option>
                            <?php }else{ ?>
                            <option value = "<?= $key ?>">  <?= $val ?>  </option>
                            <?php  } } ?>

                        </select></td>
                            </tr>
                            <tr>
                                <td>
                                    {!! Form::label('shipping_port', 'Port of Origin*', ['class' => 'control-label']) !!}
                                </td>
                                <td>
                                    <select class="form-control select2 shipping_port_import" name="shipping_port" id="shipping_port_import" style="width: 190px;"><?php $data = get_shipping_port_list(); ?>
                                        <option value="">Port of Origins</option>
                                        @foreach($data as $key => $value)
                                        <option value="{{$key}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                </td>
                            </tr>    
                            <tr>
                                <td>
                                    {!! Form::label('product_currency', 'Product Currency*', ['class' => 'control-label']) !!}
                                </td>
                                <td>
                                    <select class="form-control select2 product_currency_import" id="product_currency" id="product_currency_import" name="product_currency" style="width: 190px;">
                                        <option value="">Select Currency</option>
                                        {!!  get_limited_currency_list_with_symbols_through_id() !!}
                                    </select> 
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    {!! Form::label('product_material', 'Product Material*', ['class' => 'control-label']) !!}
                                </td>
                                <td>
                                    <select class="form-control select2 product_material_import" name="product_material" style="width: 190px;"><?php $data = get_material_list(); ?>
                                        <option value="">Select Product Material</option>
                                        @foreach($data as $key => $value)
                                        <option value="{{$key}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                </td>
                            </tr>
                            <?php /*
                            <tr>
                                <td>
                                    {!! Form::label('product_box_material', 'Product Box Material*', ['class' => 'control-label']) !!}
                                </td>
                                 
                                <td>
                                    <select class="form-control select2 product_box_material_import" name="product_box_material" style="width: 190px;"><?php $data = get_material_list(); ?>
                                        <option value="">Select Product Box Material</option>
                                        @foreach($data as $key => $value)
                                        <option value="{{$key}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                
                            </tr>
                             */ ?>
                            <tr>
                                <td>{!! Form::label('csv_file', 'Upload CSV*', ['class' => 'control-label']) !!}</td>
                                <td><input type="file" class="import_csv_file" name="csv_file" value=""/></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td colspan="2"><a href="{{ asset('sample/sample_quote_import_template.csv')}}"/>Download Sample Import Template</a></td>
                            </tr>
                        </table>
                    </div>

                    <div class="form-group">
                        <a href="javascript:void(0)" class="btn btn-primary btn-sm import_quote_line" data-added="0" style="margin:10px;"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Import</a>
                    </div>

                    <div class="progress d-none">
                        <div class="bar"></div >
                        <div class="percent"></div >
                    </div>

                    <div id="status"></div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary modalCloseQuoteImportProduct" id="modalCloseProduct" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!--_-------------------------------- Modals Ends--------------------------------->
<script>
    var base_url = "{{url('/')}}"
    $(document).ready(function () {
        Dropzone.autoDiscover = false;
        var url = "<?= url('admin/dropzoneQuoteAttachment?quote_id=' . $quote->id . '') ?>";
        $("#dZUpload").dropzone({
            url: url,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method: "POST",
            maxFilesize: 15,
            addRemoveLinks: true,
            success: function (file, response) {
                console.log(response);
                var imgName = response;
                file.previewElement.classList.add("dz-success");
                console.log("Successfully uploaded :" + imgName);
                load_quote_attachments_lines();
            },
            error: function (file, response) {
                file.previewElement.classList.add("dz-error");
            }
        });
    });
    load_quote_attachments_lines();
    function load_quote_attachments_lines() {
        var quote_id = '<?= $quote->id ?>';

        $.ajax({
            url: "{{url('admin/load_quote_attachments_lines')}}",
            method: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {quote_id: quote_id},
            beforeSend: function () {
                //                        Swal.showLoading();
            },
            onError: function () {
                swal("Error!", 'Something Went Wrong', "error");
            },
            success: function (response)
            {
                $('#load_quote_attachment_lines').html(response);

            }
        });
    }
//    strugglers code
    $("#hide_show_all").on("change", function () {
//        alert('s');
        var hide = $(this).is(":checked");
        $(".hide_show").prop("checked", hide);

        if (hide) {
            $('#tbl tr th').hide(100);
            $('#tbl tr td').hide(100);
        } else {
            $('#tbl tr th').show(100);
            $('#tbl tr td').show(100);
        }
    });
    $(document).on("change", ".hide_show", function () {
        var hide = $(this).is(":checked");

        var all_ch = $(".hide_show:checked").length == $(".hide_show").length;

        $("#hide_show_all").prop("checked", all_ch);

        var ti = $(this).index(".hide_show");

        $('#tbl tr').each(function () {
            if (!hide) {
                $('td:eq(' + ti + ')', this).hide(100);
                $('th:eq(' + ti + ')', this).hide(100);
            } else {
                $('td:eq(' + ti + ')', this).show(100);
                $('th:eq(' + ti + ')', this).show(100);
            }
        });
    });

//    ends
//    code for submitting quote lines by ajax
    var $target = '';
    $(document).on('click keyup change', '.quote_lines', function () {
        $('#quote_lines').submit();
        $target = $('#' + $(this).closest('tr').attr('id') + ' > td:nth-child(14) >  img');
    });

//    for submitting order pricing data on keyup in orders
    $('#quote_lines').on('submit', function (e) {

        e.preventDefault();

        var form_data = new FormData(this);

        form_data.append("_token", $('meta[name="csrf-token"]').attr('content'));
        $.ajax({
            url: "{{url('admin/submit_quote_ajax')}}",
            method: "POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                //                        Swal.showLoading();
            },
            onError: function () {
                swal("Error!", 'Something Went Wrong', "error");
            },
            success: function (response)
            {

                if (response) {
                    if (response.status == 'true') {
                        console.log(response.image);
//                        console.log(response.value.product_image);
                        if (response.image != '') {
                            $target.attr('src', base_url + response.image);
                        }
                        console.log('quote line saved');
                    }
                }
            }
        });
    });
//ends
//  Remove image ajax for quote line
    $(document).on('click', '.remove_image', function () {
        var didConfirm = confirm("Are you sure You want to Remove Image?");
        if (didConfirm == false) {
            return false;
        }
        var rowId = $(this).closest('tr').attr('id');
        var currentRow = $("#" + rowId);
        var quote_row_id = currentRow.find(".quote_row_id").val();
        //ajax
        $.ajax({
            url: "{{url('admin/remove_image_from_quote_lines')}}",
            method: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {quote_row_id: quote_row_id},
            beforeSend: function () {

            },
            onError: function () {
                swal("Error!", 'Something Went Wrong', "error");
            },
            success: function (response)
            {
                if (response.status == 'true') {
                    load_quote_lines();
                }
            }
        });
    });

//    code for submitting comments by ajax
    $(document).on('click', '.comment_submit', function () {
//        alert('s');
        $('#commentForm').submit();
    });

//    for submitting order pricing data on keyup in orders
    $('#commentForm').on('submit', function (e) {
        if ($('#description').val() == '') {
            alert('Please Enter Comment First');
            return false;
        }
        e.preventDefault();

        var form_data = new FormData(this);

        form_data.append("_token", $('meta[name="csrf-token"]').attr('content'));
        $.ajax({
            url: "{{url('admin/submit_comment_ajax')}}",
            method: "POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                //                        Swal.showLoading();
            },
            onError: function () {
                swal("Error!", 'Something Went Wrong', "error");
            },
            success: function (response)
            {
                $('#commentForm').trigger("reset");
                $('#comments_data').html(response);
                console.log('comment saved');
            }
        });
    });
    load_comments();
    function load_comments() {
        var quote_id = '<?= $quote->id ?>';

        $.ajax({
            url: "{{url('admin/load_comments')}}",
            method: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {quote_id: quote_id},
            beforeSend: function () {
                //                        Swal.showLoading();
            },
            onError: function () {
                swal("Error!", 'Something Went Wrong', "error");
            },
            success: function (response)
            {
                $('#comments_data').html(response);

            }
        });
    }
//ends





//    for submitting or importing excel from modal
    $(document).on('click', '.import_quote_line', function () {
        if ($('.supplier_id_import').val() == '' || $('.shipping_port_import').val() == '' || $('.product_currency_import').val() == '' || $('.product_material_import').val() == '') {
            alert('Select All Required Fields First');
        } else if ($('.import_csv_file').val() == '') {
            alert('Upload CSV first')
        } else {
            $('#importExcelProductForm').submit();
        }

    });
    
//    ends
$(function() {

var bar = $('.bar');
var percent = $('.percent');
var status = $('#status');

    $('form#importExcelProductForm').ajaxForm({
            beforeSend: function() {
                status.empty();
                $('.progress').removeClass('d-none');
                var percentVal = '0%';
                bar.width(percentVal);
                percent.html(percentVal);
            },
            uploadProgress: function(event, position, total, percentComplete) {
                var percentVal = percentComplete + '%';
                bar.width(percentVal);
                percent.html(percentVal);
            },
            complete: function(xhr) {
                // status.html(xhr.responseText);
                status.empty();
                $('.progress').addClass('d-none');
                var percentVal = '0%';
                bar.width(percentVal);
                percent.html(percentVal);
                // $(bar).css('background-color','#e9ecef');
            },
            success: function (response)
                {
                if (response) {
                    if (response.status == 'true') {
                        $('#importExcelProductForm')[0].reset();
                        $('#importExcelProductForm').find('select').val('').trigger('change');
                        console.log('quote lines import');
                        load_quote_lines();
                        $('.modalCloseQuoteImportProduct').trigger('click');
        
                        var resOfUpdatedProductLoop = [];
                        if(response.updated_products.length > 0){
                            var uProductsForLoop = response.updated_products;
                            //creating rows for updated products
                            resOfUpdatedProductLoop.push('<tr><th>Product Name</th><th>Status</th></tr>');
                            uProductsForLoop.forEach(function(item){ 
                                resOfUpdatedProductLoop.push('<tr><td>'+item+'</td><td>Updated</td></tr>');
                            })
                        }
                        var resOfCreatedProductLoop = [];
                        if(response.added_products.length > 0){
                            var cProductsForLoop = response.added_products;
                            cProductsForLoop.forEach(function(item){ 
                                resOfCreatedProductLoop.push('<tr><td>'+item+'</td><td>Inserted</td></tr>');
                            })
                        } 
                         var Suppliername = [];
                        if(response.supplier_name){
                            var supplier = response.supplier_name;                        
                                Suppliername.push('<tr><td>Supplier name</td><td>'+supplier+'</td></tr>');
                        }  
                        var container = document.createElement("div");
                        container.innerHTML = '<table class="table" style="position: relative;top: -30px;background: white;">'+resOfUpdatedProductLoop.concat(resOfCreatedProductLoop).concat(Suppliername)+'</table>';
                        console.log(container);
                        //CREATING SWAL TEXT FOR SWEETALERT
                        var swalMessageText = response.insert_count+' Products Added and '+response.update_count+' Products Updates\n\n';
                        //SWEETALERT FOR IMPORT INFO
                        swal({
                            title: "Good job!",
                            text : swalMessageText,
                            content: container,
                            icon: "success",
                            button: "Ok!",
                            });
                    } else {
                        alert("Incorrect File Format");
                    }
                }
            }
    }); 
}); 
//    code for quote lines

    function quote_line_create() {
        var quote_id = '<?= $quote->id ?>';
        var quote_product_line_id = $('#order_product_line_id').val();
        if (quote_product_line_id != null && quote_product_line_id != '') {
            $.ajax({
                url: "{{url('admin/insert_quote_lines')}}",
                method: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {quote_id: quote_id, quote_product_line_id: quote_product_line_id},
                beforeSend: function () {
                    //                        Swal.showLoading();
                },
                onError: function () {
                    swal("Error!", 'Something Went Wrong', "error");
                },
                success: function (response)
                {
                    load_quote_lines();

                }
            });
        } else {
            alert('Please try again');
        }
    }
    load_quote_lines();
    function load_quote_lines() {
        var quote_id = '<?= $quote->id ?>';

        $.ajax({
            url: "{{url('admin/load_quote_lines')}}",
            method: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {quote_id: quote_id},
            beforeSend: function () {
                //                        Swal.showLoading();
            },
            onError: function () {
                swal("Error!", 'Something Went Wrong', "error");
            },
            success: function (response)
            {
                $('#load_quote_lines').html(response);
                $('.triggered_quote').trigger('click');
                setTimeout(function () {
                    //code for formatting fob price
                    $(".format_me").each(function () {
                        var n = parseInt($(this).val().replace(/\D/g,''),10);
                        if (isNaN(n)) {
                            n = '';
                        }else{
                            $(this).val(n.toLocaleString());
                        }
                    });
                    $('.format_me_in_decimals').trigger('click');
                    $(".format_me_in_decimals").each(function (evt) {
                        if (evt.which != 190 ){//not a fullstop
                            var n = parseFloat($(this).val().replace(/\,/g,''),10);
                            if (isNaN(n)) {
                                n = '';
                            }else{
                                $(this).val(n.toLocaleString());
                            }
                        }
                    });
                    // Do something after 1 second 
                }, 1000);

            }
        });
    }

    $(document).on('change', '.order_supplier_id', function () {
        var supplier_id = $('.order_supplier_id').val();
//        alert(supplier_id);
        $.ajax({
            url: "{{url('admin/getSupplierProductsForOrders')}}",
            method: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {supplier_id: supplier_id},

            beforeSend: function () {
                //                        Swal.showLoading();
            },
            success: function (response)
            {
                if (response) {
                    if (response.status) {
                        $('.order_product_id').html(response.supplier_html);
//                        $('#modalCloseSupplier').trigger('click');
//                        swal("Good job!", "New Supplier Added! Now you can select supplier from dropdown", "success");
                    }
                }
            }
        });
    });
    $(document).on('change', '.order_product_id', function () {
        var product_id = $('.order_product_id').val();
        $.ajax({
            url: "{{url('admin/getproductdetailsonorders')}}",
            method: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {product_id: product_id},

            beforeSend: function () {
                //                        Swal.showLoading();
            },
            success: function (response)
            {
                if (response) {
                    if (response.status) {

                        //appended data in supplier product popup on order.edit
                        $('.product_name').html(response.product.name);
                        $(".product_image img:last-child").remove();
                        $('.product_image').append(response.product_img);

                        $('#order_product_line_id').val(response.product.id);
                    }
                }
            }
        });
    });

    $(document).on('change click keyup', '.quote_attachments', function () {
        $('#quote_attachments').submit();
    });
    //for submitting order documents data on keyup in orders
    $('#quote_attachments').on('submit', function (e) {

        e.preventDefault();

        var form_data = new FormData(this);

        form_data.append("_token", $('meta[name="csrf-token"]').attr('content'));
        $.ajax({
            url: "{{url('admin/submit_quote_attachments')}}",
            method: "POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                //                        Swal.showLoading();
            },
            onError: function () {
                swal("Error!", 'Something Went Wrong', "error");
            },
            success: function (response)
            {

                if (response) {
                    if (response.status) {
                        console.log('Attachments saved');
                    }
                }
            }
        });
    });
    //ends
</script> 
@endsection
