<style>

    .inner_table input {
        width: 120px;
    }
</style>
<!---------------------------- Modal for show hide column code-------------------------->
<div class="modal fade" id="showQuoteHideModalLongnew" tabindex="-1" role="dialog" aria-labelledby="showQuoteHideModalLongNewTitle" aria-hidden="true">
    <div class="modal-dialog quote_model" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background: #397657;">
                <h5 class="modal-title" id="showQuoteHideModalLongNewTitle" style="color:#fff;">Show / hide Columns</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="overflow-y: auto;">
                <div id='control_sh'>
                    <div id="showhidecolumns">
                        <div class="show_hide">
                            <div class="row">
                                <div class="col-md-6"><input type="checkbox" class="hide_show" checked><span>Id</span><br>
                                    <input type="checkbox" class="hide_show" checked><span>Product Name</span><br>
                                    <input type="checkbox" class="hide_show" checked><span>Product Length (cm)</span><br>
                                    <input type="checkbox" class="hide_show" checked><span>Product Width (cm)</span><br>
                                    <input type="checkbox" class="hide_show" checked><span>Product Height (cm)</span><br>
                                    <input type="checkbox" class="hide_show" checked><span>Product Weight (kg)</span><br>
                                    <input type="checkbox" class="hide_show" checked><span>Product Material</span><br>
                                    <input type="checkbox" class="hide_show" checked><span>Product CBM</span><br>
                                    <input type="checkbox" class="hide_show" checked><span>FOB Price</span><br>
                                    <input type="checkbox" class="hide_show" checked><span>FOB Currency</span><br>
                                    <input type="checkbox" class="hide_show" checked><span>Port of Origin</span><br>
                                    <input type="checkbox" class="hide_show" checked><span>Supplier Name</span><br>
                                </div>
                                <div class="col-md-6">
                                    <input type="checkbox" class="hide_show" checked><span>Product Image</span><br>
<!--                                    <input type="checkbox" class="hide_show" checked><span>Product Quantity Per Box</span><br>
                                    <input type="checkbox" class="hide_show" checked><span>Product Box length</span><br>
                                    <input type="checkbox" class="hide_show" checked><span>Product Box Width</span><br>
                                    <input type="checkbox" class="hide_show" checked><span>Product Box Height</span><br>
                                    <input type="checkbox" class="hide_show" checked><span>Product Box Weight</span><br>
                                    <input type="checkbox" class="hide_show" checked><span>Product Box CBM</span><br>
                                    <input type="checkbox" class="hide_show" checked><span>Boxes per stack</span><br>-->
                                    <input type="checkbox" class="hide_show" checked><span>Product qty per 20ft</span><br>
                                    <input type="checkbox" class="hide_show" checked><span>Product qty per 20fthq</span><br>
                                    <input type="checkbox" class="hide_show" checked><span>Product qty per 40ft</span><br>
                                    <input type="checkbox" class="hide_show" checked><span>Product qty per 40fthq</span><br>
                                    <input type="checkbox" class="hide_show"><span>Packaging Type 1</span><br>
                                    <input type="checkbox" class="hide_show"><span>Packaging Type 2</span><br>
                                    <input type="checkbox" class="hide_show"><span>Packaging Type 3</span><br>
                                    <input type="checkbox" class="hide_show"><span>Packaging Type 4</span><br>
                                    <input type="checkbox" class="hide_show"><span>Packaging Type 5</span><br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" id="modalCloseProduct" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->

<!--------------------------------show hide column code ends-------------------------->
<div class="wmd-view-topscroll">
    <div class="scroll-div1">
    </div>
</div>
<table class="table table-bordered tbl_quote_lines" id="tbl" width="100%" cellspacing="0" style="overflow-y: hidden;">
    <thead>
        <tr style="font-size: 12px;">
            <th>#</th>
            <th>Product Name </th>
            <th>Product Length (cm)</th>
            <th>Product Width (cm)</th>
            <th>Product Height (cm)</th>
            <th>Product Weight (kg)</th>
            <th>Product Material</th>
            <th>Product CBM</th>
            <th>FOB Price</th>
            <th>FOB Currency</th>
            <th>Port of Origin</th>
            <th>Supplier Name</th>
            <th>Product Image</th>
<!--            <th>Product Quantity Per Box</th>
            <th>Product Box length</th>
            <th>Product Box Width</th>
            <th>Product Box Height</th>
            <th>Product Box Weight</th>
            <th>Product Box CBM</th>
            <th>Boxes per stack</th>-->
            <th>Product quantity per 20ft</th>
            <th>Product quantity per 20fthq</th>
            <th>Product quantity per 40ft</th>
            <th>Product quantity per 40fthq</th>
            <th style="display:none; text-align: center;">Packaging Type 1</th>
            <th style="display:none; text-align: center;">Packaging Type 2</th>
            <th style="display:none; text-align: center;">Packaging Type 3</th>
            <th style="display:none; text-align: center;">Packaging Type 4</th>
            <th style="display:none; text-align: center;">Packaging Type 5</th>
            <th><i class="fa fa-cogs"></i></th>
        </tr>
    </thead>
    <tbody id="tbl_posts_body">

        <?php $i = '1'; ?>
        @foreach($quote_lines as $details)
        <tr id="rec-<?= $i ?>">
            <?php
//        dd($details->toArray());
//            $product_data = get_product_details_by_id($details->product_id);
//            if ($product_data->pic_id == null || $product_data->pic_id == '') {
                $product_pic = 'images/no_image.png';
//            } else {
//                $product_pic = $product_data->pic_id;
//            }
//            getting product image from quote line table
            if ($details->product_image == null || $details->product_image == '') {
                $quote_product_image = 'images/no_image.png';
            } else {
                $quote_product_image = getFile($details->product_image);
            }
            ?>
    <input type="hidden" value="{{$details->id}}" class="quote_row_id" name="quote_lines[<?= $i ?>][row_id]" />
    <td><span class="sn"><?= $i ?></span></td>
    <td>
        <input type="hidden" class="form-control product_id" name="quote_lines[<?= $i ?>][quote_id]" value="{{$quote->id}}"/>
        <input type="text" class="form-control product_name quote_lines shipping_plan_tracking" name="quote_lines[<?= $i ?>][product_name]"   title="{{$details->product_name}}" value="{{$details->product_name}}" style="width: 200px;" disabled=""/>
    </td>
    <td>
        <input type="text" class="form-control quote_lines shipping_plan_tracking" name="quote_lines[<?= $i ?>][product_length]" value="{{$details->product_length}}" disabled=""/>
    </td> 
    <td>
        <input type="text" class="form-control quote_lines shipping_plan_tracking" name="quote_lines[<?= $i ?>][product_width]" value="{{$details->product_width}}" disabled=""/>
    </td> 
    <td>
        <input type="text" class="form-control quote_lines shipping_plan_tracking" name="quote_lines[<?= $i ?>][product_height]" value="{{$details->product_height}}" disabled=""/>
    </td> 
    <td>
        <input type="text" class="form-control quote_lines shipping_plan_tracking" name="quote_lines[<?= $i ?>][product_weight]" value="{{$details->product_weight}}" disabled=""/>
    </td> 
    <td>
        <input type="text" class="form-control quote_lines shipping_plan_tracking" name="quote_lines[<?= $i ?>][product_material]" value="{{$details->product_material}}" style="width: 140px;" disabled=""/>
    </td>           
    <td>
        <input type="text" class="form-control quote_lines shipping_plan_tracking" name="quote_lines[<?= $i ?>][product_cbm]" value="{{$details->product_cbm}}" disabled=""/>
    </td> 
    <td>
        <input type="text" class="form-control quote_lines format_me_in_decimals shipping_plan_tracking" name="quote_lines[<?= $i ?>][product_unit_price]" value="{{$details->product_unit_price}}" style="width: 140px;" disabled=""/>
    </td> 
    <td>
        <input type="text" class="form-control quote_lines shipping_plan_tracking" name="quote_lines[<?= $i ?>][product_unit_currency]" value="{{$details->product_unit_currency}}" disabled=""/>
    </td> 
    <td>
        <select class="form-control quote_lines shipping_plan_tracking select_webkit_hide " name="quote_lines[<?= $i ?>][shipping_port]" style="width: 140px;" disabled=""><?php $data = get_shipping_port_list(); ?>
            <option value="">Port of Origins</option>
            @foreach($data as $key => $value)
            @if($key == $details->shipping_port)
            <option value="{{$key}}" selected>{{$value}}</option>
            @else
            <option value="{{$key}}">{{$value}}</option>
            @endif
            @endforeach
        </select>

    </td>

    <td>
        <input type="text" class="form-control quote_lines shipping_plan_tracking" name="quote_lines[<?= $i ?>][supplier]" value="{{get_supplier_name_by_id($details->supplier)}}" style="width: 140px;" disabled=""/>
    </td> 

    <td>

        <input type="file" class="hidden_quote_image_input_div edit_quote_image quote_lines shipping_plan_tracking" name="quote_lines[<?= $i ?>][product_image]" id ="quote_image_<?= $details->id ?>" value=""/>
        <img src="{{url("$quote_product_image")}}" width="50"/>
        <!--<div class = "edit_delete">-->
            <!--<a href="javascript:void(0)"><span class="fa fa-pencil edit_pencil" aria-hidden="true" onclick = "editImage()"></span></a>-->


            <!--<a href="javascript:void(0)"><span class="fa fa-times remove_image" aria-hidden="true"></span></a>-->
        <!--</div>-->
    </td> 

    <td>
        <input type="text" class="form-control quote_lines shipping_plan_tracking" name="quote_lines[<?= $i ?>][product_quantity_per_20ft]" value="{{$details->product_quantity_per_20ft}}" disabled=""/>
    </td> 
    <td>
        <input type="text" class="form-control quote_lines shipping_plan_tracking" name="quote_lines[<?= $i ?>][product_quantity_per_20fthq]" value="{{$details->product_quantity_per_20fthq}}" disabled=""/>
    </td> 
    <td>
        <input type="text" class="form-control quote_lines shipping_plan_tracking" name="quote_lines[<?= $i ?>][product_quantity_per_40ft]" value="{{$details->product_quantity_per_40ft}}" disabled=""/>
    </td> 
    <td>
        <input type="text" class="form-control quote_lines shipping_plan_tracking" name="quote_lines[<?= $i ?>][product_quantity_per_40fthq]" value="{{$details->product_quantity_per_40fthq}}" disabled=""/>
    </td> 
    <?php 
    $packaging_type = App\Models\ProductPackagingOption::where('product_id', $details->product_id)->get()->toArray();
//    dd($packaging_type);
    for($k = 0; $k < 5; $k++){
        $j = $k+1;
 ?>
    <td style="display:none;">
        <table class="inner_table">
            <tr>
                <th>Type</th>
                <th>Quantity</th>
                <th>Length (cm)</th>
                <th>Width (cm)</th>
                <th>Height (cm)</th>
                <th>Gross Weight (kg)</th>
                <th>Packaging CBM</th>
                <th>Floor Ready</th>
                <th>Stackable</th>
                <th>Nested Product</th>
            </tr>
            <tr>
                <th>
                    <?php
                        $type = get_package_type_list();
                        $select = ['1' => 'Yes', '2' => 'No'];
                        $typeValue = 'NA';
                        foreach($type as $key => $value):
                            if(isset($packaging_type[$k]['packaging_type']) && $key == $packaging_type[$k]['packaging_type']):
                                $typeValue = $value;
                            endif;
                        endforeach;
                    ?>
                    <input type="text" class="form-control shipping_plan_tracking packaging_type_<?= $j ?>" name="order_pricing[<?= $i ?>][packaging_type_<?= $j ?>]" value="<?= $typeValue ?>" disabled=""/>
                </th>
                <th>
                <input type="text" class="form-control shipping_plan_tracking packaging_type_<?= $j ?>" name="order_pricing[<?= $i ?>][packaging_type_<?= $j ?>]" value="<?php
                if (isset($packaging_type[$k]['quantity_per_page'])) {
                    echo $packaging_type[$k]['quantity_per_page'];
                }else{
                    echo 'NA';
                }
                ?>" disabled=""/>
                </th>
                <th>
                <input type="text" class="form-control shipping_plan_tracking packaging_type_<?= $j ?>" name="order_pricing[<?= $i ?>][packaging_type_<?= $j ?>]" value="<?php
                if (isset($packaging_type[$k]['packaging_length'])) {
                    echo $packaging_type[$k]['packaging_length'];
                }else{
                    echo 'NA';
                }
                ?>" disabled=""/>
                </th>
                <th>
                <input type="text" class="form-control shipping_plan_tracking packaging_type_<?= $j ?>" name="order_pricing[<?= $i ?>][packaging_type_<?= $j ?>]" value="<?php
                if (isset($packaging_type[$k]['packaging_width'])) {
                    echo $packaging_type[$k]['packaging_width'];
                }else{
                    echo 'NA';
                }
                ?>" disabled=""/>
                </th>
                <th>
                <input type="text" class="form-control shipping_plan_tracking packaging_type_<?= $j ?>" name="order_pricing[<?= $i ?>][packaging_type_<?= $j ?>]" value="<?php
                if (isset($packaging_type[$k]['packaging_height'])) {
                    echo $packaging_type[$k]['packaging_height'];
                }else{
                    echo 'NA';
                }
                ?>" disabled=""/>
                </th>
                <th>
                <input type="text" class="form-control shipping_plan_tracking packaging_type_<?= $j ?>" name="order_pricing[<?= $i ?>][packaging_type_<?= $j ?>]" value="<?php
                if (isset($packaging_type[$k]['gross_weight_including_packaging'])) {
                    echo $packaging_type[$k]['gross_weight_including_packaging'];
                }else{
                    echo 'NA';
                }
                ?>" style="width : 140px;" disabled=""/>
                </th>
                <th>
                <input type="text" class="form-control shipping_plan_tracking packaging_type_<?= $j ?>" name="order_pricing[<?= $i ?>][packaging_type_<?= $j ?>]" value="<?php
                if (isset($packaging_type[$k]['packaging_cbm'])) {
                    echo $packaging_type[$k]['packaging_cbm'];
                }else{
                    echo 'NA';
                }
                ?>" style="width : 140px;" disabled=""/>
                </th>
                <th>
                <input type="text" class="form-control shipping_plan_tracking packaging_type_<?= $j ?>" name="order_pricing[<?= $i ?>][packaging_type_<?= $j ?>]" value="<?php
                if (isset($packaging_type[$k]['floor_ready'])) {
                    if($packaging_type[$k]['floor_ready'] == '1'){
                        echo 'Yes';
                    }else if($packaging_type[$k]['floor_ready'] == '2'){
                        echo 'No';
                    }else{
                        echo 'NA';
                    }
                }else{
                    echo 'NA';
                }
                ?>" disabled=""/>
                </th>
                <th>
                <input type="text" class="form-control shipping_plan_tracking packaging_type_<?= $j ?>" name="order_pricing[<?= $i ?>][packaging_type_<?= $j ?>]" value="<?php
                if (isset($packaging_type[$k]['stackable'])) {
                    if($packaging_type[$k]['stackable'] == '1'){
                        echo 'Yes';
                    }else if($packaging_type[$k]['stackable'] == '2'){
                        echo 'No';
                    }else{
                        echo 'NA';
                    }
                }else{
                    echo 'NA';
                }
                ?>" disabled=""/>
                </th>
                <th>
                <input type="text" class="form-control shipping_plan_tracking packaging_type_<?= $j ?>" name="order_pricing[<?= $i ?>][packaging_type_<?= $j ?>]" value="<?php
                if (isset($packaging_type[$k]['nested_product'])) {
                    if($packaging_type[$k]['nested_product'] == '1'){
                        echo 'Yes';
                    }else if($packaging_type[$k]['nested_product'] == '2'){
                        echo 'No';
                    }else{
                        echo 'NA';
                    }
                }else{
                    echo 'NA';
                }
                ?>" disabled=""/>
                </th>
            </tr>
        </table>
    </td>
    <?php } ?>
<?php $product_id = $details->product_id;  ?>
    <td>
        <a href="<?php echo url('admin/products/'.$product_id.'/edit?quoteLineId='.$details->id); ?>" class="btn btn-xs" data-id="<?= $i ?>"><i class="fa fa-pencil"></i></a>

        <a class="btn btn-xs delete-record" data-id="<?= $i ?>" onclick="deleteOrderDetail('quote_lines',{{$details->id}});"><i class="fa fa-trash"></i></a>
    </td>
</tr>
<?php $i++; ?>
@endforeach

</tbody>
</table>