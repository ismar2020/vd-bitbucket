@extends('layouts.backend')

@section('content') 
<div class="container">
    <div class="row">
        @include('admin.sidebar')

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Profile</div>
                <div class="card-body">
                    @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    @endif
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#supplier_home"  id="home_active">Profile</a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="supplier_home" class="tab-pane fade in active">
                            <br>
                            <div class="row">
                                <?php //dd(\Auth::user()); ?>
                                <!--<div class="col-md-2"></div>-->
                                <div class="col-md-12">
                                    <div class="profile_img">
                                        <div class="container">
                                            <div class="row">

                                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-6 col-lg-offset-3 toppad" >


                                    <div class="panel panel-info">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">{{\Auth::user()->first_name .' '.\Auth::user()->last_name}}</h3>
                                        </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class=" col-md-9 col-lg-9 "> 
                                                <table class="table table-user-information">
                                                    <tbody>
                                                        <tr>
                                                            <td>Username :</td>
                                                            <td>{{\Auth::user()->username}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Email :</td>
                                                            <td>{{\Auth::user()->email}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Role :</td>
                                                            <td><b>{{\Auth::user()->roles->first()->name}}</b></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Mobile :</td>
                                                            <td>
                                                                <?php if(\Auth::user()->mobile != null && \Auth::user()->mobile != ''){ echo \Auth::user()->mobile;  } else { echo 'NA'; } ?>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                        <tr>
                                                            <td>Company :</td>
                                                            <td><?php if(\Auth::user()->company_id != null && \Auth::user()->company_id != ''){ echo get_companies_name_by_id(\Auth::user()->company_id);  } else { echo 'NA'; } ?></td>
                                                        </tr>

                                                    </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                                    </div>
                                                </div>
                                            </div>
<!--                                            <div class="form-group">
                                                {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
                                            </div>-->
                                        </div>
                                        <div class="col-md-4"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection
