<div class="form-group{{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('length') ? 'has-error' : ''}}">
    {!! Form::label('length', 'Length', ['class' => 'control-label']) !!}
    {!! Form::text('length', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('length', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('width') ? 'has-error' : ''}}">
    {!! Form::label('width', 'Width', ['class' => 'control-label']) !!}
    {!! Form::text('width', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('width', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('height') ? 'has-error' : ''}}">
    {!! Form::label('height', 'Height', ['class' => 'control-label']) !!}
    {!! Form::text('height', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('height', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('max_weight') ? 'has-error' : ''}}">
    {!! Form::label('max_weight', 'Max Weight', ['class' => 'control-label']) !!}
    {!! Form::text('max_weight', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('max_weight', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('container_cbm') ? 'has-error' : ''}}">
    {!! Form::label('container_cbm', 'Container Cbm', ['class' => 'control-label']) !!}
    {!! Form::text('container_cbm', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('container_cbm', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
