@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

           <div class="col-md-12">
                <div class="card">
                    <div class="card-header">ProfitMargin {{ $profitmargin->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/profit-margin') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/admin/profit-margin/' . $profitmargin->id . '/edit') }}" title="Edit ProfitMargin"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/profitmargin', $profitmargin->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'type' => 'submit',
'class' => 'btn btn-danger btn-sm deleteentry',
                                    'title' => 'Delete ProfitMargin',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $profitmargin->id }}</td>
                                    </tr>
                                    <tr><th> Type </th><td> {{ $profitmargin->type }} </td></tr><tr><th> Margin </th><td> {{ $profitmargin->margin }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
