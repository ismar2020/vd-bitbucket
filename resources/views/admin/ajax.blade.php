<script>
    var APP_URL = {!! json_encode(url('/')) !!}
    var getAllCurrencyExhangeJson = '';
//Calculation code for number_of_units_that_fit in shipping container products page 
    $(document).on('change', '.number_of_units_that_fit', function () {
        var number_of_units = $(this).val();
        var rowId = $(this).closest('tr').attr('id');
        var currentRow = $("#" + rowId);
        var product_id = currentRow.find(".ship_product_id").val();
        var container_id = $(".ship_container_id").val();
        if (number_of_units == '1') {
            var dimension_name = 'box';
        } else if (number_of_units == '2') {
            var dimension_name = 'stack';
        } else if (number_of_units == '3') {
            var dimension_name = 'product';
        } else if (number_of_units == '4') {
            var dimension_name = 'stack_opt';
        } else if (number_of_units == '5') {
            var dimension_name = 'product_opt';
        } else if (number_of_units == '6') {
            var dimension_name = 'box_opt';
        } else if (number_of_units == '7') {
            var dimension_name = 'box_quantity';
        } else if (number_of_units == '8') {
            var dimension_name = 'stack_quantity';
        } else if (number_of_units == '9') {
            var dimension_name = 'product_quantity';
        }
        $.ajax({
            url: "{{url('admin/load_dimension_details')}}",
            method: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {product_id: product_id, container_id: container_id, dimension_name: dimension_name},
            beforeSend: function () {
                //                        Swal.showLoading();
            },
            onError: function () {
                swal("Error!", 'Something Went Wrong', "error");
            },
            success: function (response)
            {
                if (dimension_name === 'box') {
                    if (response.status === 'true') {
                        currentRow.find(".ship_container_cbm").val(response.container.container_cbm);
                        //finding number of units that fit
                        var number_of_units_fits = (Math.floor(response.container.length / response.product.product_box_length) * Math.floor(response.container.width / response.product.product_box_width) * Math.floor(response.container.height / response.product.product_box_height) * response.product.product_quantity_per_box);
                        //check if max weight is greater then 0 or not
                        if ((response.container.max_weight - ((number_of_units_fits / response.product.product_quantity_per_box) * response.product.product_box_weight)) > '0') {

                            //if greater then 0 then result will be number_of-units that concluded above
                            var res = number_of_units_fits;
                            //res assigning to respective option 
                            currentRow.find('.number_of_units_that_fit option:contains("Box Dimension")').text('Box Dimension (' + res + ')').val(res);
                        } else {

                            //if above less then 0 then this calculation works and assign to select
                            var res = (Math.floor(response.container.max_weight / response.product.product_box_weight) * response.product.product_quantity_per_box);
                            //res assigning to respective option 
                            currentRow.find('.number_of_units_that_fit option:contains("Box Dimension")').text('Box Dimension -WR (' + res + ')').val(res);
                        }
                    } else {
                        //if products respective length,width,height,weight not available
                        currentRow.find('.number_of_units_that_fit option:contains("Box Dimension")').text('Box Dimension (No data Available)');
                    }
                } else if (dimension_name === 'stack') {
                    if (response.status === 'true') {
                        currentRow.find(".ship_container_cbm").val(response.container.container_cbm);
                        //finding number of units that fit
                        var number_of_units_fits = (Math.floor(response.container.length / response.product.stack_length) * Math.floor(response.container.width / response.product.stack_width) * Math.floor(response.container.height / response.product.stack_height) * response.product.units_per_stack);
                        //check if max weight is greater then 0 or not
                        if ((response.container.max_weight - ((number_of_units_fits / response.product.units_per_stack) * response.product.stack_weight)) > '0') {

                            //if greater then 0 then result will be number_of-units that concluded above
                            var res = number_of_units_fits;
                            //res assigning to respective option 
                            currentRow.find('.number_of_units_that_fit option:contains("Stack Dimension")').text('Stack Dimension (' + res + ')').val(res);
                        } else {

                            //if above less then 0 then this calculation works and assign to select
                            var res = (Math.floor(response.container.max_weight / response.product.stack_weight) * response.product.units_per_stack);
                            //res assigning to respective option 
                            currentRow.find('.number_of_units_that_fit option:contains("Stack Dimension")').text('Stack Dimension -WR (' + res + ')').val(res);
                        }
                    } else {
                        //if products respective length,width,height,weight not available
                        currentRow.find('.number_of_units_that_fit option:contains("Stack Dimension")').text('Stack Dimension (No data Available)');
                    }
                } else if (dimension_name === 'product') {
                    if (response.status === 'true') {
                        currentRow.find(".ship_container_cbm").val(response.container.container_cbm);
                        //finding number of units that fit
                        var number_of_units_fits = (Math.floor(response.container.length / response.product.product_length) * Math.floor(response.container.width / response.product.product_width) * Math.floor(response.container.height / response.product.product_height));
                        //check if max weight is greater then 0 or not
                        if ((response.container.max_weight - (number_of_units_fits * response.product.product_weight)) > '0') {

                            //if greater then 0 then result will be number_of-units that concluded above
                            var res = number_of_units_fits;
                            //res assigning to respective option 
                            currentRow.find('.number_of_units_that_fit option:contains("Product Dimension")').text('Product Dimension (' + res + ')').val(res);
                        } else {

                            //if above less then 0 then this calculation works and assign to select
                            var res = (Math.floor(response.container.max_weight / response.product.product_weight));
                            //res assigning to respective option 
                            currentRow.find('.number_of_units_that_fit option:contains("Product Dimension")').text('Product Dimension -WR (' + res + ')').val(res);
                        }
                    } else {
                        //if products respective length,width,height,weight not available
                        currentRow.find('.number_of_units_that_fit option:contains("Product Dimension")').text('Product Dimension (No data Available)');
                    }
                } else if (dimension_name === 'stack_opt') {
                    if (response.status === 'true') {
                        currentRow.find(".ship_container_cbm").val(response.container.container_cbm);
                        //finding max value from stack length, width & height for optimiszation
                        var max = Math.max(response.product.stack_length, response.product.stack_width, response.product.stack_height);
                        //if stack width is greater
                        if (response.product.stack_width === max) {
                            var stack_width = response.product.stack_length;
                            var stack_length = response.product.stack_width;
                            var stack_height = response.product.stack_height;
                        } else if (response.product.stack_height === max) {
                            var stack_height = response.product.stack_length;
                            var stack_length = response.product.stack_height;
                            var stack_width = response.product.stack_width;
                        } else {
                            var stack_length = response.product.stack_length;
                            var stack_width = response.product.stack_width;
                            var stack_height = response.product.stack_height;
                        }
//                        console.log(stack_length,stack_width,stack_height);
                        //finding number of units that fit
                        var number_of_units_fits = (Math.floor(response.container.length / stack_length) * Math.floor(response.container.width / stack_width) * Math.floor(response.container.height / stack_height) * response.product.units_per_stack);
                        //check if max weight is greater then 0 or not
                        if ((response.container.max_weight - ((number_of_units_fits / response.product.units_per_stack) * response.product.stack_weight)) > '0') {

                            //if greater then 0 then result will be number_of-units that concluded above
                            var res = number_of_units_fits;
                            //res assigning to respective option 
                            currentRow.find('.number_of_units_that_fit option:contains("Stack Opt Dimension")').text('Stack Opt Dimension (' + res + ')').val(res);
                        } else {

                            //if above less then 0 then this calculation works and assign to select
                            var res = (Math.floor(response.container.max_weight / response.product.stack_weight) * response.product.units_per_stack);
                            //res assigning to respective option 
                            currentRow.find('.number_of_units_that_fit option:contains("Stack Opt Dimension")').text('Stack Opt Dimension -WR (' + res + ')').val(res);
                        }
                    } else {
                        //if products respective length,width,height,weight not available
                        currentRow.find('.number_of_units_that_fit option:contains("Stack Opt Dimension")').text('Stack Opt Dimension (No data Available)');
                    }
                } else if (dimension_name === 'product_opt') {
                    if (response.status === 'true') {
                        currentRow.find(".ship_container_cbm").val(response.container.container_cbm);
                        //finding max value from product length, width & height for optimiszation
                        var max = Math.max(response.product.product_length, response.product.product_width, response.product.product_height);
                        //if product width is greater
                        if (response.product.product_width === max) {
                            var product_width = response.product.product_length;
                            var product_length = response.product.product_width;
                            var product_height = response.product.product_height;
                        } else if (response.product.product_height === max) {
                            var product_height = response.product.product_length;
                            var product_length = response.product.product_height;
                            var product_width = response.product.product_width;
                        } else {
                            var product_length = response.product.product_length;
                            var product_width = response.product.product_width;
                            var product_height = response.product.product_height;
                        }
//                        console.log(product_length,product_width,product_height);

                        //finding number of units that fit
                        var number_of_units_fits = (Math.floor(response.container.length / product_length) * Math.floor(response.container.width / product_width) * Math.floor(response.container.height / product_height));
                        //check if max weight is greater then 0 or not
                        if ((response.container.max_weight - (number_of_units_fits * response.product.product_weight)) > '0') {

                            //if greater then 0 then result will be number_of-units that concluded above
                            var res = number_of_units_fits;
                            //res assigning to respective option 
                            currentRow.find('.number_of_units_that_fit option:contains("Product Opt Dimension")').text('Product Opt Dimension (' + res + ')').val(res);
                        } else {

                            //if above less then 0 then this calculation works and assign to select
                            var res = (Math.floor(response.container.max_weight / response.product.product_weight));
                            //res assigning to respective option 
                            currentRow.find('.number_of_units_that_fit option:contains("Product Opt Dimension")').text('Product Opt Dimension -WR (' + res + ')').val(res);
                        }
                    } else {
                        //if products respective length,width,height,weight not available
                        currentRow.find('.number_of_units_that_fit option:contains("Product Opt Dimension")').text('Product Opt Dimension (No data Available)');
                    }
                } else if (dimension_name === 'box_opt') {
                    if (response.status === 'true') {
                        currentRow.find(".ship_container_cbm").val(response.container.container_cbm);
                        //finding max value from Product box length, width & height for optimiszation
                        var max = Math.max(response.product.product_box_length, response.product.product_box_width, response.product.product_box_height);
                        //if product Box width is greater
                        if (response.product.product_box_width === max) {
                            var product_box_width = response.product.product_box_length;
                            var product_box_length = response.product.product_box_width;
                            var product_box_height = response.product.product_box_height;
                        } else if (response.product.product_box_height === max) {
                            var product_box_height = response.product.product_box_length;
                            var product_box_length = response.product.product_box_height;
                            var product_box_width = response.product.product_box_width;
                        } else {
                            var product_box_length = response.product.product_box_length;
                            var product_box_width = response.product.product_box_width;
                            var product_box_height = response.product.product_box_height;
                        }
//                        console.log(product_box_length,product_box_width,product_box_height);

                        //finding number of units that fit
                        var number_of_units_fits = (Math.floor(response.container.length / product_box_length) * Math.floor(response.container.width / product_box_width) * Math.floor(response.container.height / product_box_height) * response.product.product_quantity_per_box);
                        //check if max weight is greater then 0 or not
                        if ((response.container.max_weight - ((number_of_units_fits / response.product.product_quantity_per_box) * response.product.product_box_weight)) > '0') {

                            //if greater then 0 then result will be number_of-units that concluded above
                            var res = number_of_units_fits;
                            //res assigning to respective option 
                            currentRow.find('.number_of_units_that_fit option:contains("Box Opt Dimension")').text('Box Opt Dimension (' + res + ')').val(res);
                        } else {

                            //if above less then 0 then this calculation works and assign to select
                            var res = (Math.floor(response.container.max_weight / response.product.product_box_weight) * response.product.product_quantity_per_box);
                            //res assigning to respective option 
                            currentRow.find('.number_of_units_that_fit option:contains("Box Opt Dimension")').text('Box Opt Dimension -WR (' + res + ')').val(res);
                        }
                    } else {
                        //if products respective length,width,height,weight not available
                        currentRow.find('.number_of_units_that_fit option:contains("Box Opt Dimension")').text('Box Opt Dimension (No data Available)');
                    }
                } else if (dimension_name === 'box_quantity') {
                    if (response.status === 'true') {
                        currentRow.find(".ship_container_cbm").val(response.container.container_cbm);
                        //finding number of units that fit
                        var number_of_units_fits = (response.product.product_quantity_per_box * response.box_quantity);
//                        console.log(number_of_units_fits);
                        //number_of_units_fits assigning to respective option 
                        currentRow.find('.number_of_units_that_fit option:contains("Box Quantity")').text('Box Quantity (' + number_of_units_fits + ')').val(number_of_units_fits);
                    } else {
                        //if products respective length,width,height,weight not available
                        currentRow.find('.number_of_units_that_fit option:contains("Box Quantity")').text('Box Quantity (No data Available)');
                    }
                } else if (dimension_name === 'stack_quantity') {
                    if (response.status === 'true') {
                        currentRow.find(".ship_container_cbm").val(response.container.container_cbm);
                        //finding number of units that fit
                        var number_of_units_fits = (response.product.units_per_stack * response.stack_quantity);
//                        console.log(number_of_units_fits);
                        //number_of_units_fits assigning to respective option 
                        currentRow.find('.number_of_units_that_fit option:contains("Stack Quantity")').text('Stack Quantity (' + number_of_units_fits + ')').val(number_of_units_fits);
                    } else {
                        //if products respective length,width,height,weight not available
                        currentRow.find('.number_of_units_that_fit option:contains("Stack Quantity")').text('Stack Quantity (No data Available)');
                    }
                } else if (dimension_name === 'product_quantity') {
                    if (response.status === 'true') {
                        currentRow.find(".ship_container_cbm").val(response.container.container_cbm);
                        //finding number of units that fit
                        var number_of_units_fits = (response.product_quantity);
//                        console.log(number_of_units_fits);
                        //number_of_units_fits assigning to respective option 
                        currentRow.find('.number_of_units_that_fit option:contains("Product Quantity")').text('Product Quantity (' + number_of_units_fits + ')').val(number_of_units_fits);
                    } else {
                        //if products respective length,width,height,weight not available
                        currentRow.find('.number_of_units_that_fit option:contains("Product Quantity")').text('Product Quantity (No data Available)');
                    }
                }

            }
        });
    });
    $(document).on('keyup', '#stack_length,#stack_width,#stack_height', function () {
//                alert('d');
        var l = $("#stack_length").val();
        var w = $("#stack_width").val();
        var h = $("#stack_height").val();
        var cbm = l * w * h;
        $('#stack_cbm').val(cbm);
    });
    $(document).on('keyup', '#product_box_length,#product_box_width,#product_box_height', function () {
//                alert('d');
        var l = $("#product_box_length").val();
        var w = $("#product_box_width").val();
        var h = $("#product_box_height").val();
        var cbm = l * w * h;
        $('#product_box_cbm').val(cbm);
    });
    //code for dynamic submission for form product quantity per container
    $(document).on('change click keyup', '.product_quantity_per_container', function () {

        $('#product_quantity_per_container_with_product').submit();
    });
    $('#product_quantity_per_container_with_product').on('submit', function (e) {

        e.preventDefault();
        var form_data = new FormData(this);
        form_data.append("_token", $('meta[name="csrf-token"]').attr('content'));
        $.ajax({
            url: "{{url('admin/product_quantity_per_container')}}",
            method: "POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                //                        Swal.showLoading();
            },
            onError: function () {
                swal("Error!", 'Something Went Wrong', "error");
            },
            success: function (response)
            {

                if (response) {
                    if (response.status) {
                        console.log('product_quantity_per_container saved');
                    }
                }
            }
        });
    });
    //code for dynamic submission for form in contacts
    $(document).on('change keyup', 'form#submit_contact_form input, form#submit_contact_form select', function () {

        $('#submit_contact_form').submit();
    });
    $('#submit_contact_form').on('submit', function (e) {

        e.preventDefault();
        var form_data = new FormData(this);
        form_data.append("_token", $('meta[name="csrf-token"]').attr('content'));
        $.ajax({
            url: "{{url('admin/submit_contact_form')}}",
            method: "POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                //                        Swal.showLoading();
            },
            onError: function () {
                swal("Error!", 'Something Went Wrong', "error");
            },
            success: function (response)
            {

                if (response) {
                    if (response.status) {
                        console.log('product_quantity_per_container saved');
                    }
                }
            }
        });
    });
    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }

    //***********CODE for formatting of the digits*******************

    //code for formatting integers not decimals
    $(window).load(function () {
        // executes when complete page is fully loaded, including all frames, objects and images
        $('.format_me').trigger('click');
        $('.format_me_in_decimals').trigger('click');
    });

    $(document).on('click keyup', '.format_me', function () {
        var n = parseInt($(this).val().replace(/\D/g, ''), 10);
        if (isNaN(n)) {
            n = '';
        } else {
            $(this).val(n.toLocaleString());
        }
    }).keyup();

    //code for formatting Decimals Values of textbox
    $(document).on('click keyup', '.format_me_in_decimals', function (evt) {
        if (evt.which != 190) {//not a fullstop
            var n = parseFloat($(this).val().replace(/\,/g, ''), 10);
            if (isNaN(n)) {
                n = '';
            } else {
                $(this).val(n.toLocaleString('en', {maximumFractionDigits: 2}));
            }
        }
    }).keyup();

    //show hide plus minus
    $('.show_hide_icon').click(function () {
        if ($('#viewcollapse').hasClass('show')) {
            $('.show_hide_icon i').addClass('fa-plus');
            $('.show_hide_icon i').removeClass('fa-minus');
        } else {
            $('.show_hide_icon i').addClass('fa-minus');
            $('.show_hide_icon i').removeClass('fa-plus');
        }
//                    alert('s');
    });

    //show hide plus minus in tracking collapse
    $('.show_hide_tracking_collapse_icon').click(function () {
        if ($('#viewTrackingCollapse').hasClass('show')) {
            $('.show_hide_tracking_collapse_icon i').addClass('fa-plus');
            $('.show_hide_tracking_collapse_icon i').removeClass('fa-minus');
        } else {
            $('.show_hide_tracking_collapse_icon i').addClass('fa-minus');
            $('.show_hide_tracking_collapse_icon i').removeClass('fa-plus');
        }
    });
    //show hide plus minus in order-overview collapse
    $('.show_hide_order_overview_collapse_icon').click(function () {
        if ($('#viewOrderOverviewCollapse').hasClass('show')) {
            $('.show_hide_order_overview_collapse_icon i').addClass('fa-plus');
            $('.show_hide_order_overview_collapse_icon i').removeClass('fa-minus');
            
            setTimeout(function () {
                $('#tbl_load_order_overview_info').css('display','block');
            }, 500);
        } else {
            
            $('.show_hide_order_overview_collapse_icon i').addClass('fa-minus');
            $('.show_hide_order_overview_collapse_icon i').removeClass('fa-plus');
            $('#tbl_load_order_overview_info').css('display','none');
        }
    });
    //show hide plus minus in tracking products collapse
//            $(document).on('click', '.show_hide_tracking_products_collapse_icon', function() {
//                var rowId = $(this).closest('tr').attr('id');
//                var currentRow = $("#" + rowId);
//                console.log(rowId);
//                console.log($('#viewTrackingProductsCollapse_'+rowId).hasClass('show'));
//                if($('#viewTrackingProductsCollapse_'+rowId).hasClass('show')){
//                     currentRow.find('.show_hide_tracking_products_collapse_icon i').addClass('fa-plus');
//                     currentRow.find('.show_hide_tracking_products_collapse_icon i').removeClass('fa-minus');
//                }else{
//                     currentRow.find('.show_hide_tracking_products_collapse_icon i').addClass('fa-minus');
//                     currentRow.find('.show_hide_tracking_products_collapse_icon i').removeClass('fa-plus');
//                }
//            });

    function changeIcon(instance) {
        var rowID = instance.closest('tr').attr('id');
        var currentRow = $("#" + rowID);
        var innertableId = instance.closest('table').find('.checkforicon').attr('id')
//                    console.log($('#'+innertableId).hasClass('show'));
        if ($('#viewTrackingProductsCollapse_' + rowID).hasClass('show')) {
            instance.closest('tr').find('i').addClass('fa-plus');
            instance.closest('tr').find('i').removeClass('fa-minus');
        } else {
            instance.closest('tr').find('i').addClass('fa-minus');
            instance.closest('tr').find('i').removeClass('fa-plus');
        }
    }
    //***********End code for formatting of the digits******************* 


//    function isNumberKey(evt)
//    {
//        var charCode = (evt.which) ? evt.which : evt.keyCode;
//        if (charCode != 46 && charCode > 31
//                && (charCode < 48 || charCode > 57))
//            return false;
//
//        return true;
//    }
//    ;
    //ends
    //code for getting current exchange rate in order pricing edit module

    $(document).on('change', '.trigger_currency_exchange', function () {
        $('.currency_exchange').click();
        $('.gp_cal').click();
        $('.order_pricing_cal').click();
    });

    $(document).on('click', '.currency_exchange', function () {
//        var quantity = $(this).val();
        var rowId = $(this).closest('tr').attr('id');
        var currentRow = $("#" + rowId);
        var product_unit_currency = currentRow.find(".product_unit_currency").data('id');
        var pricing_currency = $("#pricing_currency").val();
        //ajax
        $.ajax({
            url: "{{url('admin/get_current_exchange_rate')}}",
            method: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {product_unit_currency: product_unit_currency, pricing_currency: pricing_currency},
            beforeSend: function () {

            },
            onError: function () {
                swal("Error!", 'Something Went Wrong', "error");
            },
            success: function (response)
            {
                if (response.status == 'true') {
                    currentRow.find('.currency_exchange_rate').val(response.data.exchange_rate);
                    $('#order_pricing').submit();
                }
            }
        });
//        console.log(res);
    });
    //code for getting current exchange rate in order pricing edit module
    $(document).on('change', '.container_size', function () {
//        var quantity = $(this).val();
        var rowId = $(this).closest('tr').attr('id');
        var currentRow = $("#" + rowId);
        var container_size = currentRow.find(".container_size").val();
        var product_id = currentRow.find(".product_id").val();
//        var order_line_id = currentRow.find(".order_line_id").val();
        //ajax
        $.ajax({
            url: "{{url('admin/get_products_units_in_the_container')}}",
            method: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {container_size: container_size, product_id: product_id},
            beforeSend: function () {

            },
            onError: function () {
                swal("Error!", 'Something Went Wrong', "error");
            },
            success: function (response)
            {
                if (response.status == 'true') {
                    currentRow.find('.products_units_in_the_container').val(response.data.product_quantity);
                    $('#order_pricing').submit();
//                    load_order_linforShippingContainerCalculationes();
                    // $('.gp_cal').click()
                }
            }
        });
//        console.log(res);
    });
    //code for getting current exchange rate in order pricing edit module
    $(document).on('change', '.refresh_shipping_rate', function () {
//        var quantity = $(this).val();
        var rowId = $(this).closest('tr').attr('id');
        var currentRow = $("#" + rowId);
        var container_size = currentRow.find(".container_size").val();
        var product_id = currentRow.find(".product_id").val();
        var order_line_id = currentRow.find(".order_line_id").val();
        var port_of_dispatch = currentRow.find(".port_of_dispatch").val();
        //ajax
        $.ajax({
            url: "{{url('admin/get_refresh_shipping_rate')}}",
            method: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {container_size: container_size, product_id: product_id, order_line_id: order_line_id, port_of_dispatch: port_of_dispatch},
            beforeSend: function () {

            },
            onError: function () {
                swal("Error!", 'Something Went Wrong', "error");
            },
            success: function (response)
            {
                if (response.status == 'true') {
                    console.log('shipping rate inserted')
                }
            }
        });
//        console.log(res);
    });
    $(document).on('change click keyup', '.shipping_container_products', function () {
//          $('#shipping_container_products').submit();
        forShippingContainerCalculation();
    });
    //code for getting shipping container calculation in order > shipping plan module
    function forShippingContainerCalculation() {
//        alert('s');
        $(".table_shipping_container_products tbody tr").each(function () {
            var rowId = this.id;
//        var rowId = $(this).closest('tr').attr('id');
            var currentRow = $("#" + rowId);
//            var confirmed_quantity = currentRow.find(".confirmed_quantity").val();
            var confirmed_quantity_before_Check = currentRow.find(".confirmed_quantity").val();
            //check for variable i.e. confirmed_quantity is empty/0/null/undefined or not
            if(confirmed_quantity_before_Check != '' && confirmed_quantity_before_Check != 0){
                var confirmed_quantity = confirmed_quantity_before_Check
            }else{
                var confirmed_quantity = currentRow.find(".quantity").val();
            }
            var shipping_order_type = currentRow.find(".ship_order_type").val();
            var ship_order_line_id = currentRow.find(".ship_order_line_id").val();
//        alert(shipping_order_type);

            $.ajax({
                url: "{{url('admin/get_order_line_details_for_cal')}}",
                method: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {ship_order_line_id: ship_order_line_id},
                beforeSend: function () {

                },
                onError: function () {
                    swal("Error!", 'Something Went Wrong', "error");
                },
                success: function (response)
                {
                    // cal total cost
                    if (response.status == 'true') {
                        if (shipping_order_type == '1') {
                            var total_cost = confirmed_quantity * response.data.product_unit_price;
                            currentRow.find('.ship_total_cost').val(total_cost.toFixed(2));

                            //        getting FOB price base currnecy value
                            var converted_fob_price = 0;
                            console.log(response.data.pricing_currency, response.data.product_unit_currency, response.data.product_unit_price);
                            converted_fob_price = convertCurrentToBaseCurrency(response.data.pricing_currency, response.data.product_unit_currency, response.data.product_unit_price);
                            //                    cal customer_buy_price for FOB
                            var customer_buy_price = 0;

                            customer_buy_price = converted_fob_price * (1 + (response.data.wholesale_margin_percentage / 100)) * confirmed_quantity;

                            if (isNaN(customer_buy_price)) {
                                customer_buy_price = 0;
                            }

                            //                    customer_buy_price = total_cost * (response.data.wholesale_margin_percentage / 100);
                            currentRow.find('.ship_customer_buy_price').val(customer_buy_price.toFixed(2));


                        } else if (shipping_order_type == '2' || shipping_order_type == '3') {
                            var total_cost = confirmed_quantity * response.data.landed_product_price;
                            console.log(response.data.landed_product_price);
                            currentRow.find('.ship_total_cost').val(total_cost.toFixed(2));

                            //                    cal customer_buy_price
                            var customer_buy_price = 0;

                            customer_buy_price = response.data.landed_product_price * (1 + (response.data.wholesale_margin_percentage / 100)) * confirmed_quantity;

                            if (isNaN(customer_buy_price)) {
                                customer_buy_price = 0;
                            }

                            //                    customer_buy_price = total_cost * (response.data.wholesale_margin_percentage / 100);
                            currentRow.find('.ship_customer_buy_price').val(customer_buy_price.toFixed(2));

                        }

                        //cal profit
                        var profit = customer_buy_price - total_cost;
                        currentRow.find('.ship_profit').val(profit.toFixed(2));
//                    console.log('i am here');
                        $('#shipping_container_products').submit();
                    }
                }
            });
        });
    }
    //for submitting shipping container products on keyup in orders
    $('#shipping_container_products').on('submit', function (e) {

        e.preventDefault();

        var form_data = new FormData(this);

        form_data.append("_token", $('meta[name="csrf-token"]').attr('content'));
        $.ajax({
            url: "{{url('admin/submit_shipping_container_products')}}",
            method: "POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                //                        Swal.showLoading();
            },
            onError: function () {
                swal("Error!", 'Something Went Wrong', "error");
            },
            success: function (response)
            {

                if (response) {
                    if (response.status) {
//                        forShippingContainerCalculation();
                        console.log('Shipping container products saved');
                    }
                }
            }
        });
    });
    //ends
//  hide/unhide landed_product_price

    $(document).on('click change', '.order_type', function () {
        var order_type = $(this).val();
        var order_id = $('.order_id').val();
        if (order_type == '1' || order_type == '') {
            $('.fob_costs').css('display', 'none');
            $('.fis_costs').css('display', 'none');
//            $('#tbl_posts .hide_show_landed_product_price').hide();
        } else if (order_type == '2') {
            $('.fob_costs').css('display', 'block');
            $('.fis_costs').css('display', 'none');
//            $('#tbl_posts .hide_show_landed_product_price').show();
        } else if (order_type == '3') {
            $('.fis_costs').css('display', 'block');
            $('.fob_costs').css('display', 'none');
//            $('#tbl_posts .hide_show_landed_product_price').show();
        }

        $.ajax({
            url: "{{url('admin/update_order_type')}}",
            method: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {order_type: order_type, order_id: order_id},
            beforeSend: function () {

            },
            onError: function () {
                swal("Error!", 'Something Went Wrong', "error");
            },
            success: function (response)
            {
//                alert('received');
                load_order_lines();
                //$('.gp_cal').click();
            }
        });

    });
    function load_shipping_totals(orderId) {
        $.ajax({
            url: "{{url('admin/load_shipping_totals')}}",
            method: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {orderId: orderId},
            beforeSend: function () {
//                window.swal({
//                    title: "Checking...",
//                    text: "Please wait",
//                    imageUrl: "images/ajaxloader.gif",
//                    showConfirmButton: false,
//                    allowOutsideClick: false
//                });
            },
            onError: function () {
                swal("Error!", 'Something Went Wrong', "error");
            },
            success: function (response)
            {
//                window.swal({
//                      title: "Finished!",
//                      showConfirmButton: false,
//                      timer: 1000
//                    });
                $('#viewcollapse').html(response);
                $('#viewTrackingCollapse').html(response);
//                $('.show_hide_icon i').removeClass('fa fa-plus');
//                $('.show_hide_icon i').addClass('fa fa-minus');
            }
        });
    }
    function load_order_overview_totals() {
        $.ajax({
            url: "{{url('admin/load_order_overview_totals')}}",
            method: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {},
            beforeSend: function () {
            },
            onError: function () {
                swal("Error!", 'Something Went Wrong', "error");
            },
            success: function (response)
            {
                $('#viewOrderOverviewCollapse').html(response);
//                $('#tbl_load_order_overview_info').css('display','none')
//                $('.show_hide_order_overview_collapse_icon i').removeClass('fa fa-plus');
//                $('.show_hide_order_overview_collapse_icon i').addClass('fa fa-minus');
            }
        });
    }

    getAllCurrencyExhange();
    function getAllCurrencyExhange() {
        $.ajax({
            url: "{{url('admin/getAllCurrencyExhange')}}",
            method: "POST",
            async: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            beforeSend: function () {
                //                        Swal.showLoading();
            },
            onError: function () {
                swal("Error!", 'Something Went Wrong', "error");
            },
            success: function (response)
            {

                getAllCurrencyExhangeJson = response;
            }
        });
    }


    //function for calculating base currency value from current currency
    function convertCurrentToBaseCurrency(base, current, value) {
        for (var i = 0; i < getAllCurrencyExhangeJson.getAllCurrencyExhange.length; i++) {
            if (getAllCurrencyExhangeJson.getAllCurrencyExhange[i].currency_id_to == base && getAllCurrencyExhangeJson.getAllCurrencyExhange[i].currency_id_from == current) {
                var newvalue = value * getAllCurrencyExhangeJson.getAllCurrencyExhange[i].exchange_rate;
                return newvalue;
            }
        }

        return 0;
    }
//    $(document).on(ready, function () { });
//    Calculation code for Total order_pricing_costs_fis 
    $(document).on('change click keyup', '.order_pricing_costs_fis', function () {
        var rowId = $(this).closest('td').attr('id');
        var currentRow = $("#" + rowId);
        var shipping_rate = currentRow.find(".shipping_rate").val().replace(/,/g, '');
        var shipping_rate_currency = currentRow.find(".shipping_rate_currency").val();
        var transport = currentRow.find(".transport").val().replace(/,/g, '');
        var transport_currency = currentRow.find(".transport_currency").val();
        var port_costs = currentRow.find(".port_costs").val().replace(/,/g, '');
        var port_costs_currency = currentRow.find(".port_costs_currency").val();
        var insurance_cost = currentRow.find(".insurance_cost").val().replace(/,/g, '');
        var insurance_cost_currency = currentRow.find(".insurance_cost_currency").val();
        var import_custom_duties_percentage = currentRow.find(".import_custom_duties_percentage").val().replace(/,/g, '');
        var import_custom_duties_value = currentRow.find(".import_custom_duties_value").val().replace(/,/g, '');
        var import_taxes_percentage = currentRow.find(".import_taxes_percentage").val().replace(/,/g, '');
        var import_taxes_value = currentRow.find(".import_taxes_value").val().replace(/,/g, '');
        var handing_costs_in_destination_terminal = currentRow.find(".handing_costs_in_destination_terminal").val().replace(/,/g, '');
        var handing_costs_currency = currentRow.find(".handing_costs_currency").val();
        var costs_of_shipping_after_destination_port = currentRow.find(".costs_of_shipping_after_destination_port").val().replace(/,/g, '');
        var shipping_after_port_currency = currentRow.find(".shipping_after_port_currency").val();
        var import_per = currentRow.find(".import_custom_duties_percentage").val().replace(/,/g, '');
        var product_unit_price = currentRow.find(".product_unit_price").val();
        var product_units_in_container = currentRow.find(".packing_quantity").val();
        var product_currency = currentRow.find(".product_currency").val();
        var pricing_currency = currentRow.find(".pricing_currency").val();
        var import_tax_per = currentRow.find(".import_taxes_percentage").val().replace(/,/g, '');
//        fis part calculations whichd differs from fiw
        var domestic_transport = currentRow.find(".domestic_transport").val();
        if (typeof domestic_transport != "undefined") {
            domestic_transport = domestic_transport.replace(/,/g, '');
        }
        var domestic_transport_currency = currentRow.find(".domestic_transport_currency").val();
        var storage_overhead = currentRow.find(".storage_overhead").val();
        if (typeof storage_overhead != "undefined") {
            storage_overhead = storage_overhead.replace(/,/g, '');
        }
        var storage_overhead_currency = currentRow.find(".storage_overhead_currency").val();
        var merchandising = currentRow.find(".merchandising").val();
        if (typeof merchandising != "undefined") {
            merchandising = merchandising.replace(/,/g, '');
        }
        var merchandising_currency = currentRow.find(".merchandising_currency").val();
        var other_overheads = currentRow.find(".other_overheads").val();
        if (typeof other_overheads != "undefined") {
            other_overheads = other_overheads.replace(/,/g, '');
        }
        var other_overheads_currency = currentRow.find(".other_overheads_currency").val();

        //code for creating objet of numberformat for formatting number
        var nf = new Intl.NumberFormat();
        //ends


//      getting purchase price FIS value        
        var purchase_price = product_unit_price * product_units_in_container;
//        console.log('hello'+product_units_in_container);
//        console.log(product_unit_price);
        var purchase_price = convertCurrentToBaseCurrency(pricing_currency, product_currency, purchase_price);


//        getting shipping rate base currnecy value
        var converted_shipping_rate = 0;
        converted_shipping_rate = convertCurrentToBaseCurrency(pricing_currency, shipping_rate_currency, shipping_rate);



//        getting transport cost base currnecy value
        var converted_transport_cost = 0;
        converted_transport_cost = convertCurrentToBaseCurrency(pricing_currency, transport_currency, transport);

//        getting port cost base currnecy value
        var converted_port_costs = 0;
        converted_port_costs = convertCurrentToBaseCurrency(pricing_currency, port_costs_currency, port_costs);


//        getting insurance cost base currnecy value
        var converted_insurance_cost = 0;
        converted_insurance_cost = convertCurrentToBaseCurrency(pricing_currency, insurance_cost_currency, insurance_cost);

//   getting converted_handing_costs_in_destination_terminal to base currnecy value

        var converted_handing_costs_in_destination_terminal = 0;
        converted_handing_costs_in_destination_terminal = convertCurrentToBaseCurrency(pricing_currency, handing_costs_currency, handing_costs_in_destination_terminal);

//   getting converted_handing_costs_in_destination_terminal to base currnecy value

        var converted_costs_of_shipping_after_destination_port = 0;
        converted_costs_of_shipping_after_destination_port = convertCurrentToBaseCurrency(pricing_currency, shipping_after_port_currency, costs_of_shipping_after_destination_port);

//*********Calculating fis fields value which differs from fiw**************

        //        getting transport cost base currnecy value
        var converted_domestic_transport = 0;
        converted_domestic_transport = convertCurrentToBaseCurrency(pricing_currency, domestic_transport_currency, domestic_transport);
        console.log('Domestic Transport->' + pricing_currency, domestic_transport_currency, domestic_transport, converted_domestic_transport);
        //multiplying this by product_units_in_container
        converted_domestic_transport = converted_domestic_transport * product_units_in_container;
        //show value after multiplying in input
        currentRow.find(".domestic_transport_included_qty").val(nf.format(converted_domestic_transport.toFixed(2)));

        //        getting transport cost base currnecy value
        var converted_storage_overhead = 0;
        converted_storage_overhead = convertCurrentToBaseCurrency(pricing_currency, storage_overhead_currency, storage_overhead);
        //multiplying this by product_units_in_container
        converted_storage_overhead = converted_storage_overhead * product_units_in_container;
        //show value after multiplying in input
        currentRow.find(".storage_overhead_included_qty").val(nf.format(converted_storage_overhead.toFixed(2)));

        //        getting transport cost base currnecy value
        var converted_merchandising = 0;
        converted_merchandising = convertCurrentToBaseCurrency(pricing_currency, merchandising_currency, merchandising);
        //multiplying this by product_units_in_container
        converted_merchandising = converted_merchandising * product_units_in_container;
        //show value after multiplying in input
        currentRow.find(".merchandising_included_qty").val(nf.format(converted_merchandising.toFixed(2)));

        //        getting transport cost base currnecy value
        var converted_other_overheads = 0;
        converted_other_overheads = convertCurrentToBaseCurrency(pricing_currency, other_overheads_currency, other_overheads);
        //multiplying this by product_units_in_container
        converted_other_overheads = converted_other_overheads * product_units_in_container;
        //show value after multiplying in input
        currentRow.find(".other_overheads_included_qty").val(nf.format(converted_other_overheads.toFixed(2)));


//*******ends code for fis fields value which differs from fiw***************

//*********Calculating and Assigning value to import_custom_duties_value**********
        console.log('import val fields->' + purchase_price, import_per);
        var import_val = purchase_price * (import_per / 100);
        currentRow.find(".import_custom_duties_value").val(nf.format(import_val.toFixed(2)));
//********************ends code for import_custom_duties_value********************


//********Calculating and Assigning value to import_taxes_value*******************
        var import_taxes_fis = purchase_price + converted_shipping_rate + converted_insurance_cost + parseInt(import_custom_duties_value);

        var final_import_taxes_value = import_taxes_fis * (import_tax_per / 100);

        if (isNaN(final_import_taxes_value)) {
            final_import_taxes_value = 0;
        }

        currentRow.find(".import_taxes_value").val(nf.format(final_import_taxes_value.toFixed(2)));

//********Calculating and Assigning value to total landed costs*******************
        var total_landed_costs = Number(converted_shipping_rate) + Number(converted_transport_cost) + Number(converted_port_costs) + Number(converted_insurance_cost) + Number(import_custom_duties_value) + Number(import_taxes_value) + Number(converted_handing_costs_in_destination_terminal) + Number(converted_costs_of_shipping_after_destination_port);

        currentRow.find(".total_landed_costs").val((nf.format((total_landed_costs).toFixed(2))));
//    Calculating And assigning total_costs_fis for fIS 
        var total_landed_costs_fis = 0;
        var total_landed_costs_fis = Number(total_landed_costs) + Number(converted_domestic_transport) + Number(converted_storage_overhead) + Number(converted_merchandising) + Number(converted_other_overheads);
        //assigning

        var total_landed_costs_fis_db = currentRow.find(".total_landed_costs_fis").val();
        if (typeof total_landed_costs_fis_db != "undefined") {
            currentRow.find(".total_landed_costs_fis").val((nf.format((total_landed_costs_fis).toFixed(2))));
        }

//***********************----ends code for total landed costs----*****************
        $('#order_pricing_costs_fis').submit();

    });



//    $(document).on('click change keyup', '.gp_cal', function () {
//      
//    });


//order pricing cost fis ajax submittion
//    $(document).ready(function () {
//        $(document).on('change click keyup', '.order_pricing_costs_fis', function () {
//            $('#order_pricing_costs_fis').submit();
//        });
//    });
    //for submitting order pricing data on keyup in orders
    $('#order_pricing_costs_fis').on('submit', function (e) {

        e.preventDefault();
        var form_data = new FormData(this);
        form_data.append("_token", $('meta[name="csrf-token"]').attr('content'));
        $.ajax({
            url: "{{url('admin/submit_order_pricing_costs_fis')}}",
            method: "POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                //                        Swal.showLoading();
            },
            onError: function () {
                swal("Error!", 'Something Went Wrong', "error");
            },
            success: function (response)
            {

                if (response) {
                    if (response.status) {
                        console.log('order line saved');
//                        getProducts();
                    }
                }
            }
        });
    });



    //order pricing cost fob ajax submittion
    $(document).ready(function () {
        $(document).on('change click keyup', '.order_pricing_costs_fob', function () {
            var rowId = $(this).closest('td').attr('id');
            var currentRow = $("#" + rowId);
            var order_pricing_costs_total_fob = currentRow.find(".order_pricing_costs_total_fob").val();
            var costs_from_factory_fob = currentRow.find(".costs_from_factory_fob").val();
            var costs_from_factory_currency_fob = currentRow.find(".costs_from_factory_currency_fob").val();
            var custom_and_other_costs_fob = currentRow.find(".custom_and_other_costs_fob").val();
            var custom_and_other_costs_currency_fob = currentRow.find(".custom_and_other_costs_currency_fob").val();

            var product_unit_price = currentRow.find(".product_unit_price").val();
            var product_units_in_container = currentRow.find(".packing_quantity").val();
            var product_currency = currentRow.find(".product_currency").val();
            var pricing_currency = currentRow.find(".pricing_currency").val();

//        getting converted costs from factory base currnecy value
            var converted_costs_from_factory_fob = 0;
            converted_costs_from_factory_fob = convertCurrentToBaseCurrency(pricing_currency, costs_from_factory_currency_fob, costs_from_factory_fob);

            if (isNaN(converted_costs_from_factory_fob)) {
                converted_costs_from_factory_fob = 0;
            }
//        getting converted custom and other costs base currnecy value
            var converted_custom_and_other_costs_fob = 0;
            converted_custom_and_other_costs_fob = convertCurrentToBaseCurrency(pricing_currency, custom_and_other_costs_currency_fob, custom_and_other_costs_fob);
            if (isNaN(converted_custom_and_other_costs_fob)) {
                converted_custom_and_other_costs_fob = 0;
            }


//********Calculating and Assigning value to total pricing costs FOB*******************

            var order_pricing_costs_total_fob = Number(converted_costs_from_factory_fob) + Number(converted_custom_and_other_costs_fob);
            if (isNaN(order_pricing_costs_total_fob)) {
                order_pricing_costs_total_fob = 0;
            }
            currentRow.find(".order_pricing_costs_total_fob").val(parseFloat(order_pricing_costs_total_fob).toFixed(2));
//***********************----ends code for total pricing costs FOB----*****************
            $('#order_pricing_costs_fob').submit();
        });
    });
    //for submitting order pricing data on keyup in orders
    $('#order_pricing_costs_fob').on('submit', function (e) {

        e.preventDefault();
        var form_data = new FormData(this);
        form_data.append("_token", $('meta[name="csrf-token"]').attr('content'));
        $.ajax({
            url: "{{url('admin/submit_order_pricing_costs_fob')}}",
            method: "POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                //                        Swal.showLoading();
            },
            onError: function () {
                swal("Error!", 'Something Went Wrong', "error");
            },
            success: function (response)
            {

                if (response) {
                    if (response.status) {
                        console.log('order line saved');
                    }
                }
            }
        });
    });

    function editImage(rowId) {

        //.  var rowId = $(this).closest('tr').attr('id');
        // alert(rowId);
        //  var currentRow = $("#" + rowId);
        $("#quote_image_" + rowId).click();
    }

    //dynamic image upload function
    function uploadImageDynamically(triggeredClass, removedClass = null) {
//        console.log('triggered once or twice');
        if (triggeredClass == 'remove') {
            $('.' + removedClass)
                    .attr('src', APP_URL + '/images/no_image.png');
        } else {
            if (triggeredClass == 'product_image_hidden') {
                $("#" + triggeredClass).click();
            } else {
                $("." + triggeredClass).click();
            }

    }
    }

//    $(document).ready(function () {
//        $('.productimageEditPencil').unbind('click').bind('click', function(event) {
//            $(".edit_product_picture_dynamic").trigger("click");
//        });
//    });
//    $(document).on(ready, function () {
//        $(".edit_product_picture_dynamic").click();
//    });


    function showImage(input, imageClass) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.' + imageClass)
                        .attr('src', '');
                $('.' + imageClass)
                        .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        } else {
            console.log('error! M in else Portion line:1061')
        }
    }



    //ends


//  Add Supplier through Modal in Users
    $('#add_supplier_modal').on('submit', function (e) {

        e.preventDefault();

        var form_data = new FormData(this);

        form_data.append("_token", $('meta[name="csrf-token"]').attr('content'));
        $.ajax({
            url: "{{url('admin/add_supplier_modal_data')}}",
            method: "POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                //                        Swal.showLoading();
            },
            success: function (response)
            {

                if (response) {
//                    alert(response.status);
                    if (response.status == 'true') {
//                        alert(response.data.name);
                        $('#modalCloseSupplierProduct').trigger('click');
                        $('.user_supplier').prepend($("<option  selected='selected'></option>")
                                .attr("value", response.data.id)
                                .text(response.data.name));
                        swal("Good job!", "Supplier Added!", "success");
                    } else if (response.status == 'false') {
                        console.log(response.errors);
                        swal("Error!", 'Something Went Wrong', "error");
                    }
                }
            }
        });
    });

//    Unhide Add Supplier Section if Admin Select Suppliers User Type and same with company
    $(document).on('change', '.user_role', function () {
        var user_role = $(this).val();
        if (user_role == 'supplier') {
            $('.add_supplier_div').css('display', 'block');
            $('.company_div').css('display', 'none');
        } else if (user_role == 'company_user') {
            $('.company_div').css('display', 'block');
            $('.add_supplier_div').css('display', 'none');
        } else {
            $('.add_supplier_div').css('display', 'none');
            $('.company_div').css('display', 'none');
        }
    });
//    var last_sel = '';
//    $(document).on('click', '.hide_unhide_container_select', function () {
//        var rowId = $(this).closest('tr');
//        var last_sel = rowId.find('.hide_unhide_container_select option:selected').val();
//    });
//    

//    function check_shipping_product_exists(shipping_rowId) {
//
//        $.ajax({
//            url: "{{url('admin/check_shipping_product_exists')}}",
//            method: "POST",
//            headers: {
//                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//            },
//            data: {shipping_rowId: shipping_rowId},
//            beforeSend: function () {
//                //                        Swal.showLoading();
//            },
//            onError: function () {
//                swal("Error!", 'Something Went Wrong', "error");
//            },
//            success: function (response)
//            {
//                
////                console.log(response.status);
//                return response.data;
//
//            }
//        });
//    }
    
//    $(function(){
//    var lastSel;
//    var lastSelOptionHtml;
//        $(document).on('click', '.hide_unhide_container_select', function(){
//            var rowId = $(this).closest('tr');
//            lastSelOptionHtml = rowId.find(".hide_unhide_container_select option:selected");
//            lastSel = rowId.find(".hide_unhide_container_select option:selected").val();
//    }).on('change', '.hide_unhide_container_select', function () {
//            var rowId = $(this).closest('tr');
//            var shipping_rowId = rowId.find('.shipping_rowId').val();
//            var container = rowId.find('.hide_unhide_container_select').val();
//            
//            
//            $.ajax({
//                url: "{{url('admin/check_shipping_product_exists')}}",
//                method: "POST",
//                headers: {
//                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//                },
//                data: {shipping_rowId: shipping_rowId},
//                beforeSend: function () {
//                    //                        Swal.showLoading();
//                },
//                onError: function () {
//                    swal("Error!", 'Something Went Wrong', "error");
//                },
//                success: function (response)
//                {
//                    if(response.status == 'true'){
//                        console.log(lastSel);
//                    }
//
//                }
//            });
//        });
//    });
    
    
    
    
//    Unhide Add shipping product count Section if container isn't selected 
    $(document).on('change', '.hide_unhide_container_select', function () {
        var rowId = $(this).closest('tr');
        var container = rowId.find('.hide_unhide_container_select').val();
        
        if (container == '') {
            rowId.find('.container_first').css('display', 'block');
            rowId.find('.view_product').css('display', 'none');
            swal({
                title: "Warning!",
                text: "Please select container for view products",
                icon: "warning",
                button: "Ok!",
            });
        } else {
            rowId.find('.view_product').css('display', 'block');
            rowId.find('.container_first').css('display', 'none');
        }
    });
//    Unhide Audit Name (if other) Section if Selected Other Ethical Audit

    $(document).on('change', '.ethical_audit', function () {
        var ethical_audit = $(".ethical_audit option:selected").text();
//        alert(ethical_audit);
        if (ethical_audit == 'Other' || ethical_audit == 'other') {
            $('.audit_other_name_div').css('display', 'block');
        } else {
            $('.audit_other_name_div').css('display', 'none');
        }
    });

    //ON PAGE LOAD UNHIDE IF AUDIT IS OTHER
    var ethical_audit = $('.ethical_audit').val();
    if (ethical_audit == '4') {
        $('.audit_other_name_div').css('display', 'block');
    } else {
        $('.audit_other_name_div').css('display', 'none');
    }
    $(document).on('click', '.deleteentry', function (e) {
        e.preventDefault();
        swal({
            title: "Are you sure?",
            text: "Do you really want to delete ?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
                .then((willDelete) => {
                    if (willDelete) {
                        $(this).closest('form').submit();
                    } else {
                        //alert('here');
                    }
                });

    });

    //calculating wholesale price (Price to Customer) and Retail Price(RRP)
    $(document).on('click keyup', '.order_pricing_cal', function () {
        var rowId = $(this).closest('tr').attr('id');
        var currentRow = $("#" + rowId);
        var quantity = currentRow.find(".order_quantity").val();
        var unit_price = currentRow.find(".product_unit_price").val();
        var fob_currency = currentRow.find(".product_unit_currency").val();
        var wholesale_price = currentRow.find(".wholesale_price").val();
        var pricing_currency = $("#pricing_currency").val();
        //convert unit_proice (FOB price) to converted_fob_price
        var converted_fob_price = 0;
        converted_fob_price = convertCurrentToBaseCurrency(pricing_currency, fob_currency, unit_price);
        var wholesale_margin = currentRow.find(".wholesale_margin").val();
        var retail_margin = currentRow.find(".retail_margin").val();
//        var wholesale_price = converted_fob_price * quantity * (wholesale_margin / 100 + 1);
        var retail_price = wholesale_price * (1 + (retail_margin / 100));
//        currentRow.find('.wholesale_price').val(wholesale_price.toFixed(2));
//        currentRow.find('.retail_price').val(retail_price.toFixed(2));
//        console.log('converted fob->'+converted_fob_price);
    });
    //ends

//    $(".panel-heading").hover(
//     function() {
//        $('.panel-collapse').collapse('show');
//      }, function() {
//        $('.panel-collapse').collapse('hide');
//      }
//    );


//    $(document).on({
//        mouseenter: function () {
//            var rowId = $(this).closest('tr').attr('id');
//            $('.collpase_this_'+rowId).collapse('show').slow();
//        },
//        mouseleave: function () {
//            var rowId = $(this).closest('tr').attr('id');
//            $('.collpase_this_'+rowId).collapse('hide').slow();
//        }
//    }, ".panel-heading"); 
//    

//    $(document).on('click','.panel-heading',
//        function() {
//            var rowId = $(this).closest('tr').attr('id');
//            $(this).find('.collpase_this_'+rowId).animate({
//                top: '0%'
//            }, 'slow' );
//        },function() {
//            var rowId = $(this).closest('tr').attr('id');
//            $(this).find('.collpase_this_'+rowId).animate({
//                top: '-100%'
//            }, 'slow' );
//        });


    function removeImageFromTable(rowId, columnName, tableName, removedClass = null) {
        swal({
            title: "Are you sure?",
            text: "Do you really want to Delete/Remove Image ?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: "{{url('admin/remove_image_from_table')}}",
                            method: "POST",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data: {rowId: rowId, columnName: columnName, tableName: tableName},
                            beforeSend: function () {

                            },
                            onError: function () {
                                swal("Error!", 'Something Went Wrong', "error");
                            },
                            success: function (response)
                            {
                                console.log('Image removed');
                                if (removedClass != null) {
                                    $('.' + removedClass)
                                            .attr('src', APP_URL + '/images/no_image.png');
                                }
                            }
                        });
                        return true;
                    } else {
                        return false;
                    }
                });
    }

    $("#scroll-right").click(function () {
        event.preventDefault();
//        $("#scroll-right").css('display','none');
//        scrolled=scrolled+300;
        $("form, .responsive").animate(
                {
                    scrollLeft: "+=450px"
//                scrollLeft:  scrolled
                },
                500
                );
//        setTimeout(function () {
//            $("#scroll-left").css('display','block');
//        }, 1000);

    });
    $("#scroll-left").click(function () {
        event.preventDefault();
//        scrolled=scrolled-300;
//        $("#scroll-left").css('display','none');
        $("form, .responsive").animate(
                {
                    scrollLeft: "-=450px"
//                scrollLeft:  scrolled
                },
                500
                );
//        setTimeout(function () {

//            $("#scroll-right").css('display','block');
//        }, 1000);

    });
    $('.table').on('page.dt', function () {
                $('html, body').animate({
                    scrollTop: $(".dataTables_wrapper").offset().top
                }, 'slow');
            });
            
   
</script>