@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Shipping port</div>
                    <div class="card-body">
                        <a href="{{ url('/admin/shipping-port/create') }}" class="btn btn-success btn-sm" title="Add New ShippingPort">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>

                        <br/>
                        <br/>
                        <div class="table-responsive suppliers_table_mobile">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Port</th><th>Country</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($shippingport as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->port }}</td><td>{{ $item->country }}</td>
                                        <td>
                                            <a href="{{ url('/admin/shipping-port/' . $item->id . '/edit') }}" title="Edit ShippingPort"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                            {!! Form::open([
                                                'method' => 'DELETE',
                                                'url' => ['/admin/shipping-port', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                        'type' => 'submit',
                                                        'type' => 'submit',
'class' => 'btn btn-danger btn-sm deleteentry',
                                                        'title' => 'Delete ShippingPort'   
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
