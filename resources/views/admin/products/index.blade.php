@extends('layouts.backend')

@section('content')
<div class="container">
    <div class="row">
        @include('admin.sidebar')

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Products</div>
                <div class="card-body">
                    <a href="{{ url('/admin/products/create') }}" class="btn btn-success btn-sm" title="Add New Product">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add New
                    </a>



                    <br/>
                    <br/>
                    <div class="table-responsive suppliers_table_mobile">
                        <table class="table table-borderless">
                            <thead>
                                <tr>
                                    <th>#</th><th>Name</th><th>Category</th><th>Type</th><th>Image</th><th>Suppliers</th><th>Active</th><th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($products as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->name }}</td><td>{{ get_category_name_by_id($item->category_id) }}</td><td>{{ get_product_type_name_by_id($item->type_id) }}</td>
                                    <td>
                                        <?php if ($item->pic_id != null) { ?>
                                            <img width="60" src="{{ getFile($item->pic_id) }}" style="margin-bottom: 4px;">
                                        <?php } else { ?>
                                            <img width="60" src="{{ url('images/no_image.png') }}" style="margin-bottom: 4px;">
                                        <?php } ?>
                                    </td>
                                    <td>
                                        <?php
                                        $product_supplier = App\SupplierProduct::where('product_id', $item->id);
                                        $product_supplier_id = ($product_supplier->get()->isEmpty() != true) ? $product_supplier->first()->supplier_id : 'NA';
                                        if ($product_supplier_id != 'NA') {
                                            $supplier_name = get_supplier_name_by_id($product_supplier_id);

                                            if ($supplier_name != 'NA') {
                                                ?><a href="{{ url('admin/suppliers',$product_supplier_id) }}">{{ $supplier_name }}</a><?php
                                            } else {
                                                echo $supplier_name;
                                            }
                                        } else {
                                            echo 'NA';
                                        }
                                        ?>
                                    </td>
                                    <td><?= ($item->active == '0') ? 'Yes' : 'No' ?></td>
                                    <td>
                                        <a href="{{ url('/admin/suppliersById/' . $item->id) }}" title="View Suppliers" style="display:none;"><button class="btn btn-info btn-sm"><i class="fa fa-industry" aria-hidden="true"></i></button></a>
                                        <a href="{{ url('/admin/products/' . $item->id) }}" title="View Product" style="display:none;"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                        <a href="{{ url('/admin/products/' . $item->id . '/edit') }}" title="Edit Product"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>

                                        {!! Form::open([
                                        'method' => 'DELETE',
                                        'url' => ['/admin/products', $item->id],
                                        
                                        'style' => 'display:inline'
                                        ]) !!}
                                                
                                        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                        'type' => 'submit',
                                        'type' => 'submit',
'class' => 'btn btn-danger btn-sm deleteentry',
                                        'title' => 'Delete Product',
                                        )) !!}
                                        {!! Form::close() !!}

                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script>

    $(document).ready(function () {
        $('.table').dataTable({
            "iDisplayLength": 50
        });
    });
</script>
@endsection
