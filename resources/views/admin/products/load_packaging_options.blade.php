<table class="table table-bordered" id="tbl_product_packaging_options">
    <thead>
        <tr>
            <th>#</th>
            <th>Packaging Type</th>
            <th>Quantity Per Package</th>
            <th>Packaging Length (cm)</th>
            <th>Packaging Width (cm)</th>
            <th>Packaging Height (cm)</th>
            <th>Gross Weight (kg) including Packaging</th>
            <th>Packaging CBM</th>
            <th>Floor Ready</th>
            <th>Stackable</th>
            <th>Nested Product</th>
            <th>If Nested Product</th>
            <th><i class="fa fa-cogs"></i></th>
        </tr>
    </thead>
    <tbody id="tbl_packaging_options_body">
        <?php $i = '1'; ?>
        @foreach($packaging_options as $details)
        <tr id="rec-<?= $i ?>">

            <td><span class="sn"></span><?= $i ?>.</td>
    <input type="hidden" value="{{$details->id}}" class="row_id" name="packaging_options[<?= $i ?>][row_id]" />
    <input type="hidden" class="form-control product_id" name="packaging_options[<?= $i ?>][product_id]" value="{{$details->product_id}}"/>

    <td>
        <?php $select = ['1' => 'Yes', '2' => 'No']; ?>
        <?php /*<select class="form-control packaging_options" name="packaging_options[<?= $i ?>][packaging_type]" style="width:140px;">
            <?php 
            $type = ['1' => 'Carton Box', '2' => 'Paper Bag'];
            ?>
            <option value="">Select</option>
            @foreach($type as $key => $value)
            @if($key == $details->packaging_type)
            <option value="{{$key}}" selected>{{$value}}</option>
            @else
            <option value="{{$key}}">{{$value}}</option>
            @endif
            @endforeach
        </select> */?>
        <select class="form-control packaging_options" name="packaging_options[<?= $i ?>][packaging_type]" style="width:140px;"><?php $data = get_package_type_list(); ?>
            <option value="">Select</option>
            @foreach($data as $key => $value)
            @if($key == $details->packaging_type)
            <option value="{{$key}}" selected="">{{$value}}</option>
            @else
            <option value="{{$key}}">{{$value}}</option>
            @endif
            @endforeach
        </select>
    </td>
    <td><input type="text" class="form-control quantity_per_pagckage packaging_options" name="packaging_options[<?= $i ?>][quantity_per_page]" id="quantity_per_pagckage_{{$details->id}}" value="{{$details->quantity_per_page}}" <?=($details->nested_product == '1')?'disabled':''?>/></td>
    <td><input type="text" class="form-control packaging_length packaging_options" name="packaging_options[<?= $i ?>][packaging_length]" id="packaging_length" value="{{$details->packaging_length}}"/></td>
    <td><input type="text" class="form-control packaging_width packaging_options" name="packaging_options[<?= $i ?>][packaging_width]" id="packaging_width" value="{{$details->packaging_width}}"/></td>
    <td><input type="text" class="form-control packaging_height packaging_options" name="packaging_options[<?= $i ?>][packaging_height]" id="packaging_height" value="{{$details->packaging_height}}"/></td>
    <td><input type="text" class="form-control packaging_options" name="packaging_options[<?= $i ?>][gross_weight_including_packaging]" value="{{$details->gross_weight_including_packaging}}"/></td>
    <td><input type="text" class="form-control packaging_cbm packaging_options" name="packaging_options[<?= $i ?>][packaging_cbm]" id="packaging_cbm" value="{{$details->packaging_cbm}}"/></td>
    <td>
        <select class="form-control packaging_options" name="packaging_options[<?= $i ?>][floor_ready]" style="width:90px;">
            <option value="">Select</option>
            @foreach($select as $key => $value)
            @if($key == $details->floor_ready)
            <option value="{{$key}}" selected>{{$value}}</option>
            @else
            <option value="{{$key}}">{{$value}}</option>
            @endif
            @endforeach
        </select>
    </td>
    <td>
        <select class="form-control packaging_options" name="packaging_options[<?= $i ?>][stackable]" style="width:150px;">
            <option value="">Select</option>
            @foreach($select as $key => $value)
            @if($key == $details->stackable)
            <option value="{{$key}}" selected>{{$value}}</option>
            @else
            <option value="{{$key}}">{{$value}}</option>
            @endif
            @endforeach
        </select>
    </td>
    <td>
        <select class="form-control nested_product packaging_options" name="packaging_options[<?= $i ?>][nested_product]" style="width:150px;">
            <option value="">Select</option>
            @foreach($select as $key => $value)
            @if($key == $details->nested_product)
            <option value="{{$key}}" selected>{{$value}}</option>
            @else
            <option value="{{$key}}">{{$value}}</option>
            @endif
            @endforeach
        </select>
    </td>
    <td>
        @if($details->nested_product == '1')
        <button type="" class="btn btn-success edit_loading" data-toggle="modal" data-target="#packagingOptionsProductModalLongnew" onclick="load_nested_products({{$details->id}})" style="display :block; background-color: #000!important;border-color: #000!important; width: 157px;">Edit Loading</button>
            <span class="not_available" style="display :none; width: 157px; text-align: center;">N/A</span>
        @else
        <button type="" class="btn btn-success edit_loading" data-toggle="modal" data-target="#packagingOptionsProductModalLongnew" onclick="load_nested_products({{$details->id}})"  style="display :none; background-color: #000!important;border-color: #000!important; width: 157px;">Edit Loading</button>
        <span class="not_available" style="display :block; width: 157px; text-align: center;">N/A</span>
        @endif
    </td>
    <td><a class="btn btn-xs" data-id="<?= $i ?>" onclick="deleteOrderDetail('product_packaging_options',{{$details->id}});"><i class="fa fa-trash"></i></a></td>
</tr>
<?php $i++; ?>
@endforeach
</tbody>
</table>