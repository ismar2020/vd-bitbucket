@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Suppliers</div>
                    <div class="card-body">
                        <?php if(isset($previous)){ ?>
                            <a href="{{ url('admin/products') }}" class="btn btn-success btn-sm">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                            </a>
                        <?php } ?>
                        {!! Form::open(['method' => 'GET', 'url' => '/admin/suppliers', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Name</th><th>Supplier</th><th>Card (Front,Back)</th><th>Contact</th><th>Product</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($suppliers as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>
                                            <?php if($item->picture_company != null) { ?>
                                            <img width="60" src="<?= url($item->picture_company); ?>" style="margin-bottom: 4px;">
                                            <?php }else{ ?>
                                            <img width="60" src="{{ url('images/no_image.png') }}" style="margin-bottom: 4px;">
                                            <?php } ?>
                                        </td>
                                        <td>
                                            <?php if($item->picture_card_front != null) { ?>
                                            <img width="60" src="<?= url($item->picture_card_front); ?>" style="margin-bottom: 4px;">
                                            <?php }else{ ?>
                                            <img width="60" src="{{ url('images/no_image.png') }}" style="margin-bottom: 4px;">
                                            <?php } ?>
                                            <?php if($item->picture_card_back != null) { ?>
                                            <img width="60" src="<?= url($item->picture_card_back); ?>" style="margin-bottom: 4px;">
                                            <?php }else{ ?>
                                            <img width="60" src="{{ url('images/no_image.png') }}" style="margin-bottom: 4px;">
                                            <?php } ?>
                                        </td>
                                        
                                        <td><?php $contact_name = get_contact_name_by_id($item->contact_id); if($contact_name != 'NA'){ ?><a href="{{ url('admin/contacts',$item->contact_id) }}">{{ $contact_name }}</a><?php }else{ echo $contact_name; } ?>
                                        </td>
                                        <td><?php $product_name = get_product_name_by_id($item->product_id); if($product_name != 'NA'){ ?><a href="{{ url('admin/products',$item->product_id) }}">{{ $product_name }}</a><?php }else{ echo $product_name; } ?>
                                        </td>
                                        <td>
                                            <a href="{{ url('/admin/suppliers/' . $item->id) }}" title="View Supplier"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                            <a href="{{ url('/admin/suppliers/' . $item->id . '/edit') }}" title="Edit Supplier"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                            {!! Form::open([
                                                'method' => 'DELETE',
                                                'url' => ['/admin/suppliers', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                        'type' => 'submit',
                                                        'type' => 'submit',
'class' => 'btn btn-danger btn-sm deleteentry',
                                                        'title' => 'Delete Supplier'   
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $suppliers->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
