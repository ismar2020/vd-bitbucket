@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Supplier's Products</div>
                    <div class="card-body">
                        <a href="{{ url('/admin/suppliers/create?productid='.request()->segment(3)) }}" class="btn btn-success btn-sm" title="Add New Supplier">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New Product Suppliers
                        </a>
<!--                        <a href="{{ url(url()->previous()) }}" class="btn btn-warning btn-sm" title="back">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>-->

                        
                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Name</th><th>Email</th><th>Supplier</th><th>Card (Front,Back)</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($productsuppliers as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->email }}</td>
                                        <td>
                                            <?php if($item->picture_company != null) { ?>
                                            <img width="60" src="<?= url($item->picture_company); ?>" style="margin-bottom: 4px;">
                                            <?php }else{ ?>
                                            <img width="60" src="{{ url('images/no_image.png') }}" style="margin-bottom: 4px;">
                                            <?php } ?>
                                        </td>
                                        <td>
                                            <?php if($item->picture_card_front != null) { ?>
                                            <img width="60" src="<?= url($item->picture_card_front); ?>" style="margin-bottom: 4px;">
                                            <?php }else{ ?>
                                            <img width="60" src="{{ url('images/no_image.png') }}" style="margin-bottom: 4px;">
                                            <?php } ?>
                                            <?php if($item->picture_card_back != null) { ?>
                                            <img width="60" src="<?= url($item->picture_card_back); ?>" style="margin-bottom: 4px;">
                                            <?php }else{ ?>
                                            <img width="60" src="{{ url('images/no_image.png') }}" style="margin-bottom: 4px;">
                                            <?php } ?>
                                        </td>
                                        
                                        
                                        <td>
                                            <a href="{{ url('/admin/suppliers/' . $item->id) }}" title="View Supplier" style="display:none;"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                            <a href="{{ url('/admin/suppliers/' . $item->id . '/edit?productid='.request()->segment(3)) }}" title="Edit Supplier"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                            {!! Form::open([
                                                'method' => 'DELETE',
                                                'url' => ['/admin/suppliers', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                        'type' => 'submit',
                                                        'type' => 'submit',
'class' => 'btn btn-danger btn-sm deleteentry',
                                                        'title' => 'Delete Supplier'   
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
