<!--***************************Add product section*********************************-->
<div class="row">
    <label class="control-label col-md-3">Sets Per Package</label>
    <input type="text" class="form-control col-md-3 sets_per_package" onkeyup="update_quantity_per_package(<?=$nested_id?>,$(this).val(),'sets')" name="sets_per_package" value="{{$sets_per_package}}" /><br><br>
    <div class="col-md-3">
        <select class="form-control nested_product_searched select2" name="nested_product"><?php $data = get_products_list(); ?>
            <option value="">Select Product</option>
            @foreach($data as $key => $value)
            @if($value != '')
            <option value="{{$key}}">{{$value}}</option>
            @endif
            @endforeach
        </select>
    </div>
    <div class="col-md-3">
        <a href="javascript:void(0)" class="btn btn-primary btn-sm" onclick="nested_product_create({{$nested_id}});"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Add Products</a>
    </div>   
</div>
<br>
<!--******************************************************************************-->
<input type="hidden" value="{{$nested_id}}" name="" id="hidden_nested_id"/>
<div class="responsive" style="overflow-y:hidden">
    <table class="table table-bordered" id="tbl_nested_products_body">
        <thead>
            <tr>
                <th>#</th>
                <th>Product Name</th>
                <th>Product Image</th>
                <th>Quantity Per Package</th>
                <th><i class="fa fa-cogs"></i></th>
            </tr>
        </thead>
        <tbody id="tbl_nested_products_body">
            <?php
            $i = '1';
            foreach ($nested_products as $details):
                ?>
                <tr id="rec-<?= $i ?>">
            <input type="hidden" value="{{$details->id}}" name="nested_products[<?= $i ?>][row_id]" />
            
            <td><span class="sn"><?= $i ?></span></td>

            <?php
            $product_data = get_product_details_by_id($details->product_id);

            if ($product_data->pic_id == null || $product_data->pic_id == '') {
                $product_pic = 'images/no_image.png';
            } else {
                $product_pic = getFile($product_data->pic_id);
            }
            ?>
            <td>
                <input type="hidden" class="form-control" name="product_id" value="{{$product_data->id}}"/>
                <input type="text" class="form-control" name="product_name" value="{{$product_data->name}}" disabled/>
            </td>
            <td>
                <img src="{{ url("$product_pic") }}" width="100"/>
            </td>
            <td>
                <input type="text" class="form-control nested_quantity_per_package" name="" value="{{$details->quantity_per_page}}" onkeyup="update_quantity_per_package(<?= $details->id?>,$(this).val(),'quantity')"/>
            </td>
            <td>
                <a class="btn btn-xs" data-id="<?= $i ?>" onclick="deleteOrderDetail('product_packaging_options',{{$details->id}});"><i class="fa fa-trash"></i></a>
            </td>
            </tr>
            <?php $i++;
        endforeach;
        ?>
        </tbody>
    </table>
</div>

<script>
$(document).ready(function() {
  $(".select2").select2({
    dropdownParent: $("#packagingOptionsProductModalLongnew")
  });
});

</script>

