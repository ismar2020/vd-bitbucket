@extends('layouts.backend')
@section('content')
<div class="container">
    <div class="row">
        @include('admin.sidebar')
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Product {{ $product->id }}</div>
                <div class="card-body">
                    <?php
                    if (isset($previous)) {
                        if ($previous == 'supplier') {
                            ?>
                            <a href="{{ url('/admin/suppliers') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <?php } else { ?>

                            <a href="{{ url('/admin/contacts') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>                            
                            <?php
                        }
                    } else {
                        ?>
                        <a href="{{ url('/admin/products') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                    <?php } ?>

                    <a href="{{ url('/admin/products/' . $product->id . '/edit') }}" title="Edit Product"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                    {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['admin/products', $product->id],
                    'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                    'type' => 'submit',
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-sm deleteentry',
                    'title' => 'Delete Product',
                    'onclick'=>'return confirm("Confirm delete?")'
                    ))!!}
                    {!! Form::close() !!}
                    <br/>
                    <br/>
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th>ID</th><td>{{ $product->id }}</td>
                                </tr>
                                <tr><th> Name </th><td> {{ $product->name }} </td></tr><tr><th> Category </th><td>{{ get_category_name_by_id($product->category_id) }}</td></tr><tr><th> Product Type </th><td>{{ get_product_type_name_by_id($product->type_id) }}</td></tr>
                                <tr>
                                    <th> Product Image </th>
                                    <td>
                                        <?php if ($product->pic_id != null) { ?>
                                            <img width="60" src="<?= url($product->pic_id); ?>" style="margin-bottom: 4px;">
                                        <?php } else { ?>
                                            <img width="60" src="{{ url('images/no_image.png') }}" style="margin-bottom: 4px;">
                                        <?php } ?>
                                    </td>
                                </tr>
                                <tr><th> Product Price per Unit </th><td> {{ $product->product_price_per_unit }} </td></tr>
                                <tr><th> Product Currency </th><td> {{ get_currency_name_by_id($product->product_currency) }} </td></tr>
                                <tr><th> Product Length </th><td> {{ $product->product_length }} </td></tr>
                                <tr><th> Product Width </th><td> {{ $product->product_width }} </td></tr>
                                <tr><th> Product height </th><td> {{ $product->product_height }} </td></tr>
                                <tr><th> Product Weight </th><td> {{ $product->product_weight }} </td></tr>
                                <tr><th> Product CBM </th><td> {{ $product->product_cbm }} </td></tr>
                                <tr><th> Product Box length </th><td> {{ $product->product_box_length }} </td></tr>
                                <tr><th> Product Box Width </th><td> {{ $product->product_box_width }} </td></tr>
                                <tr><th> Product Box height </th><td> {{ $product->product_box_height }} </td></tr>
                                <tr><th> Product Box Weight </th><td> {{ $product->product_box_weight }} </td></tr>
                                <tr><th> Product Box CBM </th><td> {{ $product->product_box_cbm }} </td></tr>
                                <tr><th> Stack length </th><td> {{ $product->stack_length }} </td></tr>
                                <tr><th> Stack Width </th><td> {{ $product->stack_width }} </td></tr>
                                <tr><th> Stack height </th><td> {{ $product->stack_height }} </td></tr>
                                <tr><th> Stack Weight </th><td> {{ $product->stack_weight }} </td></tr>
                                <tr><th> Stack CBM </th><td> {{ $product->stack_cbm }} </td></tr>
                                <tr>  
                                    <th>Active</th>
                                    <td><?= ($product->active == '0') ? 'Yes' : 'No' ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
