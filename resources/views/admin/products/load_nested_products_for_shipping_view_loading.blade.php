<!--******************************************************************************-->
<input type="hidden" value="{{$nested_id}}" name="" id="hidden_nested_id"/>
<div class="responsive" style="overflow-y:hidden">
    <table class="table table-bordered" id="tbl_nested_products_body">
        <thead>
            <tr>
                <th>#</th>
                <th>Product Name</th>
                <th>Product Image</th>
                <th>Quantity Per Package</th>
            </tr>
        </thead>
        <tbody id="tbl_nested_products_body">
            <?php
            $i = '1';
            foreach ($nested_products as $details):
                ?>
                <tr id="rec-<?= $i ?>">
            <input type="hidden" value="{{$details->id}}" name="nested_products[<?= $i ?>][row_id]" />
            
            <td><span class="sn"><?= $i ?></span></td>

            <?php
            $product_data = get_product_details_by_id($details->product_id);

            if ($product_data->pic_id == null || $product_data->pic_id == '') {
                $product_pic = 'images/no_image.png';
            } else {
                $product_pic = $product_data->pic_id;
            }
            ?>
            <td>
                <input type="hidden" class="form-control" name="product_id" value="{{$product_data->id}}"/>
                <input type="text" class="form-control" name="product_name" value="{{$product_data->name}}" disabled=""/>
            </td>
            <td>
                <img src="{{ url("$product_pic") }}" width="100"/>
            </td>
            <td>
                <input type="text" class="form-control nested_quantity_per_package" name="" value="{{$details->quantity_per_page}}" onkeyup="update_quantity_per_package(<?= $details->id?>,$(this).val(),'quantity')" disabled=""/>
            </td>
            </tr>
            <?php $i++;
        endforeach;
        ?>
        </tbody>
    </table>
</div>

<script>
$(document).ready(function() {
  $(".select2").select2({
    dropdownParent: $("#packagingOptionsProductModalLongnew")
  });
});
</script>

