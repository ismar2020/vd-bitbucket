<!-- --------------****************tab code**************************------------ -->  
<ul class="nav nav-tabs">
    <li><a data-toggle="tab" href="#product_home"  id="home_active">Product Home</a></li>
    @if (\Auth::user()->roles->first()->name != 'supplier')
    <li><a data-toggle="tab" href="#product_supplier">Supplier</a></li>
    @endif
    <!--    <li><a data-toggle="tab" href="#product_box">Box</a></li>
        <li><a data-toggle="tab" href="#product_stack">Stack</a></li>-->
    @if($formMode == 'edit')
    <li><a data-toggle="tab" href="#product_quantity">Quantity Per Container</a></li>
    <li><a data-toggle="tab" href="#attachments">Attachments</a></li>
    <li><a data-toggle="tab" href="#packaging_options">Packaging Options</a></li>
    <li><a data-toggle="tab" href="#barcodes">Barcodes</a></li>
    @endif
</ul>

<div class="tab-content">
    <div id="product_home" class="tab-pane fade in active">
        <br>


        <!-- ********************supplier's ID************************************-->
        <?php if (Request::exists('supplierid')) { ?><input type="hidden" name="supplierid" class="form-control productid" value="{{ Request::get('supplierid') }}"/><?php } ?>
        
        <!--quote line id-->
        
        <?php if (Request::exists('quoteLineId')) { ?><input type="hidden" name="quote_line_id" class="form-control quote_line_id" value="{{ Request::get('quoteLineId') }}"/><?php } ?>
        
        <!--****************************ends*********************************************-->
        @if($formMode == 'edit')
        <input type="hidden" name="product_id" value="{{ $product->id }}"/>
        @endif
        <div class="row">
            <div class="col-md-6 form-group{{ $errors->has('name') ? 'has-error' : ''}}">
                {!! Form::label('name', 'Product Name', ['class' => 'control-label']) !!}
                {!! Form::text('name', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
            </div>
            <?php
            if ($formMode == 'create') {
                $image = 'images/no_image.png';
            } else if ($formMode == 'edit') {
                if ($product->pic_id != null) {
                    $image = getFile($product->pic_id);
                } else {
                    $image = 'images/no_image.png';
                }
            }
            ?>

            <div class="col-md-3 form-group{{ $errors->has('pic_id') ? 'has-error' : ''}}">
                {!! Form::label('pic_id', 'Product Picture', ['class' => 'control-label']) !!}<br>
                <input type="file" class="hidden_quote_image_input_div edit_product_picture_dynamic" name="pic_id" id = "product_image_hidden" onchange="showImage(this, 'showProductPicture');"/>
                <img src="{{url("$image")}}" width="50" class="showProductPicture"/>
                <div class = "edit_delete">

                    <!--my code for test-->
<!--                    <a href="javascript:void(0)"><span class="fa fa-pencil edit_pencil productimageEditPencil" aria-hidden="true"></span></a>-->

                    <a href="javascript:void(0)"><span class="fa fa-pencil edit_pencil" aria-hidden="true" onclick = "uploadImageDynamically('product_image_hidden')"></span></a>


                    <a href="javascript:void(0)" <?php if($formMode == 'edit'){ ?> onclick="removeImageFromTable(<?=$product->id?>, 'pic_id', 'products', 'showProductPicture');" <?php } ?>><span class="fa fa-times remove_image" aria-hidden="true"></span></a>
                </div>


                {!! $errors->first('pic_id', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 form-group{{ $errors->has('product_length') ? 'has-error' : ''}}">
                {!! Form::label('product_length', 'Product Length (cm)', ['class' => 'control-label']) !!}
                {!! Form::text('product_length', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                {!! $errors->first('product_length', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="col-md-3 form-group{{ $errors->has('product_width') ? 'has-error' : ''}}">
                {!! Form::label('product_width', 'Product Width (cm)', ['class' => 'control-label']) !!}
                {!! Form::text('product_width', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                {!! $errors->first('product_width', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="col-md-3 form-group{{ $errors->has('product_height') ? 'has-error' : ''}}">
                {!! Form::label('product_height', 'Product Height (cm)', ['class' => 'control-label']) !!}
                {!! Form::text('product_height', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                {!! $errors->first('product_height', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="col-md-3 form-group{{ $errors->has('product_weight') ? 'has-error' : ''}}">
                {!! Form::label('product_weight', 'Product Weight (kg)', ['class' => 'control-label']) !!}
                {!! Form::text('product_weight', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                {!! $errors->first('product_weight', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 form-group{{ $errors->has('product_cbm') ? 'has-error' : ''}}">
                {!! Form::label('product_cbm', 'Product Cbm', ['class' => 'control-label']) !!}
                {!! Form::text('product_cbm', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                {!! $errors->first('product_cbm', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="col-md-3 form-group{{ $errors->has('product_price_per_unit') ? 'has-error' : ''}}">
                {!! Form::label('product_price_per_unit', 'FOB Price', ['class' => 'control-label']) !!}
                {!! Form::text('product_price_per_unit', null, ('required' == 'required') ? ['class' => 'form-control format_me_in_decimals', 'required' => 'required'] : ['class' => 'form-control format_me_in_decimals']) !!}
                {!! $errors->first('product_price_per_unit', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="col-md-3 form-group{{ $errors->has('product_currency') ? 'has-error' : ''}}">
                {!! Form::label('product_currency', 'FOB Currency', ['class' => 'control-label']) !!}
                <select class="form-control select2" name="product_currency">

                    @if($formMode == 'edit')
                    <?php $id = $product->product_currency; ?>
                    {!!  get_currency_list_with_symbols_through_id($product->product_currency) !!}
                    @else
                    {!!  get_limited_currency_list_with_symbols_through_id() !!}
                    @endif

                </select>
                {!! $errors->first('product_currency', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="col-md-3 form-group{{ $errors->has('shipping_port') ? 'has-error' : ''}}">
                {!! Form::label('shipping_port', 'Port Of Origin', ['class' => 'control-label']) !!}

                <select class="form-control select2" name="shipping_port"><?php $data = get_shipping_port_list(); ?>
                    <option value="">Select Port Of Origin</option>
                    @foreach($data as $key => $value)
                    @if(isset($formMode) && $formMode == 'edit' && $key == $product->shipping_port)
                    <option value="{{$key}}" selected="">{{$value}}</option>

                    @else
                    <option value="{{$key}}">{{$value}}</option>
                    @endif
                    @endforeach
                </select>
                {!! $errors->first('shipping_port', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 form-group{{ $errors->has('category_id') ? 'has-error' : ''}}">
                {!! Form::label('category_id', 'Category', ['class' => 'control-label']) !!}
                <select class="form-control select2" name="category_id"><?php $data = get_categories_list(); ?>
                    <option value="">Select</option>
                    @foreach($data as $key => $value)
                    @if($formMode == 'edit' && $product->category_id == $key)
                    <option value="{{$key}}" selected>{{$value}}</option>
                    @else
                    <option value="{{$key}}">{{$value}}</option>
                    @endif
                    @endforeach
                </select>

                {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="col-md-3 form-group{{ $errors->has('type_id') ? 'has-error' : ''}}">
                {!! Form::label('type_id', 'Product Type', ['class' => 'control-label']) !!}
                <select class="form-control select2" name="type_id"><?php $data = get_product_type_list(); ?>
                    <option value="">Select</option>
                    @foreach($data as $key => $value)
                    @if($formMode == 'edit' && $product->type_id == $key)
                    <option value="{{$key}}" selected>{{$value}}</option>
                    @else
                    <option value="{{$key}}">{{$value}}</option>
                    @endif
                    @endforeach
                </select>
                
                {!! $errors->first('type_id', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="col-md-3 form-group{{ $errors->has('product_material') ? 'has-error' : ''}}">
                {!! Form::label('product_material', 'Product Material', ['class' => 'control-label']) !!}
                <select class="form-control select2" name="product_material"><?php $data = get_material_list(); ?>
                    <option value="">Select</option>
                    @foreach($data as $key => $value)
                    @if($formMode == 'edit' && $product->product_material == $key)
                    <option value="{{$key}}" selected>{{$value}}</option>
                    @else
                    <option value="{{$key}}">{{$value}}</option>
                    @endif
                    @endforeach
                </select>

                {!! $errors->first('product_material', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="col-md-3 form-group{{ $errors->has('notes') ? 'has-error' : ''}}">
                {!! Form::label('notes', 'Notes', ['class' => 'control-label']) !!}
                {!! Form::textarea('notes', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required', 'rows' => 3] : ['class' => 'form-control', 'rows' => 3]) !!}
                {!! $errors->first('notes', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group{{ $errors->has('active') ? 'has-error' : ''}}" style="display: none;">
            {!! Form::label('active', 'Active', ['class' => 'control-label']) !!}
            {!! Form::select('active', array('0' => 'Yes', '1' => 'No'), '0',['class' => 'form-control']) !!}
            {!! $errors->first('active', '<p class="help-block">:message</p>') !!}
        </div>


        <div class="form-group">
            @if($formMode == 'edit')
            {!! Form::submit('Update', ['class' => 'btn btn-primary', 'onclick' => 'location.reload()']) !!}
            @else
            {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
            @endif

        </div>

    </div>
    @if (\Auth::user()->roles->first()->name != 'supplier')
    <div id="product_supplier" class="tab-pane fade">
        <div class="form-group{{ $errors->has('supplier_id') ? 'has-error' : ''}}">
            </br></br>
            {!! Form::label('supplier_id', 'Link to Existing', ['class' => 'control-label']) !!} -
            <?php $suppliers = get_suppliers_list() ?>

            <select class="select2" name = "supplier_id" style = "width:120px">
                <option value = "">  Select Supplier  </option>
                <?php
                foreach ($suppliers as $key => $val) {
                    if ($formMode == 'edit') {
                        $supplier_product = \App\SupplierProduct::where('product_id', $product->id)->first();
                        if ($supplier_product != null && $key == $supplier_product->supplier_id) {
                            ?>
                            <option value = "<?= $key ?>" selected>  <?= $val ?>  </option>
                        <?php } else { ?>
                            <option value = "<?= $key ?>">  <?= $val ?>  </option>
                            <?php
                        }
                    } else if ($formMode == 'create') {
                        if (Request::exists('supplierid') && $key == Request::get('supplierid')) {
                            ?>
                            <option value = "<?= $key ?>" selected>  <?= $val ?>  </option>
                            <?php
                        } else {
                            if (isset($supplier->id) && $key == $supplier->id) {
                                ?> 
                                <option value = "<?= $key ?>" selected>  <?= $val ?>  </option>
                            <?php } else { ?>
                                <option value = "<?= $key ?>">  <?= $val ?>  </option>
                                <?php
                            }
                        }
                    } else {
                        ?> 
                        <option value = "<?= $key ?>">  <?= $val ?>  </option>
                        <?php
                    }
                }
                ?>
            </select>
            {!! $errors->first('supplier_id', '<p class="help-block">:message</p>') !!}
        </div>

    </div>
    @endif
    <div id="product_box" class="tab-pane fade"><br>
        <div class="form-group{{ $errors->has('product_quantity_per_box') ? 'has-error' : ''}}">
            {!! Form::label('product_quantity_per_box', 'Product Quantity Per Box', ['class' => 'control-label']) !!}
            {!! Form::number('product_quantity_per_box', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
            {!! $errors->first('product_quantity_per_box', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group{{ $errors->has('product_box_length') ? 'has-error' : ''}}">
            {!! Form::label('product_box_length', 'Product Box Length', ['class' => 'control-label']) !!}
            {!! Form::text('product_box_length', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
            {!! $errors->first('product_box_length', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group{{ $errors->has('product_box_width') ? 'has-error' : ''}}">
            {!! Form::label('product_box_width', 'Product Box Width', ['class' => 'control-label']) !!}
            {!! Form::text('product_box_width', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
            {!! $errors->first('product_box_width', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group{{ $errors->has('product_box_height') ? 'has-error' : ''}}">
            {!! Form::label('product_box_height', 'Product Box Height', ['class' => 'control-label']) !!}
            {!! Form::text('product_box_height', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
            {!! $errors->first('product_box_height', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group{{ $errors->has('product_box_weight') ? 'has-error' : ''}}">
            {!! Form::label('product_box_weight', 'Product Box Weight', ['class' => 'control-label']) !!}
            {!! Form::text('product_box_weight', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
            {!! $errors->first('product_box_weight', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group{{ $errors->has('product_box_cbm') ? 'has-error' : ''}}">
            {!! Form::label('product_box_cbm', 'Product Box Cbm', ['class' => 'control-label']) !!}
            {!! Form::text('product_box_cbm', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
            {!! $errors->first('product_box_cbm', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group{{ $errors->has('product_box_material') ? 'has-error' : ''}}">
            {!! Form::label('product_box_material', 'Product Box Material', ['class' => 'control-label']) !!}
            {!! Form::select('product_box_material', get_material_list(), null, ['class' => 'form-control','size' => '4']) !!}
            {!! $errors->first('product_box_material', '<p class="help-block">:message</p>') !!}
        </div>   
        <div class="form-group{{ $errors->has('boxes_per_stack') ? 'has-error' : ''}}">
            {!! Form::label('boxes_per_stack', 'Boxes Per Stack', ['class' => 'control-label']) !!}
            {!! Form::number('boxes_per_stack', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
            {!! $errors->first('boxes_per_stack', '<p class="help-block">:message</p>') !!}
        </div> 
    </div>
    <div id="product_stack" class="tab-pane fade"><br>
        <div class="form-group{{ $errors->has('units_per_stack') ? 'has-error' : ''}}">
            {!! Form::label('units_per_stack', 'Units Per Stack', ['class' => 'control-label']) !!}
            {!! Form::number('units_per_stack', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
            {!! $errors->first('units_per_stack', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group{{ $errors->has('stack_length') ? 'has-error' : ''}}">
            {!! Form::label('stack_length', 'Stack Length', ['class' => 'control-label']) !!}
            {!! Form::text('stack_length', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
            {!! $errors->first('stack_length', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group{{ $errors->has('stack_width') ? 'has-error' : ''}}">
            {!! Form::label('stack_width', 'Stack Width', ['class' => 'control-label']) !!}
            {!! Form::text('stack_width', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
            {!! $errors->first('stack_width', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group{{ $errors->has('stack_height') ? 'has-error' : ''}}">
            {!! Form::label('stack_height', 'Stack Height', ['class' => 'control-label']) !!}
            {!! Form::text('stack_height', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
            {!! $errors->first('stack_height', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group{{ $errors->has('stack_weight') ? 'has-error' : ''}}">
            {!! Form::label('stack_weight', 'Stack Weight', ['class' => 'control-label']) !!}
            {!! Form::text('stack_weight', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
            {!! $errors->first('stack_weight', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group{{ $errors->has('stack_cbm') ? 'has-error' : ''}}">
            {!! Form::label('stack_cbm', 'Stack Cbm', ['class' => 'control-label']) !!}
            {!! Form::text('stack_cbm', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
            {!! $errors->first('stack_cbm', '<p class="help-block">:message</p>') !!}
        </div> 
        <div class="form-group{{ $errors->has('product_box_material') ? 'has-error' : ''}}">
            {!! Form::label('stack_material', 'Stack Material', ['class' => 'control-label']) !!}
            {!! Form::select('stack_material', get_material_list(), null, ['class' => 'form-control','size' => '4']) !!}
            {!! $errors->first('stack_material', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div id="product_quantity" class="tab-pane fade"><br>
        <!--        <div class="form-group{{ $errors->has('product_quantity_per_20ft') ? 'has-error' : ''}}">
                    {!! Form::label('product_quantity_per_20ft', 'Product Quantity Per 20ft', ['class' => 'control-label']) !!}
                    {!! Form::number('product_quantity_per_20ft', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                    {!! $errors->first('product_quantity_per_20ft', '<p class="help-block">:message</p>') !!}
                </div>-->
        <!--        <div class="form-group{{ $errors->has('product_quantity_per_40ft') ? 'has-error' : ''}}">
                    {!! Form::label('product_quantity_per_40ft', 'Product Quantity Per 40ft', ['class' => 'control-label']) !!}
                    {!! Form::number('product_quantity_per_40ft', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                    {!! $errors->first('product_quantity_per_40ft', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group{{ $errors->has('product_quantity_per_40fthq') ? 'has-error' : ''}}">
                    {!! Form::label('product_quantity_per_40fthq', 'Product Quantity Per 40fthq', ['class' => 'control-label']) !!}
                    {!! Form::number('product_quantity_per_40fthq', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                    {!! $errors->first('product_quantity_per_40fthq', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group{{ $errors->has('product_box_quantity_per_20ft') ? 'has-error' : ''}}">
                    {!! Form::label('product_box_quantity_per_20ft', 'Product Box Quantity Per 20ft', ['class' => 'control-label']) !!}
                    {!! Form::number('product_box_quantity_per_20ft', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                    {!! $errors->first('product_box_quantity_per_20ft', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group{{ $errors->has('product_box_quantity_per_40ft') ? 'has-error' : ''}}">
                    {!! Form::label('product_box_quantity_per_40ft', 'Product Box Quantity Per 40ft', ['class' => 'control-label']) !!}
                    {!! Form::number('product_box_quantity_per_40ft', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                    {!! $errors->first('product_box_quantity_per_40ft', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group{{ $errors->has('product_box_quantity_per_40fthq') ? 'has-error' : ''}}">
                    {!! Form::label('product_box_quantity_per_40fthq', 'Product Box Quantity Per 40fthq', ['class' => 'control-label']) !!}
                    {!! Form::number('product_box_quantity_per_40fthq', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                    {!! $errors->first('product_box_quantity_per_40fthq', '<p class="help-block">:message</p>') !!}
                </div> -->
        <!--        <form name="product_quantity_per_container" action="javascript:void(0)" accept-charset="UTF-8" class="form-horizontal" id="product_quantity_per_container_ajax" enctype="multipart/form-data" style="overflow-y: hidden;">-->
        @if($formMode == 'edit')
        <table class="table-striped table-bordered product_qty_table">
            <tr>
                <th>Available Containers</th>
                <th>Product Quantity</th>
<!--                <th>Box Quantity</th>
                <th>Stack Quantity</th>-->
            </tr>  
            <?php
            $containers = \App\Container::get();
            $i = 1;
            foreach ($containers as $item):
                $product_quantity = \App\ProductQuantityPerContainers::where('container_id', $item->id)->where('product_id', $product->id)->where('product_quantity', '!=', null)->first();

                $box_quantity = \App\ProductQuantityPerContainers::where('container_id', $item->id)->where('product_id', $product->id)->where('box_quantity', '!=', null)->first();
                $stack_quantity = \App\ProductQuantityPerContainers::where('container_id', $item->id)->where('product_id', $product->id)->where('stack_quantity', '!=', null)->first();
//                dd($item->id);
                ?>

                <tr>
                    <td>{{$item->name}}
                        <input type="hidden" name="quantity[<?php echo $i; ?>][container_id]" class="form-control" value="{{$item->id}}"/>
                        <input type="hidden" name="quantity[<?php echo $i; ?>][product_id]" class="form-control" value="{{$product->id}}"/>
                    </td>
                    <td><input type="text" name="quantity[<?php echo $i; ?>][product_quantity]" class="form-control product_quantity_per_container" onkeypress="return isNumber(event)" value="<?php
                        if ($product_quantity != null) {
                            echo $product_quantity->product_quantity;
                        }
                        ?>"/></td>

                </tr>
                <?php
                $i++;
            endforeach;
            ?>
        </table>
        @endif
    </div>
    @if($formMode == 'edit')
    <div id="attachments" class="tab-pane fade"><br>
        <!--<form name="product_attachments" action="javascript:void(0)" accept-charset="UTF-8" class="form-horizontal" id="product_attachments" enctype="multipart/form-data" style="overflow-y: hidden;">-->
            <input type="hidden" class="form-horizontal product_id" name="product_id" value="<?=$product->id?>"><br>
            <div id="dZUpload" class="dropzone">
                <div class="dz-default dz-message"><span>Drop Product Attachment Here</span></div>
                <meta name="csrf-token" content="{{ csrf_token() }}">

            </div><br>
            <div id="load_product_attachments_lines">

            </div>
        <!--</form>-->
        <br>
    </div>
    @endif
    <div id="packaging_options" class="tab-pane fade"><br>
        <br>
        <a href="javascript:void(0)" class="btn btn-primary btn-sm" onclick="packaging_options_create();"><i class="fa fa-plus" aria-hidden="true"></i>Add Packaging Type</a>
        <br>
        <br>
        <form method="post" name="packaging_options" action="javascript:void(0)" accept-charset="UTF-8" class="form-horizontal" id="packaging_options" enctype="multipart/form-data">

            <div class="responsive" style="overflow-y:hidden">
                <div id = "load_packaging_options">

                </div>

            </div>
        </form>
    </div>
    <div id="barcodes" class="tab-pane fade"><br>
        <div class="row">
            <div class="col-md-6 form-group{{ $errors->has('item_number') ? 'has-error' : ''}}">
                {!! Form::label('item_number', 'Item Number', ['class' => 'control-label']) !!}
                {!! Form::text('item_number', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                {!! $errors->first('item_number', '<p class="help-block">:message</p>') !!}
            </div> 
            <div class="col-md-6 form-group{{ $errors->has('supplier_item_number') ? 'has-error' : ''}}">
                {!! Form::label('supplier_item_number', 'Supplier Item Number', ['class' => 'control-label']) !!}
                {!! Form::text('supplier_item_number', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                {!! $errors->first('supplier_item_number', '<p class="help-block">:message</p>') !!}
            </div> 
        </div>
        <div class="row">
            <div class="col-md-6 form-group{{ $errors->has('item_code') ? 'has-error' : ''}}">
                {!! Form::label('item_code', 'Item Code', ['class' => 'control-label']) !!}
                {!! Form::text('item_code', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                {!! $errors->first('item_code', '<p class="help-block">:message</p>') !!}
            </div> 
            <div class="col-md-6 form-group{{ $errors->has('supplier_item_code') ? 'has-error' : ''}}">
                {!! Form::label('supplier_item_code', 'Supplier Item Code', ['class' => 'control-label']) !!}
                {!! Form::text('supplier_item_code', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                {!! $errors->first('supplier_item_code', '<p class="help-block">:message</p>') !!}
            </div> 
        </div>
        <div class="row">
            <div class="col-md-6 form-group{{ $errors->has('barcode') ? 'has-error' : ''}}">
                {!! Form::label('barcode', 'Barcode', ['class' => 'control-label']) !!}
                {!! Form::text('barcode', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                {!! $errors->first('barcode', '<p class="help-block">:message</p>') !!}
            </div> 
            <div class="col-md-6 form-group{{ $errors->has('outer_package_barcode') ? 'has-error' : ''}}">
                {!! Form::label('outer_package_barcode', 'Outer Package Barcode', ['class' => 'control-label']) !!}
                {!! Form::text('outer_package_barcode', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                {!! $errors->first('outer_package_barcode', '<p class="help-block">:message</p>') !!}
            </div> 
        </div>

    </div>
</div>
<!-- ----------****************tab code ends**************************--------------->

