@extends('layouts.backend')
@section('content')
<div class="container">
    <div class="row">
        @include('admin.sidebar')
        <div class="col-md-12">
            <div class="card">
                <!--<div class="card-header">Edit Product {{ get_name_by_id_and_table_name('products', $product->id, 'name') }}</div>-->
                <div class="card-header">Edit Product #{{ $product->id }}</div>
                <div class="card-body">
<!--                        <a href="{{ url('/admin/products') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                    <br />
                    <br />-->
                    @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    @endif

                    {!! Form::model($product, [
                    'method' => 'PATCH',
                    'url' => ['/admin/products', $product->id],
                    'class' => 'form-horizontal product_quantity_per_container',
                    'id' => 'product_quantity_per_container_with_product',
                    'files' => true
                    ]) !!}

                    @include ('admin.products.form', ['formMode' => 'edit'])

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
<!--add container products modal-->
<div class="modal fade" id="packagingOptionsProductModalLongnew" role="dialog" aria-labelledby="packagingOptionsProductModalLongnewTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background: #397657;">
                <h5 class="modal-title" id="orderProductModalLongNewTitle" style="color:#fff;">Add Products</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="javascript:void(0)" accept-charset="UTF-8" class="form-horizontal" id="addNestedProductForm" enctype="multipart/form-data">
                    @csrf
                    <div class="responsive" style="overflow-y:hidden">
                        <div id = "load_nested_products">

                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" id="modalCloseProduct" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!--end modal-->
<script>
//    $('.edit_prod_img').unbind('click').bind('click', function(){ 
//        $('.edit_product_picture_dynamic').click(function(e){e.preventDefault();}).click();
//    });
    load_packaging_options();
    function load_packaging_options() {
        var product_id = '<?= $product->id ?>';

        $.ajax({
            url: "{{url('admin/load_packaging_options')}}",
            method: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {product_id: product_id},
            beforeSend: function () {
                //                        Swal.showLoading();
            },
            onError: function () {
                swal("Error!", 'Something Went Wrong', "error");
            },
            success: function (response)
            {
                $('#load_packaging_options').html(response);

            }
        });
    }
    function load_nested_products(nested_id) {
        $.ajax({
            url: "{{url('admin/load_nested_products')}}",
            method: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {nested_id: nested_id},
            beforeSend: function () {
                //                        Swal.showLoading();
            },
            onError: function () {
                swal("Error!", 'Something Went Wrong', "error");
            },
            success: function (response)
            {
                $('#load_nested_products').html(response);

            }
        });
    }
    function packaging_options_create() {
        var product_id = '<?= $product->id ?>';

        $.ajax({
            url: "{{url('admin/insert_packaging_options')}}",
            method: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {product_id: product_id},
            beforeSend: function () {
                //                        Swal.showLoading();
            },
            onError: function () {
                swal("Error!", 'Something Went Wrong', "error");
            },
            success: function (response)
            {
                load_packaging_options();
            }
        });
    }
    function nested_product_create(nested_id) {
        var product_id = $('.nested_product_searched').val();
        if (product_id != '') {
            $.ajax({
                url: "{{url('admin/insert_nested_product')}}",
                method: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {nested_id: nested_id, product_id: product_id},
                beforeSend: function () {
                    //                        Swal.showLoading();
                },
                onError: function () {
                    swal("Error!", 'Something Went Wrong', "error");
                },
                success: function (response)
                {
                    load_packaging_options();
                    load_nested_products(nested_id);
                }
            });
        } else {
            swal('Error', 'Please Select Product First !');
        }
    }
    function update_quantity_per_package(row_id, values, type) {
//        alert($('.sets_per_package').val());
        if (row_id != '') {
            $.ajax({
                url: "{{url('admin/update_quantity_per_package')}}",
                method: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {row_id: row_id, values: values, type: type},
                beforeSend: function () {
                    //                        Swal.showLoading();
                },
                onError: function () {
                    swal("Error!", 'Something Went Wrong', "error");
                },
                success: function (response)
                {
                    if (response.status == 'true') {
//                        console.log(response.nested_quantity);
                        $('#quantity_per_pagckage_' + response.main_row_id).val(response.nested_quantity);
                        $('#quantity_per_pagckage_' + response.main_row_id).click();
                    }
                }
            });
        } else {
            swal('Error', 'Error somewhere !');
        }
    }


    $(document).on('change', '.nested_product', function () {
        var rowId = $('#' + $(this).closest('tr').attr('id'));
        var nested_product = rowId.find('.nested_product').val();
        var row_id = rowId.find('.row_id').val();
        var product_id = rowId.find('.product_id').val();
//        console.log(rowId);
        if (nested_product == '1') {
            rowId.find('.edit_loading').css('display', 'block');
            rowId.find('.not_available').css('display', 'none');
            rowId.find('.quantity_per_pagckage').prop('disabled', true);
        } else {
            rowId.find('.not_available').css('display', 'block');
            rowId.find('.edit_loading').css('display', 'none');
            rowId.find('.quantity_per_pagckage').prop('disabled', false);
        }

//        ajax for changing default nested product
        $.ajax({
            url: "{{url('admin/add_remove_default_nested_products')}}",
            method: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {row_id: row_id, product_id: product_id, nested_product: nested_product},
            beforeSend: function () {
                //                        Swal.showLoading();
            },
            onError: function () {
                swal("Error!", 'Something Went Wrong', "error");
            },
            success: function (response)
            {

                $('#quantity_per_pagckage_' + row_id).val('0');
                $('#quantity_per_pagckage_' + row_id).click();



            }
        });

    });
    //calculations for packaging cbm
    $(document).on('change click keyup', '.packaging_length,.packaging_width,.packaging_height', function () {
        //        alert('d');
        var rowId = $('#' + $(this).closest('tr').attr('id'));

        var l = rowId.find(".packaging_length").val();
        var w = rowId.find(".packaging_width").val();
        var h = rowId.find(".packaging_height").val();
        var cbm = l * w * h;
        var cbm = cbm / 1000000;
        rowId.find('.packaging_cbm').val(cbm.toFixed(2));
    });
    var base_url = "{{url('/')}}"
    $(document).ready(function () {
        Dropzone.autoDiscover = false;
        var url = "<?= url('admin/dropzoneProductAttachment?product_id=' . $product->id . '') ?>";
        $("#dZUpload").dropzone({
            url: url,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method: "POST",
            maxFilesize: 15,
            addRemoveLinks: true,
            success: function (file, response) {
                console.log(response);
                var imgName = response;
                file.previewElement.classList.add("dz-success");
                console.log("Successfully uploaded :" + imgName);
                load_product_attachments_lines();
            },
            error: function (file, response) {
                file.previewElement.classList.add("dz-error");
            }
        });
    });
    load_product_attachments_lines();
    function load_product_attachments_lines() {
        var product_id = '<?= $product->id ?>';

        $.ajax({
            url: "{{url('admin/load_product_attachments_lines')}}",
            method: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {product_id: product_id},
            beforeSend: function () {
                //                        Swal.showLoading();
            },
            onError: function () {
                swal("Error!", 'Something Went Wrong', "error");
            },
            success: function (response)
            {
                $('#load_product_attachments_lines').html(response);

            }
        });
    }

    //ends
//add quantity per package from nested product to packaging options
//    $(document).on('click', '.edit_loading', function () {

//        var rowId = $('#' + $(this).closest('tr').attr('id'));
//        $(".quantity_per_pagckage").prop('disabled', true);

//        var sum = 0;
//        $('.nested_quantity_per_package').each(function () {
//            sum += parseFloat(this.value);
//        });
//        rowId.find('.packaging_cbm').val(cbm.toFixed(2));
//    });


    //ON PAGE LOAD UNHIDE IF AUDIT IS OTHER
//    var ethical_audit = $('.ethical_audit').val();
//    if (ethical_audit == '4') {
//        $('.audit_other_name_div').css('display', 'block');
//    } else {
//        $('.audit_other_name_div').css('display', 'none');
//    }
</script>
@endsection
