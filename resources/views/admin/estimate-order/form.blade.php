<div class="form-group{{ $errors->has('quick_container_type') ? 'has-error' : ''}}">
    {!! Form::label('quick_container_type', 'Quick Container Type', ['class' => 'control-label']) !!}
    
    {!! Form::select('quick_container_type', get_container_list(), null, ['class' => 'form-control','size' => '2']) !!}
    {!! $errors->first('quick_container_type', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('number_of_containers') ? 'has-error' : ''}}">
    {!! Form::label('number_of_containers', 'Number Of Containers', ['class' => 'control-label']) !!}
    {!! Form::number('number_of_containers', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('number_of_containers', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('cost_id') ? 'has-error' : ''}}">
    {!! Form::label('cost_id', 'Cost Name', ['class' => 'control-label']) !!}
    {!! Form::select('cost_id', $costs, null, ['class' => 'form-control','size' => '3']) !!}
    {!! $errors->first('cost_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('product_price_per_unit') ? 'has-error' : ''}}">
    {!! Form::label('product_price_per_unit', 'Product Price Per Unit', ['class' => 'control-label']) !!}
    {!! Form::number('product_price_per_unit', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('product_price_per_unit', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('product_currency') ? 'has-error' : ''}}">
    {!! Form::label('product_currency', 'Product Currency', ['class' => 'control-label']) !!}
    <select class="form-control" id="product_currency" name="product_currency" size="5">
    {!!  get_currency_list_with_symbols() !!}
    </select>
    {!! $errors->first('product_currency', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
