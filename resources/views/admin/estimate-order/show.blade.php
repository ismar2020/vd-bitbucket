@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">EstimateOrder {{ $estimateorder->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/estimate-order') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/admin/estimate-order/' . $estimateorder->id . '/edit') }}" title="Edit EstimateOrder"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/estimateorder', $estimateorder->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'type' => 'submit',
'class' => 'btn btn-danger btn-sm deleteentry',
                                    'title' => 'Delete EstimateOrder',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $estimateorder->id }}</td>
                                    </tr>
                                    <tr><th> Quick Container Type </th><td> {{ get_container_name_by_id($estimateorder->quick_container_type) }} </td></tr><tr><th> Number Of Containers </th><td> {{ $estimateorder->number_of_containers }} </td></tr><tr><th> Cost Name</th><td> {{ get_cost_name_by_id($estimateorder->cost_id) }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
