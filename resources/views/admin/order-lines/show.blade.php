@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">OrderLine {{ $orderline->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/order-lines') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/admin/order-lines/' . $orderline->id . '/edit') }}" title="Edit OrderLine"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/orderlines', $orderline->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'type' => 'submit',
'class' => 'btn btn-danger btn-sm deleteentry',
                                    'title' => 'Delete OrderLine',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $orderline->id }}</td>
                                    </tr>
                                    <tr><th> Purchase Order </th><td> {{ $orderline->purchase_order }} </td></tr><tr><th> Shipping Port </th><td> {{ $orderline->shipping_port }} </td></tr><tr><th> Product Unit Quantity </th><td> {{ $orderline->product_unit_quantity }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
