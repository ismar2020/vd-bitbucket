<div class="form-group{{ $errors->has('purchase_order') ? 'has-error' : ''}}">
    {!! Form::label('purchase_order', 'Purchase Order', ['class' => 'control-label']) !!}
    {!! Form::text('purchase_order', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('purchase_order', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('shipping_port') ? 'has-error' : ''}}">
    {!! Form::label('shipping_port', 'Shipping Port', ['class' => 'control-label']) !!}
    {!! Form::text('shipping_port', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('shipping_port', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('product_unit_quantity') ? 'has-error' : ''}}">
    {!! Form::label('product_unit_quantity', 'Product Unit Quantity', ['class' => 'control-label']) !!}
    {!! Form::number('product_unit_quantity', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('product_unit_quantity', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('container') ? 'has-error' : ''}}">
    {!! Form::label('container', 'Container', ['class' => 'control-label']) !!}
    {!! Form::number('container', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('container', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('purchase_cost') ? 'has-error' : ''}}">
    {!! Form::label('purchase_cost', 'Purchase Cost', ['class' => 'control-label']) !!}
    {!! Form::number('purchase_cost', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('purchase_cost', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('wholesale_cost') ? 'has-error' : ''}}">
    {!! Form::label('wholesale_cost', 'Wholesale Cost', ['class' => 'control-label']) !!}
    {!! Form::number('wholesale_cost', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('wholesale_cost', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('retail_cost') ? 'has-error' : ''}}">
    {!! Form::label('retail_cost', 'Retail Cost', ['class' => 'control-label']) !!}
    {!! Form::number('retail_cost', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('retail_cost', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('total_number_of_containers') ? 'has-error' : ''}}">
    {!! Form::label('total_number_of_containers', 'Total Number Of Containers', ['class' => 'control-label']) !!}
    {!! Form::number('total_number_of_containers', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('total_number_of_containers', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('total_purchase_cost') ? 'has-error' : ''}}">
    {!! Form::label('total_purchase_cost', 'Total Purchase Cost', ['class' => 'control-label']) !!}
    {!! Form::number('total_purchase_cost', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('total_purchase_cost', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('total_wholesale_cost') ? 'has-error' : ''}}">
    {!! Form::label('total_wholesale_cost', 'Total Wholesale Cost', ['class' => 'control-label']) !!}
    {!! Form::number('total_wholesale_cost', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('total_wholesale_cost', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('total_retail_cost') ? 'has-error' : ''}}">
    {!! Form::label('total_retail_cost', 'Total Retail Cost', ['class' => 'control-label']) !!}
    {!! Form::number('total_retail_cost', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('total_retail_cost', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('confirmed') ? 'has-error' : ''}}">
    {!! Form::label('confirmed', 'Confirmed', ['class' => 'control-label']) !!}
    {!! Form::number('confirmed', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('confirmed', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('active') ? 'has-error' : ''}}">
    {!! Form::label('active', 'Active', ['class' => 'control-label']) !!}
    {!! Form::number('active', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('active', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
