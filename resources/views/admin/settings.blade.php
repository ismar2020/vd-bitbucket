@extends('layouts.backend')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="container-fluid content-top-gap">

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb my-breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/home')}}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Settings</li>
                    </ol>
                </nav>
                <!--                <div class="welcome-msg pt-3 pb-4">
                                    <h1>Hi <span class="text-primary">{{ Auth::user()->username}}</span>, Welcome back</h1>
                
                                </div>-->

                <!-- statistics data -->
                <div class="statistics">
                    <div class="row">
                        <div ss class="col-xl-12 pr-xl-2">
                            <div class="row">
                                <div class="col-lg-4 col-sm-6 statistics-grid">
                                    <a href = "{{url('admin/margins')}}">
                                        <div class="card card_border border-primary-top p-4">
                                            <i class="lnr lnr-text-align-left"> </i>
                                            <h3 class="number">@php echo get_margins_count(); @endphp</h3>
                                            <p class="stat-text">Margins</p>

                                        </div>
                                    </a>
                                </div>



                                <?php if (Auth::check() && (Auth::user()->hasRole('super_admin'))) { ?>
                                    <div class="col-lg-4 col-sm-6 statistics-grid">
                                        <a href = "{{url('admin/containers')}}">
                                            <div class="card card_border border-primary-top p-4">
                                                <i class="lnr lnr-database"> </i>
                                                <h3 class="number">@php echo get_containers_count(); @endphp</h3>
                                                <p class="stat-text">Containers</p>

                                            </div>
                                        </a>
                                    </div>
                                <?php } ?>
                                <div class="col-lg-4 col-sm-6 statistics-grid">
                                    <a href = "{{url('admin/materials')}}">
                                        <div class="card card_border border-primary-top p-4">
                                            <i class="lnr lnr-briefcase"> </i>
                                            <h3 class="number">@php echo get_materials_count(); @endphp</h3>
                                            <p class="stat-text">Material</p>

                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-4 col-sm-6 statistics-grid">
                                    <a href = "{{url('admin/category')}}">
                                        <div class="card card_border border-primary-top p-4">
                                            <i class="lnr lnr-list"> </i>
                                            <h3 class="number">@php echo get_categories_count(); @endphp</h3>
                                            <p class="stat-text">Product Category</p>

                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-4 col-sm-6 statistics-grid">
                                    <a href = "{{url('admin/quote-categories')}}">
                                        <div class="card card_border border-primary-top p-4">
                                            <i class="lnr lnr-list"> </i>
                                            <h3 class="number">@php echo get_quote_categories_count(); @endphp</h3>
                                            <p class="stat-text">Quote Category</p>

                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-4 col-sm-6 statistics-grid">
                                    <a href = "{{url('admin/supplier-category')}}">
                                        <div class="card card_border border-primary-top p-4">
                                            <i class="lnr lnr-list"> </i>
                                            <h3 class="number">@php echo get_supplier_categories_count(); @endphp</h3>
                                            <p class="stat-text">Supplier Category</p>

                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-4 col-sm-6 statistics-grid">
                                    <a href = "{{url('admin/product-type')}}">
                                        <div class="card card_border border-primary-top p-4">
                                            <i class="lnr lnr-indent-decrease"> </i>
                                            <h3 class="number">@php echo get_product_types_count(); @endphp</h3>
                                            <p class="stat-text">Product Type</p>

                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-4 col-sm-6 statistics-grid">
                                    <a href = "{{url('admin/fis-template')}}">
                                        <div class="card card_border border-primary-top p-4">
                                            <i class="lnr lnr-indent-increase"> </i>
                                            <h3 class="number">@php echo get_fis_templates_count(); @endphp</h3>
                                            <p class="stat-text">Landed Cost Templates</p>

                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-4 col-sm-6 statistics-grid">
                                    <a href = "{{url('admin/shipping-rate')}}">
                                        <div class="card card_border border-primary-top p-4">
                                            <i class="lnr lnr-indent-increase"> </i>
                                            <h3 class="number">@php echo get_shipping_rates_count(); @endphp</h3>
                                            <p class="stat-text">Shipping Rates</p>

                                        </div>
                                    </a>
                                </div>
                                <?php if (Auth::check() && (Auth::user()->hasRole('super_admin'))) { ?>
                                    <div class="col-lg-4 col-sm-6 statistics-grid">
                                        <a href = "{{url('admin/shipping-port')}}">
                                            <div class="card card_border border-primary-top p-4">
                                                <i class="lnr lnr-indent-increase"> </i>
                                                <h3 class="number">@php echo get_shipping_port_count(); @endphp</h3>
                                                <p class="stat-text">Shipping Port</p>

                                            </div>
                                        </a>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div> 
                <!-- //statistics data -->
            </div> 
        </div>
    </div>
</div>
@endsection
