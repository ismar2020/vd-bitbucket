@extends('layouts.backend')

@section('content')
<div class="container">
    <div class="row">
        @include('admin.sidebar')

        <div class="col-md-9">
            <div class="card">
                <div class="card-header">Margin {{ $margin->id }}</div>
                <div class="card-body">

                    <a href="{{ url('/admin/margins') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>

                    <a href="{{ url('/admin/margins/' . $margin->id . '/edit') }}" title="Edit Margin"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                    <?php if (\Auth::user()->roles->first()->name == 'super_admin') { ?>
                    {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['admin/margins', $margin->id],
                    'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                    'type' => 'submit',
                    'type' => 'submit',
'class' => 'btn btn-danger btn-sm deleteentry',
                    'title' => 'Delete Margin',
                    'onclick'=>'return confirm("Confirm delete?")'
                    ))!!}
                    {!! Form::close() !!}
                    <?php } ?>
                    <br/>
                    <br/>
                    
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th>ID</th><td>{{ $margin->id }}</td>
                                </tr>
                                <tr><th> Name </th><td> {{ $margin->name }} </td></tr><tr><th> Margin (%) </th><td> {{ $margin->value }} </td></tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
