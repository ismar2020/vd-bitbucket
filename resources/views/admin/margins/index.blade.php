@extends('layouts.backend')

@section('content')
<div class="container">
    <div class="row">
        @include('admin.sidebar')

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Margins</div>
                <div class="card-body">
                    <?php if (\Auth::user()->roles->first()->name == 'super_admin' && \App\Margin::where('company_id', null)->count() < '2') { ?>
                        <a href="{{ url('/admin/margins/create') }}" class="btn btn-success btn-sm" title="Add New Margin">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
                        <br/>
                        <br/>
                    <?php } ?>

                    <div class="table-responsive suppliers_table_mobile">
                        <table class="table table-borderless">
                            <thead>
                                <tr>
                                    <th>#</th><th>Name</th><th>Margin (%)</th>
                                    <?php if (\Auth::user()->roles->first()->name == 'super_admin') { ?>
                                        <th>Company</th>
                                    <?php } ?>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($margins as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->value }}</td>
                                    <?php if (\Auth::user()->roles->first()->name == 'super_admin') { ?>
                                        <td><?php
                                            $company_name = get_companies_name_by_id($item->company_id);
                                            if ($company_name != 'NA') {
                                                ?><a href="{{ url('admin/usercompany',$item->company_id) }}">{{ $company_name }}</a><?php
                                            } else {
                                                echo 'Default';
                                            }
                                            ?></td>
                                    <?php } ?>
                                    <td>
                                        <?php if (\Auth::user()->roles->first()->name == 'super_admin') { ?>
                                            <a href="{{ url('/admin/margins/' . $item->id) }}" title="View Margin" style="display:none;"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                        <?php } ?>
                                        <a href="{{ url('/admin/margins/' . $item->id . '/edit') }}" title="Edit Margin"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                        <?php if (\Auth::user()->roles->first()->name == 'super_admin') { ?>
                                            {!! Form::open([
                                            'method' => 'DELETE',
                                            'url' => ['/admin/margins', $item->id],
                                            'style' => 'display:inline'
                                            ]) !!}
                                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                            'type' => 'submit',
                                            'type' => 'submit',
'class' => 'btn btn-danger btn-sm deleteentry',
                                            'title' => 'Delete Margin'  
                                            )) !!}
                                            {!! Form::close() !!}
                                        <?php } ?>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
