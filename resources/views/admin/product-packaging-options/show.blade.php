@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">ProductPackagingOption {{ $productpackagingoption->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/product-packaging-options') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/admin/product-packaging-options/' . $productpackagingoption->id . '/edit') }}" title="Edit ProductPackagingOption"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/productpackagingoptions', $productpackagingoption->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'type' => 'submit',
'class' => 'btn btn-danger btn-sm deleteentry',
                                    'title' => 'Delete ProductPackagingOption',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $productpackagingoption->id }}</td>
                                    </tr>
                                    <tr><th> Product Id </th><td> {{ $productpackagingoption->product_id }} </td></tr><tr><th> Packaging Type </th><td> {{ $productpackagingoption->packaging_type }} </td></tr><tr><th> Quantity Per Page </th><td> {{ $productpackagingoption->quantity_per_page }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
