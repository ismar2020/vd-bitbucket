<div class="form-group{{ $errors->has('product_id') ? 'has-error' : ''}}">
    {!! Form::label('product_id', 'Product Id', ['class' => 'control-label']) !!}
    {!! Form::number('product_id', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('product_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('packaging_type') ? 'has-error' : ''}}">
    {!! Form::label('packaging_type', 'Packaging Type', ['class' => 'control-label']) !!}
    {!! Form::text('packaging_type', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('packaging_type', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('quantity_per_page') ? 'has-error' : ''}}">
    {!! Form::label('quantity_per_page', 'Quantity Per Page', ['class' => 'control-label']) !!}
    {!! Form::text('quantity_per_page', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('quantity_per_page', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('packaging_length') ? 'has-error' : ''}}">
    {!! Form::label('packaging_length', 'Packaging Length', ['class' => 'control-label']) !!}
    {!! Form::number('packaging_length', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('packaging_length', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('packaging_width') ? 'has-error' : ''}}">
    {!! Form::label('packaging_width', 'Packaging Width', ['class' => 'control-label']) !!}
    {!! Form::number('packaging_width', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('packaging_width', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('packaging_height') ? 'has-error' : ''}}">
    {!! Form::label('packaging_height', 'Packaging Height', ['class' => 'control-label']) !!}
    {!! Form::number('packaging_height', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('packaging_height', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('gross_weight_including_packaging') ? 'has-error' : ''}}">
    {!! Form::label('gross_weight_including_packaging', 'Gross Weight Including Packaging', ['class' => 'control-label']) !!}
    {!! Form::text('gross_weight_including_packaging', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('gross_weight_including_packaging', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('packaging_cbm') ? 'has-error' : ''}}">
    {!! Form::label('packaging_cbm', 'Packaging Cbm', ['class' => 'control-label']) !!}
    {!! Form::number('packaging_cbm', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('packaging_cbm', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('floor_ready') ? 'has-error' : ''}}">
    {!! Form::label('floor_ready', 'Floor Ready', ['class' => 'control-label']) !!}
    {!! Form::text('floor_ready', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('floor_ready', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('stackable') ? 'has-error' : ''}}">
    {!! Form::label('stackable', 'Stackable', ['class' => 'control-label']) !!}
    {!! Form::text('stackable', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('stackable', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('nested_product') ? 'has-error' : ''}}">
    {!! Form::label('nested_product', 'Nested Product', ['class' => 'control-label']) !!}
    {!! Form::text('nested_product', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('nested_product', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
