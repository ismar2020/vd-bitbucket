@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Costs</div>
                    <div class="card-body">
                        <a href="{{ url('/admin/costs/create') }}" class="btn btn-success btn-sm" title="Add New Cost">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>

                        <br/>
                        <br/>
                        <div class="table-responsive suppliers_table_mobile">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Cost name</th>
                                        <th>Use</th>
                                        <th>Calculated value</th>
                                        <th>Currency</th>
                                        <th>Apply to</th>
                                        <th>Apply to all containers</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($costs as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->use }}</td>
                                        <td>{{ $item->value }}</td>
                                        <td>{{ get_currency_name_by_id($item->currency) }}</td>
                                        <td>{{ $item->apply_condition }}</td>
                                        <td>
                                            <select class="form-control apply_to_all_containers" name="apply_to_all_containers">
                                                <option value="0" <?php if($item->apply_to_all_containers == '0'){ echo 'selected'; }else{ echo ''; } ?>>Yes</option>
                                                <option value="1" <?php if($item->apply_to_all_containers == '1'){ echo 'selected'; }else{ echo ''; } ?>>No</option>
                                            </select>
                                        </td>
                                        <td>
                                            <a href="{{ url('/admin/costs/' . $item->id) }}" title="View Cost" style="display:none;"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                            <a href="{{ url('/admin/costs/' . $item->id . '/edit') }}" title="Edit Cost" style="display:none;"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                            {!! Form::open([
                                                'method' => 'DELETE',
                                                'url' => ['/admin/costs', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                        'type' => 'submit',
                                                        'type' => 'submit',
'class' => 'btn btn-danger btn-sm deleteentry',
                                                        'title' => 'Delete Cost'   
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
