<div class="form-group{{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('value') ? 'has-error' : ''}}">
    {!! Form::label('value', 'Value', ['class' => 'control-label']) !!}
    {!! Form::number('value', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('value', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('currency') ? 'has-error' : ''}}">
    {!! Form::label('currency', 'Currency', ['class' => 'control-label']) !!}
    <select class="form-control" id="currency" name="currency" size="6">
    {!!  get_currency_list_with_symbols() !!}
    </select>
    {!! $errors->first('currency', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('cost_percentage') ? 'has-error' : ''}}">
    {!! Form::label('cost_percentage', 'Cost Percentage (%)', ['class' => 'control-label']) !!}
    {!! Form::number('cost_percentage', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('cost_percentage', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('use') ? 'has-error' : ''}}">
    {!! Form::label('use', 'Use', ['class' => 'control-label']) !!}
    {!! Form::select('use', array('Cost Value' => 'Cost Value', 'Cost Percentage' => 'Cost Percentage'), 'Cost Percentage',['class' => 'form-control','size' => '2']) !!}
    {!! $errors->first('use', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('When_to_apply') ? 'has-error' : ''}}">
    {!! Form::label('When_to_apply', 'When To Apply', ['class' => 'control-label']) !!}
    {!! Form::select('When_to_apply', array('Each order line' => 'Each order line (total unit cost = unit price*quantity amount)', 'Total cost of the order' => 'Total cost of the order (sum of all order lines for unit price*quantity plus costs'), 'Each order line',['class' => 'form-control','size' => '2']) !!}
    {!! $errors->first('When_to_apply', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('apply_condition') ? 'has-error' : ''}}">
    {!! Form::label('apply_condition', 'Apply Condition', ['class' => 'control-label']) !!}
    {!! Form::select('apply_condition', array('None' => 'None', 'Specific product type' => 'Specific product type', 'Specific product category' => 'Specific product category', 'Specific product material' => 'Specific product material'), 'None',['class' => 'form-control','size' => '4']) !!}
    {!! $errors->first('apply_condition', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('order_id') ? 'has-error' : ''}}">
    {!! Form::label('order_id', 'Order Name', ['class' => 'control-label']) !!}
    {!! Form::select('order_id', $orders, null, ['class' => 'form-control','size' => '5']) !!}
    {!! $errors->first('order_id', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
