@extends('layouts.backend')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="container-fluid content-top-gap">

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb my-breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/home')}}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                    </ol>
                </nav>
                <div class="welcome-msg pt-3 pb-4">
                    <h1>Hi <span class="">{{ Auth::user()->username}}</span>, Welcome back</h1>

                </div>

                <!-- statistics data -->
                <div class="statistics">
                    <div class="row">
                        <div ss class="col-xl-12 pr-xl-2">
                            <div class="row">
                                
                                <?php if (Auth::check() && (Auth::user()->hasRole('super_admin') || Auth::user()->hasRole('company_user'))) { ?>
                                    <div class="col-sm-6 statistics-grid">
                                        <a href = "{{url('admin/users')}}">
                                            <div class="card card_border border-primary-top p-4">
                                                <i class="lnr lnr-users"> </i>
                                                <h3 class="number">@php echo get_user_count(); @endphp</h3>
                                                <p class="stat-text"><?= (\Auth::user()->roles->first()->name == 'company_user' && \Auth::user()->company_id != null) ? get_companies_name_by_id(\Auth::user()->company_id) : 'Total' ?> Users</p>

                                            </div>
                                        </a>
                                    </div>

                                    <div class="col-sm-6 statistics-grid">
                                        <a href = "{{url('admin/orders')}}">
                                            <div class="card card_border border-primary-top p-4">
                                                <i class="lnr lnr-briefcase"> </i>
                                                <h3 class="number">@php echo get_orders_count(); @endphp</h3>
                                                <p class="stat-text">Total Orders</p>

                                            </div>
                                        </a>
                                    </div>
                                <?php } ?>

                                <div class="col-sm-6 statistics-grid">
                                    <a href = "{{url('admin/suppliers')}}">
                                        <div class="card card_border border-primary-top p-4">
                                     <svg class="svg-icon" viewBox="0 0 20 20">
							<path d="M8.627,7.885C8.499,8.388,7.873,8.101,8.13,8.177L4.12,7.143c-0.218-0.057-0.351-0.28-0.293-0.498c0.057-0.218,0.279-0.351,0.497-0.294l4.011,1.037C8.552,7.444,8.685,7.667,8.627,7.885 M8.334,10.123L4.323,9.086C4.105,9.031,3.883,9.162,3.826,9.38C3.769,9.598,3.901,9.82,4.12,9.877l4.01,1.037c-0.262-0.062,0.373,0.192,0.497-0.294C8.685,10.401,8.552,10.18,8.334,10.123 M7.131,12.507L4.323,11.78c-0.218-0.057-0.44,0.076-0.497,0.295c-0.057,0.218,0.075,0.439,0.293,0.495l2.809,0.726c-0.265-0.062,0.37,0.193,0.495-0.293C7.48,12.784,7.35,12.562,7.131,12.507M18.159,3.677v10.701c0,0.186-0.126,0.348-0.306,0.393l-7.755,1.948c-0.07,0.016-0.134,0.016-0.204,0l-7.748-1.948c-0.179-0.045-0.306-0.207-0.306-0.393V3.677c0-0.267,0.249-0.461,0.509-0.396l7.646,1.921l7.654-1.921C17.91,3.216,18.159,3.41,18.159,3.677 M9.589,5.939L2.656,4.203v9.857l6.933,1.737V5.939z M17.344,4.203l-6.939,1.736v9.859l6.939-1.737V4.203z M16.168,6.645c-0.058-0.218-0.279-0.351-0.498-0.294l-4.011,1.037c-0.218,0.057-0.351,0.28-0.293,0.498c0.128,0.503,0.755,0.216,0.498,0.292l4.009-1.034C16.092,7.085,16.225,6.863,16.168,6.645 M16.168,9.38c-0.058-0.218-0.279-0.349-0.498-0.294l-4.011,1.036c-0.218,0.057-0.351,0.279-0.293,0.498c0.124,0.486,0.759,0.232,0.498,0.294l4.009-1.037C16.092,9.82,16.225,9.598,16.168,9.38 M14.963,12.385c-0.055-0.219-0.276-0.35-0.495-0.294l-2.809,0.726c-0.218,0.056-0.351,0.279-0.293,0.496c0.127,0.506,0.755,0.218,0.498,0.293l2.807-0.723C14.89,12.825,15.021,12.603,14.963,12.385"></path>
						</svg>
                                            <h3 class="number">@php echo get_suppliers_count(); @endphp</h3>
                                            <p class="stat-text">Total Suppliers</p>

                                        </div>
                                    </a>
                                </div>


                                <div class="col-sm-6 statistics-grid">
                                    <a href = "{{url('admin/contacts')}}">
                                        <div class="card card_border border-primary-top p-4">
                                         <svg xmlns="http://www.w3.org/2000/svg" width="24" height="40" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" class="feather feather-phone block mx-auto"><path d="M22 16.92v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path></svg>
                                            <h3 class="number">@php echo get_contacts_count(); @endphp</h3>
                                            <p class="stat-text">Total Contacts</p>

                                        </div>
                                    </a>
                                </div>
                                <div class="col-sm-6 statistics-grid">
                                    <a href = "{{url('admin/products')}}">
                                        <div class="card card_border border-primary-top p-4">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" class="feather feather-archive block mx-auto"><polyline points="21 8 21 21 3 21 3 8"></polyline><rect x="1" y="3" width="22" height="5"></rect><line x1="10" y1="12" x2="14" y2="12"></line></svg>
                                            <h3 class="number">@php echo get_product_count(); @endphp</h3>
                                            <p class="stat-text">Total Products</p>

                                        </div>
                                    </a>
                                </div>
                                <div class="col-sm-6 statistics-grid">
                                    <a href = "{{url('admin/quotes')}}">
                                        <div class="card card_border border-primary-top p-4">
                                       <i class="lnr lnr-layers"> </i>
                                            <h3 class="number">@php echo get_quotes_count(); @endphp</h3>
                                            <p class="stat-text">Total Quotes</p>

                                        </div>
                                    </a>
                                </div>
                                <?php if (Auth::check() && Auth::user()->hasRole('super_admin')) { ?>
                                    <div class="col-sm-6 statistics-grid">
                                        <a href = "{{url('admin/users/new_users')}}">
                                            <div class="card card_border border-primary-top p-4">
                                                <i class="lnr lnr-shirt "> </i>
                                                <h3 class="number">@php echo get_new_users_count(); @endphp</h3>
                                                <p class="stat-text">Total Registered</p>

                                            </div>
                                        </a>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div> 
                <!-- //statistics data -->



            </div> 
        </div>
    </div>
</div>
@endsection
