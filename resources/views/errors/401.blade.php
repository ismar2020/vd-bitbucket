<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <title>Stocks</title>

        <link rel="stylesheet" href="{{ asset('assets/css/all.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/style-starter.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/new-style.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/table.dataTable.css') }}">

        <!-- google fonts -->
        <link href="//fonts.googleapis.com/css?family=Nunito:300,400,600,700,800,900&display=swap" rel="stylesheet">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="{{ asset('assets/js/jquery-3.3.1.min.js') }}"></script>
        <script src="{{ asset('assets/js/jquery-1.10.2.min.js') }}"></script>
    </head>
    <body>
        <div id="notfound">
            <div class="notfound">
                <div class="notfound-404">
                    <h3>You are not authorized to access this resource.</h3>
                    <h1><span>4</span><span>0</span><span>1</span></h1>
                </div>
                <a class="btn-primary btn-sm" href="{{url('/home')}}" role="button">Continue to home</a>
            </div>
        </div>

        <!--======================================================================-->
        <script src="{{ asset('assets/js/jquery.nicescroll.js') }}"></script>

        <script src="{{ asset('assets/js/utils.js') }}"></script>
        <script src="{{ asset('assets/js/scripts.js') }}"></script>

        <!-- Bootstrap Core JavaScript -->

        <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
        <!-- Scripts -->

        <script src="{{ asset('assets/js/modernizr.js') }}"></script>
        <!--======================================================================-->        
    </body>

</html>