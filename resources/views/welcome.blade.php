@extends('layouts.welcome')

@section('content')


        <div class="banner banner-six">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="banne-content-wrapper-six">
                            <h3 class="sub-title wow fadeInUp" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">
                                Welcome 
                                <!--to <span>TradeWind</span>-->
                            </h3>

                        </div>
                        <!-- /.banne-content-wrapper-six -->
                    </div>
                    <!-- /.col-md-6 -->
                    <div class="col-md-12">
                        <div class="banner-image text-center">
                            <video width="600" 
                                   height="340" 
                                   controls poster= 
                                   "{{ asset('/images/thumb.png') }}">
                                <source src= 
                                        "{{ asset('/images/Trade.mp4') }}"
                                        type="video/mp4">            
                                </div>
                                <!-- /.banner-six-promo-image -->
                                </div>

                                <!-- /.col-md-6 -->
                                </div>
                                <!-- /.row -->
                                </div>
                                <!-- /.container -->
                                <!-- /.particles-six -->
                                </div>

                                 
                                    <!-- /.container -->
                                </div>

                                @endsection
