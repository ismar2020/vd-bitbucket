<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <title>Trade Wind</title>
        <link rel="icon" type="image/png" sizes="16x16" href="{{url('images/fav.png')}}">
        <link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/all.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/style-starter.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/new-style.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/table.dataTable.css') }}">
        <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css">


        <!-- google fonts -->
        <link href="//fonts.googleapis.com/css?family=Nunito:300,400,600,700,800,900&display=swap" rel="stylesheet">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="{{ asset('assets/js/jquery-3.3.1.min.js') }}"></script>
        <script src="{{ asset('assets/js/jquery-1.10.2.min.js') }}"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css">

        <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
        <!--//select2-->
        <?php if (Request::root() == 'https://www.thetradewind.com.au') { ?>  
            <!-- Global site tag (gtag.js) - Google Analytics -->
            <script async src="https://www.googletagmanager.com/gtag/js?id=UA-180790216-1"></script>
            <script>
    window.dataLayer = window.dataLayer || [];
    function gtag() {
    dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-180790216-1');
            </script>
        <?php } ?>
        <style>
            .addProduct {
                margin: 12px;
            }
            .addSupplier {
                margin: 12px;
            }
            #supplier_id {
                width: 100%;
            }
            #side_toggle_div {

            }
            #loading {
                width: 100%;
                height: 100%;
                top: 0;
                left: 0;
                position: fixed;
                display: block;
                opacity: 0.7;
                background-color: #fff;
                z-index: 99;
                text-align: center;
            }

            #loading-image {
                position: absolute;
                left: 50%;
                top: 50%;
                -webkit-transform: translate(-50%, -50%);
                transform: translate(-50%, -50%);
            }


            #mySidenav a {
                position: fixed;
                z-index: 999;
                right: -84px;
                transition: 0.3s;
                padding: 10px;
                text-decoration: none;
                font-size: 20px;
                color: white;
                border-radius: 7px 0px 0px 7px;
                top: 208px;
                padding: 7px 0px 7px 20px;
            }

            .toggle-sidebar {
                margin-left: 18px !IMPORTANT;
                margin: 0;
            }

            div#mySidenav span {
                padding-right: 11px;
            }

            #mySidenav a:hover {
                right: 0;
            }


            #contact {
                top: 10px;
                background-color: #000
            }
            div#mySidenav span {
                padding-right: 50px;
            }
            a#contact i {
                padding-right: 4px;
            }


            div#mySidenav span {
                padding-right: 11px;
            }



            .fa-rotate-45 {
                animation: animName 2s linear infinite;
            }

            @keyframes animName {
                0%{
                    transform: rotate(0deg);
                }
                100%{
                    transform: rotate(360deg);
                }
            }

            @media (max-width: 991px) {
  .navbar-header {
      float: none;
  }
  .navbar-left,.navbar-right {
      float: none !important;
  }
  .navbar-toggle {
      display: block;
  }
  .navbar-collapse {
      border-top: 1px solid transparent;
      box-shadow: inset 0 1px 0 rgba(255,255,255,0.1);
  }
  .navbar-fixed-top {
      top: 0;
      border-width: 0 0 1px;
  }
  .navbar-collapse.collapse {
      display: none!important;
  }
  .navbar-nav {
      float: none!important;
      margin-top: 7.5px;
  }
  .navbar-nav>li {
      float: none;
  }
  .navbar-nav>li>a {
      padding-top: 10px;
      padding-bottom: 10px;
  }
  .collapse.in{
      display:block !important;
  }
}





@media (max-width: 850px){
    a.toggle-btn {
        display: none !important;
    }
}

</style>
    </head>

    <body class="custom-sidebar">
        <div id="loading">
            <img id="loading-image" src="{{url('assets/images/loader.gif')}}" alt="Loading..." />
        </div>





        <div id="app">
            <!-- sidebar menu start -->
            <div class="sidebar-menu sticky-sidebar-menu">

                <!-- logo start -->
                <div class="logo">

                    <a href="{{ url('/home') }}"><img src="{{url('images/tradewind_logo.png')}}" width=""/></a>

                </div>

                <div class="logo-icon text-center">
                    <a href="{{ url('/home') }}" title="logo">T </a>
                </div>
                <!-- //logo end -->

                <div class="sidebar-menu-inner">

                    <!-- sidebar nav start -->
                    <ul class="nav nav-pills nav-stacked custom-nav">
<!--                        <li><a href="{{ url('/home') }}"><i class="fa fa-tachometer" aria-hidden="true"></i> <span>Dashboard</span></a></li>-->
                        <?php if (Auth::check() && (Auth::user()->hasRole('supplier') || Auth::user()->hasRole('super_admin') || Auth::user()->hasRole('company_user'))) { ?>
                            <li><a href="{{ url('/admin/suppliers') }}">   <svg viewBox="0 0 20 20">
							<path d="M8.627,7.885C8.499,8.388,7.873,8.101,8.13,8.177L4.12,7.143c-0.218-0.057-0.351-0.28-0.293-0.498c0.057-0.218,0.279-0.351,0.497-0.294l4.011,1.037C8.552,7.444,8.685,7.667,8.627,7.885 M8.334,10.123L4.323,9.086C4.105,9.031,3.883,9.162,3.826,9.38C3.769,9.598,3.901,9.82,4.12,9.877l4.01,1.037c-0.262-0.062,0.373,0.192,0.497-0.294C8.685,10.401,8.552,10.18,8.334,10.123 M7.131,12.507L4.323,11.78c-0.218-0.057-0.44,0.076-0.497,0.295c-0.057,0.218,0.075,0.439,0.293,0.495l2.809,0.726c-0.265-0.062,0.37,0.193,0.495-0.293C7.48,12.784,7.35,12.562,7.131,12.507M18.159,3.677v10.701c0,0.186-0.126,0.348-0.306,0.393l-7.755,1.948c-0.07,0.016-0.134,0.016-0.204,0l-7.748-1.948c-0.179-0.045-0.306-0.207-0.306-0.393V3.677c0-0.267,0.249-0.461,0.509-0.396l7.646,1.921l7.654-1.921C17.91,3.216,18.159,3.41,18.159,3.677 M9.589,5.939L2.656,4.203v9.857l6.933,1.737V5.939z M17.344,4.203l-6.939,1.736v9.859l6.939-1.737V4.203z M16.168,6.645c-0.058-0.218-0.279-0.351-0.498-0.294l-4.011,1.037c-0.218,0.057-0.351,0.28-0.293,0.498c0.128,0.503,0.755,0.216,0.498,0.292l4.009-1.034C16.092,7.085,16.225,6.863,16.168,6.645 M16.168,9.38c-0.058-0.218-0.279-0.349-0.498-0.294l-4.011,1.036c-0.218,0.057-0.351,0.279-0.293,0.498c0.124,0.486,0.759,0.232,0.498,0.294l4.009-1.037C16.092,9.82,16.225,9.598,16.168,9.38 M14.963,12.385c-0.055-0.219-0.276-0.35-0.495-0.294l-2.809,0.726c-0.218,0.056-0.351,0.279-0.293,0.496c0.127,0.506,0.755,0.218,0.498,0.293l2.807-0.723C14.89,12.825,15.021,12.603,14.963,12.385"></path>
						</svg>
                                    </i><span>Suppliers</span></a></li>

                            <li><a href="{{ url('/admin/contacts') }}"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" class="feather feather-phone block mx-auto"><path d="M22 16.92v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path></svg></i>
                                    <span>Contacts</span></a></li>

                            <li><a href="{{ url('/admin/products') }}"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" class="feather feather-archive block mx-auto"><polyline points="21 8 21 21 3 21 3 8"></polyline><rect x="1" y="3" width="22" height="5"></rect><line x1="10" y1="12" x2="14" y2="12"></line></svg><span>Products</span></a></li>

                            <li><a href="{{ url('/admin/quotes') }}"><i class="lnr lnr-layers"> </i> <span>Quotes</span></a></li>      

                        
                            <li><a href="{{ url('/admin/orders') }}"><i class="lnr lnr-briefcase"> </i> <span>Orders</span></a></li>
                            <?php } if (Auth::check() && Auth::user()->hasRole('super_admin') || Auth::user()->hasRole('company_user')) { ?>
                            <li><a href="{{ url('/admin/setting') }}">  <i class="lnr lnr-cog"></i></i>
                                    <span>Settings</span></a></li>
                            <li><a href="{{ url('/admin/admin') }}"><i class="lnr lnr-user"></i></i>
                                    <span>Admin</span></a></li>
                        <?php } ?>

<!--                        <li><a href="{{ url('/admin/margins')  }}"><i class="fa fa-cog" aria-hidden="true"></i>
                                <span>Margins</span></a></li> -->

<!--                        <li><a href="{{ url('/admin/users') }}"><i class="fa fa-user"></i><span> Users</span></a>
                        </li>        


                        <li><a href="{{ url('/admin/company') }}"><i class="fa fa-building" aria-hidden="true"></i>
                                <span>Company</span></a></li>-->





<!--                        <li><a href="{{ url('/admin/costs') }}"><i class="fa fa-usd" aria-hidden="true"></i>
                                <span>Costs</span></a></li>-->
<!--                        <li><a href="{{ url('/admin/estimate-order')  }}"><i class="fa fa-briefcase" aria-hidden="true"></i>
                                <span>Estimate-order</span></a></li> 

                        <li><a href="{{ url('/admin/display-ready')  }}"><i class="fa fa-sitemap" aria-hidden="true"></i>
                                <span>Display-ready</span></a></li> -->

<!--                        <li><a href="{{ url('/admin/containers')  }}"><i class="fa fa-hourglass-half" aria-hidden="true"></i>
                                <span>Containers</span></a></li> 
                        <li><a href="{{ url('/admin/materials')  }}"><i class="fa fa-industry" aria-hidden="true"></i>
                                <span>Material</span></a></li> 
                        <li><a href="{{ url('/admin/category')  }}"><i class="fa fa-list-alt" aria-hidden="true"></i>
                                <span>Categories</span></a></li> 
                        <li><a href="{{ url('/admin/product-type')  }}"><i class="fa fa-product-hunt" aria-hidden="true"></i>
                                <span>Product Type</span></a></li> -->
                    </ul>
                    <!-- //sidebar nav end -->
                    <!-- toggle button start -->
                    <a class="toggle-btn">
                        <i class="fa fa-angle-double-left menu-collapsed__left"><span></span></i>
                        <i class="fa fa-angle-double-right menu-collapsed__right"></i>
                    </a>
                    <a class="toggle-btn new_btn">
                        <i class="fa fa-bars" aria-hidden="true menu-collapsed__right"></i>
                    </a>
                    </a>
                    <!-- //toggle button end -->
                </div>
            </div>
            <!-- //sidebar menu end -->
            <!-- header-starts -->
            <div class="header sticky-header">

                <!-- notification menu start -->
                <div class="menu-right">
                    <div class="navbar user-panel-top">

                        <div class="user-dropdown-details d-flex">
                            <div class="profile_details_left">
                                <ul class="nofitications-dropdown">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i
                                                class="fa fa-bell-o"></i><span class="badge blue">1</span></a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <div class="notification_header">
                                                    <h3>You have new notifications</h3>
                                                </div>
                                            </li>
                                            <li><a href="#" class="grid">

                                                    <div class="user_img"><img src="{{ asset('/images/no_image.png') }}" alt=""></div>
                                                    <div class="notification_desc">
                                                        <p>New Product added</p>
                                                        <span>Just Now</span>
                                                    </div>
                                                </a></li>
                                            <div class="notification_bottom">
                                                <a href="javascript:void(0)" class="see" style="background-color :#000;">See all notifications</a>
                                            </div>
                                    </li>
                                </ul>
                                </li>

                                </ul>
                            </div>
                            <div class="profile_details">
                                <ul>
                                    <li class="dropdown profile_details_drop">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="dropdownMenu3" aria-haspopup="true"
                                           aria-expanded="false">
                                            <div class="profile_img">
                                                <img src="{{ asset('images/user.png') }}" class="rounded-circle" alt="" />
                                                <div class="user-active">
                                                    <span></span>
                                                </div>
                                            </div>
                                        </a>
                                        <ul class="dropdown-menu drp-mnu" aria-labelledby="dropdownMenu3">
                                            <li class="user-info">
                                                <h5 class="user-name">{{ Auth::user()->username}}({{\Auth::user()->roles->first()->name}})</h5>

                                            </li>
                                            <li> <a href="{{url('admin/profile')}}"><i class="lnr lnr-user"></i>My Profile</a> </li>

                                            <li class="logout"> <a href="javascript:void(0)"  onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();"><i class="fa fa-power-off"></i> Logout</a> </li>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!--notification menu end -->
            </div>
            <!-- //header-ends -->


            <main class="main-content">
<!--                <a href="javascript:void(0)" class="swipe_btn" onclick="sidetoggleFunction()"> <i class="fa fa-angle-left"></i></a>

                <div id="side_toggle_div" class="mySidenav side_btn sidenav" style="display: none;">
                    <div class="side_btn">
                        <a class="toggle-sidebar" id="scroll-left">
                            <i class="fa fa-angle-double-left"></i>
                        </a>
                        <a class="toggle-sidebar" id="scroll-right">
                            <i class="fa fa-angle-double-right"></i>
                        </a>
                    </div>
                </div>-->

                <div id="mySidenav" class="sidenav">

                    <a href="javascript:void(0)" id="contact">

                        <i class="fa fa-cog fa-rotate-45" aria-hidden="true"></i>

                        <span class="toggle-sidebar" id="scroll-left">
                            <i class="fa fa-angle-double-left"></i>
                        </span>
                        <span class="toggle-sidebar" id="scroll-right">
                            <i class="fa fa-angle-double-right"></i>
                        </span></a>
                </div>


                @yield('content')
            </main>



        </div>
        <!-- Modal -->
        <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Add Product</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="javascript:void(0)" accept-charset="UTF-8" class="form-horizontal" id="addProductForm" enctype="multipart/form-data">
                            @csrf
                            @include ('admin.products.form', ['formMode' => 'create'])
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" id="modalCloseProduct" data-dismiss="modal">Close</button>

                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="contactModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Add Contact</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="javascript:void(0)" accept-charset="UTF-8" class="form-horizontal" id="addContactForm" enctype="multipart/form-data">
                            @csrf
                            @include ('admin.contacts.ajaxform', ['formMode' => 'create'])
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" id="modalCloseContact" data-dismiss="modal">Close</button>

                    </div>
                </div>
            </div>
        </div>
        <!--modal ends-->
        <div class="modal fade" id="supplierModalLong" tabindex="-1" role="dialog" aria-labelledby="supplierModalLongTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Add Supplier</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="javascript:void(0)" accept-charset="UTF-8" class="form-horizontal" id="addSupplierForm" enctype="multipart/form-data">
                            @csrf
                            @include ('admin.suppliers.ajaxform', ['formMode' => 'create'])
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" id="modalCloseSupplier" data-dismiss="modal">Close</button>

                    </div>
                </div>
            </div>
        </div>
        <!--modal ends-->
        <div class="modal fade" id="shipping_product_packaging_option" tabindex="-1" role="dialog" aria-labelledby="shippingProductPackagingOptionTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="shippingProductPackagingOptionTitle">Product Packaging Options</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="javascript:void(0)" accept-charset="UTF-8" class="form-horizontal" id="shipping_product_packaging_option_form" enctype="multipart/form-data">
                            @csrf
                            <div id="shipping_product_packaging_option_html">

                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" id="modalCloseShippingProductPackaging" data-dismiss="modal">Close</button>

                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="view_particular_shipping_product_packaging_option" tabindex="-1" role="dialog" aria-labelledby="shippingProductPackagingOptionTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="viewShippingProductPackagingOptionTitle">View Selected Packaging Option Details</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="javascript:void(0)" accept-charset="UTF-8" class="form-horizontal" id="view_shipping_product_packaging_option_form" enctype="multipart/form-data">
                            @csrf
                            <div id="view_shipping_product_packaging_option_html">

                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" id="modalCloseShippingProductPackaging" data-dismiss="modal">Close</button>

                    </div>
                </div>
            </div>
        </div>
        <!--modal ends-->
        <div class="modal fade" id="packagingOptionsProductShippingViewLoadingModalLongnew" role="dialog" aria-labelledby="packagingOptionsProductModalLongnewTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="background: #397657;">
                        <h5 class="modal-title" id="orderProductModalLongNewTitle" style="color:#fff;">View Loading</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="responsive" style="overflow-y:hidden">
                            <div id = "load_nested_products_for_shipping_view_loading">

                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" id="modalCloseProduct" data-dismiss="modal">Close</button>

                    </div>
                </div>
            </div>
        </div>
        <!--end modal-->

        <script src="{{ asset('assets/js/jquery.nicescroll.js') }}"></script>

        <script src="{{ asset('assets/js/utils.js') }}"></script>
        <script src="{{ asset('assets/js/scripts.js') }}"></script>

        <!-- Bootstrap Core JavaScript -->

        <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
        <!-- Scripts -->

        <script src="{{ asset('assets/js/modernizr.js') }}"></script>





        <!-- DataTables -->


        <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}">

        </script>

        <script src="{{ asset('assets/plugins/datatables/be_tables_datatables.min.js') }}"></script> 
        <script src="{{ asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
        <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.colVis.min.js"></script>

        <script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <!--<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>-->

        @include('admin.ajax')
        <script>
                                                $(window).load(function () {
                                                    $('#loading').hide();
                                                });
                                                $(document).ready(function () {
                                                    $(document).ajaxStart(function () {
//                    $("#loading").show();
                                                    }).ajaxStop(function () {
                                                        $("#loading").hide();
                                                    });
                                                });
                                                function sidetoggleFunction() {
                                                    var x = document.getElementById("side_toggle_div");
                                                    if (x.style.display === "none") {
                                                        x.style.display = "block";
                                                    } else {
                                                        x.style.display = "none";
                                                    }
                                                }
                                                function load_nested_products_for_shipping_view_loading(nested_id) {
                                                    $.ajax({
                                                        url: "{{url('admin/load_nested_products_for_shipping_view_loading')}}",
                                                        method: "POST",
                                                        headers: {
                                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                                        },
                                                        data: {nested_id: nested_id},
                                                        beforeSend: function () {
                                                            //                        Swal.showLoading();
                                                        },
                                                        onError: function () {
                                                            swal("Error!", 'Something Went Wrong', "error");
                                                        },
                                                        success: function (response)
                                                        {
                                                            $('#load_nested_products_for_shipping_view_loading').html(response);

                                                        }
                                                    });
                                                }
        </script>
        <script type="text/javascript">
//Calculation code for total cost nd wholesale cost in shipping container products page
            $(document).on('keyup', '.ship_container_order_quantity', function () {
                var order_quantity = $(this).val();
                var rowId = $(this).closest('tr').attr('id');
                var currentRow = $("#" + rowId);
                var unit_price = currentRow.find(".ship_product_unit_price").val();
                var orderline_wholesale_margin = currentRow.find(".ship_orderline_wholesale_margin").val();
                var container_total_cost = unit_price * order_quantity;

                var container_total_wholesale_cost = container_total_cost * (orderline_wholesale_margin / 100 + 1);
                currentRow.find('.ship_container_total_costs').val(container_total_cost);
                currentRow.find('.container_total_wholesale_price').val(container_total_wholesale_cost.toFixed(2));

            });
// *************************** --ends-- ****************************************

            $(document).on('keyup', '.ship_container_order_quantity', function () {
                var order_quantity = $(this).val();
                var rowId = $(this).closest('tr').attr('id');
                var currentRow = $("#" + rowId);
                var ship_container_cbm = currentRow.find(".ship_container_cbm").val();
                var number_of_units_that_fit = currentRow.find(".number_of_units_that_fit").val();
                if (number_of_units_that_fit > 0) {
                    var container_order_quantity_cbm = (ship_container_cbm / number_of_units_that_fit) * order_quantity;
                    currentRow.find('.container_order_quantity_cbm').val(container_order_quantity_cbm);
                }
            });

// *************************** --ends-- ****************************************



// **************************************** --ends-- ****************************************
            $(document).on('click', '.deleteRow', function () {
                var didConfirm = confirm("Are you sure You want to delete");
                if (didConfirm == true) {
                    $(this).closest('tr').remove();
                }
            });
            $(document).ready(function () {
                //redirecting to home tab after every time user click on order module
                setTimeout(function () {
                    $("li #home_active").trigger('click');
                }, 10);
                //ends

                //redirecting on required tabs when coming from different page
                var activeTab = window.location.hash;
                if (activeTab) {
                    setTimeout(function () {
                        $('a[href="' + activeTab + '"]').trigger('click');
                    }, 10);
                }
                //ends
//applying select2
//    $('.select2').select2();
<?php if (Route::currentRouteName() == 'orders.edit') { ?>
                    $(".select2").select2({
                        dropdownParent: $("#packagingOptionsProductModalLongnew"),
                        dropdownParent: $("#orderProductModalLongnew")
                    });
                    $(".select2_port").select2();
<?php } else if (Route::currentRouteName() == 'quotes.edit') { ?>
                    //they both are for quote line modal
                    $(".select2").select2({
                        dropdownParent: $("#importExcelModalLongnew")
                    });

                    $(".select2_quote_line_modal").select2({
                        dropdownParent: $("#quoteLineModalLongnew"),
                    });
<?php } else if (Route::currentRouteName() == 'suppliers.edit') { ?>
                    $(".select2").select2({
                        dropdownParent: $(".supplier_contact_modal_in_supplier")
                    });
                    $(".supplier_select2").select2();
<?php } else { ?>
                    $(".select2").select2();
                    $(".supplier_select2").select2();
<?php } ?>



<?php
$name = Route::currentRouteName();
$shipping = Route::current()->uri;
//dd($name,$shipping);
if ($name != "orders.create" && $name != "orders.edit" && $name != "quotes.edit" && $name != "suppliers.create" && $shipping != 'admin/shipping_container_products' && $shipping != 'admin/order_pricing_costs_fis/{order_id}' && $shipping != 'admin/order_pricing_costs_fob' && $shipping != 'admin/order_pricing_costs_fiw/{order_id}' && $name != "fis-template.create" && $name != "fis-template.edit" && $name != 'products.edit') {
    ?>
                    $(document).ready(function () {
                        $('.table').DataTable();
                    });

<?php } if ($shipping == 'admin/order_pricing_costs_fis' || $shipping == 'admin/order_pricing_costs_fiw') { ?>
                    $(document).ready(function () {
                        $(".order_pricing_costs_fis").trigger('click');

                    });
<?php } if ($shipping == 'admin/order_pricing_costs_fob') { ?>
                    $(document).ready(function () {
                        $(".order_pricing_costs_fob").trigger('click');
                    });
<?php } ?>

                $('#product_length,#product_width,#product_height').on('keyup', function () {
                    //        alert('d');
                    var l = $("#product_length").val();
                    var w = $("#product_width").val();
                    var h = $("#product_height").val();

                    var cbm = l * w * h;
                    var cbm = cbm / 1000000;
                    $('#product_cbm').val(cbm.toFixed(4));
                });

                $('#length,#width,#height').on('keyup', function () {
                    //        alert('d');
                    var l = $("#length").val();
                    var w = $("#width").val();
                    var h = $("#height").val();

                    var cbm = l * w * h;
                    var cbm = cbm / 1000000;
                    $('#container_cbm').val(cbm.toFixed(4));
                });
                $(document).on('keyup', '#product_box_length,#product_box_width,#product_box_height', function () {
//                alert('d');
                    var l = $("#product_box_length").val();
                    var w = $("#product_box_width").val();
                    var h = $("#product_box_height").val();

                    var cbm = l * w * h;
                    $('#product_box_cbm').val(cbm);
                });
                $(document).on('keyup', '#stack_length,#stack_width,#stack_height', function () {
//                alert('d');
                    var l = $("#stack_length").val();
                    var w = $("#stack_width").val();
                    var h = $("#stack_height").val();

                    var cbm = l * w * h;
                    $('#stack_cbm').val(cbm);
                });
                //************---onselect supplier add supplier in contact's supplier------**********

                $(document).on('change', '#supplier_id', function (e) {
                    e.preventDefault();
                    var supplierId = $(this).val();
                    var contactId = $('.contactidforajax').val();
                    //alert(contactId);
                    var form_data = new FormData();
                    form_data.append("supplierId", supplierId);
                    form_data.append("contactId", contactId);
                    form_data.append("_token", $('meta[name="csrf-token"]').attr('content'));
                    $.ajax({
                        url: "{{url('admin/addcontactsupplier')}}",

                        method: "POST",
                        data: form_data,
                        contentType: false,
                        cache: false,
                        processData: false,
                        beforeSend: function () {
                            //                        Swal.showLoading();
                        },
                        success: function (response)
                        {

                            if (response) {

                                if (response.status == false) {
                                    swal("Error!", response.msg, "error");
                                } else {
                                    $('.databyajax').append().html(response);
                                }
                            }
                        }
                    });
                });

                //************************----ends-----************************


                //************---onselect contact add contact in supplier's contact------*************

                $(document).on('click', '#saveContactFromList', function (e) {
                    e.preventDefault();
                    var contactId = $('#contact_id').val();
                    //this similar contact id made by struggler for specific relation purpose
                    var contact_id = $('.supplier_contact').val();
                    var supplierId = $('.supplieridforajax').val();
                    //                alert(supplierId);
                    var form_data = new FormData();
                    form_data.append("contact_id", contact_id);
                    form_data.append("contactId", contactId);
                    form_data.append("supplierId", supplierId);
                    form_data.append("_token", $('meta[name="csrf-token"]').attr('content'));
                    $.ajax({
                        url: "{{url('admin/addsuppliercontact')}}",
                        method: "POST",
                        data: form_data,
                        contentType: false,
                        cache: false,
                        processData: false,
                        beforeSend: function () {
                            //                        Swal.showLoading();
                        },
                        onError: function () {
                            swal("Error!", 'Something Went Wrong', "error");
                        },
                        success: function (response)
                        {
                            getContacts();
                        }
                    });
                });

                //************************----ends-----************************            

                //************************---onselect product add supplier's products------********************

                $(document).on('click', '#saveProductFromList', function (e) {
                    e.preventDefault();
                    var productId = $('#product_id').val();
                    var product_id = $('.supplier_product').val();
                    var supplierId = $('.supplieridforajax').val();
                    //alert(contactId);
                    var form_data = new FormData();
                    form_data.append("product_id_custom", product_id);
                    form_data.append("product_id", productId);
                    form_data.append("supplierId", supplierId);
                    form_data.append("_token", $('meta[name="csrf-token"]').attr('content'));
                    $.ajax({
                        url: "{{url('admin/addsupplierproduct')}}",
                        method: "POST",
                        data: form_data,
                        contentType: false,
                        cache: false,
                        processData: false,
                        beforeSend: function () {
                            //                        Swal.showLoading();
                        },
                        onError: function () {
                            swal("Error!", 'Something Went Wrong', "error");
                        },
                        success: function (response)
                        {

                            getProducts();
                        }

                    });
                });

                //************************----ends-----************************            


                //************************---onselect product bring values------***********************

                $(document).on('change', '.product_id', function (e) {
                    e.preventDefault();
                    var divId = $(this).closest('tr').attr('id');
                    var form_data = new FormData();
                    form_data.append("product_id", this.value);
                    form_data.append("_token", $('meta[name="csrf-token"]').attr('content'));
                    $.ajax({
                        url: "{{url('admin/getproductdetails')}}",
                        method: "POST",
                        data: form_data,
                        contentType: false,
                        cache: false,
                        processData: false,
                        beforeSend: function () {
                            //                        Swal.showLoading();
                        },
                        success: function (response)
                        {

                            if (response) {
                                if (response.status) {
                                    $('#' + divId + ' .shipping_port').val(response.product.shipping_port);
                                    //                            swal("Good job!", "Product Added! Now you can select product from dropdown", "success");
                                } else {
                                    swal("Error!", 'Something Went Wrong', "error");
                                }
                            }
                        }
                    });
                });

                //************************----ends-----************************


                //************-//for submitting order pricing data on keyup in orders//**************-

                $(document).on('click keyup', '.shipping_container_products', function () {
                    $('#shipping_container_products').submit();
                });
                $('#shipping_container_products').on('submit', function (e) {

                    e.preventDefault();

                    var form_data = new FormData(this);

                    form_data.append("_token", $('meta[name="csrf-token"]').attr('content'));
                    $.ajax({
                        url: "{{url('admin/shipping_container_products')}}",
                        method: "POST",
                        data: form_data,
                        contentType: false,
                        cache: false,
                        processData: false,
                        beforeSend: function () {
                            //                        Swal.showLoading();
                        },
                        onError: function () {
                            swal("Error!", 'Something Went Wrong', "error");
                        },
                        success: function (response)
                        {

                            if (response) {
                                if (response.status) {
                                    console.log('shipping container products saved');

                                }
                            }
                        }
                    });
                });

                //************************- //ends//************************-

                $('#addProductForm').on('submit', function (e) {

                    e.preventDefault();

                    var form_data = new FormData(this);

                    form_data.append("_token", $('meta[name="csrf-token"]').attr('content'));
                    $.ajax({
                        url: "{{url('admin/addproduct')}}",
                        method: "POST",
                        data: form_data,
                        contentType: false,
                        cache: false,
                        processData: false,
                        beforeSend: function () {
                            //                        Swal.showLoading();
                        },
                        onError: function () {
                            swal("Error!", 'Something Went Wrong', "error");
                        },
                        success: function (response)
                        {

                            if (response) {
                                if (response.status) {
                                    $('#product_id').html(response.product_html);
                                    $('#Product_id').html(response.product_html);
                                    $('#modalCloseProduct').trigger('click');
                                    swal("Good job!", "Product Added!", "success");

                                    getProducts();
                                }
                            }
                        }
                    });
                });

                $('#addSupplierForm').on('submit', function (e) {

                    e.preventDefault();

                    var form_data = new FormData(this);

                    form_data.append("_token", $('meta[name="csrf-token"]').attr('content'));
                    $.ajax({
                        url: "{{url('admin/addsupplier')}}",
                        method: "POST",
                        data: form_data,
                        contentType: false,
                        cache: false,
                        processData: false,
                        beforeSend: function () {
                            //                        Swal.showLoading();
                        },
                        success: function (response)
                        {

                            if (response) {
                                if (response.status) {
                                    $('#supplier_id').html(response.supplier_html);
                                    $('#modalCloseSupplier').trigger('click');
                                    swal("Good job!", "New Supplier Added! Now you can select supplier from dropdown", "success");
                                } else {
                                    swal("Error!", 'Something Went Wrong', "error");
                                }
                            }
                        }
                    });
                });
            });


//*********************code for shipping container products,costs add and load*************
<?php if ($shipping == 'admin/shipping_container_products') { ?>
                load_shipping_container_products();
<?php } ?>

            function load_shipping_container_products() {
                //    alert('s');
                var order_id = $('#container_products_order_id').val();
                var shipping_plan_id = $('#container_products_shipping_plan_id').val();
                //   alert(shipping_plan_id);
                $.ajax({
                    url: "{{url('admin/load_shipping_container_products')}}",
                    method: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {order_id: order_id, shipping_plan_id: shipping_plan_id},
                    beforeSend: function () {
                        //                        Swal.showLoading();
                    },
                    onError: function () {
                        swal("Error!", 'Something Went Wrong', "error");
                    },
                    success: function (response)
                    {
                        //            alert('s');
                        $('#load_shipping_container_products').html(response);
//            forShippingContainerCalculation();
                        setTimeout(function () {
                            $(".table_shipping_container_products tr").each(function () {
                                var rowId = this.id;
                                //        var rowId = $(this).closest('tr').attr('id');
                                var currentRow = $("#" + rowId);
                                var ship_total_cost = currentRow.find(".ship_total_cost").val();
                                if (ship_total_cost === '') {
                                    currentRow.find('.confirmed_quantity').trigger('click');
                                }
                            });
                            $(".format_me").each(function () {
                                var n = parseInt($(this).val().replace(/\D/g, ''), 10);
                                if (isNaN(n)) {
                                    n = '';
                                } else {
                                    $(this).val(n.toLocaleString());
                                }
                            });
                            $(".format_me_in_decimals").each(function (evt) {
                                if (evt.which != 190) {//not a fullstop
                                    var n = parseFloat($(this).val().replace(/\,/g, ''), 10);
                                    if (isNaN(n)) {
                                        n = '';
                                    } else {
                                        $(this).val(n.toLocaleString());
                                    }
                                }
                            });
                            // Do something after 1 second 
                        }, 1000);

                    }
                });
            }

            function shipping_container_products_create(order_line_id, order_id, shipping_plan_id) {
                //    alert(order_id);
                $.ajax({
                    url: "{{url('admin/insert_shipping_container_products')}}",
                    method: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {order_line_id: order_line_id, order_id: order_id, shipping_plan_id: shipping_plan_id},
                    beforeSend: function () {
                        //                        Swal.showLoading();
                    },
                    onError: function () {
                        swal("Error!", 'Something Went Wrong', "error");
                    },
                    success: function (response)
                    {
                        if (response.status == 'true') {
                            load_shipping_container_products();
                            $('#modalCloseContainerProductModal').trigger('click');
                        } else {
                            $('#shipping_product_packaging_option_html').html(response);
                            $('#modalCloseContainerProductModal').trigger('click');
                            $('#shipping_product_packaging_option').modal('show');
                        }
                    }
                });
            }
            function load_shipping_packaging_option(packaging_id) {
                //    alert(order_id);
                $.ajax({
                    url: "{{url('admin/get_shipping_packaging_option_view')}}",
                    method: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {packaging_id: packaging_id},
                    beforeSend: function () {
                        //                        Swal.showLoading();
                    },
                    onError: function () {
                        swal("Error!", 'Something Went Wrong', "error");
                    },
                    success: function (response)
                    {
                        $('#view_shipping_product_packaging_option_html').html(response);

                    }
                });
            }
            function add_shipping_container_product(packaging_id, using, product_id, order_id, shipping_plan_id, order_line_id) {
//        alert(using);
                $.ajax({
                    url: "{{url('admin/insert_shipping_container_products_with_using')}}",
                    method: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {packaging_id: packaging_id, using: using, product_id: product_id, order_id: order_id, shipping_plan_id: shipping_plan_id, order_line_id: order_line_id},
                    beforeSend: function () {
                        //                        Swal.showLoading();
                    },
                    onError: function () {
                        swal("Error!", 'Something Went Wrong', "error");
                    },
                    success: function (response)
                    {
                        if (response.status == 'true') {
                            load_shipping_container_products();
                            $('#modalCloseShippingProductPackaging').trigger('click');
                        }
                    }
                });
            }


//******-//  function for delete order details dynamically with table mname and row id

            function deleteOrderDetail(table_name, row_id) {
                //alert('s');
//    var didConfirm = confirm("Are you sure You want to delete");
                swal({
                    title: "Are you sure?",
                    text: "Do you really want to delete ?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                        .then((willDelete) => {
                            if (willDelete) {
                                $.ajax({
                                    url: "{{url('admin/deleteOrderDetail')}}",
                                    method: "POST",
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    },
                                    data: {
                                        table_name: table_name,
                                        row_id: row_id
                                    },

                                    beforeSend: function () {
                                        //                        Swal.showLoading();
                                    },
                                    success: function (response)
                                    {
                                        if (table_name == 'order_lines') {
                                            load_order_lines();
                                        } else if (table_name == 'shipping_plans') {
                                            load_shipping_plans();
                                        } else if (table_name == 'shipping_container_products') {
                                            load_shipping_container_products();
                                        } else if (table_name == 'order_documents') {
                                            load_order_documents_lines();
                                        } else if (table_name == 'quote_lines') {
                                            load_quote_lines();
                                        } else if (table_name == 'quotes_attachments') {
                                            load_quote_attachments_lines();
                                        } else if (table_name == 'products_attachments') {
                                            load_product_attachments_lines();
                                        } else if (table_name == 'certificate_attachments') {
                                            load_certificate_attachments_lines();
                                        } else if (table_name == 'supplier_certifications') {
                                            load_certificate_attachments_lines();
                                        } else if (table_name == 'product_packaging_options') {
                                            load_packaging_options();
                                            load_nested_products($('#hidden_nested_id').val());
                                        }
                                    }
                                });
                                return true;
                            } else {
                                return false;
                            }
                        });
//    if (didConfirm == true) {
//        $.ajax({
//            url: "{{url('admin/deleteOrderDetail')}}",
//            method: "POST",
//            headers: {
//                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//            },
//            data: {
//                table_name: table_name,
//                row_id: row_id
//            },
//
//            beforeSend: function () {
//                //                        Swal.showLoading();
//            },
//            success: function (response)
//            {
//                if (table_name == 'order_lines') {
//                    load_order_lines();
//                } else if (table_name == 'shipping_plans') {
//                    load_shipping_plans();
//                } else if (table_name == 'shipping_container_products') {
//                    load_shipping_container_products();
//                } else if (table_name == 'order_documents') {
//                    load_order_documents_lines();
//                } else if (table_name == 'quote_lines') {
//                    load_quote_lines();
//                } else if (table_name == 'quotes_attachments') {
//                    load_quote_attachments_lines();
//                } else if (table_name == 'certificate_attachments') {
//                    load_certificate_attachments_lines();
//                } else if (table_name == 'supplier_certifications') {
//                    load_certificate_attachments_lines();
//                }
//            }
//        });
//        return true;
//    } else {
//        return false;
//    }


            }

//************************- //ends//************************-

            $(function () {

                var url = window.location.pathname,
                        urlRegExp = new RegExp(url.replace(/\/$/, '') + "$"); // create regexp to match current url pathname and remove trailing slash if present as it could collide with the link in navigation in case trailing slash wasn't present there
                // now grab every link from the navigation
                $('.custom-nav a').each(function () {
                    // and test its normalized href against the url pathname regexp
                    if (urlRegExp.test(this.href.replace(/\/$/, ''))) {
                        $(this).addClass('active');
                    }
                });

            })
            var closebtns = document.getElementsByClassName("close-grid");
            var i;

            for (i = 0; i < closebtns.length; i++) {
                closebtns[i].addEventListener("click", function () {
                    this.parentElement.style.display = 'none';
                });
            }
        </script>
        <!-- //close script -->

        <!-- disable body scroll when navbar is in active -->
        <script>
            $(function () {
                $('.sidebar-menu-collapsed').click(function () {
                    $('body').toggleClass('noscroll');
                })
            });


            function deleteData(id, table, method, param = '') {
                if (confirm("Are you sure you want to delete?")) {


                    $.ajax({
                        url: "{{url('admin/deleteData')}}",
                        method: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: {id: id, table: table, param: param},

                        beforeSend: function () {
                            //                        Swal.showLoading();
                        },
                        success: function (response)
                        {




                            window[method]();


                        }
                    });
            }
            }

        </script>
        <!--for Sweet alert on flash message-->
        @if (Session::has('flash_message'))
        <script>
            swal({
                title: "Good job!",
                text: "{{ Session::get('flash_message') }}",
                icon: "success",
                button: "Ok!",
            });
        </script>
        @endif
        <!--ends-->
        <!-- disable body scroll when navbar is in active -->
        <script>

            //$(document).ready(function() {
            //  $(".select2").select2({
            //    dropdownParent: $("#packagingOptionsProductModalLongnew")
            //  });
            //});

        </script>
        <script>  $(document).ready( function () {
    $('.table').DataTable();
} );</script>
    </body>
</html>

</html>
