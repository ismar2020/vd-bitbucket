<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
               <link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}">
        <link rel="stylesheet" href="{{ url('assets/css/new-style.css') }}">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Trade Wind</title>
        <link rel="icon" type="image/png" sizes="16x16" href="{{url('images/fav.png')}}">
            <script src="{{ url('assets/js/jquery-3.3.1.min.js') }}"></script>
        <!-- Latest compiled JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
            <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
        <!-- Scripts -->
     
        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

        <!-- Styles -->
        <link href="{{ url('css/app.css') }}" rel="stylesheet">
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>


    </head>
    <body>
        <div id="app" class="login_page">
            <div class="nav-bar">
                <div class="container">
                    <div class="row align-items-center">
                        <a class="navbar-brand" href="#">
                            <img class="navbar-brand-regular" src="{{ asset('/images/trade_wind.png') }}" alt="brand-logo">
                        </a>
                        @if (Route::has('login'))
                        <div class="top-links">
                            @auth
                            <a href="{{ url('/home') }}">Home</a>
                            @else
                            <a href="{{ route('login') }}">Login</a>
                            @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                            @endif
                            @endauth
                        </div>
                        @endif
                    </div>
                </div>
            </div>

            <main class="py-4">
                @yield('content')
            </main>
        </div> 

    
        <script type="text/javascript">

$(".select2").select2({
   
});

        </script>


    </body>
</html>
